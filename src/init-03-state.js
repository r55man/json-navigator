
log.trace('loading init-state.js');

// Environment variables
cmsjs.State.init('ENV.RawContent',null);
cmsjs.State.init('ENV.RawContentProcessed',false);
cmsjs.State.init('ENV.JSON',null);
cmsjs.State.init('ENV.PageLoaded',null);
cmsjs.State.init('ENV.Bytes',null);
cmsjs.State.init('ENV.CurrentPage',null);
cmsjs.State.init('ENV.CurrentPath',null);
cmsjs.State.init('ENV.StandaloneMode',false);
cmsjs.State.init('ENV.EditorChanged',false);

// Default config variables
cmsjs.State.init('Config.BoldKeys',true);
cmsjs.State.init('Config.ContentTypes',['application/json']);
cmsjs.State.init('Config.ControlKeys',false);
cmsjs.State.init('Config.ConvertLinks',false);
cmsjs.State.init('Config.CodeFont',null);
cmsjs.State.init('Config.CodeFontSize',3);
cmsjs.State.init('Config.ConsoleFont',null);
cmsjs.State.init('Config.ConsoleFontSize',3);
cmsjs.State.init('Config.Debug',false);
cmsjs.State.init('Config.DisplayFont',null);
cmsjs.State.init('Config.DisplayFontSize',3);
cmsjs.State.init('Config.Extensions',[]);
//cmsjs.State.init('Config.EditorMode',false);
cmsjs.State.init('Config.FontList',['Roboto Mono','Kode Mono','Rubik','Anta','Kanit','Noto Sans','Roboto','Barlow','Lora','Open Sans']);
cmsjs.State.init('Config.Hyperlinks',true);
cmsjs.State.init('Config.Language','en');
cmsjs.State.init('Config.LineWrap',true);
cmsjs.State.init('Config.MarkupHTML',true);
cmsjs.State.init('Config.MarkupMode',true);
cmsjs.State.init('Config.NullOpacity',0.4);
cmsjs.State.init('Config.ShowBytes',true);
cmsjs.State.init('Config.ShowCount',true);
cmsjs.State.init('Config.ShowFooter',true);
cmsjs.State.init('Config.ShowNulls',true);
cmsjs.State.init('Config.ShowSearch',true);
cmsjs.State.init('Config.ShowTooltips',true);
cmsjs.State.init('Config.ShowURL',true);
cmsjs.State.init('Config.SortKeys',true);
cmsjs.State.init('Config.Spacing',3);
cmsjs.State.init('Config.StyleEditor',null);
cmsjs.State.init('Config.ThemeEditorOpened',false);
cmsjs.State.init('Config.IndentDepth',3);
cmsjs.State.init('Config.ShowGuidelines',true);
cmsjs.State.init('Config.AutoExpand',false);
cmsjs.State.init('Config.ExpandDepth',3);
cmsjs.State.init('Config.DarkMode',true);
cmsjs.State.init('Config.SelectedLight','Default');
cmsjs.State.init('Config.SelectedDark','Default');
//cmsjs.State.init('Fonts.JSON',['Default','Roboto Mono','Noto Sans Mono','Kode Mono','Rubik','Noto Sans']);
//cmsjs.State.init('Fonts.Console',['Default','Open Sans','Rubik','Montserrat','Anta','Kanit']);
//cmsjs.State.init('Fonts.Display',['Default','Open Sans','Rubik','Roboto','Barlow','Lora']);



/********************************
 * USER-DEFINED STATE VARIABLES *
 ********************************/

/*
 * Config loads from localStorage when run as a web page, and from
 * chrome.storage.local when run as an extension.
 *
 * Put the following in your .html file to start in standalone mode:
 *
 * <script>const JSON_NAVIGATOR_STANDALONE_MODE = true;</script>
 */
if ( typeof JSON_NAVIGATOR_STANDALONE_MODE !== 'undefined' && JSON_NAVIGATOR_STANDALONE_MODE ) {
	log.debug('standalone mode enabled');
	cmsjs.State.set('ENV.StandaloneMode',true);
}
if ( ! cmsjs.State.get('ENV.StandaloneMode') ) {
	cmsjs.Log.debug('running in extension mode');
	initExtension(() => {
		cmsjs.Log.debug('init(): extension initialized');
		cmsjs.State.set('ENV.CurrentPath','$');
		cmsjs.State.loadChromeStorage();
	});
}
else {
	cmsjs.Log.debug('running in standalone mode');
	cmsjs.State.loadLocalStorage();
}



/**************************
 * STATE CHANGE LISTENERS *
 **************************/

/*
 * Config.IndentDepth
 */
cmsjs.State.addListener('Config.IndentDepth', (depth) => {
	if ( depth < 1 ) { cmsjs.State.set('Config.IndentDepth',1); return; }
	else if ( depth > 10 ) { cmsjs.State.set('Config.IndentDepth',10); return; }
	$.id('root').querySelectorAll('div.contents').forEach((div) => {
		div.style.marginLeft = depth/2 + 'rem';
	});
});

/*
 * Config.ShowGuidelines
 */
cmsjs.State.addListener('Config.ShowGuidelines', (state) => {
    Array.from(document.querySelectorAll('div.contents')).forEach((div) => {
		if (state) {
			div.style.borderLeftColor = getBorderColor(parseInt(div.parentElement.getAttribute('depth')) + 1);
		}
		else {
			div.style.borderLeftColor = 'rgba(0,0,0,0.0)';
		}
	});
});

/*
 * Config.Spacing
 */
cmsjs.State.addListener('Config.Spacing', (size) => {
	if ( size < 1 ) { cmsjs.State.set('Config.Spacing',1); return; }
	else if ( size > 10 ) { cmsjs.State.set('Config.Spacing',10); return; }
	const verticalPadding = cmsjs.State.get('Config.Spacing')/30;
	cmsjs.Component.get('content-markup').element.querySelectorAll('div.item').forEach((div) => {
		div.style.paddingTop = verticalPadding + 'rem';
		div.style.paddingBottom = verticalPadding + 'rem';
	});
});

/*
 * Config.LineWrap
 */
cmsjs.State.addListener('Config.LineWrap', (value) => {
	cmsjs.Component.get('content-markup').querySelectorAll('div.item[datatype="string"] > div.label > span.value').forEach((item) => {
		if ( value ) { item.style.whiteSpace = ''; }
		else { item.style.whiteSpace = 'nowrap'; }
	});
});

/*
 * Config.BoldKeys
 */
cmsjs.State.addListener('Config.BoldKeys', (value) => {
	cmsjs.Component.get('content-markup').element.querySelectorAll('div.item > div.label > span.key').forEach((item) => {
		if ( value ) { item.style.fontWeight = 'bold'; }
		else { item.style.fontWeight = 'normal'; }
	});
});

/*
 * Config.AutoExpand
 */
cmsjs.State.addListener('Config.AutoExpand', (state) => {
	if (state) { expandAll(); }
});

/*
 * Config.MarkupHTML
 */
cmsjs.State.addListener('Config.MarkupHTML', (state) => {
	if (state) {
		cmsjs.Component.get('content-markup').querySelectorAll('div.item[datatype="string"] > div.label > span.value').forEach((valElt) => {
			if ( ! valElt.classList.contains('converted-url') ) {
				valElt.innerHTML = HTML.unescapeString(valElt.innerHTML);
				if ( cmsjs.State.get('Config.LineWrap') ) {
					valElt.style.whiteSpace = '';
				}
				else {
					valElt.style.whiteSpace = 'nowrap';
				}
			}
		});
	}
	else {
		cmsjs.Component.get('content-markup').querySelectorAll('div.item > div.label > span.value.string').forEach((item) => {
			if ( ! item.classList.contains('converted-url') ) {
				item.innerHTML = HTML.escapeString(item.innerHTML);
			}
			if ( cmsjs.State.get('Config.LineWrap') ) {
				item.style.whiteSpace = '';
			}
			else {
				item.style.whiteSpace = 'nowrap';
			}
		});
	}
});

/*
 * Config.ConvertLinks
 */
cmsjs.State.addListener('Config.ConvertLinks', (state) => {
	if (state) {
		cmsjs.Component.get('content-markup').querySelectorAll('div.item > div.label > span.value.string').forEach((valElt) => {
			convertToLinkIfPossible(valElt);
		});
	}
	else {
		cmsjs.Component.get('content-markup').querySelectorAll('div.item > div.label > span.value > a.converted-url').forEach((a) => {
			a.parentElement.innerHTML = '"' + a.href + '"';
		});
	}
});

/*
 * Config.MarkupMode
 */
cmsjs.State.addListener('Config.MarkupMode', (state) => {
	/*
	if ( cmsjs.State.get('ENV.EditorChanged') ) {
		console.log('changed mofo');
		buildPage('markup');
		buildPage('raw');
		cmsjs.State.set('ENV.EditorChanged',false);
	}
	*/
	if ( state && cmsjs.State.get('ENV.CurrentPage') !== 'content-markup' ) {
		showPage('content-markup');
	}
	else if ( cmsjs.State.get('ENV.CurrentPage') == 'content-markup' ) {
		/*
		if ( cmsjs.State.get('Config.EditorMode') ) {
			showPage('editor');
		}
		*/
		//else {
			showPage('content-raw');
		//}
	}
});

/*
 * Config.EditorMode
 *
 * EditorMode must set JSON on exit, but only if EditorMode has already
 * been loaded. 
 * 'init' => () { }; // when the flag is first initialized.
 */
/*
cmsjs.State.addListener('Config.EditorMode', (value) => {
	if ( cmsjs.State.get('Config.EditorMode') && ! cmsjs.State.get('Config.MarkupMode') ) {
		showPage('editor');
	}
	else {
		const input = $.id('editor').value;
		cmsjs.State.set('ENV.JSON',JSON.parse(input.trim()));
		if ( cmsjs.State.get('ENV.EditorChanged') ) {
			buildPage('markup');
			buildPage('raw');
			cmsjs.State.set('ENV.EditorChanged',false);
		}
		if ( cmsjs.State.get('Config.MarkupMode') ) {
			showPage('markup');
		}
		else {
			showPage('raw');
		}
	}
});
*/

/*
 * Config.ShowNulls
 */
cmsjs.State.addListener('Config.ShowNulls', (state) => {
    Array.from(document.querySelectorAll('div.null')).forEach((div) => {
		if (state) { div.style.display = 'block'; }
		else { div.style.display = 'none'; }
	});
});

/*
 * Config.SortKeys
 */
cmsjs.State.addListener('Config.SortKeys', (state) => {
	cmsjs.Component.get('content-markup').element.querySelectorAll('.expanded-once.object').forEach((item) => {
		const jsonKeys = Object.keys(getJsonForItem(item));
		if ( jsonKeys.length > 0 ) {
			const contents = item.querySelector('div.contents');
			const keys = (state) ? jsonKeys.sort() : jsonKeys;
			keys.forEach((key) => {
				const subItem = contents.querySelector(`div.item[key="${key}"]`);
				contents.append(subItem);
			});
		}
	});
});

/*
 * Config.ShowCount
 */
cmsjs.State.addListener('Config.ShowCount', (state) => {
    Array.from(document.querySelectorAll('span.count')).forEach((div) => {
		if (state === 'on' || state === true ) {
			div.innerHTML = ' ' + div.getAttribute('count') + ' ';
		}
		else if (state === 'off' || state === false) {
			div.innerHTML = '...';
		}
	});
});

/*
 * Config.ShowSearch
 */
cmsjs.State.addListener('Config.ShowSearch', (state) => {
	if (state) {
		$.id('search').style.display = 'flex';
		cmsjs.Component.get('header').parentElement.style.display = 'block';
	}
	else {
		$.id('search').style.display = 'none';
		if ( ! cmsjs.State.get('Config.ShowURL') ) {
			cmsjs.Component.get('header').parentElement.style.display = 'none';
		}
	}
});

/*
 * Config.ShowURL
 */
cmsjs.State.addListener('Config.ShowURL', (state) => {
	if (state) {
		$.id('url').style.display = 'block';
		cmsjs.Component.get('header').parentElement.style.display = 'block';
	}
	else {
		$.id('url').style.display = 'none';
		if ( ! cmsjs.State.get('Config.ShowSearch') ) {
			cmsjs.Component.get('header').parentElement.style.display = 'none';
		}
	}
});

/*
 * Config.ShowFooter
 */
cmsjs.State.addListener('Config.ShowFooter', (state) => {
	if (state) { cmsjs.Component.get('footer').parentElement.style.display = 'block'; }
	else { cmsjs.Component.get('footer').parentElement.style.display = 'none'; }
});

/*
 * Config.Debug
 */
cmsjs.State.addListener('Config.Debug', (state) => {
});

/*
 * Config.ShowBytes
 */
cmsjs.State.addListener('Config.ShowBytes', (state) => {
    Array.from(document.querySelectorAll('span.bytes')).forEach((div) => {
		if (state) {
			if ( ! cmsjs.State.get('ENV.Bytes') ) {
				cmsjs.State.set('ENV.Bytes',JSON.stringify(cmsjs.State.get('ENV.JSON')).length);
				// FIXME: Need to implement looping over existing divs and updating bytes
			}
			div.style.display = 'inline';
		}
		else {
			div.style.display = 'none';
		}
	});
});

/*
 * Config.DarkMode
 */
cmsjs.State.addListener('Config.DarkMode', (state) => {
	if ( state ) {
		const theme = cmsjs.State.get('Config.SelectedDark');
		cmsjs.Themes.Colors.setTheme('Dark.' + theme);
	}
	else {
		const theme = cmsjs.State.get('Config.SelectedLight');
		cmsjs.Themes.Colors.setTheme('Light.' + theme);
	}
});

/*
 * Config.SelectedDark
 */
cmsjs.State.addListener('Config.SelectedDark', (value) => {
	cmsjs.Controls.setValue('Config.DarkMode',true);
	cmsjs.Themes.Colors.setTheme('Dark.' + value);
});

/*
 * Config.SelectedLight
 */
cmsjs.State.addListener('Config.SelectedLight', (value) => {
	cmsjs.Controls.setValue('Config.DarkMode',false);
	cmsjs.Themes.Colors.setTheme('Light.' + value);
});

/*
 * Config.CodeFont
 */
cmsjs.State.addListener('Config.CodeFont', (font) => {
	cmsjs.Themes.Fonts.setGoogleFont(cmsjs.Component.get('content-markup'),font,'monospace, sans-serif');
	cmsjs.Themes.Fonts.setGoogleFont(cmsjs.Component.get('content-raw'),font,'monospace, sans-serif');
});

/*
 * Config.ConsoleFont
 */
cmsjs.State.addListener('Config.ConsoleFont', (font) => {
	const components = ['header','controls','footer','themes','fonts'];
	components.forEach((item) => {
		cmsjs.Themes.Fonts.setGoogleFont(cmsjs.Component.get(item),font,'sans-serif');
	});
});

/*
 * Config.DisplayFont
 */
cmsjs.State.addListener('Config.DisplayFont', (font) => {
	cmsjs.Themes.Fonts.setGoogleFont(cmsjs.Component.get('content-docs'),font,'sans-serif');
});

/*
 * Config.CodeFontSize
 */
cmsjs.State.addListener('Config.CodeFontSize', (size) => {
	cmsjs.Component.get('content-markup').style.fontSize = cmsjs.Themes.Fonts.getFontSize(size);
	cmsjs.Component.get('content-raw').style.fontSize = cmsjs.Themes.Fonts.getFontSize(size);
});

/*
 * Config.ConsoleFontSize
 */
cmsjs.State.addListener('Config.ConsoleFontSize', (size) => {
	const components = ['header','controls','footer','themes','fonts'];
	components.forEach((item) => {
		cmsjs.Component.get(item).style.fontSize = cmsjs.Themes.Fonts.getFontSize(size);
	});
});

/*
 * Config.DisplayFontSize
 */
cmsjs.State.addListener('Config.DisplayFontSize', (size) => {
	cmsjs.Component.get('content-docs').style.fontSize = cmsjs.Themes.Fonts.getFontSize(size);
});

/*
 * Config.Language
 */
cmsjs.State.addListener('Config.Language', (lang) => {
	cmsjs.Dictionary.PreferredLanguage = lang;
	cmsjs.State.set('Config.Language',lang);
	cmsjs.Component.get('header').build();
	cmsjs.Component.get('footer').build();
	cmsjs.Component.get('controls').build();
	cmsjs.Component.get('content-docs').build();
});

/*
 * Config.ControlKeys
 */
cmsjs.State.addListener('Config.ControlKeys', (value) => {
	if ( cmsjs.State.get('Config.ControlKeys') ) { cmsjs.Keys.enableListener(); }
	else { cmsjs.Keys.disableListener(); }
});

