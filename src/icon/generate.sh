#!/usr/bin/bash
set -eu
original="256.png"
for i in 192 128 96 64 48 32 24 16; do
	convert -strip -resize ${i} ${original} ${i}.png
done
exit 0
