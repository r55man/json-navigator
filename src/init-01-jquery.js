// jQuery-like functions

class $ {

	// document.createElement, extended to take contents as an arg
	static ce(tag,contents,attributes) {
		const elt = document.createElement(tag);
		if ( contents ) {
			if ( typeof contents === 'string' ) {
				elt.innerHTML = contents;
			}
			else {
				elt.append(contents);
			}
		}
		if ( attributes && typeof attributes === 'object' ) {
			Object.keys(attributes).forEach((item) => {
				item.setAttribute(item,attributes[item]);
			});
		}
		return elt;
	}

	// document.getElementById
	static id(str) { return document.getElementById(str); }

	// document.createTextNode
	static ctn(str) { return document.createTextNode(str); }

	// document.createDocumentFragment
	static cdf() { return document.createDocumentFragment(); }

	// This returns arrays.
	// Use $.id() if you want a single element.
	// Use $.ss() if you want output type to vary depending on query.
	static s(selector) { return $.select(selector); }
	static select(selector) {
		if ( ! selector || typeof selector !== 'string' ) {
			error('$.select: invalid selector',selector);
		}
		if ( selector.startsWith('#') ) {
			const match = document.getElementById(selector.slice(1));
			if ( match ) { return [match]; }
			else { return []; }
		}
		else {
			const matches = Array.from(document.querySelectorAll(selector));
			if ( matches ) { return matches; }
			else { return []; }
		}
		return [];
	}
	
	// Smart select.
	// Returns null if no matches.
	// Returns a single element if an id is requested.
	// Returns a single element if only a single element matches.
	// Returns an array if there are multiple matches.
	static ss(selector) { return $.smartSelect(selector); }
	static smartSelect(selector) {
		const matches = $.select(selector);
		if ( ! matches || matches.length == 0 ) {
			return null;
		}
		else if ( selector.startsWith('#') ) {
			return matches[0];
		}
		else {
			if ( matches.length == 1 ) {
				return matches[0];
			}
			return matches;
		}
	}
	
	// Sets the style on all elements matching 'selector'
	// 'name' and 'value' can be either strings or arrays.
	// Arrays are more efficient than calling this multiple times.
	static style(selector,name,value) {
		if ( typeof name === 'string' && typeof value === 'string' ) {
			name = [name];
			value = [value];
		}
		const elements = $.select(selector);
		elements.forEach(element => {
			name.forEach((key,index) => {
				element.style[key] = value[index]
			});
		});
	}
	
}

