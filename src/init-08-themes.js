
log.trace('loading init-themes.js');

let theme = null;

cmsjs.Themes.Colors.addKey('Default.Background','#f7f7f7');
cmsjs.Themes.Colors.addKey('Default.Text','#004');
cmsjs.Themes.Colors.addKey('Default.Heading','#004');
cmsjs.Themes.Colors.addKey('Default.Highlight','#404');

cmsjs.Themes.Colors.addKey('Docs.Text1','#000');
cmsjs.Themes.Colors.addKey('Docs.Text2','#040');
cmsjs.Themes.Colors.addKey('Docs.Text3','#004');
cmsjs.Themes.Colors.addKey('Docs.Borders','#777');

cmsjs.Themes.Colors.addKey('Controls.Labels');
cmsjs.Themes.Colors.addKey('Controls.ToggleSliderOn');
cmsjs.Themes.Colors.addKey('Controls.ToggleSliderOff');
cmsjs.Themes.Colors.addKey('Controls.ToggleButtonOn');
cmsjs.Themes.Colors.addKey('Controls.ToggleButtonOff');

cmsjs.Themes.Colors.addKey('JSON.Key');
cmsjs.Themes.Colors.addKey('JSON.Null');
cmsjs.Themes.Colors.addKey('JSON.Boolean');
cmsjs.Themes.Colors.addKey('JSON.Number');
cmsjs.Themes.Colors.addKey('JSON.String');
cmsjs.Themes.Colors.addKey('JSON.Object');

cmsjs.Themes.Colors.addKey('JSON2.Bytes');
cmsjs.Themes.Colors.addKey('JSON2.Controls');
cmsjs.Themes.Colors.addKey('JSON2.Guideline1');
cmsjs.Themes.Colors.addKey('JSON2.Guideline2');
cmsjs.Themes.Colors.addKey('JSON2.Guideline3');
cmsjs.Themes.Colors.addKey('JSON2.Guideline4');

theme = new cmsjs.Themes.Colors('Neon');
theme.set('Default.Background','#2f2f20');
theme.set('Default.Text','#fff');
theme.set('Default.Heading','#e87');
theme.set('Default.Highlight','#ff7');
theme.set('Docs.Text1','#cc0');
theme.set('Docs.Text2','#0cc');
theme.set('Docs.Text3','#ecf');
theme.set('Docs.Borders','#777');
theme.set('JSON.Key','#7df');
theme.set('JSON.Null','#77f');
theme.set('JSON.Boolean','#0ff');
theme.set('JSON.Number','#0ff');
theme.set('JSON.String','#0f0');
theme.set('JSON.Object','#999');
theme.set('JSON2.Bytes','#f0f');
theme.set('JSON2.Controls','#f7f');
theme.set('JSON2.Guideline1','#0a0');
theme.set('JSON2.Guideline2','#f00');
theme.set('JSON2.Guideline3','#aaa');
theme.set('JSON2.Guideline4','#77f');
theme.set('Controls.Labels','#9e7');
theme.set('Controls.ToggleSliderOn','#7ef');
theme.set('Controls.ToggleSliderOff','#666');
theme.set('Controls.ToggleButtonOn','#17f');
theme.set('Controls.ToggleButtonOff','#999');

if ( cmsjs.State.get('Core.GoogleFontList') ) {
	cmsjs.State.get('Core.GoogleFontList').forEach((font) => {
		cmsjs.Themes.Fonts.registerGoogleFont(font);
	});
}


theme = new cmsjs.Themes.Layouts('Default');
theme.setGridRows("auto 1fr auto auto");
theme.setGridCols("1fr auto");
theme.addPanel('Header','1,1','1,1').addComponent('header');
theme.addPanel('Controls','1,2','2,1').addComponent('controls');
theme.addPanel('Content','2,1','1,1').addComponent;
theme.addPanel('Footer','3,1','1,2').addComponent('footer');
theme.addPanel('ThemeEditor','4,1','1,2');

	cmsjs.Themes.Colors.bindElement(cmsjs.Console.Elements.Console,'background-color','background-color');
	cmsjs.Themes.Colors.bindElement(cmsjs.Console.Elements.Console,'color','text-color');
	
	cmsjs.Console.bindComponent('header','Header');
	cmsjs.Console.bindComponent('controls','RightSidebar');
	cmsjs.Console.bindComponent('footer','Footer');
	cmsjs.Console.bindComponent('colors','ThemeEditor');
	cmsjs.Console.bindComponent('fonts','ThemeEditor');
	
	cmsjs.Console.getComponent('header').build();
	cmsjs.Console.getComponent('controls').build();
	cmsjs.Console.getComponent('footer').build();
	cmsjs.Console.getComponent('themes').build();
	cmsjs.Console.getComponent('fonts').build();
	
	cmsjs.Console.getComponent('header').show();
	cmsjs.Console.getComponent('controls').show();
	cmsjs.Console.getComponent('footer').show();


	const pages = [];
	pages.push('markup');
	pages.push('raw');
	//pages.push('editor');
	pages.push('docs');
	pages.push('demo');
	//pages.push('config');
	pages.forEach((page) => {
		const pid = 'content-' + page;
		cmsjs.Console.addComponent(pid,cbf.get(pid));
		cmsjs.Console.bindComponent(pid,'Content');
	});

	if ( cmsjs.State.get('Core.StyleEditor') ) {
		showStyleEditor(cmsjs.State.get('Core.StyleEditor'));
	}


//cmsjs.Themes.Layouts.import(theme);

//theme = cmsjs.Themes.Layouts.new('Tablet');
//theme = cmsjs.Themes.Layouts.new('Mobile');


//cmsjs.Themes.load(STATE.get('Core.CustomThemes'));


/*
// Placeholder variable to build the themes
let theme = null;

// Light.Default
theme = cmsjs.Themes.addTheme('Light.Default');
theme.set('text-color','#000');
theme.set('text-2-color','#040');
theme.set('heading-color','#004');
theme.set('background-color','#f7f7ff');
theme.set('guideline-1-color','#00f');
theme.set('guideline-2-color','#f00');
theme.set('guideline-3-color','#222');
theme.set('guideline-4-color','#0c0');
theme.set('key-color','#700');
theme.set('null-color','#555');
theme.set('boolean-color','#007');
theme.set('number-color','#007');
theme.set('string-color','#050');
theme.set('object-color','#555');
theme.set('bytes-color','#707');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#707');
theme.set('console-toggle-slider-on-color','#acf');
theme.set('console-toggle-slider-off-color','#bbb');
theme.set('console-toggle-button-on-color','#79f');
theme.set('console-toggle-button-off-color','#888');

// Light.Firefox
theme = STYLE.addTheme('Light.Firefox');
theme.set('background-color','#fff');
theme.set('heading-color','#004');
theme.set('label-color','#004');
theme.set('highlight-color','#040');
theme.set('text-main-color','#000');
theme.set('text-alt-color','#040');
theme.set('guideline-1-color','#28e');
theme.set('guideline-2-color','#82e');
theme.set('guideline-3-color','#e28');
theme.set('guideline-4-color','#2e8');
theme.set('key-color','#2789ec');
theme.set('null-color','#777');
theme.set('boolean-color','#33a02f');
theme.set('number-color','#33a02f');
theme.set('string-color','#de0bac');
theme.set('object-color','#555');
theme.set('bytes-color','#707');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#707');
theme.set('console-toggle-slider-on-color','#acf');
theme.set('console-toggle-slider-off-color','#bbb');
theme.set('console-toggle-button-on-color','#79f');
theme.set('console-toggle-button-off-color','#888');

// Light.Subdued
theme = STYLE.addTheme('Light.Subdued');
theme.set('background-color','#ccd');
theme.set('heading-color','#004');
theme.set('label-color','#004');
theme.set('highlight-color','#004');
theme.set('text-main-color','#000');
theme.set('text-alt-color','#040');
theme.set('guideline-1-color','#28e');
theme.set('guideline-2-color','#82e');
theme.set('guideline-3-color','#e28');
theme.set('guideline-4-color','#2e8');
theme.set('key-color','#070');
theme.set('null-color','#777');
theme.set('boolean-color','#33a02f');
theme.set('number-color','#33a02f');
theme.set('string-color','#007');
theme.set('object-color','#555');
theme.set('bytes-color','#707');
theme.set('controls-color','#070');
theme.set('button-outline-color','#707');
theme.set('console-toggle-slider-on-color','#acc');
theme.set('console-toggle-slider-off-color','#bbb');
theme.set('console-toggle-button-on-color','#79f');
theme.set('console-toggle-button-off-color','#888');

// Light.Sandbox
theme = STYLE.addTheme('Light.Sandbox');
theme.set('text-color','#000');
theme.set('text-2-color','#040');
theme.set('heading-color','#004');
theme.set('background-color','#ffe');
theme.set('guideline-1-color','#28e');
theme.set('guideline-2-color','#82e');
theme.set('guideline-3-color','#e28');
theme.set('guideline-4-color','#2e8');
theme.set('key-color','#a60');
theme.set('null-color','#777');
theme.set('boolean-color','#33a02f');
theme.set('number-color','#33a02f');
theme.set('string-color','#f91');
theme.set('object-color','#555');
theme.set('bytes-color','#707');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#707');
theme.set('console-toggle-slider-on-color','#acf');
theme.set('console-toggle-slider-off-color','#bbb');
theme.set('console-toggle-button-on-color','#79f');
theme.set('console-toggle-button-off-color','#888');

// Light.Monochrome
theme = STYLE.addTheme('Light.Monochrome');
theme.set('text-color','#111');
theme.set('text-2-color','#333');
theme.set('heading-color','#004');
theme.set('background-color','#eee');
theme.set('guideline-1-color','#555');
theme.set('guideline-2-color','#777');
theme.set('guideline-3-color','#999');
theme.set('guideline-4-color','#aaa');
theme.set('key-color','#111');
theme.set('null-color','#999');
theme.set('boolean-color','#777');
theme.set('number-color','#999');
theme.set('string-color','#444');
theme.set('object-color','#aaa');
theme.set('bytes-color','#111');
theme.set('controls-color','#77f');
theme.set('button-outline-color','#70f');
theme.set('console-toggle-slider-on-color','#a7a7a7');
theme.set('console-toggle-slider-off-color','#ccc');
theme.set('console-toggle-button-on-color','#777777');
theme.set('console-toggle-button-off-color','#bbb');

// Dark.Default
theme = STYLE.addTheme('Dark.Default');
theme.set('text-color','#cce');
theme.set('text-2-color','#ecc');
theme.set('heading-color','#e87');
theme.set('background-color','#2f2f20');
theme.set('guideline-1-color','#0a0');
theme.set('guideline-2-color','#f00');
theme.set('guideline-3-color','#aaa');
theme.set('guideline-4-color','#77f');
theme.set('key-color','#e77');
theme.set('null-color','#999');
theme.set('boolean-color','#77f');
theme.set('number-color','#77f');
theme.set('string-color','#7d7');
theme.set('object-color','#999');
theme.set('bytes-color','#c5c');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#ff7');
theme.set('console-toggle-slider-on-color','#3ab');
theme.set('console-toggle-slider-off-color','#666');
theme.set('console-toggle-button-on-color','#05d');
theme.set('console-toggle-button-off-color','#999');

// Dark.Neon
theme = STYLE.addTheme('Dark.Neon');
theme.set('text-color','#cc0');
theme.set('text-2-color','#0cc');
theme.set('heading-color','#e87');
theme.set('background-color','#2f2f20');
theme.set('guideline-1-color','#0a0');
theme.set('guideline-2-color','#f00');
theme.set('guideline-3-color','#aaa');
theme.set('guideline-4-color','#77f');
theme.set('key-color','#7df');
theme.set('null-color','#77f');
theme.set('boolean-color','#0ff');
theme.set('number-color','#0ff');
theme.set('string-color','#0f0');
theme.set('object-color','#999');
theme.set('bytes-color','#f0f');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#ff7');
theme.set('console-toggle-slider-on-color','#7ef');
theme.set('console-toggle-slider-off-color','#666');
theme.set('console-toggle-button-on-color','#17f');
theme.set('console-toggle-button-off-color','#999');

// Dark.Nature
theme = STYLE.addTheme('Dark.Nature');
theme.set('text-color','#da0');
theme.set('text-2-color','#0ad');
theme.set('heading-color','#e87');
theme.set('background-color','#2f2f20');
theme.set('guideline-1-color','#d8a');
theme.set('guideline-2-color','#881');
theme.set('guideline-3-color','#18a');
theme.set('guideline-4-color','#a81');
theme.set('key-color','#3a4');
theme.set('null-color','#fa4');
theme.set('boolean-color','#fa4');
theme.set('number-color','#fa4');
theme.set('string-color','#3d4');
theme.set('object-color','#999');
theme.set('bytes-color','#2ae');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#ff7');
theme.set('console-toggle-slider-on-color','#7ef');
theme.set('console-toggle-slider-off-color','#666');
theme.set('console-toggle-button-on-color','#17f');
theme.set('console-toggle-button-off-color','#999');

// Dark.Lilac
theme = STYLE.addTheme('Dark.Lilac');
theme.set('text-color','#9ad');
theme.set('text-2-color','#da9');
theme.set('heading-color','#e87');
theme.set('background-color','#2f2f20');
theme.set('guideline-1-color','#d8a');
theme.set('guideline-2-color','#881');
theme.set('guideline-3-color','#18a');
theme.set('guideline-4-color','#a81');
theme.set('key-color','#b6d');
theme.set('null-color','#fa4');
theme.set('boolean-color','#fa4');
theme.set('number-color','#fa4');
theme.set('string-color','#3d4');
theme.set('object-color','#999');
theme.set('bytes-color','#2ae');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#ff7');
theme.set('console-toggle-slider-on-color','#9ad');
theme.set('console-toggle-slider-off-color','#666');
theme.set('console-toggle-button-on-color','#b8b');
theme.set('console-toggle-button-off-color','#999');

// Dark.Monochrome
theme = STYLE.addTheme('Dark.Monochrome');
theme.set('text-color','#ddd');
theme.set('text-2-color','#fff');
theme.set('heading-color','#e87');
theme.set('background-color','#333');
theme.set('guideline-1-color','#555');
theme.set('guideline-2-color','#777');
theme.set('guideline-3-color','#999');
theme.set('guideline-4-color','#aaa');
theme.set('key-color','#eee');
theme.set('null-color','#999');
theme.set('boolean-color','#777');
theme.set('number-color','#999');
theme.set('string-color','#eee');
theme.set('object-color','#999');
theme.set('bytes-color','#bbb');
theme.set('controls-color','#7f0');
theme.set('button-outline-color','#7ff');
theme.set('console-toggle-slider-on-color','#888');
theme.set('console-toggle-slider-off-color','#666');
theme.set('console-toggle-button-on-color','#ccc');
theme.set('console-toggle-button-off-color','#222');
*/
/*
 * Returns a border color based on the depth.
 * This is for the depth guidelines.
 */
function getBorderColor(depth,theme) {
	const colorNumber = ((depth-1) % 4) + 1;
	return STYLE.getThemeValue('guideline-' + colorNumber + '-color');
}


// Called whenever the theme changes
STYLE.setThemeChangeFunction((theme) => {
	const rootElt = CONSOLE.getComponent('content-markup');
	rootElt.querySelectorAll('div.contents').forEach((elt) => {
		if ( STATE.get('Indent.ShowGuidelines') ) {
			const depth = elt.getAttribute('depth');
			const colorNumber = ((depth-1) % 4) + 1;
			const color = theme.get('guideline-' + colorNumber + '-color');
			elt.style.borderLeftColor = color;
		}
		else {
			elt.style.borderLeftColor = 'rgba(0,0,0,0.0)';
		}
	});
	//CONSOLE.buildComponent('controls');
	CONSOLE.getComponent('controls').querySelectorAll('[cmsjs-control-type="toggle"]').forEach((toggle) => {
		const colorStyleElt = toggle.querySelector('style.cmsjs-control-toggle-colors');
		const sliderElt = toggle.querySelector('.cmsjs-control-toggle-slider');
		const inputElt = toggle.querySelector('input');
		const sliderColorOn = theme.get('console-toggle-slider-on-color');
		const sliderColorOff = theme.get('console-toggle-slider-off-color');
		const buttonColorOn = theme.get('console-toggle-button-on-color');
		const buttonColorOff = theme.get('console-toggle-button-off-color');
		colorStyleElt.innerHTML = `
			.cmsjs-control-toggle-slider:before { background-color: ${buttonColorOff}; }
			input:checked + .cmsjs-control-toggle-slider:before { background-color: ${buttonColorOn}; }
		`;
		//if ( inputElt.getAttribute('checked') !== null ) {
		if ( inputElt.checked ) {
			sliderElt.style.backgroundColor = theme.get('console-toggle-slider-on-color');
		}
		else {
			sliderElt.style.backgroundColor = theme.get('console-toggle-slider-off-color');
		}
	});
	STATE.set('ENV.TempTheme',null);
	CONSOLE.buildComponent('themes');
	//CONSOLE.buildComponent('fonts');
});

