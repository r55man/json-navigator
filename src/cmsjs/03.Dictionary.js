/**
 * Dictionary class.
 *
 * This class provides a framework for internationalization.
 * Dictionarires are uniquely defined by their IDs at creation.
 * Each dictionary instance maintains its own collection of
 * supported languages.
 */
cmsjs.Dictionary = class {

	// Class logger. For internal use only.
	static Log = cmsjs.Logger.create('cmsjs.Dictionary');

	// Collection of Dictionary objects.
	static Instances = new Map();

	// A map of language codes -> language names. See the end
	// of this file for example definitions,
	static Languages = new Map();

	// The user's preferred language
	static PreferredLanguage = null;
	
	// The user's list of preferred language
	static PreferredLanguages = [];

	/**
	 * Retrievs the Dictionary instance with the specified id.
	 */
	static get(id) {
		cmsjs.Dictionary.Log.trace('get(id)',id);
		if ( ! cmsjs.Dictionary.Instances.has(id) ) {
			cmsjs.Dictionary.Log.error('get(id) id not found:',id);
			return null;
		}
		const instance = cmsjs.Dictionary.Instances.get(id);
		return instance;
	}

	/**
	 * Creates a new Dictionary object.
	 *
	 * Always use this function to create new Dictionary objects.
	 * Do not call the constructor directly.
	 */
	static create(id) {
		cmsjs.Dictionary.Log.trace('create(id)',id);
		if ( cmsjs.Dictionary.Instances.has(id) ) {
			cmsjs.Dictionary.Log.error('create(id) dictionary already exists for id:',id);
		}
		const instance = new cmsjs.Dictionary(id);
		cmsjs.Dictionary.Instances.set(id,instance);
		return instance;
	}

	/**
	 * Sets the preferred language.
	 */
	static setPreferredLanguage(id) {
		cmsjs.Dictionary.Log.trace('setPreferredLanguage(id)',id);
		if ( ! cmsjs.Dictionary.Languages.has(id) ) {
			cmsjs.Dictionary.Log.error('setPreferredLanguage(id) no language defined for id:',id);
			return;
		}
		cmsjs.Dictionary.PreferredLanguage = id;
	}

	/**
	 * Initializes the preferred language from the browser environment.
	 */
	static initializePreferredLanguagesFromBrowser() {
		const fname = 'initializePreferredLanguagesFromBrowser()';
		cmsjs.Dictionary.Log.trace(fname);
		if ( navigator.language ) {
			cmsjs.Dictionary.Log.debug(fname + ': set preferred language from browser:', navigator.language);
			cmsjs.Dictionary.PreferredLanguage = navigator.language;
		}
		if ( navigator.languages ) {
			cmsjs.Dictionary.Log.debug(fname + ': set preferred language array from browser:', navigator.languages);
			cmsjs.Dictionary.PreferredLanguages = [...navigator.languages];
		}
		if ( ! navigator.language in navigator.languages ) {
			cmsjs.Dictionary.PreferredLanguages.unshift(navigator.language);
		}
	}

	/**
	 * Returns an array of dictionary titles.
	 */
	static listInstances() {
		cmsjs.Dictionary.Log.trace('listInstances()');
		return Array.from(cmsjs.Dictionary.Instances.keys());
	}

	/**
	 * Adds a language to the static collection.
	 *
	 * A collection of default languages is defined at the end of
	 * this file.
	 */
	static addLanguage(id,name) {
		cmsjs.Dictionary.Log.trace('addLanguage(id)',id,name);
		cmsjs.Dictionary.Languages.set(id,name);
	}
	
	/**
	 * List all available languages in the static collection.
	 */
	static listLanguageCodes() {
		cmsjs.Dictionary.Log.trace('listLanguageCodes()');
		return Array.from(cmsjs.Dictionary.Languages.keys());
	}
	
	/**
	 * List all available languages in the static collection.
	 */
	static listLanguageStrings() {
		cmsjs.Dictionary.Log.trace('listLanguageStrings()');
		return Array.from(cmsjs.Dictionary.Languages.values());
	}

	/**
	 * Constructor.
	 *
	 * Never call this directly. Use create(id) instead.
	 */
	constructor(id) {
		cmsjs.Dictionary.Log.trace('constructor(id)',id);
		this.log = cmsjs.Logger.create('cmsjs.Dictionary(' + id + ')');
		this.id = id;
		this.languages = new Map();
		this.rootLanguage = null;
		this.fallback = null;
	}

	/**
	 * Set the root language.
	 *
	 * This is the base language map that defines the keyset and serves
	 * as the fallback if the entry for a preferred language is unavailable.
	 */
	setRootLanguage(id) {
		this.log.trace('setRootLanguage(id)',id);
		this.rootLanguage = id;
	}
	
	/**
	 * Get a list of all language (as codes) used in this dictionary.
	 */
	getLanguageCodeList() {
		this.log.trace('getLanguageCodeList()');
		return Array.from(this.languages.keys());
	}

	/**
	 * Get a list of all languages (as strings) used in this dictionary.
	 */
	getLanguageStringList() {
		this.log.trace('getLanguageStringList()');
		const list = [];
		Array.from(this.languages.keys()).forEach((item) => {
			list.push(cmsjs.Dictionary.Languages[item]);
		});
		return list;
	}

	/**
	 * Adds a language map to this dictionary.
	 */
	addLanguage(id) {
		this.log.trace('addLanguage(id)',id);
		if ( ! cmsjs.Dictionary.Languages.has(id) ) {
			this.log.error('addLanguage(id) language definition not found:',id);
			return;
		}
		if ( this.languages.has(id) ) {
			this.log.error('addLanguage(id) language already exists:',id);
			return;
		}
		this.languages.set(id,new Map());
	}

	/**
	 * Retrieves all the keys that have been defined for this dictionary.
	 *
	 * Remember: The root language defines the keys.
	 */
	keys() {
		this.log.trace('keys()');
		return Array.from(this.languages.get(this.rootLanguage).keys());
	}

	/**
	 * Adds all languages specified to this dictionary.
	 *
	 * @array {Array} Array of language codes supported by this dictionary.
	 */
	addLanguages(array) {
		this.log.trace('addLanguages(array)',array);
		array.forEach((item) => {
			this.addLanguage(item);
		});
	}

	/**
	 * Gets the Map for the specified language.
	 */
	getLanguageMap(id) {
		this.log.trace('getLanguageMap(id)',id);
		if ( ! this.languages.has(id) ) {
			this.log.error('getLanguageMap(language) language does not exist in dictionary:',id);
			return undefined;
		}
		return this.languages.get(id);
	}

	/**
	 * Check if this dictionary supports the specified language.
	 */
	hasLanguage(id) {
		this.log.trace('hasLanguage(id)',id);
		return this.languages.has(id);
	}

	/**
	 * Retrieve a dictionary entry for the specified key.
	 *
	 * If language is unspecified, it is determined by PreferredLanguage(s)
	 *
	 * If no value is defined for the specified key in the desired language,
	 * the root language is used instead.
	 *
	 * If no match is found at all, the key name is returned so that at 
	 * least /something/ is printed.
	 */
	get(key,language) {
		this.log.trace('get(key,language)',key,language);
		if ( language ) {
			let returnValue = key;
			if ( ! this.hasLanguage(language) && language.includes('-') ) {
				language = language.split('-')[0];
			}
			if ( ! this.hasLanguage(language) ) {
				this.log.error('get(key,language) language does not exist in dictionary:',language);
				if ( this.rootLanguage && this.languages.get(this.rootLanguage) ) {
					this.log.warn('get(key,language) looking for key in root language:',language);
					const value = this.languages.get(this.rootLanguage).get(key);
					if ( value ) {
						returnValue = value;
					}
					else {
						this.log.warn('get(key,language) key not found:',key);
						returnValue = key;
					}
				}
				else {
					this.log.warn('get(key,language) no definition found for key:',key);
					returnValue = key;
				}
				returnValue = key;
			}
			else {
				returnValue = this.languages.get(language).get(key);
				if ( ! returnValue ) {
					if ( this.rootLanguage ) {
						returnValue = this.languages.get(this.rootLanguage).get(key);
						if ( ! returnValue ) {
							returnValue = key;
						}
					}
					else {
						returnValue = key;
					}
				}
			}
			return returnValue;
		}
		else {
			language = this.rootLanguage;
			let returnValue = key;
			log.debug(cmsjs.Dictionary.PreferredLanguage, cmsjs.Dictionary.PreferredLanguages);
			let searchArray = cmsjs.Dictionary.PreferredLanguages;
			if ( ! searchArray.includes(cmsjs.Dictionary.PreferredLanguage) ) {
				searchArray.unshift(cmsjs.Dictionary.PreferredLanguage);
			}
			log.debug(searchArray);
			let foundMatch = false;
			searchArray.forEach((lang) => {
				if ( ! foundMatch ) {
					if ( this.hasLanguage(lang) ) {
						language = lang;
						foundMatch = true;
					}
					else {
						if ( this.hasLanguage(lang.split('-')[0]) ) {
							language = lang.split('-')[0];
							foundMatch = true;
 						}
					}
				}
			});
			if ( ! foundMatch ) {
				language = this.rootLanguage;
			}
			if ( ! language ) {
				cmsjs.Dictionary.Log.error('get(key): could not determine language for key:',key);
				return key;
			}
			return this.get(key,language);
		}
	}
}

/*
 * Add some default languages.
 */
cmsjs.Dictionary.addLanguage('ar','\u0627\u0644\u0639\u0631\u0628\u064a\u0629'); // Arabic
cmsjs.Dictionary.addLanguage('bn','\u09ac\u09be\u0982\u09b2\u09be'); // Bengali
cmsjs.Dictionary.addLanguage('de','Deutsch'); // German
cmsjs.Dictionary.addLanguage('en','English'); // English
cmsjs.Dictionary.addLanguage('es','Espa\u00f1ol'); // Spanish
cmsjs.Dictionary.addLanguage('fr','Fran\u00e7ais'); // French
cmsjs.Dictionary.addLanguage('hi','\u0939\u093f\u0928\u094d\u0926\u0940'); // Hindi
cmsjs.Dictionary.addLanguage('id','Indonesia'); // Indonesian
cmsjs.Dictionary.addLanguage('it','Italiano'); // Italian
cmsjs.Dictionary.addLanguage('ja','\u65e5\u672c\u8a9e'); // Japanese
cmsjs.Dictionary.addLanguage('ko','\ud55c\uad6d\uc5b4'); // Korean
cmsjs.Dictionary.addLanguage('ms','Bahasa Melayu'); // Malay
cmsjs.Dictionary.addLanguage('nl','Nederlands'); // Dutch
cmsjs.Dictionary.addLanguage('pt','Portugu\u00eas'); // Portuguese
cmsjs.Dictionary.addLanguage('ru','\u0420\u0443\u0441\u0441\u043a\u0438\u0439'); // Russian
cmsjs.Dictionary.addLanguage('ta','\u0ba4\u0bae\u0bbf\u0bb4\u0bcd'); // Tamil
cmsjs.Dictionary.addLanguage('te','\u0c24\u0c46\u0c32\u0c41\u0c67\u0c41'); // Telugu
cmsjs.Dictionary.addLanguage('th','\u0e44\u0e17\u0e22'); // Thai
cmsjs.Dictionary.addLanguage('tr','T\u00fcrk\u00e7e'); // Turkish
cmsjs.Dictionary.addLanguage('vi','Ti\u1ebfng Vi\u1ec7t'); // Vietnamese
cmsjs.Dictionary.addLanguage('zh','\u4e2d\u6587'); // Chinese
