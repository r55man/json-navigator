
/**
 * Keybindings
 */

cmsjs.Keys = class {

	static Log = cmsjs.Logger.create('cmsjs.Keys');

	// This is not used now, but may be in the future to have separate
	// categories of Keybindings.
	static Instances = new Map();
	
	// This has action names as keys and functions as values.
	static Actions = new Map();

	// This has strings like "C-n" for map keys, and action names as values.
	static KeyBindings = new Map();

	// Adds an action. Set "docs" to null to omit documentation.
	static addAction(name,action) {
		cmsjs.Keys.Actions.set(name,action);
	}

	static enableListener() {
		document.addEventListener('keydown',cmsjs.Keys.processEvent);
}

	static disableListener() {
		document.removeEventListener('keydown',cmsjs.Keys.processEvent);
	}

	// Assigns a keybinding to an action.
	static assign(keyBinding,actionName) {
		if ( ! actionName in cmsjs.Keys.Actions ) {
			cmsjs.Keys.Log.error('assign(...): no action with this name has been defined:',actionName);
		}
		if ( keyBinding.includes('-') ) {
			const parts = keyBinding.split('-');
			const modifiers = parts.slice(0, -1).sort();
			const key = parts[parts.length - 1];
			const validModifiers = ["Alt","Ctrl","Meta","Shift"];
			for (let modifier of modifiers) {
				if ( ! validModifiers.includes(modifier) ) {
					cmsjs.Keys.Log.error('assign(...): invalid modifier:',modifier);
					cmsjs.Keys.Log.error('valid modifiers are: Alt, Ctrl, Meta, and Shift');
					return;
				}
			}
			keyBinding = modifiers.join('-') + '-' + key;
		}
		if ( cmsjs.Keys.isBound(keyBinding) ) {
			console.warn('{cmsjs.Keys} assign(...): keybinding is already defined, skipping: ' + keyBinding);
			return;
		}
		cmsjs.Keys.Log.debug('attaching keybinding ' + keyBinding + ' to action ' + actionName);
		cmsjs.Keys.KeyBindings.set(keyBinding,actionName);
	}

	// Checks to see if a key is bound.
	static isBound(keyBinding) {
		if ( cmsjs.Keys.KeyBindings.has(keyBinding) ) { return true; }
		return false;
	}

	// Returns a text representation of the key sequence associated with the specified keybinding.
	// Example:   getBinding('search') would return "Ctrl-s" if it were bound.
	static getBinding(action) {
		for ( let [key,val] of cmsjs.Keys.KeyBindings.entries() ) {
			if ( val === action ) {
				return key;
			}
		}
		return undefined;
	}


	// Runs the action associated with the given keybinding.
	static runActionForKeyBinding(keyBinding) {
		cmsjs.Keys.Actions.get(cmsjs.Keys.KeyBindings.get(keyBinding))();
	}

	// Processes a keypress.
	static processEvent(event) {
		const parts = [];
		if ( event.altKey ) { parts.push('Alt'); }
		if ( event.ctrlKey ) { parts.push('Ctrl'); }
		if ( event.metaKey ) { parts.push('Meta'); }
		if ( event.shiftKey ) { parts.push('Shift'); }
		parts.push(event.key);
		const keyBinding = parts.join('-');
		if ( cmsjs.Keys.isBound(keyBinding) ) {
			event.preventDefault();
			cmsjs.Keys.runActionForKeyBinding(keyBinding);
		}
	}

}

