
cmsjs.HTML = class {
	
	static Log = cmsjs.Logger.create('cmsjs.HTML');

	static escapeString(text) {
		return text.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;').replace(/'/g,'&#39;');
	}
	
	static unescapeString(text) {
		return text.replace(/&amp;/g,'&').replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&quot;/g,'"').replace(/&#39;/g,"'");
	}

	static getGoogleIcon(id) {
		const fontName = 'Material Symbols Outlined';
		var existingLink = document.head.querySelector('link[cmsjs-font-link="' + fontName + '"]');
		if ( ! existingLink ) {
			var link = document.createElement('link');
			//link.href = 'https://fonts.googleapis.com/css2?family=Material%20Symbols%20Outlined;opsz,wght,FILL,GRAD@20,400,0,0';
			link.href = 'https://fonts.googleapis.com/css2?family=Material%20Symbols%20Outlined';
			link.rel = 'stylesheet';
			link.setAttribute('cmsjs-font-link', fontName);
			document.head.appendChild(link);
			var style = document.createElement('style');
			style.setAttribute('cmsjs-font-style', fontName);
			//style.innerHTML = '.material-symbols-outlined { font-size: inherit !important; font-weight: bold !important; }';
			style.innerHTML = '.material-symbols-outlined { font-size: inherit !important; }';
			document.head.appendChild(style);
		}
		const elt = document.createElement('span');
		elt.classList.add('material-symbols-outlined');
		elt.style.verticalAlign = 'middle';
		elt.style.textAlign = 'center';
		elt.innerHTML = id;
		return elt;

	}

}

