
cmsjs.Themes = class {

	/*
	static Keys = new Map();
	static DefaultMap = new Map();
	static NamespaceMaps = new Map();
	static ElementMap = new Map();
	static CurrentTheme = null;
	static EditorTheme = null;
	static OnThemeChange = null;
	*/

	/**
	 * Adds a key and default value to the specified object.
	 *
	 * This is only for internal use by the subclasses.
	 *
	 * @private
	 * @param {Map}    Map   The Map to add the key to.
	 * @param {string} key   The key to add to the Map.
	 * @param {string} value The default value for the key.
	 */
	static _addKeyVal(map, key, value) {
		function isValidJSONKey(str) {
			try { JSON.parse(`{"${str}":null}`); return true; }
			catch (error) { return false; }
		}
		function isValidIdentifier(str) {
			return /^[a-zA-Z_$][a-zA-Z0-9_$]*$/.test(str);
		}
		const parts = key.split('.');
		for ( let i = 0; i < parts.length; i++ ) {
			const str = parts[i];
			if ( ! isValidJSONKey(key) ) {
				LOG.error('{THEMES} invalid component "' + str + '" in key: ',key);
				return;
			}
		}
		let current = map;
		for ( let i = 0; i < parts.length; i++ ) {
			const part = parts[i];
			if ( ! current.has(part) ) {
				if ( i === parts.length - 1 ) {
					current.set(part,value);
				} else {
					current.set(part,new Map());
				}
			}
			current = current.get(part);
		}
	}


	static _jsonify(map) {
	}

	
	/**
	 * Loads a theme set from a JSON file.
	 *
	 * JSON Format:
	 *
	 * {
	 *   "Version" : "0.9.6",
	 *   "Themes" : {
	 *     "Colors" : {
	 *       "Light" : {
	 *         "Default.Background" : "#fff",
	 *         "Default.Text" : "#000",
	 *         ...
	 *       },
	 *       "Dark" : {
	 *         "Default.Background" : "#000",
	 *         "Default.Text" : "#fff",
	 *         ...
	 *       },
	 *       ...
	 *     },
	 *     "Fonts" : {
	 *       "Standard" : {
	 *         "Headings" : "Roboto",
	 *         "Text" : "Open Sans",
	 *         "Console" : "Rubik",
	 *         ...
	 *       },
	 *       "Gothic" : {
	 *         "Headings" : "Kanit",
	 *         "Text" : "Anta",
	 *         "Console" : "Jacquard 24",
	 *         ...
	 *       },
	 *       ...
	 *     },
	 *     "Layouts" : {
	 *       "Standard" : {
	 *         "GridRows" : "auto 1fr auto auto",
	 *         "GridCols" : "1fr auto",
	 *         "Panels" : {
	 *           "Header" : {
	 *             "GridRowStart" : "1",
	 *             "GridRowSpan" : "1",
	 *             "GridColStart" : "1",
	 *             "GridColSpan" : "2",
	 *           },
	 *           "content" : {
	 *             "GridRowStart" : "2",
	 *             "GridRowSpan" : "1",
	 *             "GridColStart" : "1",
	 *             "GridColSpan" : "1",
	 *           },
	 *           ...
	 *         }
	 *         ...
	 *       },
	 *       "Mobile" : {
	 *         "LayoutType" : "flex",
	 *         "GridRows" : "auto 1fr auto auto auto",
	 *         "GridCols" : "1fr",
	 *         ...
	 *       },
	 *       ...
	 *     },
	 *     "Controls" : {
	 *       "Standard" : {
	 *         "Toggle.Console" : "Slider",
	 *         "Select.Console" : "Scroller",
	 *         ...
	 *       },
	 *       "Plain" : {
	 *         "Toggle.Console" : "Checkbox",
	 *         "Select.Console" : "Standard",
	 *         ...
	 *       },
	 *       ...
	 *     }
	 *   }
	 * }
	 *
	 * @private
	 * @param {Object} object The object to add the key to.
	 * @param {string} key    The key to add to the object.
	 * @param {string} value  The default value for the key.
	 */
	static load(json) {
	}

	
}

/**
 * 
 *
 */
cmsjs.Themes.Colors = class {
	
	static Log = cmsjs.Logger.create('cmsjs.Themes.Colors');
	
	static ElementMap = new Map();
	static ValidKeys = new Map();
	static CurrentTheme = null;
	static OnThemeChange = null;
	static getTheme(name) {
		cmsjs.Themes.Colors.Log.trace('getTheme(name)',name);
		return ( typeof name === 'string' ) ? cmsjs.Themes.Colors.Map(name) : name;
	}
	static addKey(key,val) {
		cmsjs.Themes.Colors.Log.trace('addKey(key,val)',key,val);
		cmsjs.Themes.Colors.ValidKeys.set(key,val);
	}
	static getCurrentTheme() {
		cmsjs.Themes.Colors.Log.trace('getCurrentTheme()');
		return cmsjs.Themes.Colors.getTheme(cmsjs.Themes.Colors.CurrentTheme);
	}
	static setCurrentTheme(theme) {
		cmsjs.Themes.Colors.Log.trace('setCurrentTheme(theme)',theme);
		theme = cmsjs.Themes.Colors.getTheme(theme);
		cmsjs.Themes.Colors.apply(theme)
		cmsjs.Themes.Colors.OnThemeChange(theme);
		cmsjs.Themes.Colors.CurrentTheme = theme.name;
	}
	static applyCurrentTheme(rootElement=document) {
		cmsjs.Themes.Colors.Log.trace('applyCurrentTheme(rootElement)',rootElement);
		const theme = cmsjs.Themes.Colors.getTheme(cmsjs.Themes.Colors.CurrentTheme);
		cmsjs.Themes.Colors.apply(theme,rootElement);
	}
	static applyTheme(theme,rootElement=document) {
		cmsjs.Themes.Colors.Log.trace('applyTheme(theme,rootElement)',theme,rootElement);
		theme = cmsjs.Themes.Colors.getTheme(theme);
		cmsjs.Themes.Colors.ValidKeys.forEach((set,csskey) => {
			Array.from(set).forEach((variable) => {
				const selector = '[cmsjs-theme-' + csskey + '="' + variable + '"]';
				//cmsjs.Themes.Colors.Log.debug('applyTheme() ' + selector + ' set to value: ',theme.get(variable));
				rootElement.querySelectorAll(selector).forEach((elt) => {
					elt.style.setProperty(csskey,theme.get(variable));
				});
			});
		});
	}
	/*
	 * bindElement(someDiv,'color','string-color');
	 * bindElement(someDiv,'border-left-color','string-color');
	 */
	static bindElement(element,csskey,variable) {
		cmsjs.Themes.Colors.Log.trace('bindElement(element,csskey,variable)',element,csskey,variable);
		element.setAttribute('cmsjs-theme-' + csskey, variable);
		if ( ! cmsjs.Themes.Colors.ElementMap.has(csskey) ) {
			cmsjs.Themes.Colors.ElementMap.set(csskey,new Set());
		}
		const variables = cmsjs.Themes.Colors.ElementMap.get(csskey);
		variables.add(variable);
		//console.warn('setting ' + csskey + ' to ' + STYLE.getThemeValue(variable) + ' for element: ',element);
		if ( cmsjs.Themes.Colors.getCurrentTheme() ) {
			element.style.setProperty(csskey,cmsjs.Themes.Colors.getCurrentTheme().get(variable));
		}
		//[cmsjs-theme-background-color='background-color'] { background-color: var(--background-color); }
		//[cmsjs-theme-color='string-color'] { color: var(--string-color); }
		return element;
	}
	// Rethemes a specific element, in the event that it has been modified somehow.
	// This most often occurs during mouseenter events, where an element changes
	// color.
	static rethemeElement(element) {
		cmsjs.Themes.Colors.Log.trace('rethemeElement()',element);
		for ( let i = 0; i < element.attributes.length; i++ ) {
			const attr = element.attributes[i];
			if ( attr.name.startsWith('cmsjs-theme-') ) {
				const csskey = attr.name.replace('cmsjs-theme-','');
				const value = cmsjs.Themes.Colors.getThemeValue(attr.value);
				element.style.setProperty(csskey,value);
			}
		}
	}
	constructor(name) {
		cmsjs.Themes.Colors.Log.trace('constructor()',name);
		this.log = cmsjs.Logger.create('cmsjs.Themes.Colors(' + name + ')');
		this.name = name;
		this.keys = new Map();
		cmsjs.Themes.Colors.Map.set(this.name,this);
	}
	set(key,val) {
		this.log.trace('set()',key,val);
		if ( ! cmsjs.Themes.Colors.ValidKeys.has(key) ) {
			this.log.error('set(key,val) attempt to set invalid key:',key);
			return;
		}
		this.keys.set(key,val);
	}
	get(key) {
		this.log.trace('get()',key);
		return this.keys.get(key);
	}
	static setThemeChangeFunction(f) {
		this.log.trace('setThemeChangeFunction()',f);
		cmsjs.Themes.OnThemeChange = f;
	}
}


/**
 *
 * Fonts
 *
 */
cmsjs.Themes.Fonts = class {


	// Given an integer in the  range of 1-10, return
	// a string that can be used in elt.style.fontSize (e.g. '1.1rem')
	static getFontSize(size) {
		const realSize = 0.5 + (size*0.1)
		const sizeString = realSize + 'rem';
		return sizeString;
	}
	
	constructor(name,type,config) {
		this.name = name;
		this.type = type;
		this.config = config;
		cmsjs.Themes.Fonts.Map.set(this.name,this);
	}
	
	static registerLocalFont(fontName,defaults='sans-serif') {
	}

	static insertGooglePreconnects() {
		if ( ! document.querySelector('[cmsjs-fonts-google-preconnect]') ) {
			const link1 = document.createElement('link');
			link1.rel = 'preconnect';
			link1.href = 'https://fonts.googleapis.com';
			const link2 = document.createElement('link');
			link2.rel = 'preconnect';
			link2.href = 'https://fonts.gstatic.com';
			link2.setAttribute('crossorigin','');
			document.head.appendChild(link1);
			document.head.appendChild(link2);
		}
	}


	static registerGoogleFont(fontName,defaults='sans-serif') {
		var existingLink = document.head.querySelector('link[cmsjs-font-link="' + fontName + '"]');
		if ( ! existingLink ) {
			insertGooglePreconects();
			var link = document.createElement('link');
			const encodedName = fontName.replace(/ /g,'%20');
			link.setAttribute('cmsjs-font-link', fontName);
			link.href = 'https://fonts.googleapis.com/css2?family=' + encodedName + '&display=swap';
			link.rel = 'stylesheet';
			document.head.appendChild(link);
		}
	}

	/**
	 * Given an element, a Google font name, and a string (defaults),
	 * this first adds the <link rel='stylesheet'> element in <head>
	 * to load the font if it does not already exist, then sets the
	 * element's font family to "Font Name" followed by 'defaults'.
	 */
	static setGoogleFont(element,fontName,defaults='sans-serif') {
		if ( typeof element === 'string' ) {
			element = document.getElementById(element);
		}
		// I keep changing the 'default' name...
		if ( ! fontName || fontName === 'Default' || fontName === 'System Default' ) {
			element.style.fontFamily = defaults;
			return;
		}
		cmsjs.Themes.Fonts.registerGoogleFont(fontName,defaults);
		const fontString = '"' + fontName + '"' + ', ' + defaults;
		element.style.fontFamily = fontString;
	}
}

cmsjs.Themes.Controls = class {
}

cmsjs.Themes.Layouts = class {
	static Map = new Map();
	static ValidPanels = new Map();
	static getTheme(name) {
		return ( typeof name === 'string' ) ? cmsjs.Themes.Layouts.Map(name) : name;
	}
	static addPanel(str,defaultConfig) {
		cmsjs.Themes.Layouts.ValidPanels.set(str,defaultConfig);
	}
	static apply(theme) {
		theme = cmsjs.Themes.Layouts.getTheme(theme);
		cmsjs.Console.removeAllPanels();
		cmsjs.Console.setGridRows(theme.gridRows);
		cmsjs.Console.setGridColumns(theme.gridCols);
		theme.panels.forEach(([position,span],panelName) => {
			panel = cmsjs.Console.getPanel(panelName);
			cmsjs.Console.attachPanel(panel,position,span);
		});
		theme.components.forEach((panelName,componentName) => {
			panel = cmsjs.Console.getPanel(panelName);
			component = cmsjs.Component.get(componentName);
			panel.attachComponent(component);
		});
	}
	constructor(name) {
		this.name = name;
		this.panels = new Map();
		this.gridRows = '1fr';
		this.gridCols = '1fr';
	}
	apply() {
		cmsjs.Themes.Layouts.apply(this);
	}
	setGridRows(str) {
		this.gridRows = str; // Should look like "auto 1fr auto auto"
	}
	setGridCols(str) {
		this.gridCols = str; // Should look like "auto 1fr"
	}
	addPanel(panelID,position,span) {
		this.panels.set(panelID,[position,span]);
	}
	addComponent(panelID,componentID) {
		this.components.add(componentID,panelID);
	}
}



/**************************************
 *                                    *
 *           THEME EDITOR             *
 *                                    *
 **************************************/

cmsjs.ThemeEditor = class {

	static show(id) {
		cmsjs.State.setAndSave('cmsjs.ThemeEditor',id);
		const editor = CONSOLE.getPanel('cmsjs.ThemeEditor');
		themes.style.transition = 'max-height 0.3s ease-out';
		themes.style.maxHeight = '0';
		themes.style.overflow = 'hidden';
		//console.warn(id,editor);
		CONSOLE.showOnlyComponent(id);
		editor.style.maxHeight = '33vh';
		//editor.style.overflowY = 'scroll';
		editor.style.display = 'block';
	}
	
	static hide(id) {
		cmsjs.State.setAndSave('cmsjs.ThemeEditor',null);
		const editor = cmsjs.Console.getPanel('style');
		editor.style.display = 'none';
	}

	
	// Theme Editor
	static getThemeEditor(div) {
		const close = getCloseButton();
		close.addEventListener('click', (event) => { hideStyleEditor(); });
		div.append(close);
		div.style.fontSize = cmsjs.Themes.Fonts.getFontSize(cmsjs.State.get('Config.ConsoleFontSize'));
		cmsjs.Themes.Fonts.setGoogleFont(div,cmsjs.State.get('Config.ConsoleFont'),'sans-serif');
		div.style.textAlign = 'left';
		div.style.borderTop = 'solid 1px';
		div.style.padding = '1rem';
		const fragment = document.createDocumentFragment();
	
		const currentTheme = cmsjs.Themes.Colors.getCurrentTheme();
		cmsjs.State.set('ENV.CurrentTheme',currentTheme);
		let newTheme = cmsjs.State.get('ENV.TempTheme');
		if ( ! newTheme ) {
			newTheme = new Map(currentTheme);
		//	cmsjs.State.set('ENV.TempTheme',newTheme);
		}
		//STYLE.EditorTheme = cmsjs.State.get('ENV.TempTheme');
	
		function getColorInput(label,id) {
			const config = {};
			config['label'] = label;
			config['value'] = newTheme.get(id);
			config['label-position'] = 'right';
			config['listener:change'] = ((event) => {
				newTheme.set(id,event.target.value);
				cmsjs.Themes.Colors.applyTheme(newTheme);
				CONSOLE.buildComponent('controls');
			});
			const wrapper = cmsjs.Controls.create('input:color',id,config);
			wrapper.style.margin = '0.5em';
			return wrapper;
		}
	
		const heading = document.createElement('h1','Theme Editor');
		heading.style.fontSize = '1.5em';
		fragment.append(cmsjs.Themes.Colors.bindElement(heading,'color','heading-color'));
		const themeDiv = document.createElement('div');
		themeDiv.style.display = 'flex';
		fragment.append(themeDiv);
		const div1 = document.createElement('div');
		div1.style.flex = '1';
		themeDiv.append(div1);
		const div2 = document.createElement('div');
		div2.style.flex = '1';
		themeDiv.append(div2);
		const div3 = document.createElement('div');
		div3.style.flex = '1';
		themeDiv.append(div3);
		const div4 = document.createElement('div');
		div4.style.flex = '1';
		themeDiv.append(div4);
	
		const h1 = document.createElement('h2','Console');
		h1.style.fontSize = '1.25em';
		div1.append(cmsjs.Themes.Colors.bindElement(h1,'color','heading-color'));
		div1.append(getColorInput('Background','background-color'));
		div1.append(getColorInput('Text','text-color'));
		div1.append(getColorInput('Text 2','text-2-color'));
		div1.append(getColorInput('Heading','heading-color'));
		
		const h2 = document.createElement('h2','Controls');
		h2.style.fontSize = '1.25em';
		div2.append(cmsjs.Themes.Colors.bindElement(h2,'color','heading-color'));
		div2.append(getColorInput('Slider On','console-toggle-slider-on-color'));
		div2.append(getColorInput('Button On','console-toggle-button-on-color'));
		div2.append(getColorInput('Slider Off','console-toggle-slider-off-color'));
		div2.append(getColorInput('Button Off','console-toggle-button-off-color'));
	
		const h3 = document.createElement('h2','Keys/Values');
		h3.style.fontSize = '1.25em';
		div3.append(cmsjs.Themes.Colors.bindElement(h3,'color','heading-color'));
		div3.append(getColorInput('Keys','key-color'));
		div3.append(getColorInput('Nulls','null-color'));
		div3.append(getColorInput('Booleans','boolean-color'));
		div3.append(getColorInput('Numbers','number-color'));
		div3.append(getColorInput('Strings','string-color'));
		div3.append(getColorInput('Objects','object-color'));
	
		const h4 = document.createElement('h2','JSON Controls');
		h4.style.fontSize = '1.25em';
		div4.append(cmsjs.Themes.Colors.bindElement(h4,'color','heading-color'));
		div4.append(getColorInput('Bytes','bytes-color'));
		div4.append(getColorInput('Controls','controls-color'));
		div4.append(getColorInput('Outlines','button-outline-color'));
		div4.append(getColorInput('Guideline 1','guideline-1-color'));
		div4.append(getColorInput('Guideline 2','guideline-2-color'));
		div4.append(getColorInput('Guideline 3','guideline-3-color'));
		div4.append(getColorInput('Guideline 4','guideline-4-color'));
	
		div.append(fragment);
		//STYLE.applyCurrentTheme(CONSOLE.getPanel('themes'));
		return div;
	}

	
	// Font Editor
	static getFontEditor(div) {
		const close = getCloseButton();
		close.addEventListener('click', (event) => { hideStyleEditor(); });
		div.append(close);
		//div.style.display = 'none';
		div.style.fontSize = cmsjs.Themes.Fonts.getFontSize(cmsjs.State.get('Core.ConsoleFontSize'));
		cmsjs.Themes.Fonts.setGoogleFont(div,cmsjs.State.get('Core.ConsoleFont'),'sans-serif');
		div.style.textAlign = 'left';
		div.style.borderTop = 'solid 1px';
		div.style.padding = '1rem';
		const fragment = document.createDocumentFragment();
		
		function getFontListInput(label) {
			const defaults = 'Roboto Mono, Kode Mono, Rubik, Anta, Kanit, Noto Sans, Roboto, Barlow, Lora, Open Sans';
			const config = {};
			config['label'] = 'Available Fonts:';
			console.warn(cmsjs.State.get('Core.FontList'));
			config['value'] = cmsjs.State.get('Core.FontList').join(', ');
			config['placeholder'] = 'Enter a comma-separated list of Google font names, e.g. "Roboto, Rubik, Noto Sans"';
			config['listener:change'] = ((event) => {
				cmsjs.State.changeAndSave('Core.FontList',event.target.value.split(',').map((item) => item.trim()));
			});
			return CONTROLS.create('input:text','Core.FontList',config);
		}
	
		const heading = document.createElement('h1','Font Editor');
		heading.style.fontSize = '1.5em';
		fragment.append(cmsjs.Themes.Colors.bindElement(heading,'color','heading-color'));
		fragment.append(getFontListInput());
	
		const fontDiv = document.createElement('div');
		fontDiv.style.display = 'flex';
		fontDiv.style.justifyContent = 'center';
		fontDiv.style.marginTop = '1rem';
		fragment.append(fontDiv);
		const fontTypes = ['Console','Code','Display'];
		fontTypes.forEach((fontType,index) => {
			const _div = document.createElement('div');
			_div.style.flex = '1';
			_div.style.display = 'flex';
			_div.style.justifyContent = 'center';
			_div.style.flexDirection = 'column';
			fontDiv.append(_div);
			const fontID = 'Core.' + fontType + 'Font';
			const fontSizeID = 'Core.' + fontType + 'FontSize';
			const fontControl = createControl('select',fontID,{items:cmsjs.State.get('Core.FontList')});
			const fontSizeControl = createControl('select:integer',fontSizeID,{min:1,max:10});
			fontControl.style.margin = 'auto';
			fontControl.style.marginBottom = '0.75em';
			fontSizeControl.style.margin = 'auto';
			_div.append(fontControl);
			_div.append(fontSizeControl);
		});
	
		div.append(fragment);
		cmsjs.Themes.Colors.applyCurrentTheme(CONSOLE.getPanel('fonts'));
		return div;
	}

}
