
/**
 * SPA: Single Page Application
 *
 * Manages hash tag URLs and behavior based on hash changes.
 */
cmsjs.SPA = class {

	static Log = cmsjs.Logger.create('cmsjs.SPA');
	
	static addWindowListener() {
		cmsjs.SPA.Log.trace('addWindowListener()');
		// Initial page load
		window.addEventListener('load', async (event) => {
			cmsjs.SPA.Log.trace('window load listener fired');
			console.log('cms: initializing from window "load" event');
			const log = new Log();
			await cmsjs.SPA.processURL();
			const module = await cmsjs.Module.load(cmsjs.SPA.ENV.Module);
			await module.action(cmsjs.SPA.ENV.Action);
		});
		// When the hash changes
		window.addEventListener('hashchange', async (event) => {
			cmsjs.SPA.Log.trace('window hashchange listener fired');
			const oldModule = await cmsjs.Module.load(cmsjs.SPA.ENV.Module);
			cmsjs.SPA.processURL();
			const module = await cmsjs.Module.load(cmsjs.SPA.ENV.Module);
			// only reload the page when the package changes
			if ( oldModule.getPackageRoot() != module.getPackageRoot() ) {
				location.reload();
			}
			else {
				await module.action(cmsjs.SPA.ENV.Action);
			}
		});
	}

	static CONFIG = {
		ModuleRoot: 'mod.root',
		ModuleDirectoryPrefix: 'mod.',
	};

	static ENV = {
	    RealURL: null,
		VirtualURL: null,
		Module: null,
		Action: null,
		Params: null,
		Hash: null
	};

	/**
	 * Parses the current URL and sets environment variables.
	 */
	static processURL() {
		const realURLString = window.location.href;
		const realURL = new URL(realURLString);
		const realHash = realURL.hash;
		const realOrigin = realURL.origin;
		const realPathname = realURL.pathname;
		const realParams = realURL.searchParams;
		// redirect to the root: /#/
		// Note that you need to redirect to the path directory or else
		// index.html will redirect to index.html#/
		const realPathDir = realPathname.substring(0,realPathname.lastIndexOf('/')+1);
	    if ( ! realHash ) {
			cmsjs.SPA.Log.info('no hash, redirecting to /#/');
		    window.location.href = realOrigin + realPathDir + '#/';
	    }
		else if ( realHash === '#' ) {
			cmsjs.SPA.Log.info('hash is simply "#", redirecting to /#/');
		    window.location.href = realOrigin + realPathDir + '#/';
		}
		const cmsURLString = realOrigin + realHash.replace(/^#/,'')
		const cmsURL = new URL(cmsURLString);
		const cmsPathname = cmsURL.pathname;
	    //  /path/to/dir/ -> ['', 'path', 'to', 'dir', '']
	    //  /path/to/dir/file -> ['', 'path', 'to', 'dir', 'file']
	    const cmsPathArray = cmsPathname.split('/');
		// grab the first four components of the above to recreate the path
		const module = cmsPathArray.slice(0,-1).join('/') + '/';
		// the path array will at a minimum be ['',''], so the action will exist
		const action = cmsPathArray[cmsPathArray.length - 1];
		// turn params into a map
		const params = new Map(cmsURL.searchParams.entries());
		// I forget what this wizardry does, and why I don't just use cmsURL.hash
		const hash = (cmsURL.hash || '#').replace(/^#/,'') || null;
		cmsjs.SPA.ENV.Module = module;
		cmsjs.SPA.ENV.Action = action;
		cmsjs.SPA.ENV.Params = params;
		cmsjs.SPA.ENV.Hash = hash;
		cmsjs.SPA.ENV.VirtualURL = cmsURL;
		cmsjs.SPA.ENV.RealURL = realURL;
		cmsjs.SPA.Log.info('processURL(): ENV set',cmsjs.SPA.ENV);
	}

	/*
	 * Runs the request as derived from the URL
	 */
	static async action() {
		cmsjs.SPA.Log.trace('action()');
		cmsjs.SPA.Log.debug('action(): running action: ' + cmsjs.SPA.ENV.Module + cmsjs.SPA.ENV.Action);
	}

}

