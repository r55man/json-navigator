
/**
 * Interactive controls
 */
cmsjs.Controls = class {

	static Log = cmsjs.Logger.create('cmsjs.Controls');

	static Instances = new Map();
	
	static Creators = new Map();

	constructor(id,type,config) {
		this.id = id;
		this.type = type;
		this.config = config;
		const creatorFunction = cmsjs.Controls.Creators.get(type);
		this.element = creatorFunction(config);
	}
	
	static create(id,type,config={}) {
		cmsjs.Controls.Log.trace('create(id,type)',id,type);
		if ( ! id ) {
			cmsjs.Controls.Log.error('create(id) called with no id specified');
			return;
		}
		if ( ! type ) {
			cmsjs.Controls.Log.error('create(id) no type specified for id:',id);
			return;
		}
		if ( cmsjs.Controls.Instances.has(id) ) {
			cmsjs.Controls.Log.error('create(id) id exists:',id);
			return cmsjs.Controls.Instances.get(id);
		}
		const creatorFunction = cmsjs.Controls.Creators.get(type);
		if ( ! creatorFunction ) {
			cmsjs.Controls.Log.error('no creator function found for type <' + type + '> (id: ' + id + ')');
		}
		const control = new cmsjs.Controls(id,type,config);
		if ( ! control ) {
			cmsjs.Controls.Log.error('creator function did not return a control for type: ',type);
		}
		//const control = new Control(id,type,config);
		cmsjs.Controls.Instances.set(id,control);
		control.element.setAttribute('cmsjs-control-type',type);
		//cmsjs.Controls.Log.debug('{cmsjs.Controls} created <' + type + '> with id "' + id + '"');
		return control;
	}

	static get(id) {
		return cmsjs.Controls.Instances.get(id);
	}

	static getValue(id) {
		const control = cmsjs.Controls.Instances.get(id);
		const controlType = control.getAttribute('cmsjs-control-type');
		if ( controlType === 'toggle' ) {
			const input = control.querySelector('input');
			return input.checked;
		}
		else if ( controlType.startsWith('select') ) {
			const select = control.querySelector('select');
			return select.options[select.selectedIndex].textContent;
		}
		else if ( controlType.startsWith('input') ) {
			const input = control.querySelector('input');
			return input.value;
		}
	}

	static getLabelElement(id) {
		const control = cmsjs.Controls.get(id);
		const label = cmsjs.Controls.querySelector 
	}

	// Set a control value
	static setValue(id,value) {
		const control = cmsjs.Controls.get(id);
		const controlType = control.element.getAttribute('cmsjs-control-type');
		if ( controlType === 'toggle' ) {
			const slider = control.element.querySelector('.cmsjs-control-toggle-slider');
			const input = control.element.querySelector('input');
			if ( value === true && ! input.checked ) {
				input.click();
			}
			else if ( value === false && input.checked ) {
				input.click();
			}
		}
		else if ( controlType.startsWith('select') ) {
			const select = control.element.querySelector('select');
			for ( let i = 0; i < select.options.length; i++ ) {
				if ( select.options[i].text === value.toString() ) {
					if ( select.selectedIndex !== i ) {
						select.selectedIndex = i;
						select.dispatchEvent(new Event('change'));
					}
					break;
				}
			}
		}
		else if ( controlType.startsWith('input') ) {
			const input = control.querySelector('input');
			const currentValue = input.value;
			if ( currentValue !== value ) {
				input.value = value;
				input.dispatchEvent(new Event('change'));
			}
		}
	}
	
	// Toggles a boolean control.
	static toggle(id) {
		const control = cmsjs.Controls.get(id);
		if ( ! control.element.getAttribute('cmsjs-control-type') === 'toggle' ) {
			cmsjs.Controls.Log.error('{cmsjs.Controls} attempt to toggle non-tobblable control: ',control);
			return;
		}
		const input = control.element.querySelector('input');
		input.click();
	}

	// Utility function to set up listeners through the config mapping
	static addListeners(element,config) {
		for ( const key in config ) {
			if ( key.startsWith('listener:') ) {
				const action = key.split(':')[1];
				const functionToRun = config[key];
				element.addEventListener(action, (event) => {
					functionToRun(event);
				});
			}
		}
	}

	static appendLabel(control,config) {
		const wrapper = document.createElement('div');
		wrapper.setAttribute('cmsjs-control','');
		const controlID = 'cmsjs-control-' + Math.random().toString(36).substring(2,8);
		control.id = controlID;
		const labelText = config['label'] || config['text'];
		if ( ! labelText ) {
			wrapper.append(control);
			return wrapper;
		}
		wrapper.style.display = 'flex';
		wrapper.style.alignItems = 'center';
		const labelPosition = config['label-position'] || 'left';
		const label = document.createElement('div');
		label.setAttribute('cmsjs-control-label','');
		label.setAttribute('for',controlID);
		label.style.whiteSpace = 'nowrap';
		label.innerHTML = labelText;
		if ( labelPosition === 'left' ) {
			label.style.display = 'inline-block';
			label.style.marginRight = '1em';
			wrapper.style.justifyContent = 'flex-end';
			wrapper.append(label);
			wrapper.append(control);
		}
		else if ( labelPosition === 'right' ) {
			label.style.display = 'inline-block';
			label.style.marginLeft = '1em';
			wrapper.style.justifyContent = 'flex-start';
			wrapper.append(control);
			wrapper.append(label);
		}
		else if ( labelPosition === 'top-left' ) {
			label.style.marginBottom = '0.25em';
			wrapper.style.flexDirection = 'column';
			wrapper.style.alignItems = 'flex-start';
			wrapper.append(label);
			wrapper.append(control);
		}
		else if ( labelPosition === 'top-right' ) {
			label.style.marginBottom = '0.25em';
			wrapper.style.flexDirection = 'column';
			wrapper.style.alignItems = 'flex-end';
			wrapper.append(label);
			wrapper.append(control);
		}
		return wrapper;
	}

}

/*
 * Shortcut to get a drop-down box of integers.
 */
cmsjs.Controls.Creators.set('select:integer', (config) => {
	const min = config['min'] || 1;
	const max = config['max'] || 10;
	const array = [];
	for ( let i = min; i < max + 1; i++ ) {
		array.push(i);
	}
	config['items'] = array;
	config['inline'] = true;
	config['datatype'] = 'integer';
	const element = cmsjs.Controls.Creators.get('select')(config);
	return element;
});

/*
 * Drop-down select box
 */
cmsjs.Controls.Creators.set('select', (config) => {
	const text = config['text'] || '';
	const height = config['height'] || 16;
	const width = config['width'] || 3*height;
	const selected = config['value'] || null;
	const tabindex = config['tabindex'] || null;
	const datatype = config['datatype'] || 'string';
	let items = config['items'] || {};
	if ( Array.isArray(items) ) {
		items = items.reduce((map, item) => { map[item] = item; return map; }, {});
	}
	const borderRadius = height/2;
	const onChange = config['listener:change'];
	const select = document.createElement('select');
	select.style.borderRadius = '0.2em';
	select.style.fontSize = 'inherit';
	select.style.color = 'inherit';
	select.style.backgroundColor = 'inherit';
	if ( tabindex ) { select.setAttribute('tabindex',tabindex); }
	Object.keys(items).forEach((key) => {
		const option = document.createElement('option');
		option.value = items[key];
		option.style.fontSize = 'inherit';
		option.style.color = 'black';
		option.style.backgroundColor = 'white';
		option.textContent = key;
		if ( items[key] == selected ) { option.selected = true; }
		select.append(option);
	});
	if ( onChange ) {
		select.addEventListener('change', (event) => {
			//changeState(id,event.target.value);
			let selectedValue = event.target.value;
			if ( datatype === 'integer' ) {
				selectedValue = parseInt(selectedValue);
			}
			else if ( datatype === 'boolean' ) {
				selectedValue = selectedValue ? true : false;
			}
			onChange(selectedValue);
		});
	}
	const wrapper = cmsjs.Controls.appendLabel(select,config);
	return wrapper;
});

/*
 * General-purpose toggle switch creator.
 * Used by drawControls().
 *
 * <div class='toggle'>
 *   <label class='switch'>  
 *   <input type='checkbox'>
 *   <span class='slider'>  NOTE: color/bgColor on this affects the GROOVE
 * </div>
 */
cmsjs.Controls.Creators.set('toggle', (config) => {
	const labelText = config['text'] || '';
	const tabindex = config['tabindex'] || null;
	const height = config['height'] || 12;
	const width = config['width'] || 3*height;
	const isOn = ( config['value'] === true || config['value'] === 'on' );
	const onChange = config['listener:change'];
	const sliderOnColor = config['slider-on-color'] || '#acf';
	const sliderOffColor = config['slider-off-color'] || '#bbb';
	const buttonOnColor = config['button-on-color'] || '#3af';
	const buttonOffColor = config['button-off-color'] || '#999';
	const sliderOnThemeValue = 	config['slider-on-theme-value'];
	const sliderOffThemeValue = config['slider-off-theme-value'];
	const buttonOnThemeValue = config['button-on-theme-value'];
	const buttonOffThemeValue = config['button-off-theme-value'];
	const borderRadius = height/2;
	const input = document.createElement('input')
	input.setAttribute('type','checkbox');
	if ( tabindex ) { input.setAttribute('tabindex',tabindex); }
	const wrapper = cmsjs.Controls.appendLabel(input,config);
	const toggleType = 'switch';
	if ( toggleType === 'switch' ) {
		const dummy = document.createElement('label');
		wrapper.append(dummy);
		const slider = document.createElement('span');
		slider.classList.add('cmsjs-control-toggle-slider');
		dummy.append(input);
		dummy.append(slider);
		dummy.style.position = 'relative';
		dummy.style.display = 'inline-block';
		dummy.style.display = 'inline-block';
		dummy.style.width = width + 'px';
		dummy.style.height = height + 'px';
		input.style.opacity = '0';
		input.style.width = '0';
		input.style.height = '0';
		slider.style.position = 'absolute';
		slider.style.cursor = 'pointer';
		slider.style.top = '0';
		slider.style.left = '0';
		slider.style.right = '0';
		slider.style.bottom = '0';
		slider.style.transition = sliderOffColor;
		slider.style.borderRadius = borderRadius + 'px';
		if (isOn) {
			slider.style.backgroundColor = sliderOnColor;
			input.setAttribute('checked','');
		}
		else {
			slider.style.backgroundColor = sliderOffColor;
			input.removeAttribute('checked');
		}
		input.addEventListener('change', function() {
			//changeState(id,this.checked);
			onChange(this.checked);
			if (this.checked) { slider.style.backgroundColor = sliderOnColor }
			else { slider.style.backgroundColor = sliderOffColor }
		});
		// Pseudo-elements cannot be set using javascript... :/
		const styleElt = document.createElement('style');
		const buttonDiameter = (4/3) * height;
		const translate = width - buttonDiameter;
		const bottomShift = (buttonDiameter - height)/2;
		styleElt.innerHTML = `
			.cmsjs-control-toggle-slider:before {
				content: "";
				position: absolute;
				height: ${buttonDiameter}px;
				width: ${buttonDiameter}px;
				left: 0px;
				bottom: -${bottomShift}px;
				transition: .25s;
				border-radius: 50%;
			}
			input:checked + .cmsjs-control-toggle-slider:before {
				transform: translateX(${translate}px);
			}
			`;
		wrapper.append(styleElt);
		const colorStyleElt = document.createElement('style');
		colorStyleElt.classList.add('cmsjs-control-toggle-colors');
		const buttonColorOn = buttonOnColor;
		const buttonColorOff = buttonOffColor;
		colorStyleElt.innerHTML = `
			.cmsjs-control-toggle-slider:before { background-color: ${buttonColorOff}; }
			input:checked + .cmsjs-control-toggle-slider:before { background-color: ${buttonColorOn}; }
		`;
		wrapper.append(colorStyleElt);
	}
	return wrapper;
});


/*
 * Color Selector
 */
cmsjs.Controls.Creators.set('input:color', (config) => {
	const label = config['label'] || '';
	let value = config['value'] || '#ffffff';
	if ( value.length === 4 && value[0] === '#' ) { value = '#' + value[1] + value[1] + value[2] + value[2] + value[3] + value[3]; }
	const inputElt = document.createElement('input');
	inputElt.type = 'color';
	inputElt.value = value;
	cmsjs.Controls.addListeners(inputElt,config);
	const wrapper = cmsjs.Controls.appendLabel(inputElt,config);
	return wrapper;
});

/*
 * Text Input
 */
cmsjs.Controls.Creators.set('input:text', (config) => {
	const label = config['label'] || '';
	const value = config['value'] || '';
	const placeholder = config['placeholder'] || '';
	const inputElt = document.createElement('input');
	inputElt.type = 'text';
	inputElt.value = value;
	inputElt.placeholder = placeholder;
	inputElt.style.width = '100%';
	inputElt.style.boxSizing = 'border-box';
	inputElt.style.fontSize = 'inherit';
	const wrapper = cmsjs.Controls.appendLabel(inputElt,config);
	cmsjs.Controls.addListeners(inputElt,config);
	return wrapper;
});
