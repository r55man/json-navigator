/**
 * Component class.
 */
cmsjs.Component = class {
	
	static Log = cmsjs.Logger.create('cmsjs.Component');

	static Instances = new Map();

	// This is a separate class, defined at end of the file.
	// It contains dummy content for 
	static Mockup = null;

	static get(id) {
		cmsjs.Component.Log.trace('get(id)',id);
		const instance = cmsjs.Component.Instances.get(id);
		return instance;
	}

	static create(id, buildFunction = function() {}) {
		cmsjs.Component.Log.trace('create(id)',id);
		if ( cmsjs.Component.Instances.has(id) ) {
			cmsjs.Component.Log.error('create(id) id exists:',id);
			return null;
		}
		const instance = new cmsjs.Component(id,buildFunction);
		cmsjs.Component.Instances.set(id,instance);
		return instance;
	}
	
	constructor(id, buildFunction = function() {}) {
		cmsjs.Component.Log.trace('constructor(id)',id);
		this.id = id;
		this.log = cmsjs.Logger.create('cmsjs.Component(' + id + ')');
		this.buildFunction = buildFunction;
		this.wrapper = document.createElement('div');
		this.wrapper.setAttribute('cmsjs-component-wrapper','');
		this.element = document.createElement('div');
		this.element.setAttribute('cmsjs-component','');
		this.element.setAttribute('cmsjs-component-id',id);
		this.wrapper.appendChild(this.element);
	}
	
	setScrollable(xScroll,yScroll) {
		this.log.trace('setScrollable(x,y)',xScroll,yScroll);
		if ( arguments.length === 0 ) {
			xScroll = true;
			yScroll = true;
		}
		else if ( arguments.length == 1 ) {
			yScroll = xScroll;
		}
		if ( xScroll || yScroll ) {
			//wrapper.style.padding = '1rem';
			this.wrapper.setAttribute('cmsjs-component-scroller','');
			this.wrapper.style.height = '100%';
			this.wrapper.style.overflow = 'auto';
			//component.style.height = '100%';
		}
		else {
			this.wrapper.style.padding = '';
			this.wrapper.removeAttribute('cmsjs-component-scroller');
			this.wrapper.style.height = '';
			this.wrapper.style.overflow = '';
		}
	}
	
	getWrapper() {
		return this.wrapper;
	}
	
	getElement() {
		return this.element;
	}
	
	isBuilt() {
		if ( this.element.getAttribute('cmsjs-component-built') ) { return true; }
		return false;
	}

	/**
	 * Builds a component. 
	 *
	 * By default, a component is only built if it has not already been built.
	 * Setting 'force' to 'true' will build the component regardless.
	 */
	build(force=false) {
		this.log.trace('build()');
		if ( this.element.hasAttribute('cmsjs-component-built') && !force ) {
			this.log.debug('not building');
			return;
		}
		this.element.innerHTML = '';
		this.buildFunction(this);
		// Add a dummy spacer if this is a scrollable element. This is ugly.
		// If there is an easy way to get the desired padding at the bottom
		// of a scrollable component, I cannot find it...
		if ( this.wrapper.hasAttribute('cmsjs-component-scroller') ) {
			const dummy = document.createElement('div');
			dummy.style.visibility = 'hidden';
			dummy.setAttribute('cmsjs-dummy-spacer','');
			dummy.style.paddingBottom = 'inherit';
			this.element.appendChild(dummy);
		}
		this.element.setAttribute('cmsjs-component-built','');
	}
	
	hide() {
		this.log.trace('hide()');
		this.wrapper.style.display = 'none';
	}
	
	show() {
		this.log.trace('show()');
		if ( ! this.isBuilt() ) { this.build(); }
		this.wrapper.style.display = 'block';
	}
	
	remove() {
		this.log.trace('remove()');
		this.wrapper.remove();
	}

}

/**
 * Class for generating dummy content.
 */
cmsjs.Component.Mockup = class {

	/**
	 * Generate text.
	 *
	 * @param {int} n - total number of lines to print.
	 * @params {int} p - create a paragraph every p lines
	 */
	static text(n=1,p=0) {
		cmsjs.Component.Log.trace('generateDummyText(n)',n);
		if ( ! Number.isInteger(n) || n < 1 || n > 10000 ) {
			cmsjs.Component.Log.error('generateDummyText(n) invalid value for n:',n);
			n = 1;
		}
		const output = [''];
		let outputIndex = 0;
		const text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
		for ( let i = 0; i < n; i++ ) {
			output[outputIndex] += (' ' + text);
			if ( p !== 0 ) {
				if ( (i+1)%p === 0 ) {
					outputIndex += 1;
					output[outputIndex] = '';
				}
			}
		}
		if ( output.length === 1 ) {
			if ( p !== 0 ) {
				return ('<p>' +  output[0] + '</p>');
			}
			else {
				return output[0];
			}
			
		}
		else {
			let outputString = '';
			for ( let i = 0; i < output.length; i++ ) {
				outputString += ('<p>' + output[i] + '</p>');
			}
			return outputString;
		}
	}

	/**
	 * Generate a list.
	 *
	 * @param {int} n - total number of items to generate.
	 */
	static list(n=1,style='white-space:nowrap;list-style-type:none;margin:0;padding:0') {
		if ( ! Number.isInteger(n) || n < 1 || n > 10000 ) {
			cmsjs.Component.Log.error('generateDummyList(n) invalid value for n:',n);
			n = 1;
		}
		const output = [];
		//const output = [`<ul style='${style}'>`];
		for ( let i = 1; i <= n; i++ ) {
			//output.push(`<li style='margin:0;padding:0'>List item ${i}</li>`);
			output.push(`List&nbsp;item&nbsp;${i}<br>`);
		}
		//output.push('<ul>');
		return output.join('');
	}

	/**
	 * Generate a set of <div> elements.
	 *
	 * @param {int} n - total number of divs to generate (default 1)
	 * @param {int} width - width of each div (default 100px)
	 * @param {int} height - height of each div (default (100px)
	 * @param {string} style - (optional) override the default box styling
	 */
	static divs(n=1,width=100,height=100,style='box-sizing:border-box;border:solid 1px black;margin:10px;padding:10px;') {
		const output = [];
		for ( let i = 1; i <= n; i++ ) {
			output.push(`<div style='width:${width}px;height:${height}px;${style}'></div>`);
		}
		return output.join('');
	}
	
}


