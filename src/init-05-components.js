
log.trace('loading init-component.js');


// Header
cmsjs.Component.create('header', (component) => {
	const wrapper = component.wrapper;
	wrapper.style.borderBottom = 'solid 1px';
	wrapper.style.padding = '0.25rem 0.5rem';
	const header = component.element;
	header.innerHTML = '';
	cmsjs.Themes.Fonts.setGoogleFont(wrapper,cmsjs.State.get('Config.ConsoleFont'),'sans-serif');
	const urlDiv = $.ce('div');
	urlDiv.id = 'url';
	urlDiv.style.width = '100%';
	urlDiv.style.fontSize = '90%';
	urlDiv.style.padding = '0.25rem 0rem';
	urlDiv.style.marginBottom = '0.25rem';
	//urlDiv.classList.add('heading');
	if ( cmsjs.State.get('ENV.StandaloneMode') ) {
		urlDiv.innerHTML = 'JSON Navigator'
	}
	else {
		urlDiv.innerHTML = window.location.href.split('#')[0];
	}
	if ( ! cmsjs.State.get('Config.ShowURL') ) { urlDiv.style.display = 'none'; }
	header.append(urlDiv);
	const searchDiv = $.ce('div');
	searchDiv.id = 'search';
	searchDiv.style.width = '100%';
	searchDiv.style.padding = '0.25rem 0rem';
	searchDiv.style.display = 'flex';
	searchDiv.style.alignItems = 'left';
	if ( ! cmsjs.State.get('Config.ShowSearch') ) { searchDiv.style.display = 'none'; }
	header.append(searchDiv);
	if ( ! cmsjs.State.get('Config.ShowURL') && ! cmsjs.State.get('Config.ShowSearch') ) {
		header.parentElement.style.display = 'none';
	}
	const searchLabel = $.ce('label');
	searchLabel.setAttribute('for','search-input');
	searchLabel.style.marginRight = '0.5rem'
	searchLabel.innerHTML = cmsjs.Dictionary.get('Controls').get('Search');
	//searchLabel.innerHTML = cmsjs.Dictionary.get('Controls').languages.get('en').get('Search');
	searchDiv.append(searchLabel);
	const searchInput = $.ce('input');
	searchInput.id = 'search-input';
	searchInput.setAttribute('tabindex','-1');
	searchInput.style.flex = '1';
	searchInput.style.paddingRight = '1rem';
	searchDiv.append(searchInput);
	searchInput.addEventListener('keypress', function(event) {
		if ( event.key === 'Enter' ) {
			const value = event.target.value;
			log.debug('search document for value: ',value);
			const currentJSON = getValueForJsonPath(cmsjs.State.get('ENV.CurrentPath'));
			$.id('root').classList.remove('filtered');
			searchJsonWithPath(currentJSON,value);
			$.id('root').querySelectorAll('div.item > div.contents > div.item').forEach((item) => {
			});
			$.id('root').classList.add('filtered');
		}
	});
	//searchInput.addEventListener('input', function(event) {
	//});
});





// Footer
cmsjs.Component.create('footer', (component) => {
	const wrapper = component.wrapper;
	wrapper.style.borderTop = 'solid 1px';
	wrapper.style.padding = '0.1rem';
	const footer = component.element;
	footer.innerHTML = '';
	footer.style.width = '100%';
	footer.style.textAlign = 'center';
	footer.style.padding = '0.5rem';
	footer.style.fontSize = cmsjs.Themes.Fonts.getFontSize(cmsjs.State.get('Config.ConsoleFontSize'));
	cmsjs.Themes.Fonts.setGoogleFont(wrapper,cmsjs.State.get('Config.ConsoleFont'),'sans-serif');
	let footerDiv = $.ce('div');
	const docLink = $.ce('a');
	docLink.href = '#';
	docLink.textContent = cmsjs.Dictionary.get('Controls').get('Documentation');
	docLink.addEventListener('click',function(event) { event.preventDefault(); showPage('content-docs'); });
	footerDiv.append(docLink);
	footerDiv.append($.ctn('\u00A0\u00A0\u00A0\u2022\u00A0\u00A0\u00A0'));
	const gitlabLink = $.ce('a');
	gitlabLink.href = "https://gitlab.com/r55man/json-navigator";
	gitlabLink.textContent = 'GitLab';
	footerDiv.append(gitlabLink);
	footerDiv.append($.ctn('\u00A0\u00A0\u00A0\u2022\u00A0\u00A0\u00A0'));
	const chromeLink = $.ce('a');
	chromeLink.href = "https://chromewebstore.google.com/detail/glgilclfmgofaeffphalkglkgbbpmigf";
	chromeLink.textContent = 'Chrome Store';
	footerDiv.append(chromeLink);
	footer.append(footerDiv);
});




/**
 * Control Panel
 */
cmsjs.Component.create('controls', (component) => {
	log.debug('cms.component.js: CREATING CONTROLS');
	component.setScrollable(true);
	const wrapper = component.wrapper;
	wrapper.style.borderLeft = 'solid 1px';
	const controls = component.element;
	controls.style.textAlign = 'right';
	controls.style.padding = '1rem';
	/*
	controls.innerHTML = '';
	controls.parentElement.style.padding = '1rem 0.5rem 2rem 0.5rem';
	controls.parentElement.style.overflowY = 'scroll'; // keep 'scroll' to prevent redraw problems
	controls.style.height = '100%';
	controls.style.paddingBottom = '2rem';
	*/
	
	controls.style.fontSize = cmsjs.Themes.Fonts.getFontSize(cmsjs.State.get('Config.ConsoleFontSize'));
	cmsjs.Themes.Fonts.setGoogleFont(wrapper,cmsjs.State.get('Config.ConsoleFont'),'sans-serif');

	// <div> control panel header
	const headerCP = document.createElement('div');
	//cmsjs.Themes.Colors.bindElement(headerCP,'color','heading-color');
	headerCP.style.fontSize = '1rem';
	headerCP.style.fontWeight = 'bold';
	headerCP.style.textDecoration = 'underline';
	headerCP.style.textAlign = 'center';
	headerCP.style.marginBottom = '0.75rem';
	headerCP.classList.add('heading');
	headerCP.innerHTML = 'JSON Navigator';
	controls.append(headerCP);
	// JSON Interaction
	controls.append(createControl('toggle','Config.MarkupMode').element);
	controls.append(createControl('toggle','Config.ShowNulls').element);
	controls.append(createControl('toggle','Config.BoldKeys').element);
	controls.append(createControl('toggle','Config.ShowBytes').element);
	controls.append(createControl('toggle','Config.ShowCount').element);
	controls.append(createControl('toggle','Config.SortKeys').element);
	controls.append(createControl('toggle','Config.ShowGuidelines').element);
	controls.append(createControl('toggle','Config.AutoExpand').element);
	controls.append(createControl('toggle','Config.MarkupHTML').element);
	controls.append(createControl('toggle','Config.ConvertLinks').element);
	controls.append(createControl('select:integer','Config.IndentDepth',{min:1,max:10}).element);
	controls.append(createControl('select:integer','Config.ExpandDepth',{min:1,max:10}).element);
	controls.append(createControl('select:integer','Config.Spacing',{min:1,max:10}).element);
	controls.append(createControl('toggle','Config.LineWrap').element);
	//controls.append(createControl('toggle','Config.EditorMode'));
	// UI
	const uiHeader = $.ce('div');
	uiHeader.style.fontWeight = 'bold';
	uiHeader.style.textAlign = 'center';
	uiHeader.style.margin = '0.5rem 0rem';
	uiHeader.style.padding = '0.25rem';
	uiHeader.style.border = 'solid 1px';
	uiHeader.classList.add('heading');
	uiHeader.innerHTML = cmsjs.Dictionary.get('Controls').get('Console');
	cmsjs.Themes.Colors.bindElement(uiHeader,'color','heading-color');
	controls.append(uiHeader);
	//controls.append(createControl('toggle','Config.DarkMode'));
	controls.append(createControl('toggle','Config.ShowURL').element);
	controls.append(createControl('toggle','Config.ShowSearch').element);
	controls.append(createControl('toggle','Config.ControlKeys').element);
	controls.append(createControl('toggle','Config.ShowFooter').element);
	controls.append(createControl('toggle','Config.Debug').element);
	//controls.append(createControl('select','Config.SelectedLight',{items:Array.from(STYLE.getThemeNamespace('Light').keys())}));
	//controls.append(createControl('select','Config.SelectedDark',{items:Array.from(STYLE.getThemeNamespace('Dark').keys())}));
	//controls.append(createControl('select','Config.ControlPosition',{items:Object.keys{'Right':'Right','Left':'Left'}));
	//controls.append(createControl('select','Config.Language',{items:cmsjs.Dictionary.getDictionaryIndex()}));
	
	const themeLink = $.ce('div');
	themeLink.style.margin = '2rem 0px 1rem 0px';
	themeLink.style.textAlign = 'center';
	themeLink.style.fontWeight = 'bold';
	themeLink.style.cursor = 'pointer';
	themeLink.innerHTML = 'Theme Editor'
	themeLink.addEventListener('click', () => { showStyleEditor('themes'); });
	controls.append(themeLink);

	const fontLink = $.ce('div');
	fontLink.style.margin = '2rem 0px 1rem 0px';
	fontLink.style.textAlign = 'center';
	fontLink.style.fontWeight = 'bold';
	fontLink.style.cursor = 'pointer';
	fontLink.innerHTML = 'Font Editor'
	fontLink.addEventListener('click', () => { showStyleEditor('fonts'); });
	controls.append(fontLink);

});



/****************************************
 *										*
 *			PAGE BUILDING				*
 *										*
 ****************************************/

/*
 * Never call a page building fuction directly. Use this wrapper function instead.
 * It ensures that the page is only built if it is not already built, and
 * also makes sure that it is not displayed unless it is the current page.
 */
function showPage(id) {
	if ( id === cmsjs.State.get('ENV.CurrentPage') ) {
		log.debug('showPage(id): was already this page',id);
		//return;
	}
	cmsjs.Console.Panel.get('Main.Content').showOnlyComponent(id);
	cmsjs.State.set('ENV.CurrentPage',id);
	//const component = cmsjs.Component.get(id);
	//cmsjs.Themes.Colors.applyCurrentTheme(component);
}


/*
 * This is called in the various content pages below.
 * Put code common to all pages here.
 */
function initPageComponent(component) {
	component.setScrollable();
	component.wrapper.style.padding = '0.5rem 0.5rem 1.5rem 0.5rem';
}

/*
 * PAGE: Markup View
 */
cmsjs.Component.create('content-markup', (component) => {
	initPageComponent(component);
	const div = component.element;
	cmsjs.Themes.Fonts.setGoogleFont(div,cmsjs.State.get('Config.CodeFont'),'monospace, sans-serif');
	div.style.fontSize = cmsjs.Themes.Fonts.getFontSize(cmsjs.State.get('Config.CodeFontSize'));
	//const rootElement = getItemElement(null,ENV.JSON,cmsjs.State.get('Config.ShowBytes'));
	if ( ! cmsjs.State.get('ENV.CurrentPath') ) {
		cmsjs.State.set('ENV.CurrentPath',window.location.hash.substring(1) || '$');
	}
	const pathDiv = getClickablePath(cmsjs.State.get('ENV.CurrentPath'));
	if ( ! $.id('json-path') ) {
		pathDiv.id = 'json-path';
		pathDiv.style.display = 'none';
		//pathDiv.style.marginTop = '-0.75rem';
		pathDiv.style.marginBottom = '0.5rem';
		pathDiv.style.position = 'sticky';
		pathDiv.style.zIndex = '100';
		cmsjs.Themes.Colors.bindElement(pathDiv,'background-color','background-color');
		cmsjs.Themes.Colors.bindElement(pathDiv,'color','heading-color');
		pathDiv.style.opacity = '1.0';
		pathDiv.style.top = '0';
		pathDiv.style.width = '100%';
		pathDiv.style.justifyContent = 'flex-start';
		div.parentElement.prepend(pathDiv);
		//div.append(pathDiv);
	}
	const pathElt = $.id('json-path');
	if ( cmsjs.State.get('ENV.CurrentPath') !== '$' ) {
		pathElt.style.display = 'flex';
	}
	const json = getValueForJsonPath(cmsjs.State.get('ENV.CurrentPath'));
	const rootElement = getItemElement(null,json,true);
	rootElement.id = 'root';
	rootElement.setAttribute('depth','0');
	rootElement.setAttribute('key','$');
	div.appendChild(rootElement);
	if ( (typeof json === 'object' && json !== null) || Array.isArray(json) ) {
		const valElt = rootElement.querySelector('div.label > span.value');
		//let isNull = false;
		if ( Array.isArray(json) ) {
			if ( json.length !== 0 ) {
				expandItem(rootElement,json);
			}
		}
		else if ( Object.keys(json).length !== 0 ) {
			expandItem(rootElement,json);
		}
		valElt.setAttribute('tabindex','1');
		if ( cmsjs.State.get('Config.AutoExpand') ) { expandAll(); }
		valElt.focus();
		//console.warn(json);
		//console.warn(valElt);
	}
	// reset the scrollbar to the top.
	div.parentElement.scrollTop = 0;
	//cmsjs.Themes.Colors.applyCurrentTheme(div);
});

/*
 * PAGE: Raw View
 */
cmsjs.Component.create('content-raw', (component) => {
	initPageComponent(component);
	const div = component.element;
	const jsonText = JSON.stringify(cmsjs.State.get('ENV.JSON'),null,4);
	const escapedText = HTML.escapeString(jsonText);
	div.innerHTML = escapedText;
	div.style.whiteSpace = 'pre';
});

/*
 * PAGE: Editor View
 */
/*
cmsjs.Component.create('editor-raw', (component) => {
	div.style.height = '100%';
	const textarea = $.ce('textarea');
	textarea.id = 'editor';
	textarea.style.width = '100%';
	textarea.style.height = '100%';
	textarea.style.resize = 'none';
	textarea.style.whiteSpace = 'nowrap';
	textarea.addEventListener('change', (event) => {
		cmsjs.State.set('ENV.EditorChanged',true);
    });          
	div.append(textarea);
	textarea.value = JSON.stringify(cmsjs.State.get('ENV.JSON'),null,4);
});
*/

/*
function getContentHeading(text) {
	const heading = $.ce('div');
	heading.style.fontSize = '2rem';
	heading.style.fontWeight = 'bold';
	heading.style.marginTop = '1rem';
	heading.style.marginBottom = '1rem';
	heading.style.display = 'flex';
	heading.style.justifyContent = 'space-between';
	heading.classList.add('heading');
	cmsjs.Themes.Colors.bindElement(heading,'color','heading-color');
	const title = $.ce('span');
	title.innerHTML = text;
	heading.append(title);
	return heading;
}
*/

function getCloseButton() {
	const close = $.ce('div');
	close.append(cmsjs.HTML.getGoogleIcon('close'));
	close.style.cursor = 'pointer';
	close.style.padding = '0.25rem 0.5rem';
	close.style.border = 'solid 1px';
	close.style.position = 'sticky';
	close.style.top = '0.5rem';
	close.style.float = 'right';
	cmsjs.Themes.Colors.bindElement(close,'background-color','background-color');
	close.style.borderRadius = '5px';
	close.addEventListener('mouseenter', (event) => {
		close.style.backgroundColor = cmsjs.Themes.getThemeValue('heading-color');
	});
	close.addEventListener('mouseleave', (event) => {
		cmsjs.Themes.rethemeElement(close);
	});
	return close;
}

/*
 * PAGE: Documentation
 */
cmsjs.Component.create('content-docs', (component) => {
	initPageComponent(component);
	const div = component.element;
	const close = getCloseButton();
	close.addEventListener('click', (event) => {
		if ( cmsjs.State.get('Config.MarkupMode') ) {
			showPage('content-markup');
		}
		else {
			showPage('content-raw');
		}
	});
	div.append(close);
	cmsjs.Themes.Fonts.setGoogleFont(div,cmsjs.State.get('Config.DisplayFont'),'serif');
	div.style.fontSize = cmsjs.Themes.Fonts.getFontSize(cmsjs.State.get('Config.DisplayFontSize'));
	const fragment = document.createDocumentFragment();
	
	function getDataTable(data) {
		const table = $.ce('table');
		table.style.border = 'solid 1px';
		//const currentBorderColor = window.getComputedStyle(cmsjs.Console.getPanel('header')).color.replace('rgb','rbga').replace(')',', 0.1)');
		const currentBorderColor = '#777';
		console.warn(currentBorderColor);
		table.style.borderColor = currentBorderColor;
		table.style.marginBottom = '1rem';
		table.style.borderCollapse = 'collapse';
		const tbody = $.ce('tbody');
		table.append(tbody);
		data.forEach((row) => {
			log.debug(row);
			const tr = $.ce('tr');
			tbody.append(tr);
			row.forEach((item) => {
				const td = $.ce('td');
				tr.append(td);
				td.style.border = 'solid 1px';
				td.style.borderColor = currentBorderColor;
				td.style.borderCollapse = 'collapse';
				td.style.padding = '0.75rem';
				if ( item ) {
					const content = $.ce('div');
					td.append(content);
					content.innerHTML = item;
				}
			});
		});
		return table;
	}

	const cpHeader = $.ce('h1');
	cpHeader.innerHTML = 'Control Panel';
	cmsjs.Themes.Colors.bindElement(cpHeader,'color','heading-color');
	fragment.append(cpHeader);
	const dictionary = cmsjs.Dictionary.get('Controls');
	const cpData = [];
	dictionary.keys().forEach((entry) => {
		if ( entry.startsWith('Docs.') ) {
			const arr = [];
			const labelKey = entry.replace('Docs.','Label.');
			const labelText = dictionary.get(labelKey);
			arr.push(labelText);
			arr.push(dictionary.get(entry));
			cpData.push(arr);
		}
	});
	const cpTable = getDataTable(cpData);
	cpTable.querySelectorAll('tr td:nth-child(1) div').forEach((div) => {
		cmsjs.Themes.Colors.bindElement(div,'color','text-2-color');
		div.style.whiteSpace = 'pre';
		div.style.fontWeight = 'bold';
	});
	fragment.append(cpTable);

	const keyHeader = $.ce('h1');
	keyHeader.innerHTML = 'Keybindings';
	cmsjs.Themes.Colors.bindElement(keyHeader,'color','heading-color');
	fragment.append(keyHeader);

	const keyDict = cmsjs.Dictionary.get('Keybindings');
	const keyDataArray = [];
	keyDict.keys().forEach((action) => {
		const keyBinding = cmsjs.Keys.getBinding(action);
		const docs = keyDict.get(action);
		keyDataArray.push([action,keyBinding,docs]);
	});
	const keyTable = getDataTable(keyDataArray);
	keyTable.querySelectorAll('tr td:nth-child(1) div').forEach((div) => {
		cmsjs.Themes.Colors.bindElement(div,'color','text-2-color');
	});
	keyTable.querySelectorAll('tr td:nth-child(2) div').forEach((div) => {
		cmsjs.Themes.Colors.bindElement(div,'color','text-2-color');
	});
	fragment.append(keyTable);

	
	// Append the fragment at the end.
	div.append(fragment);
});

/*
 * PAGE: Advanced Options
 */
/*
pbf.set('advanced', (div) => {
	populateThemeDiv(div);
});
*/

/*
 * PAGE: Standalone
 */
cmsjs.Component.create('content-demo', (component) => {
	initPageComponent(component);
	const div = component.element;
	div.style.fontSize = cmsjs.Themes.Fonts.getFontSize(cmsjs.State.get('Config.DisplayFontSize'));
	const params = new URLSearchParams(window.location.search);
	const files = [
		{ 'tests/json1-psrd-20k.json' : '20kB Pathfinder System Reference Document (PFSRD)' },
		{ 'tests/json2-psrd-1MB.json' : '1MB Pathfinder System Reference Document (PFSRD)' },
		{ 'tests/json3-wotr-2MB.json' : '2MB Pathfinder WotR save file data' },
		{ 'tests/json4-pfkm-11MB-deep-nesting.json' : '11MB Pathfinder Kingmaker save file data (deep nesting)' },
		{ 'tests/json5-pfkm-8MB-35000-line-array.json' : '8MB Pathfinder Kingmaker language file (array with 35,000 items) [NOTE: may take several seconds to open with expand depth set higher than 1]' }
	]
	if ( params.get('f') ) {
		const index = params.get('f');
		const href = Object.keys(files[index])[0];
		fetch(href).then((r) => r.json()).then((json) => {
			cmsjs.State.set('ENV.JSON',json);
			if ( cmsjs.State.get('Config.MarkupMode') ) {
				cmsjs.State.set('ENV.CurrentPath',window.location.hash.substring(1) || '$');
				showPage('content-markup');
			}
			else {
				showPage('content-raw');
			}
		})
		.catch(error => {
			console.error('Fetch error:', error);
		});
	}
	else {
		const fragment = document.createDocumentFragment();
		fragment.append($.ce('h2','JSON Navigator Demo'));
		fragment.append($.ce('p','This is a demonstration of the <strong>JSON Navigator</strong> Chrome extension.'));
		fragment.append($.ce('p','When the extension is installed, all JSON files that you visit using Chrome will be loaded into this console.'));
		fragment.append($.ce('p','Some example JSON files are provided below. Select a file to load into the console. Reloading the page will return you to this screen:'));
		const ul = $.ce('ul');
		ul.style.marginTop = '1rem';
		fragment.append(ul);
		files.forEach((file,index) => {
			const a = $.ce('a');
			const href = Object.keys(file)[0];
			a.href = '?f=' + index;
			a.innerHTML = file[href];
			const li = $.ce('li');
			li.style.marginBottom = '1rem';
			li.append(a);
			ul.append(li);
		});
		const upload = $.ce('input');

		fragment.append($.ce('p','This is a demonstration of the <strong>JSON Navigator</strong> Chrome extension.'));
		div.append(fragment);
	}
});


function getFileUpload() {
	//ui.appendContent(self,$.ce('style',null,null,'#drop-area.hover { outline: dashed 2px #707 !important; background-color: #fef !important; }'));
	const dropArea = $.ce('div');
	dropArea.addEventListener('hover', (event) => {
		event.target.style.outline = 'dashed 2px #707 !important';
		event.target.style.backgroundColor = 'dashed 2px #fef !important';
	});
	dropArea.style.width = '80%';
	dropArea.style.textAlign = 'center';
	dropArea.style.margin = '1rem auto'
	dropArea.style.padding = '1rem 3rem'
	dropArea.style.borderRadius = '1rem'
	dropArea.style.outline = 'dashed 2px #060';
	dropArea.style.backgroundColor = '#dfd';
	dropArea.addEventListener('dragenter', da_hover, false);
	dropArea.addEventListener('dragleave', da_unhover, false);
	dropArea.addEventListener('dragover', da_hover, false);
	dropArea.addEventListener('drop', da_unhover, false);
	['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
		dropArea.addEventListener(eventName, da_preventDefaults, false);
	})
	dropArea.addEventListener('drop', da_handleDrop, false)
	function da_preventDefaults (e) { e.preventDefault(); e.stopPropagation(); }
	function da_hover (e) { dropArea.classList.add('hover') }
	function da_unhover(e) { dropArea.classList.remove('hover') }
	function da_handleDrop(e) { loadZKS(((e.dataTransfer).files)[0]); }
	dropArea.append($.ce('style',null,null,'p { margin: 2rem auto; max-width: 30rem; } p.note { margin-top: 2rem; font-style: italic; font-size: 0.9rem; } p.output { display: hidden; }'));
	dropArea.append($.ce('h2',null,null,'Kingmaker Save File Analyzer'));
	dropArea.append($.ce('p',null,null,'Welcome to the Kingmaker Save File Analyzer :)'));
	dropArea.append($.ce('p',null,null,'Uploading a .zks save file will allow you to get a full history of your game path and see what you have uncovered and what you might have missed.'));
	dropArea.append($.ce('p',null,null,'<strong>Drag and drop a .zks save file into this area.</strong>'));
	const fileSelector = $.ce('div');
	fileSelector.id = 'file-selector';
	const fileSelectorInput = $.ce('input');
	fileSelectorInput.type = 'file';
	fileSelectorInput.id = 'file-selector-input';
	fileSelectorInput.style.display = 'none';
	fileSelectorInput.addEventListener('change', function() {
		loadZKS(this.files[0]);
	});
	const fileSelectorButton = $.ce('button');
	fileSelectorButton.id = 'file-selector-button';
	fileSelectorButton.textContent = 'Or click here to select a .zks file';
	fileSelectorButton.style.fontSize = '1.1rem';
	fileSelectorButton.style.padding = '0.5rem';
	fileSelectorButton.style.borderRadius = '0.5rem';
	fileSelectorButton.addEventListener('click', function() {
		document.getElementById('file-selector-input').click();
	});
	fileSelector.appendChild(fileSelectorInput);
	fileSelector.appendChild(fileSelectorButton);
	dropArea.appendChild(fileSelector);
	const feedback = $.ce('p','droparea-feedback',null,'');
	dropArea.append(feedback);
	feedback.style.border = 'dotted 1px black';
	feedback.style.borderRadius = '0.3rem';
	feedback.style.backgroundColor = '#eff';
	feedback.style.padding = '1rem';
	if ( zks ) {
		feedback.innerHTML = 'You currently have a .zks file loaded.<br>Uploading a new file will replace the current one.';
	}
	else {
		feedback.innerHTML = 'You do not currently have a .zks file loaded.';
	}
	dropArea.append($.ce('p',null,'note','<strong>Note:</strong> Using "cleaner" mods may lead to incorrect results, as they sometimes erase the "history" file which is required for the analyzer to work properly.'));
	return dropArea;
}


/*
 * This performs a full-text search for keys or values matching str.
 */
function searchJsonWithPath(json, str) {
	const results = [];
	function search(obj, path = '') {
		if ( Array.isArray(obj) ) {
			obj.forEach((item, index) => {
				search(item, `${path}[${index}]`);
			});
		}
		else if ( typeof obj === 'object' && obj !== null ) {
			Object.keys(obj).forEach(key => {
				const newPath = path ? `${path}.${key}` : key;
				if ( typeof obj[key] === 'string' && obj[key].includes(str) ) {
					results.push(newPath);
				}
				else if ( key.includes(str) ) {
					results.push(newPath);
				}
				else {
					search(obj[key], newPath);
				}
			});
		}
	}
	search(json);
	if ( results.length === 0 ) { return; }
	const rootElt = $.id('root');
	if ( ! rootElt.classList.contains('expanded') ) {
		rootElt.querySelector('div.label > span.value').click();
	}
	for ( let i = 0; i < results.length; i++ ) {
		let currentDiv = rootElt;
		const jsonPath = results[i];
		//log.debug('processing search match: ', jsonPath);
		const components = jsonPath.split('.');
		components.forEach((item) => {
			//console.warn(item);
			if ( item.endsWith(']') ) {
				let [name,index] = item.split('[');
				index = index.slice(0,-1);
				const match = currentDiv.querySelector('div.contents > div.item[key="' + name + '"]');
				if ( match ) {
					if ( ! match.classList.contains('expanded') ) {
						match.querySelector('div.label > span.value').click();
					}
					const nthDiv = match.querySelector('div.contents > div.item:nth-child(' + (parseInt(index)+1) + ')')
					if ( ! nthDiv.classList.contains('expanded') ) {
						nthDiv.querySelector('div.label > span.value').click();
					}
					currentDiv = nthDiv;
				}
			}
			else {
				let name = item;
				const match = currentDiv.querySelector('div.contents > div.item[key="' + name + '"]');
				if ( match ) {
					if ( ! match.classList.contains('expandable') ) {
						match.classList.add('unfiltered');
					}
					else if ( ! match.classList.contains('expanded') ) {
						match.querySelector('div.label > span.value').click();
					}
					currentDiv = match;
				}
				else {
					//log.error('no match for component "' + item + '" in matching path: ',results[i]);
				}
			}
		});
	}
	return results;
}
	


/*
 * This takes a JSON Path syntax for a key and returns the corresponding value.
 */
function getValueForJsonPath(jsonPath) {
	if ( jsonPath === '$' ) { return cmsjs.State.get('ENV.JSON'); }
	const parts = jsonPath.split('.').slice(1);
	let value = cmsjs.State.get('ENV.JSON');
	for ( const part of parts ) {
		if ( part.endsWith(']') ) {
			let [keyname,index] = part.split('[');
			index = parseInt(index.slice(0,-1));
			value = value[keyname][index];
		} else {
			value = value[part];
		}
		
	}
	//log.debug('getValueForJsonPath(' + jsonPath + '): value is ',value);
	return value;
}

/*
 * Gets a clickable path for quick navigation.
 */
function getClickablePath(jsonPath) {
	const container = $.ce('div');
	//cmsjs.Themes.Colors.bindElement(container,'color','heading-color');
	container.style.display = 'flex';
	container.style.fontFamily = 'sans-serif';
	container.style.alignItems = 'center';
	container.style.verticalAlign = 'middle';
	const div = $.ce('div');
	div.style.border = 'solid 1px';
	div.style.width = '100%';
	div.style.padding = '0.25rem 0.75rem';
	const parts = jsonPath.split('.');
	for (let i = 0; i < parts.length; i++) {
		if ( parts[i].endsWith(']') ) {
			let [key,index] = parts[i].split('[');
			index = index.slice(0,-1);
			const keyPath = parts.slice(0, i).join('.') + '.' + key;
			const link = $.ce('a');
			link.style.paddingLeft = '0.4em';
			link.href = '#' + keyPath;
			link.textContent = key;
			div.append(link);
			const link2 = $.ce('a');
			const indexPath = parts.slice(0, i+1).join('.');
			link2.href = '#' + indexPath;
			link2.textContent = ' [ ' + index + ' ] ';
			div.append(link2);
			if ( i < parts.length - 1 ) {
				div.append($.ctn('.'));
			}
		}
		else {
			const path = parts.slice(0, i+1).join('.');
			if ( i < parts.length ) {
				const link = $.ce('a');
				link.textContent = parts[i];
				link.style.paddingLeft = '0.4em';
				link.style.paddingRight = '0.4em';
				link.href = '#' + path;
				div.append(link);
			}
			else {
				div.append($.ctn(parts[i]));
			}
			if ( i < parts.length - 1 ) {
				div.append($.ctn('.'));
			}
		}
	}
	container.append(div);
	return container;
}

/*
 * Takes a JSON Path and returns the corresponding div.item element.
 */
function getItemForJsonPath(jsonPath) {
	const rootElement = $.id('root');
	if ( jsonPath === '$' ) { return rootElement; }
	const parts = jsonPath.split('.').slice(1);
	//parts.shift();
	let element = rootElement;
	for ( const part of parts ) {
		if ( part.endsWith(']') ) {
			let [keyname,index] = part.split('[');
			index = parseInt(index.slice(0,-1)) + 1;
			element = element.querySelector('div.contents > div.item[key="' + keyname + '"] > div.contents > :nth-child(' + index + ')');
		}
		else {
			element = element.querySelector('div.contents > div.item[key="' + part + '"]');
		}
		if ( ! element ) {
			log.error('could not find element for JSON Path: ', jsonPath);
		}
	}
	log.debug('getItemForJsonPath(' + jsonPath + '): returning element: ',element);
	return element;
}

/*
 * Takes a div.item element and returns the corresponding JSON Path.
 */
function getJsonPathForItem(element) {
	let jsonPath = '';
	while ( element.id !== 'root' ) {
		const parent = element.parentElement.parentElement;
		const keyAttribute = element.getAttribute('key');
		const parentAttribute = parent.getAttribute('key');
		if ( parent.classList.contains('array') ) {
			if ( jsonPath !== '' ) {
				jsonPath = `[${keyAttribute}].${jsonPath}`;
			}
			else jsonPath = `[${keyAttribute}]`;
		}
		else {
			if ( jsonPath.startsWith('[') ) {
				jsonPath = `${keyAttribute}${jsonPath}`;
			}
			else if ( jsonPath !== '' ) {
				jsonPath = `${keyAttribute}.${jsonPath}`;
			}
			else {
				jsonPath = `${keyAttribute}`;
			}
		}
		element = parent;
	}
	if ( jsonPath !== '' ) {
		jsonPath = '$.' + jsonPath;
	}
	else {
		jsonPath = '$';
	}
	//log.debug('getJsonPathForItem(): ' + jsonPath);
	return jsonPath;
}


/*
 * Takes a div.item element and returns the corresponding JSON data.
 */
function getJsonForItem(element) {
	const jsonPath = getJsonPathForItem(element);
	const json = getValueForJsonPath(jsonPath);
	return json;
}

/*
 * Expandable items are those that contain (or map to) a non-empty
 * array or object. This function takes an existing array/object
 * item (div.item.expandable) and the json array/object
 * that it maps to, and builds out the next level of items (div.item)
 *
 * This process is only done once. Once the element has been drawn
 * we simply turn it on and off with display:block/none.
 */
function expandItem(item,json) {
	if ( ! item.querySelector('div.contents') ) {
		const newItem = $.ce('div');
		const newDepth = parseInt(item.getAttribute('depth')) + 1;
		newItem.setAttribute('depth',newDepth);
		newItem.classList.add('contents');
		newItem.style.marginLeft = cmsjs.State.get('Config.IndentDepth')/2 + 'rem';
		newItem.style.paddingLeft = '0.25rem';
		newItem.style.borderLeft = 'dashed 1px';
		cmsjs.Themes.Colors.bindElement(newItem,'border-left-color','guideline-' + newDepth + '-color');
		if ( cmsjs.State.get('Config.ShowGuidelines') ) {
			newItem.style.borderLeftColor = getBorderColor(newDepth);
		}
		else {
			newItem.style.borderLeftColor = 'rgba(0,0,0,0.0)';
		}
		const fragment = document.createDocumentFragment();
		if ( typeof json === 'object' )  { // this catches arrays too
			const _k = Object.keys(json);
			const keys = (Array.isArray(json)) ? _k : (cmsjs.State.get('Config.SortKeys')) ? _k.sort() : _k;
			keys.forEach((key,index) => {
				const value = json[key];
				const subItem = getItemElement(key,value,item.hasAttribute('bytes'));
				subItem.setAttribute('depth',newDepth);
				fragment.append(subItem);
			});
		}
		newItem.append(fragment);
		item.append(newItem)
	}
	checkItemNull(item);
	item.classList.add('expanded');
	item.classList.add('expanded-once');
	item.querySelector('div.contents').style.display = 'block';
	if ( $.id('root').classList.contains('filtered') ) {
		while ( item.parentElement && item.parentElement.id !== 'root' ) {
			item.parentElement.classList.add('unfiltered');
			item = item.parentElement;
		}
		//console.error('test--->');
		item.querySelectorAll('div.contents > div.item').forEach((x) => {
			x.classList.add('unfiltered');
		});
	}
	cmsjs.Themes.Colors.applyCurrentTheme(item);
}

/*
 * As you reveal the DOM, items will be discovered to be null because 
 * all of their children are null. This function wraps the calls
 * necessary to set something null on the fly.
 */
function checkItemNull(item) {
	if ( item.classList.contains('null') ) {
		if ( item.parentElement && item.parentElement.classList.contains('item') ) {
			checkItemNull(item.parentElement);
		}
	}
	else {
		let hasNonNull = false;
		item.querySelectorAll('div.contents > div.item').forEach((item) => {
			if ( ! item.classList.contains('null') ) {
				hasNonNull = true;
				return;
			}
		});
		if ( ! hasNonNull ) {
			item.classList.add('null');
			item.style.opacity = (cmsjs.State.get('Config.NullOpacity') + 1)/2;
			if ( ! cmsjs.State.get('Config.ShowNulls') ) {
				item.style.display = 'none';
			}
			if ( item.parentElement && item.parentElement.classList.contains('item') ) {
				checkItemNull(item.parentElement);
			}
		}
	}
}

/*
 * Collapse the specified item.
 * This simply hides the element.
 */
function collapseItem(item) {
	const contents = item.querySelector('div.contents');
	if ( contents ) {
		contents.style.display = 'none';
	}
	item.classList.remove('expanded');
	item.classList.add('collapsed');
}

/*
 * Expand all elements from the specified up to Config.ExpandDepth.
 *
 * "endval" is an ugly hack to allow search/filter to use this without being
 * limited by depth.
 */
function expandAll(element,endval) {
	let currentDepth = 0;
	if ( ! element ) {
		//element = document.getElementById('console-content-page-markup');
		element = cmsjs.Console.getComponent('content-markup');
	}
	else if ( ! 'getAttribute' in element ) {
		log.error('expandAll(): called with something other than an element',element);
	}
	else {
		currentDepth = element.hasAttribute('depth') ? element.getAttribute('depth') : null;
		if ( ! currentDepth ) {
			log.error('expandAll(): called with element that has no depth attribute',element);
			log.error('expandAll(): setting currentDepth to zero and hoping for the best...');
			currentDepth = 0;
		}
	}
	const startVal = parseInt(currentDepth);
	log.debug("expandAll is still calling parseInt...");
	const endVal = parseInt(currentDepth) + parseInt(cmsjs.State.get('Config.ExpandDepth'));
	const startTime = log.timer();
	for ( let i = startVal; i < endVal; i++ ) {
		const t0 = log.timer();
		const matches = element.querySelectorAll('div.expandable[depth="' + i + '"]:not(.expanded) > div.label > span.value');
		if ( matches == 0 ) { break; }
		matches.forEach((val) => {
			val.dispatchEvent(new MouseEvent('click'));
		});
		const t1 = log.timer();
		const timeTaken = t1 - t0;
		if ( ! cmsjs.State.get('ENV.PageLoaded') ) {
			if ( timeTaken > 50 ) {
				log.debug('expandAll(): initial page load processing taking too long: ' + timeTaken + 'ms');
				log.debug('expandAll(): printing what we have and bailing');
				break;
			}
		}
		else if ( timeTaken > 250 ) {
			log.debug('expandAll(): processing taking too long: ' + timeTaken + 'ms');
			log.debug('printing what we have and bailing');
			break;
		}
	}
	const endTime = log.timer();
	log.debug('expandAll(): expansion took ' + (endTime - startTime) + 'ms');
}

/*
 * Collapse all children of specified item.
 */
function collapseAll(item) {
	item.querySelectorAll('div.expanded').forEach((c) => {
		collapseItem(c);
	});
}


/*
 * Process a key/value pair and return a DOM element.
 * div.item
 * - div.label
 *   - div.key
 *   - div.val
 *   - div.bytes
 *   - div.controls
 *     - div.expand
 *     - div.collapse
 *     - div.copy
 * - div.contents
 */
function getItemElement(key,val,computeBytes=false) {
	const item = $.ce('div');
	item.classList.add('item');
	//item.style.border = 'solid 1px white';
	const verticalPadding = cmsjs.State.get('Config.Spacing')/30;
	item.style.paddingTop = verticalPadding + 'rem';
	item.style.paddingBottom = verticalPadding + 'rem';
	const label = $.ce('div');
	label.classList.add('label');
	label.style.padding = '0.1rem';
	label.style.display = 'flex';
	label.style.flexWrap = 'nowrap';
	label.style.alignItems = 'top';
	if ( key !== null ) { // the root element does not have a key!
		const keyElt = $.ce('span');
		keyElt.classList.add('key');
		if ( cmsjs.State.get('Config.BoldKeys') ) {
			keyElt.style.fontWeight = 'bold';
		}
		keyElt.style.marginRight = '1rem';
		keyElt.style.alignSelf = 'flex-start';
		item.setAttribute('key',key);
		cmsjs.Themes.Colors.bindElement(keyElt,'color','key-color');
		keyElt.innerHTML = key + ':';
		keyElt.setAttribute('tabindex','-1');
		label.append(keyElt);
	}

	/*********************
	 *      EDITOR       *
	 *********************/
	const editorElt = $.ce('span');
	editorElt.classList.add('editor');
	editorElt.style.display = 'none';
	label.append(editorElt);

	const valElt = $.ce('span');
	label.append(valElt);
	valElt.classList.add('value');
	if ( val == null ) {
		item.classList.add('null');
		item.setAttribute('datatype','null');
		valElt.classList.add('null');
		valElt.setAttribute('datatype','null');
		cmsjs.Themes.Colors.bindElement(valElt,'color','null-color');
		item.style.opacity = parseFloat(cmsjs.State.get('Config.NullOpacity'));
		if ( ! cmsjs.State.get('Config.ShowNulls') ) { item.style.display = 'none'; }
		valElt.innerHTML = 'null';
	}
	else if ( typeof val === 'string' ) {
		valElt.classList.add('string');
		item.setAttribute('datatype','string');
		valElt.setAttribute('datatype','string');
		cmsjs.Themes.Colors.bindElement(valElt,'color','string-color');
		if ( key !== null ) {
			valElt.style.maxWidth = '100%';
			if ( ! cmsjs.State.get('Config.LineWrap') ) {
				valElt.style.whiteSpace = 'nowrap';
			}
			valElt.style.overflow = 'hidden';
		}
		if ( val === '' ) {
			if ( ! cmsjs.State.get('Config.ShowNulls') ) { item.style.display = 'none'; }
			item.classList.add('null');
		}
		if ( ! cmsjs.State.get('Config.MarkupHTML') ) {
			val = HTML.escapeString(val);
		}
		valElt.innerHTML = '"' + val + '"';
		if ( cmsjs.State.get('Config.ConvertLinks') ) {
			convertToLinkIfPossible(valElt);
		}
	}
	else if ( typeof val === 'number' ) {
		valElt.classList.add('number');
		valElt.setAttribute('datatype','number');
		item.setAttribute('datatype','number');
		cmsjs.Themes.Colors.bindElement(valElt,'color','number-color');
		valElt.innerHTML = val;
	}
	else if ( typeof val === 'boolean' ) {
		valElt.classList.add('boolean');
		valElt.setAttribute('datatype','boolean');
		item.setAttribute('datatype','boolean');
		cmsjs.Themes.Colors.bindElement(valElt,'color','boolean-color');
		valElt.innerHTML = val;
	}
	else if ( typeof val === 'object' ) { // catches arrays
		valElt.classList.add('object');
		cmsjs.Themes.Colors.bindElement(valElt,'color','object-color');
		valElt.style.cursor = 'pointer';
		valElt.style.borderRadius = '5px';
		valElt.style.outline = 'solid 1px transparent';
		valElt.addEventListener('mouseenter', () => {
			cmsjs.Themes.Colors.bindElement(valElt,'outline-color','button-outline-color');
		});
		valElt.addEventListener('mouseleave', () => {
			valElt.style.outline = '';
		});
		const isArray = Array.isArray(val);
		const isObject = !isArray;
		if ( isArray ) {
			item.classList.add('array');
			valElt.setAttribute('datatype','array');
			item.setAttribute('datatype','array');
		}
		else {
			item.classList.add('object');
			valElt.setAttribute('datatype','object');
			item.setAttribute('datatype','object');
		}
		const length = (isArray) ? val.length : Object.keys(val).length;
		if ( length === 0 ) {
			item.classList.add('null');
			item.style.opacity = parseFloat(cmsjs.State.get('Config.NullOpacity'));
			if ( isArray ) { valElt.innerHTML = '[ ]'; }
			else { valElt.innerHTML = '{ }'; }
			if ( ! cmsjs.State.get('Config.ShowNulls') ) { item.style.display = 'none'; }
		}
		else {
			// THIS IS WHERE IT IS A NON-EMPTY ARRAY OR OBJECT. THE VALUE ELEMENT MUST BECOME CLICKABLE
			const tabindex = (key) ? '2' : '1';
			valElt.setAttribute('tabindex',tabindex);
			valElt.addEventListener('click', () => {
				item.classList.toggle('expanded');
				//valElt.focus();
				if ( item.classList.contains('expanded') ) {
					expandItem(item,val);
				}
				else {
					collapseItem(item);
				}
			});
			// Make "Enter" expand/collapse items
			valElt.addEventListener('keydown', (event) => {
				if ( event.keyCode === 13 ) {
					valElt.click();
				}
				else if ( event.ctrlKey && event.key == 'c' ) {
					navigator.clipboard.writeText(JSON.stringify(val,null,4));
				}
				// open (expand)
				else if ( event.ctrlKey && event.key == 'o' ) {
					event.preventDefault();
					expandAll(valElt.parentElement.parentElement);
					valElt.focus();
				}
				// invert (collapse)
				else if ( event.ctrlKey && event.key == 'i' ) {
					event.preventDefault();
					collapseAll(valElt.parentElement.parentElement);
					valElt.focus();
				}
			});
			item.classList.add('expandable');
			valElt.append(document.createTextNode((isArray) ? '[' : '{'));
			const count = $.ce('span');
			count.classList.add('count');
			count.setAttribute('count',length);
			if ( cmsjs.State.get('Config.ShowCount') ) { count.innerHTML = ' ' + length + ' '; }
			else { count.innerHTML = '...'; }
			valElt.append(count);
			valElt.append(document.createTextNode((isArray) ? ']' : '}'));

			// PRINT THE BYTES IF REQUESTED
			if ( computeBytes ) {
				const bytes = JSON.stringify(val).length;
				const ratio = Math.round((bytes/cmsjs.State.get('ENV.Bytes'))*100);
				if ( key === null || (bytes >= 5000 && ratio >= 1) ) {
					item.setAttribute('bytes',bytes);
					item.setAttribute('ratio',ratio);
					const bytesElt = $.ce('span');
					bytesElt.classList.add('bytes');
					const kb = Math.round(bytes/1000);
					bytesElt.innerHTML = '' + kb + 'k';
					bytesElt.style.margin = '0px 1rem';
					//bytesElt.style.fontSize = '0.9rem';
					bytesElt.style.fontWeight = 'bold';
					cmsjs.Themes.Colors.bindElement(bytesElt,'color','bytes-color');
					if ( ! cmsjs.State.get('Config.ShowBytes') ) { bytesElt.style.display = 'none'; }
					label.append(bytesElt);
				}
			}
		}
	}
	else {
		log.error('unknown type (this should never happen)',typeof val);
	}
	/************************************
	 *									*
	 *		POPUP CONTROL PANEL			*
	 *									*
	 ************************************/
	const controls = $.ce('span');
	controls.classList.add('controls');
	controls.style.marginLeft = '1rem';
	controls.style.display = 'flex';
	controls.style.flexWrap = 'nowrap';
	controls.style.alignSelf = 'flex-start';
	cmsjs.Themes.Colors.bindElement(controls,'color','controls-color');
	controls.style.fontWeight = 'bold';
	label.append(controls);
	function showControls() {
		if ( val !== null && typeof val === 'object' ) {
			if ( controls.innerHTML !== '' ) { return; }
			// COLLAPSE ALL CHILDREN
			const collapseElt = getJSONControl('COLLAPSE','arrow_upward', () => {
				collapseAll(item);
			});
			controls.append(collapseElt);
			// EXPAND ALL CHILDREN
			const expandElt = getJSONControl('EXPAND','arrow_downward', () => {
				expandAll(item);
			});
			controls.append(expandElt);
		}
		if ( val !== null && val !== true && val !== false ) {
			// COPY DATA
			const copyElt = getJSONControl('COPY','content_copy', () => {
				let copiedItem = val;
				if ( ! cmsjs.State.get('Config.ShowNulls') ) {
					copiedItem = removeNullValues(val);
				}
				navigator.clipboard.writeText(JSON.stringify(copiedItem,null,4));
				log.debug('{COPY} copied json: ',copiedItem);
			});
			controls.append(copyElt);
		}
		if ( key !== null ) {
			// BURROW
			let jsonPath = getJsonPathForItem(item);
			if ( cmsjs.State.get('ENV.CurrentPath') !== '$' ) {
				const wrongPath = jsonPath.slice(2);
				if ( wrongPath.startsWith('[') ) {
					jsonPath = cmsjs.State.get('ENV.CurrentPath') + jsonPath.slice(2);
				}
				else {
					jsonPath = cmsjs.State.get('ENV.CurrentPath') + '.' + jsonPath.slice(2);
				}
			}
			const burrow = getJSONControl('BURROW','arrow_forward', '#' + jsonPath);
			controls.append(burrow);
		}
		if ( key !== null ) {
			// EDIT
			// Javascript's "typeof" operator is almost useless on its own :/
			function getDataType(item) {
				if ( item === null ) { return 'null'; }
				if ( typeof item !== 'object' ) { return typeof item; }
				if ( Array.isArray(item) ) { return 'array'; }
				return 'object';
			}
			const edit = getJSONControl('EDIT','edit_note', () => {
				// get the json path for this item (will be a key)
				// get the value associated with this json path
				// get the value type (object, array, number, boolean, string, null)
				let jsonPath = getJsonPathForItem(item);
				let jsonValue = getValueForJsonPath(jsonPath);
				const valEltDataType = valElt.getAttribute('datatype');
				const editorSelect = $.ce('select');
				const editorOptions = ['null','true','false','number','string','array','object'];
				editorOptions.forEach((o) => {
					const option = $.ce('option');
					option.value = o;
					option.textContent = o;
					const type = getDataType(jsonValue);
					if ( jsonValue === true && o === 'true' ) {  option.selected = true; }
					else if ( jsonValue === false && o === 'false' ) {  option.selected = true; }
					else if ( getDataType(jsonValue) === o ) { option.selected = true; }
					editorSelect.appendChild(option);
				});
				const textEntry = $.ce('input');
				editorElt.appendChild(editorSelect);
				editorElt.appendChild(textEntry);
				editorElt.style.display = 'inline';
				valElt.style.display = 'none';
				const tmpValElt = valElt;
			});
			//controls.append(edit);
		}
	}
	function hideControls() {
		const delay = (controls.hasAttribute('control-clicked')) ? 1200 : 100;
		controls.removeAttribute('control-clicked');
		setTimeout(() => { controls.innerHTML = ''; },delay);
	}
	label.addEventListener('mouseenter', () => {
		showControls();
	});
	label.addEventListener('mouseleave', () => {
		hideControls();
	});
	item.append(label);
	return item;
}



/*
 * Removed null values from a json object.
 * Returns a copy. The original is unmolested.
 */
function removeNullValues(obj) {
    if ( typeof obj !== 'object' || obj === null ) {
        return obj;
    }
    else if ( Array.isArray(obj) ) {
		const newArray = obj.map(item => removeNullValues(item)).filter(item => item !== null);
		return newArray.length === 0 ? null : newArray;
    }
	else {
	    const newObject = Object.keys(obj).reduce((acc, key) => {
		    const value = removeNullValues(obj[key]);
			if ( value !== null ) { acc[key] = value; }
		    return acc;
	    }, {});
		if ( Object.keys(newObject).length === 0 ) {
			return null;
		}
		return newObject;
	}
}


/*
 * Takes a value element and converts it to a link if it starts with an acceptable prefix.
 *
 * These need to get tagged with a 'converted-url' class so MarkupHTML does not 
 * unlinkify them when it is turned off.
 */
function convertToLinkIfPossible(valElt) {
	if ( /^\"(http|https|pfsrd):\/\/.*$/.test(valElt.innerHTML) ) {
		const link = $.ce('a');
		link.href = valElt.innerHTML.replace(/^"|"$/g,'');
		link.textContent = valElt.innerHTML.slice(1,-1);;
		link.classList.add('converted-url');
		link.setAttribute('tabindex','-1');
		valElt.innerHTML = '';
		valElt.append(link);
		valElt.classList.add('converted-url');
	}
}


/*
 * This is the little popup control panel next to expandable items.
 *
 * 'action' is just used for logging
 */
function getJSONControl(action,icon,onClick) {
	const i = HTML.getGoogleIcon(icon);
	i.style.cursor = 'pointer';
	i.style.margin = '0px 0.2rem';
	i.style.padding = '0px 1rem';
	i.style.borderRadius = '0.25rem';
	const elt = $.ce('a');
	elt.appendChild(i);
	if ( typeof onClick === 'string' ) {
		elt.href = onClick;
	}
	else {
		elt.addEventListener('click', function(event) {
			event.preventDefault();
			log.debug('{' + action + '}');
			this.style.cursor = 'wait';
			cmsjs.Themes.Colors.bindElement(this,'color','null-color');
			this.parentElement.setAttribute('control-clicked','true');
			setTimeout(() => {
				onClick();
				const spanElt = this.querySelector('span');
				const spanEltText = spanElt.innerHTML;
				cmsjs.Themes.Colors.bindElement(this,'color','string-color');
				this.style.cursor = 'pointer';
				this.querySelector('span').innerHTML = 'done';
				setTimeout(() => {
					cmsjs.Themes.Colors.bindElement(this,'color','controls-color');
					this.querySelector('span').innerHTML = spanEltText;
				},3000);
			},50);
		});
		elt.classList.add('button');
	}
	elt.style.outline = 'solid 1px transparent';
	elt.addEventListener('mouseenter', function() {
		cmsjs.Themes.Colors.bindElement(elt,'outline-color','button-outline-color');
	});
	elt.addEventListener('mouseleave', function() {
		elt.style.outline = '';
	});
	return elt;
}


function getPopupTextElement(text,hoverText) {
	log.trace('getPopupTextElement(text,hoverText)',text,hoverTet);
	const textWrapper = $.ce('span');
	textWrapper.style.position = 'relative';
	const textElt = $.ce('span');
	textElt.classList.add('text');
	textElt.innerHTML = text;
	textElt.style.marginRight = '1rem';
	textElt.style.padding = '0.25rem';
	textWrapper.append(textElt);
	const popupNote = $.ce('span');
	popupNote.classList.add('popup-doc');
	popupNote.style.display = 'none';
	popupNote.style.position = 'absolute';
	popupNote.style.right = '100%'
	popupNote.style.width = '12rem'
	popupNote.style.top = '0'
	popupNote.style.color = '#eee';
	popupNote.style.backgroundColor = '#000';
	popupNote.style.opacity = '1.0';
	popupNote.style.border = 'solid 1px #ccc';
	popupNote.style.padding = '0.5rem 1rem';
	popupNote.style.zIndex = '20';
	popupNote.style.borderRadius = '0.5rem';
	popupNote.style.textAlign = 'left';
	popupNote.innerHTML = hoverText;
	textWrapper.append(popupNote);
	//textElt.addEventListener('mouseenter', function() {
	//	if ( cmsjs.State.get('Config.ShowTooltips') ) { popupNote.style.display = 'block'; }
	//});
	textElt.addEventListener('mouseleave', function() { popupNote.style.display = 'none'; } );
	return textWrapper;
}


function createControl(type,key,config={}) {
	log.trace('createControl(type,key)',type,key);
	config['id'] = key;
	config['value'] = cmsjs.State.get(key);
	config['tabindex'] = '3';
	config['label'] = cmsjs.Dictionary.get('Controls').get('Label.' + key.split('.')[1]);
	config['listener:change'] = ((value) => {cmsjs.State.changeAndSave(key,value);});
	config['label-position'] = 'left';
	if ( type === 'toggle' ) {
//		config['slider-on-color'] = STYLE.getThemeValue('console-toggle-slider-on-color');
//		config['slider-off-color'] = STYLE.getThemeValue('console-toggle-slider-off-color');
//		config['button-on-color'] = STYLE.getThemeValue('console-toggle-button-on-color');
//		config['button-off-color'] = STYLE.getThemeValue('console-toggle-button-off-color');
	}
	else if ( type === 'select' ) {
		config['label-position'] = 'top-right';
	}
	const control = cmsjs.Controls.create(key,type,config);
	control.element.style.margin = '0.75em 0em';
	return control;
}

/****************************************
 *										*
 *		CONTROL PANEL FUNCTIONS			*
 *										*
 ****************************************/

function changeState(id,value) {
	log.trace('changeState(id,value)',id,value);
	cmsjs.State.changeAndSave(id,value);
}

