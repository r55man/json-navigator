
// Dictionary definitions

log.trace('loading init-dictionary.js');

cmsjs.Dictionary.initializePreferredLanguagesFromBrowser();
//cmsjs.Dictionary.setPreferredLanguage('en');

// Wrap dictionary definitions in an IIFE so variables don't go global.
(function() {

	let dict = null;
	let d = null;
	
	/**
	 * Console Labels
	 */
	dict = cmsjs.Dictionary.create('Controls');
	dict.addLanguages(['en','de','es','fr','ru','zh']);
	dict.setRootLanguage('en');
	
	/**
	 * ENGLISH
	 */
	d = dict.getLanguageMap('en');
	// Misc
	d.set('Language','Language')
	d.set('Documentation','Documentation');
	d.set('Controls','Controls');
	d.set('Interface','Interface');
	d.set('Search','Search');
	d.set('Console','Console');
	// Labels
	d.set('Label.MarkupMode','Navigator On');
	d.set('Label.ShowNulls','Show Nulls');
	d.set('Label.BoldKeys','Bold Keys');
	d.set('Label.ShowBytes','Show Bytes');
	d.set('Label.ShowCount','Show Count');
	d.set('Label.SortKeys','Sort Keys');
	d.set('Label.ShowGuidelines','Guidelines');
	d.set('Label.AutoExpand','Auto Expand');
	d.set('Label.MarkupHTML','Markup HTML');
	d.set('Label.ConvertLinks','Convert Links');
	d.set('Label.IndentDepth','Indent Depth');
	d.set('Label.ExpandDepth','Expand Depth');
	d.set('Label.DarkMode','Dark Mode');
	d.set('Label.ShowURL','Show URL');
	d.set('Label.ShowSearch','Show Search');
	d.set('Label.ControlKeys','Control Keys');
	d.set('Label.ShowFooter','Show Footer');
	d.set('Label.Debug','Debugging');
	d.set('Label.Spacing','Line Spacing');
	d.set('Label.LineWrap','Wrap Lines');
	d.set('Label.SelectedLight','Light Theme');
	d.set('Label.SelectedDark','Dark Theme');
	d.set('Label.CodeFont','JSON Font');
	d.set('Label.CodeFontSize','Font Size');
	d.set('Label.ConsoleFont','Console Font');
	d.set('Label.ConsoleFontSize','Font Size');
	d.set('Label.DisplayFont','Display Font');
	d.set('Label.DisplayFontSize','Font Size');
	d.set('Label.Language','Language');
	// Docs
	d.set('Docs.MarkupMode','Turning this off will display the raw underlying JSON content.');
	d.set('Docs.ShowNulls','Turning this off will prevent the display of null values, empty objects, and empty arrays. Note that this will result in the object counts not representing the number of items actually shown on the screen.');
	d.set('Docs.BoldKeys','Turning this on will display JSON keys in bold font.');
	d.set('Docs.ShowBytes','Displays byte counts next to objects and arrays that contain more than a certain threhold of data. By default, byte counts are only given for items that contain more than 5kB of data and represent more than 1% of the total data in the file.');
	d.set('Docs.ShowCount','Displays item counts for non-empty objects and arrays. Turning this on and off is purely cosmetic; it has no effect on performance.');
	d.set('Docs.SortKeys','Sort object keys. <b>NOTE: Currently requires a manual page refresh to take effect!</b>');
	d.set('Docs.ShowGuidelines','Toggle depth guidlines.');
	d.set('Docs.AutoExpand','Automatically expand JSON file to "Expand" depth on initial page load. Note that this has a short-circult to ensure files open quickly. The JSON file is expanded breadth-first, and after each pass through at a new depth a timer is checked. If too much time has elapsed, expansion will stop and the browser will display what has been processed up to the point of short-ciruiting.');
	d.set('Docs.MarkupHTML','Markup HTML, i.e. render things like &lt;p&gt; and &lt;table&gt; as HTML items.');
	d.set('Docs.ConvertLinks','Converts any standalone strings starting with "http://" or "https://" to active hyperlinks.');
	d.set('Docs.IndentDepth','Sets the block indentation level. You can also use Ctrl-LeftArrow and Ctrl-RightArrow to increase or decrease indent.');
	d.set('Docs.ExpandDepth','Sets the depth of operation for the initial JSON expansion (if "Auto Expand" is turned on), as well as the depth of operation for the "Expand all children" controls.');
	d.set('Docs.DarkMode','Switches between the selected Dark Theme and the selected Light Theme.');
	d.set('Docs.ShowURL','Turning this off will prevent the URL from appearing at the top of the page.');
	d.set('Docs.ShowSearch','Turning this off will prevent the "Search" bar from appearing at the top of the page.');
	d.set('Docs.ControlKeys','Enables the use of custom keybindings, e.g. ctrl-g to enable/disable guidelines');
	d.set('Docs.ShowFooter','Turning this off will prevent display off the footer at the bottom of the page.');
	d.set('Docs.Debug','This will turn console debugging on and off.');
	d.set('Docs.Spacing','Sets the vertical spacing between lines.');
	d.set('Docs.LineWrap','Wrap text lines that would extend past the right side of the screen.');
	d.set('Docs.SelectedLight','This is the theme that will be used when "Dark Mode" is disabled.');
	d.set('Docs.SelectedDark','This is the theme that will be used when "Dark Mode" is enabled.');
	d.set('Docs.CodeFont','Sets the JSON code font. The "Font Size" setting just below sets the code font size');
	d.set('Docs.ConsoleFont','Sets the console font. The "Font Size" setting just below sets the console font size');
	d.set('Docs.DisplayFont','Sets the display font. The "Font Size" setting just below sets the display font size');
	
	/**
	 * FRENCH
	 */
	d = dict.getLanguageMap('fr');
	// Misc
	d.set('Language','Language')
	d.set('Language','Langue');
	d.set('Documentation','Documentation');
	d.set('Controls','Contr\u00F4les');
	d.set('Interface','Interface');
	d.set('Search','Recherche');
	d.set('Console','Console');
	// Label
	d.set('Label.MarkupMode','Nav. Activ\u00E9e');
	d.set('Label.ShowNulls','Aff. Nulls');
	d.set('Label.BoldKeys','Cl\u00E9s En Gras');
	d.set('Label.ShowBytes','Aff. Octets');
	d.set('Label.ShowCount','Aff. Comptage');
	d.set('Label.SortKeys','Trier Cl\u00E9s');
	d.set('Label.ShowGuidelines','Lignes Direct.');
	d.set('Label.AutoExpand','Auto \u00C9tendre');
	d.set('Label.MarkupHTML','Baliser HTML');
	d.set('Label.ConvertLinks','Convertir Liens');
	d.set('Label.IndentDepth','Prof. Indent.');
	d.set('Label.ExpandDepth','Prof. \u00c9tend.');
	d.set('Label.DarkMode','Mode Sombre');
	d.set('Label.ShowURL','Aff. URL');
	d.set('Label.ShowSearch','Aff. Recherche');
	d.set('Label.ControlKeys','Cl\u00e9s Contr\u00f4le');
	d.set('Label.ShowFooter','Aff. Pied Page');
	d.set('Label.Spacing','Espacement');
	d.set('Label.SelectedLight','Th\u00e8me Clair');
	d.set('Label.SelectedDark','Th\u00e8me Sombre');
	d.set('Label.CodeFont','Police JSON');
	d.set('Label.CodeFontSize','Taille Police');
	d.set('Label.ConsoleFont','Police Console');
	d.set('Label.ConsoleFontSize','Taille Police');
	d.set('Label.DisplayFont','Police Affichage');
	d.set('Label.DisplayFontSize','Taille Police');
	d.set('Label.Language','Langue');
	// Docs
	
	
	/**
	 * GERMAN
	 */
	d = dict.getLanguageMap('de');
	// Misc
	d.set('Language','Sprache');
	d.set('Documentation','Dokumentation');
	d.set('Controls','Steuerungen');
	d.set('Interface','Schnittstelle');
	d.set('Search','Suche');
	d.set('Console','Konsole');
	// Labels
	d.set('Label.MarkupMode','Navigator Ein');
	d.set('Label.ShowNulls','Nullen Anzeigen');
	d.set('Label.BoldKeys','Fette Schl\u00fcssel');
	d.set('Label.ShowBytes','Bytes Anzeigen');
	d.set('Label.ShowCount','Z\u00e4hlung Anzeigen');
	d.set('Label.SortKeys','Schl\u00fcssel Sortieren');
	d.set('Label.ShowGuidelines','Leitlinien');
	d.set('Label.AutoExpand','Auto Erweitern');
	d.set('Label.MarkupHTML','HTML Markup');
	d.set('Label.ConvertLinks','Links Konvertieren');
	d.set('Label.IndentDepth','Einzugstiefe');
	d.set('Label.ExpandDepth','Erweitern Tiefe');
	d.set('Label.DarkMode','Dunkler Modus');
	d.set('Label.ShowURL','URL Anzeigen');
	d.set('Label.ShowSearch','Suche Anzeigen');
	d.set('Label.ControlKeys','Steuerungstasten');
	d.set('Label.ShowFooter','Fu\u00dfzeile Anzeigen');
	d.set('Label.Spacing','Zeilenabstand');
	d.set('Label.SelectedLight','Helles Thema');
	d.set('Label.SelectedDark','Dunkles Thema');
	d.set('Label.CodeFont','JSON-Schrift');
	d.set('Label.CodeFontSize','Schriftgr\u00f6\u00dfe');
	d.set('Label.ConsoleFont','Konsolenschrift');
	d.set('Label.ConsoleFontSize','Schriftgr\u00f6\u00dfe');
	d.set('Label.DisplayFont','Anzeigeschrift');
	d.set('Label.DisplayFontSize','Schriftgr\u00f6\u00dfe');
	d.set('Label.Language','Sprache');
	// Docs
	
	
	
	/**
	 * SPANISH
	 */
	d = dict.getLanguageMap('es');
	// Misc
	d.set('Language','Idioma');
	d.set('Documentation','Documentaci\u00F3n');
	d.set('Controls','Controles');
	d.set('Interface','Interfaz');
	d.set('Search','B\u00FAsqueda');
	d.set('Console','Consola');
	// Labels
	d.set('Label.MarkupMode','Navegador Activado');
	d.set('Label.ShowNulls','Mostrar Nulos');
	d.set('Label.BoldKeys','Claves en Negrita');
	d.set('Label.ShowBytes','Mostrar Bytes');
	d.set('Label.ShowCount','Mostrar Conteo');
	d.set('Label.SortKeys','Ordenar Claves');
	d.set('Label.ShowGuidelines','Pautas');
	d.set('Label.AutoExpand','Autoexpandir');
	d.set('Label.MarkupHTML','Marcado HTML');
	d.set('Label.ConvertLinks','Convertir Enlaces');
	d.set('Label.IndentDepth','Prof. de Sangr\u00eda');
	d.set('Label.ExpandDepth','Prof. de Expansi\u00f3n');
	d.set('Label.DarkMode','Modo Oscuro');
	d.set('Label.ShowURL','Mostrar URL');
	d.set('Label.ShowSearch','Mostrar B\u00fasqueda');
	d.set('Label.ControlKeys','Teclas de Control');
	d.set('Label.ShowFooter','Mostrar Pie');
	d.set('Label.Spacing','Espaciado');
	d.set('Label.SelectedLight','Tema Claro');
	d.set('Label.SelectedDark','Tema Oscuro');
	d.set('Label.CodeFont','Fuente JSON');
	d.set('Label.CodeFontSize','Tama\u00f1o de Fuente');
	d.set('Label.ConsoleFont','Fuente de Consola');
	d.set('Label.ConsoleFontSize','Tama\u00f1o de Fuente');
	d.set('Label.DisplayFont','Fuente de Visualizaci\u00f3n');
	d.set('Label.DisplayFontSize','Tama\u00f1o de Fuente');
	d.set('Label.Language','Idioma');
	// Docs
	
	
	/**
	 * RUSSIAN
	 */
	d = dict.getLanguageMap('ru');
	// Misc
	d.set('Language','\u042F\u0437\u044B\u043A');
	d.set('Documentation','\u0414\u043E\u043A\u0443\u043C\u0435\u043D\u0442\u0430\u0446\u0438\u044F');
	d.set('Controls','\u0423\u043F\u0440\u0430\u0432\u043B\u0435\u043D\u0438\u0435');
	d.set('Interface','\u0418\u043D\u0442\u0435\u0440\u0444\u0435\u0439\u0441');
	d.set('Search','\u041F\u043E\u0438\u0441\u043A');
	d.set('Console','\u041A\u043E\u043D\u0441\u043E\u043B\u044C');
	// Labels
	d.set('Label.MarkupMode','\u041d\u0430\u0432\u0438\u0433\u0430\u0442\u043e\u0440');
	d.set('Label.ShowNulls','\u041d\u0443\u043b\u0438');
	d.set('Label.BoldKeys','\u0416\u0438\u0440\u043d\u044b\u0435 \u041a\u043b\u044e\u0447\u0438');
	d.set('Label.ShowBytes','\u0411\u0430\u0439\u0442\u044b');
	d.set('Label.ShowCount','\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e');
	d.set('Label.SortKeys','\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430');
	d.set('Label.ShowGuidelines','\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438');
	d.set('Label.AutoExpand','\u0410\u0432\u0442\u043e \u0420\u0430\u0437\u0432\u0435\u0440\u043d\u0443\u0442\u044c');
	d.set('Label.MarkupHTML','HTML');
	d.set('Label.ConvertLinks','\u0421\u0441\u044b\u043b\u043a\u0438');
	d.set('Label.IndentDepth','\u0412\u043b\u043e\u0436\u0435\u043d\u0438\u0435');
	d.set('Label.ExpandDepth','\u0413\u043b\u0443\u0431\u0438\u043d\u0430 \u0420\u0430\u0437\u0432\u0435\u0440\u043d\u0443\u0442\u044c');
	d.set('Label.DarkMode','\u0422\u0435\u043c\u043d\u044b\u0439 \u0420\u0435\u0436\u0438\u043c');
	d.set('Label.ShowURL','\u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c URL');
	d.set('Label.ShowSearch','\u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c \u041f\u043e\u0438\u0441\u043a');
	d.set('Label.ControlKeys','\u041a\u043b\u0430\u0432\u0438\u0448\u0438 \u0423\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f');
	d.set('Label.ShowFooter','\u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c \u041f\u043e\u0434\u0432\u0430\u043b');
	d.set('Label.Spacing','\u0418\u043d\u0442\u0435\u0440\u0432\u0430\u043b \u0421\u0442\u0440\u043e\u043a');
	d.set('Label.SelectedLight','\u0421\u0432\u0435\u0442\u043b\u0430\u044f \u0422\u0435\u043c\u0430');
	d.set('Label.SelectedDark','\u0422\u0435\u043c\u043d\u0430\u044f \u0422\u0435\u043c\u0430');
	d.set('Label.CodeFont','\u0428\u0440\u0438\u0444\u0442 JSON');
	d.set('Label.CodeFontSize','\u0420\u0430\u0437\u043c\u0435\u0440 \u0428\u0440\u0438\u0444\u0442\u0430');
	d.set('Label.ConsoleFont','\u0428\u0440\u0438\u0444\u0442 \u041a\u043e\u043d\u0441\u043e\u043b\u0438');
	d.set('Label.ConsoleFontSize','\u0420\u0430\u0437\u043c\u0435\u0440 \u0428\u0440\u0438\u0444\u0442\u0430');
	d.set('Label.DisplayFont','\u0428\u0440\u0438\u0444\u0442 \u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f');
	d.set('Label.DisplayFontSize','\u0420\u0430\u0437\u043c\u0435\u0440 \u0428\u0440\u0438\u0444\u0442\u0430');
	d.set('Label.Language','\u042F\u0437\u044B\u043A');
	// Docs
	
	
	
	/**
	 * CHINESE
	 */
	d = dict.getLanguageMap('zh');
	// Misc
	d.set('Language','\u8BED\u8A00');
	d.set('Documentation','\u6587\u6863');
	d.set('Controls','\u63A7\u5236');
	d.set('Interface','\u754C\u9762"');
	d.set('Search','\u641C\u7D22');
	d.set('Console','\u63A7\u5236\u53F0');
	// Labels
	d.set('Label.MarkupMode','\u5bfc\u822a\u5668\u5f00\u542f');
	d.set('Label.ShowNulls','\u663e\u793a\u7a7a\u503c');
	d.set('Label.BoldKeys','\u52a0\u7c97\u952e');
	d.set('Label.ShowBytes','\u663e\u793a\u5b57\u8282');
	d.set('Label.ShowCount','\u663e\u793a\u6570\u91cf');
	d.set('Label.SortKeys','\u6392\u5e8f\u952e');
	d.set('Label.ShowGuidelines','\u6309\u539f\u7ebf');
	d.set('Label.AutoExpand','\u81ea\u52a8\u5c55\u5f00');
	d.set('Label.MarkupHTML','\u6807\u8bb0 HTML');
	d.set('Label.ConvertLinks','\u8f6c\u6362\u94fe\u63a5');
	d.set('Label.IndentDepth','\u7f29\u8fdb\u6df1\u5ea6');
	d.set('Label.ExpandDepth','\u5c55\u5f00\u6df1\u5ea6');
	d.set('Label.DarkMode','\u6697\u8272\u6a21\u5f0f');
	d.set('Label.ShowURL','\u663e\u793a URL');
	d.set('Label.ShowSearch','\u663e\u793a\u641c\u7d22');
	d.set('Label.ControlKeys','\u63a7\u5236\u952e');
	d.set('Label.ShowFooter','\u663e\u793a\u5c3e\u90e8');
	d.set('Label.Spacing','\u884c\u95f4\u8ddd');
	d.set('Label.SelectedLight','\u6d45\u8272\u4e3b\u9898');
	d.set('Label.SelectedDark','\u6697\u8272\u4e3b\u9898');
	d.set('Label.CodeFont','\u4ee3\u7801\u5b57\u4f53');
	d.set('Label.CodeFontSize','\u5b57\u4f53\u5927\u5c0f');
	d.set('Label.ConsoleFont','\u63a7\u5236\u53f0\u5b57\u4f53');
	d.set('Label.ConsoleFontSize','\u5b57\u4f53\u5927\u5c0f');
	d.set('Label.DisplayFont','\u663e\u793a\u5b57\u4f53');
	d.set('Label.DisplayFontSize','\u5b57\u4f53\u5927\u5c0f');
	d.set('Label.Language','\u8BED\u8A00');
	// Docs


})();

//log.debug('Dictionary codes:',cmsjs.Dictionary.listLanguageCodes());
//log.debug('Dictionary strings:',cmsjs.Dictionary.listLanguageStrings());
