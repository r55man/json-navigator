
log.trace('loading init-keys.js');

// Keybinding Definitions

(function() {

	/*
	 * Keybinding Documentation
	 * Populate english first, then other langugages separately
	 */
	let dict = null;
	let d = null;
	dict = cmsjs.Dictionary.create('Keybindings');
	dict.addLanguages(['en','de','es','fr','ru','zh']);
	dict.setRootLanguage('en');
	
	/**
	 * ENGLISH
	 */
	d = dict.getLanguageMap('en');
	
	// open search and take focus
	cmsjs.Keys.addAction('search', () => {
		cmsjs.Controls.setValue('Config.ShowSearch',true);
		$.id('search-input').focus();
	});
	d.set('search','Open search bar and take focus');
	
	
	// toggle guidlines
	cmsjs.Keys.addAction('guidelines', () => { cmsjs.Controls.toggle('Config.ShowGuidelines'); });
	d.set('guidelines','Toggle depth guidelines');
	
	// toggle bytes
	cmsjs.Keys.addAction('bytes', () => { cmsjs.Controls.toggle('Config.ShowBytes'); });
	d.set('bytes','Toggle display of bytes next to large objects and arrays');
	
	// toggle bold keys
	cmsjs.Keys.addAction('bold-keys', () => { cmsjs.Controls.toggle('Config.BoldKeys'); });
	d.set('bold-keys','Toggle bold font on JSON keys');
	
	// toggle nulls
	cmsjs.Keys.addAction('nulls', () => { cmsjs.Controls.toggle('Config.ShowNulls'); });
	d.set('nulls','Toggle display of null values');
	
	// toggle item counts
	cmsjs.Keys.addAction('count', () => { cmsjs.Controls.toggle('Config.ShowCount'); });
	d.set('count','Toggle item counts on objects and arrays');
	
	// toggle item counts
	cmsjs.Keys.addAction('sort-keys', () => { cmsjs.Controls.toggle('Config.SortKeys'); });
	d.set('sort-keys','Toggle alphabetical sorting of item keys');
	
	// increase indent
	cmsjs.Keys.addAction('indent-more', () => { cmsjs.Controls.setValue('Config.IndentDepth',cmsjs.State.get('Config.IndentDepth')+1); });
	d.set('indent-more','Increase indentation');
	
	// decrease indent
	cmsjs.Keys.addAction('indent-less', () => { cmsjs.Controls.setValue('Config.IndentDepth',cmsjs.State.get('Config.IndentDepth')-1); });
	d.set('indent-less','Decrease indentation');
	
	// increase spacing
	cmsjs.Keys.addAction('more-space', () => { cmsjs.Controls.setValue('Config.Spacing',cmsjs.State.get('Config.Spacing')+1) });
	d.set('more-space','Increase line spacing');
	
	// decrease spacing
	cmsjs.Keys.addAction('less-space', () => { cmsjs.Controls.setValue('Config.Spacing',cmsjs.State.get('Config.Spacing')-1) });
	d.set('less-space','Decrease line spacing');
	
	// next key
	cmsjs.Keys.addAction('next-key', () => {
		let focused = document.activeElement;
		if ( focused && focused.classList ) {
			if ( focused.classList.contains('object') && focused.classList.contains('value') ) {
				const parentItem = focused.parentElement.parentElement;
				if ( parentItem ) {
		            const firstItem = parentItem.querySelector('div.contents > div.item');
					if ( firstItem ) {
					    const nextKey = firstItem.querySelector('div.label > span.key');
						if ( nextKey ) { nextKey.focus(); }
					}
				}
			}
			else if ( focused.classList.contains('key') ) {
				let parent = focused.parentElement.parentElement;
				let foundParent = false;
				while ( parent && parent.id !== 'root' && ! foundParent ) {
					const nextItem = parent.nextSibling;
					if ( nextItem ) {
						const nextKey = nextItem.querySelector('div.label > span.key');
						if ( nextKey ) { nextKey.focus(); foundParent = true; break; }
					}
					else {
						parent = parent.parentElement;
					}
				}
			}
		}
	});
	d.set('next-key','Advance to next visible key (depth first)');
	
	// prev key
	docs = 'Advance to previous visible key (depth first)';
	cmsjs.Keys.addAction('prev-key', () => {
		let focused = document.activeElement;
		if ( focused && focused.classList ) {
			if ( focused.classList.contains('object') && focused.classList.contains('value') ) {
				const prevKey = focused.previousSibling;
				if ( prevKey ) { prevKey.focus(); }
			}
			else if ( focused.classList.contains('key') ) {
				const parentItem = focused.parentElement.parentElement;
				if ( parentItem ) {
					const previousItem = parentItem.previousSibling;
					if ( previousItem ) {
						const prevKey = previousItem.querySelector('div.label > span.key');
						if ( prevKey ) { prevKey.focus(); }
					}
					else {
						const grandparentItem = parentItem.parentElement.parentElement;
						//console.log(grandparentItem);
						const prevKey = grandparentItem.querySelector('div.label > span.key');
						if ( prevKey ) { prevKey.focus(); }
					}
				}
			}
		}
	});
	d.set('prev-key','Advance to next visible key (depth first)');
	
	// next sibling
	cmsjs.Keys.addAction('next-sibling', () => {
		if ( document.activeElement ) {
			//console.log(document.activeElement);
		}
	});
	d.set('next-sibling','Move to next sibling');
	
	// prev sibling
	cmsjs.Keys.addAction('prev-sibling', () => {
		if ( document.activeElement ) {
			//console.log(document.activeElement);
		}
	});
	d.set('prev-sibling','Move to previous sibling');
	
	// these all need to be in a function and called after logging loads
	//function attachKeyBindings() {
		cmsjs.Keys.assign('Alt-f','search');
		cmsjs.Keys.assign('Alt-g','guidelines');
		cmsjs.Keys.assign('Alt-y','bytes');
		cmsjs.Keys.assign('Alt-b','bold-keys');
		cmsjs.Keys.assign('Alt-n','nulls');
		cmsjs.Keys.assign('Alt-s','sort-keys');
		cmsjs.Keys.assign('Alt-k','next-key');
		cmsjs.Keys.assign('Alt-i','prev-key');
		cmsjs.Keys.assign('Alt-l','next-sibling');
		cmsjs.Keys.assign('Alt-j','prev-sibling');
		cmsjs.Keys.assign('Alt-h','count');
		cmsjs.Keys.assign('Alt-ArrowLeft','indent-less');
		cmsjs.Keys.assign('Alt-ArrowRight','indent-more');
		cmsjs.Keys.assign('Alt-ArrowUp','less-space');
		cmsjs.Keys.assign('Alt-ArrowDown','more-space');
	//}


	cmsjs.Keys.enableListener();
	
})();
