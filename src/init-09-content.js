
log.trace('loading init-content.js');


if ( typeof JSON_NAVIGATOR_STANDALONE_MODE !== 'undefined' && JSON_NAVIGATOR_STANDALONE_MODE ) {
	log.info('running in standalone mode');
	cmsjs.State.set('ENV.StandaloneMode',true);
	cmsjs.State.loadLocalStorage(initConsole);
}
else {
	cmsjs.Log.debug('running in extension mode');
	cmsjs.State.set('ENV.StandaloneMode',false);
	initExtension(() => {
		cmsjs.Log.debug('extension initialized');
		cmsjs.State.set('ENV.CurrentPath','$');
		cmsjs.State.loadChromeStorage(initConsole);
	});
}

/*
 * This is the starting point for non-environment-specific code.
 */
function initConsole() {

	if ( ! cmsjs.State.get('ENV.StandaloneMode') && cmsjs.State.get('ENV.JSON') ) {
		cmsjs.State.set('ENV.Bytes',JSON.stringify(cmsjs.State.get('ENV.JSON')).length);
		t0 = cmsjs.Log.timer('initConsole() got bytes (' + cmsjs.State.get('ENV.Bytes') + ')',t0);
	}
	const console = cmsjs.Console.init();
	console.style.cursor = 'wait';

	cmsjs.Theme.Layout.apply(cmsjs.State.get('cmsjs.Theme.Layout'));
	cmsjs.Theme.Colors.apply(cmsjs.State.get('cmsjs.Theme.Colors'));
	cmsjs.Theme.Fonts.apply(cmsjs.State.get('cmsjs.Theme.Fonts'));
	cmsjs.Theme.Controls.apply(cmsjs.State.get('cmsjs.Theme.Controls'));
	

	if ( cmsjs.State.get('ENV.StandaloneMode') ) {
		showPage('content-demo');
	}
	else if ( cmsjs.State.get('Core.MarkupMode') ) {
		cmsjs.State.set('ENV.CurrentPath',window.location.hash.substring(1) || '$');
		showPage('content-markup');
		//cmsjs.State.get('Core.EditorMode') = false;
	}
	//else if ( cmsjs.State.get('Core.EditorMode') ) {
	//	showPage('editor');
	//}
	else {
		showPage('content-raw');
	}
	t0 = cmsjs.Log.timer('buildConsole() displayed json',t0);
	//STYLE.applyTheme(STYLE.getCurrentTheme());
	//STYLE.OnThemeChange();
	STYLE.applyCurrentTheme();
	t0 = cmsjs.Log.timer('buildConsole() applied theme',t0);
	attachKeyBindings();
	t0 = cmsjs.Log.timer('buildConsole() attached keybindings',t0);
	cmsjs.State.set('ENV.PageLoaded',true);
	cmsjs.Log.timer('buildConsole() total execution time',startTime);
	window.addEventListener('hashchange', () => {
		$.id('json-path').remove();
		cmsjs.State.set('ENV.CurrentPath',window.location.hash.substring(1) || '$');
		cmsjs.Console.buildComponent('content-markup');
	});
	document.documentElement.style.cursor = '';
	cmsjs.Lang.CurrentLanguage = cmsjs.State.get('Core.Language');
}


/* 
 * If not in standalone (demo) mode, initialize the extension.
 *
 * The only thing this really accomplishes is to get the JSON content 
 * into ENV.JSON from wherever the browser dumps it.
 *
 * It takes one parameter, 'callback', which is a function that
 * should be run after ENV.JSON is set.
 *
 * This is quite hacky because there's no easy way of intercepting
 * a request and telling the browser not to generate a wrapper web
 * page for every resource, and no way to do it all that doesn't
 * require a bunch of browser-specific code.
 *
 * So, what we have to do is wait for the browser to generate the
 * wrapper page, pull the raw text JSON out using DOM queries,
 * parse it, then wipe the whole page out and start with a blank
 * slate. The wrappers that different browsers use are basically
 * all the same: <html><body><pre>JSON-CODE</pre></body></html>
 *
 * On large files, the time the browser spends printing the
 * JSON text is non-neglible and can take severaal seconds.
 * In order to prevent slow page loads, you have to grab the
 * <pre> tag as soon it becomes available and set it to
 * display:none. But it is not actually available right away,
 * necessitating a setTimeout loop.
 *
 */
function initExtension(callback) {
	// This auxilliary function does no error checking, it simply matches
	// content-type and/or extension to quickly decide whether this resource
	// should be processed at all.
	function isJSON() {
	    const contentType = document.contentType.toLowerCase();
		const isValidContentType = cmsjs.State.get('Core.ContentTypes').includes(contentType);
		if ( isValidContentType ) {
			cmsjs.Log.debug('isJSON(): matched content-type:',contentType);
			return true;
		}
		const url = new URL(document.location.href);
	    const extension = url.pathname.split('/').pop().split('.').pop().toLowerCase();
		const isValidExtension = cmsjs.State.get('Core.Extensions').includes(extension);
		if ( isValidExtension ) {
			cmsjs.Log.debug('isJSON(): matched extension:',extension);
			return true;
		}
		if ( cmsjs.State.get('Core.Debug') ) {
			cmsjs.Log.debug('isJSON(): extension did not match:',extension);
			cmsjs.Log.debug('isJSON(): content-type did not match:',contentType);
			cmsjs.Log.debug('isJSON(): deferring to browser');
		}
		return false;
	}

	let t0 = cmsjs.Log.timer('starting a timer at the beginning of init()');
	const startTime = t0;
	if ( ! isJSON() ) { return; } // if content is not JSON, bail immediately.
	t0 = cmsjs.Log.timer('initExtension(): isJSON() returned true, so treating as JSON content',t0);
	/*
	 * Right away, do the following: (NOTE: only documentElement (<html>) will be available at this time!!)
	 * - Make the document content hidden, to prevent display of raw JSON
	 * - Set a text color and background color
	 * - Set up a listener to watch when DOM content is finished loading
	 *    - When the DOM is finished, the listener:
     *       - Gets the content from the <pre> tag
	 *       - Parses it as JSON
     *       - runs the function passed as 'callback()'
	 */
	document.documentElement.style.visibility = 'hidden';
	document.documentElement.style.color = '#000';
	document.documentElement.style.backgroundColor = '#def';
	document.addEventListener('DOMContentLoaded', function() {
		try {
			let t = cmsjs.Log.timer('*DOMContentLoaded* listener fired, timer started');
			const pre = document.body.querySelector('pre');
			if ( ! pre ) {
				cmsjs.Log.error('*DOMContentLoaded* there was no <pre> tag, so the browser does not think this is text content.');
				document.documentElement.style.visibility = 'visible';
				return;
			}
			cmsjs.State.set('ENV.RawContent',pre.textContent);
			t = cmsjs.Log.timer('*DOMContentLoaded* got ENV.RawContent',t);
			cmsjs.State.set('ENV.JSON',JSON.parse(cmsjs.State.get('ENV.RawContent')));
			t = cmsjs.Log.timer('*DOMContentLoaded* got ENV.JSON from ENV.RawContent',t);
		}
		catch (error) {
			document.body.style.cursor = '';
			cmsjs.Log.error('*DOMContentLoaded* JSON parsing error: JSON.parse(document.body.textContent) failed.');
			console.error(error);
			const p = getP('error');
			p.style.color = 'red';
			//document.body.style.cursor = '';
			if ( $.id('loading') ) { $.id('loading').remove(); }
			document.body.appendChild(p);
			p.innerHTML = '<strong>Error parsing JSON file.<br>Parser reported:</strong> ';
			p.innerHTML += error.message;
			p.innerHTML += '<br><br><a style="color:black" href="?mode=edit">Open file in editor</a>';
			//cmsjs.Log.warn('*DOMContentLoaded* falling back to browser');
			//document.documentElement.style.visibility = 'visible';
			return;
		}
		cmsjs.State.set('ENV.RawContentProcessed',true);
		callback();
	});
	// This is an auxiliary function to get a <p> tag for use on the loading page:
	// The 'document.body' stuff is there to ensure that regardless of when a <p>
	// is requested, it always sets the appropriate body style.
	function getP(id) {
		const p = $.ce('p');
		if ( id ) { p.id = id; }
		p.style.position = 'absolute';
		p.style.top = '25%';
		p.style.left = '50%';
		p.style.transform = 'translate(-50%,-50%)';
		document.body.style.height = '100vh';
		document.body.style.display = 'flex';
		document.body.style.justifyContent = 'flex';
		document.body.style.alignItems = 'center';
		return p;
	}
	// Do not remove or modify this without benchmarking. For whatever reason,
	// the page loads significantly faster on large files with this code here. I
	// do not know if it is due to setting the <pre> tag to display:none or if
	// it's due to some other cause, but keep this code as-is until you figure out
	// why it speeds things up.
	function loading(ms=0) {
		if (document.body) {
			cmsjs.Log.debug('initExtension(): body arrived');
			if ( ! cmsjs.State.get('ENV.RawContentProcessed') ) {
				cmsjs.Log.debug('init(): this is the body that will have the autogenerated <pre> tag, so hide it');
				const pre = document.body.querySelector('pre');
				// BUT, if it's not a json file, it will not have the pre, so...
				if ( ! pre ) { return; }
				pre.style.display = 'none';
				const p = getP('loading');
				p.innerHTML = 'Please wait while document loads.'
				p.id = 'loading';
				if ( ! $.id('error') ) {
					document.body.style.cursor = 'wait';
					document.body.appendChild(p);
				}
				document.documentElement.style.visibility = 'visible';
			}
			else {
				cmsjs.Log.debug('init(): but as it turns out, DOMContentLoaded already fired, so this is the real body');
			}
			return;
		}
		else if ( ms < 5000 ) {
			cmsjs.Log.debug('initExtension(): waiting for body to become available to hide it...');
			const waitTime = 5;
			const total = ms + waitTime;
			setTimeout(() => loading(total), waitTime);
		}
		else {
			cmsjs.Log.debug('initExtension(): taking too long, aborting.');
		}
	}
	loading();
	return;
}
