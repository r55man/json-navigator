# JSON Navigator

## Description

Fast and flexible JSON viewer. This is the one you want :)

- Live demo: https://r55man.com/json-navigator/

* FAST FAST FAST. Nothing else compares. Supports 100MB+ file sizes. Loads files virtually instantly.

* JSON Burrowing!!! This powerful feature allows you to quickly descend into deep hierarchies of data without becoming overwhelmed with content on the screen. Clickable JSON Path Bar to instantly navigate to any point in the current hierarchy. Works with the browser's native forward/back buttons to quickly navigate the JSON tree, and allows you to open subtrees in new tabs and/or windows.

* The on-screen control panel gives instant access to tons of useful features:
    - Toggleable raw JSON view.
    - Toggleable null value hiding. Null value hiding cascades upwards from all-null children.
    - Toggleable bold keys.
    - Toggleable byte counts: shows how much data is inside a given hierarchy.
    - Toggleable array sizes and object item counts.
    - Toggleable key sorting.
    - Toggleable color-coded depth guides.
    - Toggleable auto-expand.
    - Toglleable HTML Markup.
    - Toggleable hyperlink conversion.
    - Toggleable line wrapping.
    - Toggleable dark/light mode.
    - Configurable indent depth.
    - Configurable auto-expand and click-to-expand depth.
    - Configurable line spacing.
    - Configurable multi-level expand on all objects and arrays.
    - Configurable fonts and font sizing. Console fonts and JSON code fonts are independently configurable.

* Furthermore, each entry in the JSON file has it's own menu giving you powerful navigation tools:
    - Collapse-all-children feature on all objects and arrays.
    - Expand-all-children feature on all objects and arrays.
    - One-click copy-to-clipboard on all non-null values.
        - Smart clipboard omits null entries when "Show Nulls" is turned off.
    - "Burrow" link, opening this key/val pair isolated from the rest of the document.

* One-click in-app documentation.

* Full Internationalization support. Controls and documentation are available in six languages:
    - English
    - French
    - German
    - Spanish
    - Russian
    - Chinese

* No-clutter display omits syntax operators and allows you to turn off the header and footer.

* Full set of keybindings for easy keyboard navigation.

* Settings saved between sessions.

* Multiple Dark Mode and Light Mode themes to chose from. A theme editor is included allowing you to easily design your own custom color schemes and add custom fonts.

* Works on local files as well as remote.

- GitLab Project Page: https://gitlab.com/r55man/json-navigator


## Known Issues :: To be addressed before v1.0

- Real-time key sorting is buggy. It gets keys mixed up when the tree is expanded more than a couple levels deep.

- JSON Path Bar does not work correctly on nested arrays.

- Wrapped lines shift jarringly when the item menu is displayed. The item menu should not trigger a reflow of wrapped lines.

- "Show Bytes" incurs a 25-50% overhead in JSON processing time, which gets progressively worse as the data gets shallower. On small files (less than 1MB) this increase is unnoticeable, but it gets worse the larger the files get. Currently there is no way to prevent this overhead; turning off "Show Bytes" will still incur the penalty, the bytes just won't be displayed.

- The controls next to object/arrays need to be better documented, explaining what they do and how they tie into "Expand Depth". In fact, the entire program needs to be better documented :/

- The search bar is rather flaky in its interaction with other functions at the moment. Searching from the root level generally works as expected, but searching from subtrees is bugged. The search UI is primitive and unintuitive, and does not allow for any kind of search customization. Search will eventually support key/value/key+val matching, with the ability to search by exact match, substring, or regex.

- In the UI, cascading nulls only trigger once you uncover nulls deep enough to reveal that all the children are null. This will eventually be need to fixed, perhaps by pre-scanning the initial JSON file to reveal all the nulls when the document loads.

- Expanded objects/arrays and the scroll position are not preserved on previous views when burrowing.

- There is no way to configure the plugin to work with mime types other than application/json, or to key off of filename extension, or to force-convert a page that you know has JSON data but the browser insists on rendering itself. These issues will be addressed in an upcoming release.

- Large arrays (1000+ elements) result in a slow UI and are pretty much useless in terms of finding data. There should instead be a configurable cutoff length, with progressive/selective display of additional items.

- The live demo should be reworked to be a general purpose JSON browser, where the user can upload their own files and/or paste JSON content into a textarea.


## Known Non-Issues :: These are not bugs

- There are no line numbers and no plans to include them, as it is difficult to find a compelling case for them in the context of a JSON file. The line numbers would not match those of the original file due to the formatting conversion, and therefore would be meaningless.

- The raw view would look better if it were marked up (c.f. highlight.js), but unfortunately syntax highlighters bomb on large files because they create an ungodly number of nodes. The raw view is already slow, taking several seconds to render on large files. Syntax highlighting would bring this to a crawl.


## Post-1.0 Ideas

- Advanced search, supporting one or more JSON query standards (e.g. JSONPath, JMESPath, JSONiq)

- Logic layer for building a custom UI around the JSON files.

- Editing and saving, in conjunction with the logic layer.

- File import / export / upload / download.

- Firefox/Edge/Safari versions.

- Mobile versions for Android and iOS.

