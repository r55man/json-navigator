console.log('[json-navigator] service worker loaded');

chrome.action.onClicked.addListener((tab) => {
	console.log('[json-navigator] action listener activated');
	chrome.scripting.executeScript({
		target: {tabId: tab.id},
		files: ['test.js']
	});
});


chrome.webRequest.onBeforeRequest.addListener(
    (details) => {
        if (details.responseHeaders) {
            const contentTypeHeader = details.responseHeaders.find(header => header.name.toLowerCase() === 'content-type');
            if (contentTypeHeader && contentTypeHeader.value.includes('application/json')) {
                return { cancel: true }; // Cancel the request to prevent the browser from processing it further.
            }
        }
    },
    { urls: ["<all_urls>"], types: ["main_frame"] },
    ["blocking"]
);

chrome.webRequest.onCompleted.addListener(
	(details) => {
		console.log('[json-navigator] webRequest completed',details);
		if (details.responseHeaders) {
			console.log('[json-navigator] response headers found',details.responseHeaders);
			const ct = details.responseHeaders.find(header => header.name.toLowerCase() === 'content-type');
			if (ct && ct.value.includes('application/json')) {
				console.log('[json-navigator] application/json detected',ct);
				fetch(details.url)
					.then(response => response.json())
					.then(jsonData => {
						console.log("processing json data", jsonData);
						// Example: Save data to storage
						//chrome.storage.local.set({ 'jsonData': jsonData });
					})
					.catch(error => console.error("Error processing JSON:", error));
			}
		}
	},
	{ urls: ["<all_urls>"], types: ["main_frame"] },
	["responseHeaders"]
);
