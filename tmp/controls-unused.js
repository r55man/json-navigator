
/*
 * Number up/down stepper
 */
cpe.set('stepper', (config) => {
	const style = {
	}
	const text = config['text'] || '';
	const id = config['id'] || '';
	const height = config['height'] || 16;
	const width = config['width'] || 3*height;
	const min = parseInt(config['min']) || 1;
	const max = parseInt(config['max']) || 5;
	const value = config['value'] || min;
	const onChange = config['onChange'];
	const borderRadius = height/2;
	const wrapper = $.ce('div');
	wrapper.style.margin = '0.25rem';
	wrapper.style.padding = '0.25rem';
	wrapper.style.display = 'flex';
	wrapper.style.alignItems = 'center';
	wrapper.style.justifyContent = 'flex-end';
	//if ( text ) { wrapper.append(getPopupTextElement(text,hoverDocs.get(id))); }
	const textLabel = $.ce('span',text);
	textLabel.style.marginRight = '1rem';
	wrapper.append(textLabel);
	const numberInput = $.ce('span');
	numberInput.setAttribute('class','number-input');
	numberInput.style.display = 'flex';
	numberInput.style.alignItems = 'center';
	wrapper.append(numberInput);
	const inputField = $.ce('input');
	inputField.setAttribute('type','text');
	inputField.setAttribute('class','input-field');
	inputField.setAttribute('value',value);
	inputField.setAttribute('min',min);
	inputField.setAttribute('max',max);
	inputField.setAttribute('readonly','');
	inputField.style.width = '1rem';
	inputField.style.textAlign = 'center';
	const up = $.ce('span');
	up.innerHTML = '+';
	up.setAttribute('class','down');
	up.style.cursor = 'pointer';
	up.style.padding = '2px';
	up.style.width = '1rem';
	up.style.height = '1.2rem';
	up.style.textAlign = 'center';
	up.style.border = 'solid 1px #ccc';
	up.style.fontSize = '1.2rem';
	up.style.order = '1';
	up.addEventListener('click', () => {
		const value = parseInt(inputField.value);
		if (value < max) {
			inputField.value = value + 1;
			CONFIG[id] = value + 1;
			localStorage.setItem('CONFIG.' + id,value + 1);
			onChange(value + 1);
		}
	});
	const down = $.ce('span');
	down.innerHTML = '-';
	down.setAttribute('class','down');
	down.style.cursor = 'pointer';
	down.style.padding = '2px';
	down.style.width = '1rem';
	down.style.height = '1.2rem';
	down.style.textAlign = 'center';
	down.style.border = 'solid 1px #ccc';
	down.style.fontSize = '1.2rem';
	down.style.order = '-1';
	down.addEventListener('click', () => {
		const value = parseInt(inputField.value);
		if (value > min) {
			inputField.value = value - 1;
			CONFIG[id] = value - 1;
			localStorage.setItem('CONFIG.' + id,value - 1);
			onChange(value - 1);
		}
	});
	numberInput.append(down);
	numberInput.append(inputField);
	numberInput.append(up);
	return wrapper;
});
