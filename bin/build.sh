#!/usr/bin/bash
set -eu

cd src/cmsjs/
cat cmsjs.js > cms.js
cat cmsjs.Logger.js >> cms.js
cat cmsjs.State.js >> cms.js
cat cmsjs.Console.js >> cms.js
cat cmsjs.Component.js >> cms.js
cat cmsjs.Controls.js >> cms.js
cat cmsjs.Database.js >> cms.js
cat cmsjs.Dictionary.js >> cms.js
cat cmsjs.Keys.js >> cms.js
cat cmsjs.Module.js >> cms.js
cat cmsjs.SPA.js >> cms.js
cat cmsjs.Themes.js >> cms.js
cat cmsjs.util.js >> cms.js

mv cms.js ../

cd ..

cat cms.js > ../build/content.js
cat cms.logger.js >> ../build/content.js
cat cms.state.js >> ../build/content.js
cat cms.component.js >> ../build/content.js
cat cms.console.js >> ../build/content.js
cat cms.themes.js >> ../build/content.js
cat cms.dictionary.js >> ../build/content.js
cat cms.keys.js >> ../build/content.js
cat content.js >> ../build/content.js

cd ../

rm -f build.zip

zip -r build.zip build/

exit 0
