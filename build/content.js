
/**
 * Root class.
 *
 * Defines the root namespace.
 */
class cmsjs {
	static Version = '0.9.0';
}

// This line allows cmsjs.Logger.js to run in standalone mode.
if ( typeof cmsjs === 'undefined' ) { cmsjs = class {}; }

/**
 * Logging.
 *
 * All classes require this class to be defined in order to create 
 * their static class loggers, so it must be the first class loaded
 * after the base class.
 */
cmsjs.Logger = class {

	/**
	 * Grab a handle to the environment console on class load.
	 *
	 * In the event the user carelessly defines a global variable 
	 * called 'console', we want the logger to keep working.
	 */
	static CONSOLE = console;

	/**
	 * Instances.
	 *
	 * Holds the collection of Logger instances.
	 */
	static Instances = new Map();

	/**
	 * Override instance logger settings.
	 *
	 * Setting a boolean value here overrides any instance logger
	 * settings. Setting a value to 'null' disables the override.
	 */
	static OverrideLogLevel = new Map();
	static OverrideAll = null;

	/**
	 * Log if possible.
	 *
	 * This class cannot do its own logging until after the class
	 * is defined and default log levels have been created. So for
	 * functions which are called before loggers have been created,
	 * defer to this function instead of calling the log directly.
	 *
	 * @private
	 */
	static _logIfPossible(logLevel, ...args) {
		if ( cmsjs.Logger.Log && cmsjs.Logger.Log[logLevel] ) {
			cmsjs.Logger.Log[logLevel](...args);
		}
	}
	
	/**
	 * Overrides all instance logger print settings for the given
	 * log level.
	 */
	static override(logLevel,value) {
		cmsjs.Logger.Log.trace('override(logLevel,value)',logLevel,value);
		cmsjs.Logger.OverrideLogLevel.set(logLevel,value);
	}
	
	/**
	 * Overrides all instance settings for all log levels.
	 */
	static overrideAll(value) {
		cmsjs.Logger.OverrideAll = value;
	}

	/**
	 * Disables the specified log level for all instances.
	 */
	static disableAll(logLevel) {
	}
	
	/**
	 * Returns the instance logger with the specified id.
	 */
	static get(id) {
		cmsjs.Logger.Log.trace('get(id)',id);
		return cmsjs.Logger.Instances.get(id);
	}

	/**
	 * Creates a new logger instance.
	 *
	 * Always use this instead of the constructor.
	 */
	static create(id) {
		cmsjs.Logger._logIfPossible('trace','create(id):',id);
		if ( cmsjs.Logger.Instances.has(id) ) {
			cmsjs.Logger._logIfPossible('error','create(id) id exists:',id);
			return null;
		}
		const instance = new cmsjs.Logger(id);
		if ( instance ) { cmsjs.Logger.Instances.set(id,instance); }
		return instance;
	}


	/**
	 * Creates a new Logger instance.
	 *
	 * Never call this directly. Always use create, as it will return
	 */
	constructor(id) {
		this.ID = id;
		this.Enabled = true;
		this.Flag = null;
		this.Style = null;
		cmsjs.Logger.LogLevel.Instances.forEach((levelObj,levelName) => {
			this.addLogLevel(levelName);
		});
		this.LogLevelEnabled = new Map();
		this.LogLevelFlags = new Map();
		this.LogLevelStyle = new Map();
		this.Time = (new Date()).getTime();
	}

	/**
	 * Enables an individual log level, or the logger as a whole.
	 */
	enable(logLevel) {
		this.trace('enable(logLevel)',logLevel);
		if ( arguments.length === 0 ) {
			this.Enabled = true;
		}
		else {
			this.LogLevelEnabled.set(logLevel,true);
		}
	}

	/**
	 * Enables all log levels.
	 */
	enableAll() {
		this.trace('enableAll()',logLevel);
		this.Enabled = true;
		this.LogLevelEnabled.forEach((value,key) => {
			LogLevelEnabled.set(key,true);
		});
	}
	
	/**
	 * Disables an individual log level, or the logger as a whole.
	 */
	disable(logLevel) {
		this.trace('disable(logLevel)',logLevel);
		if ( arguments.length === 0 ) {
			this.Enabled = false;
		}
		else {
			this.LogLevelEnabled.set(logLevel,false);
		}
	}
	
	/**
	 * Disables all log levels.
	 */
	disableAll() {
		this.trace('disableAll()',logLevel);
		this.Enabled = false;
		this.LogLevelEnabled.forEach((value,key) => {
			LogLevelEnabled.set(key,false);
		});
	}
	
	/**
	 * Sets the flag for the logger or log level.
	 */
	setFlag(...args) {
		this.trace('setFlag(...args)',...args);
		if ( args.length === 1 ) {
			const flag = args[0];
			this.Flag = flag;
		}
		else {
			const logLevel = args[0];
			const flag = args[1];
			if ( ! cmsjs.Logger.LogLevel.get(logLevel) ) {
				cmsjs.Logger.Log.error('setFlag(logLevel,flagName) log level not found:',logLevel);
				return;
			}
			this.LogLevelFlags.set(logLevel,flag);
		}
	}
	
	/**
	 * Sets the style for the logger or log level.
	 */
	setStyle(...args) {
		this.trace('setStyle(...args)',...args);
		if ( args.length === 1 ) {
			const str = args[0];
			this.Style = str;
		}
		else {
			const logLevel = args[0];
			const str = args[1];
			if ( ! cmsjs.Logger.LogLevel.get(logLevel) ) {
				cmsjs.Logger.Log.error('setStyle(logLevel,str) log level not found:',logLevel);
				return;
			}
			this.LogLevelStyle.set(logLevel,str);
		}
	}


	/**
	 * Adds a log level to this logger.
	 */
	addLogLevel(logLevel) {
		//if ( this && this.trace ) { this.trace('addLogLevel(logLevel)',logLevel); }
		if ( this.hasOwnProperty(logLevel) ) {
			cmsjs.Logger.Log.error('addLogLevel() function exists',logLevel);
			return;
		}
		this[logLevel] = function(str, ...args) {
			if ( this._isEnabled(logLevel) ) {
				this._log(logLevel, str, ...args);
			}
		}
	}

	
	/**
	 * Checks to see if a given log level is enabled.
	 *
	 * This is for internal use only, called right before _log().
	 */
	_isEnabled(logLevel) {
		if ( typeof cmsjs.Logger.OverrideAll === 'boolean' ) {
			return cmsjs.Logger.OverrideAll;
		}
		if ( typeof cmsjs.Logger.OverrideLogLevel.get(logLevel) === 'boolean' ) {
			return cmsjs.Logger.OverrideLogLevel.get(logLevel);
		}
		if ( ! this.Enabled ) {
			return false;
		}
		if ( typeof this.LogLevelEnabled.get(logLevel) === 'boolean' ) {
			return this.LogLevelEnabled.get(logLevel);
		}
		return cmsjs.Logger.LogLevel.Instances.get(logLevel).Enabled;
	}
	
	/**
	 * Returns the flag(s) for the specified log level.
	 *
	 * This is for internal use only, called from _log().
	 */
	_getFlag(logLevel) {
		let flag = '';
		if ( this.LogLevelFlags.get(logLevel) ) {
			if ( flag.length > 0 ) { flag += ' '; }
			flag += this.LogLevelFlags.get(logLevel);
		}
			if ( flag.length > 0 ) { flag += ' '; }
		if ( this.Flag ) {
			flag += this.Flag;
		}
		if ( cmsjs.Logger.LogLevel.get(logLevel).Flag ) {
			if ( flag.length > 0 ) { flag += ' '; }
			flag += cmsjs.Logger.LogLevel.get(logLevel).Flag;
		}
		if ( flag !== '' ) { return flag; }
		return null;
	}

	/**
	 * Returns the style for the specified log level.
	 *
	 * This is for internal use only, called from _log().
	 */
	_getStyle(logLevel) {
		let style = '';
		if ( cmsjs.Logger.LogLevel.get(logLevel).Style ) {
			style += cmsjs.Logger.LogLevel.get(logLevel).Style;
		}
		if ( this.Style ) {
			style += this.Style;
		}
		if ( this.LogLevelStyle.get(logLevel) ) {
			style += this.LogLevelStyle.get(logLevel);
		}
		if ( style !== '' ) { return style; }
		return null;
	}
	
	/**
	 * Log something to the console.
	 *
	 * This should never be called directly. Use the logging functions instead.
	 *
	 * @private
	 */
	_log(logLevel, str, ...args) {
		if ( cmsjs.Logger.LogLevel.get(logLevel).Preprocessor ) {
			[str, ...args] = cmsjs.Logger.LogLevel.get(logLevel).Preprocessor(this, str, ...args);
		}
		if ( typeof str === 'string' ) {
			let prefix = '[' + this.ID + '] ' + '<' + logLevel + '>';
			const style = this._getStyle(logLevel);
			if ( style ) {
				prefix = '%c' + prefix;
				args.unshift(style);
			}
			str = prefix + ' ' + str;
			const flag = this._getFlag(logLevel);
			if ( flag ) {
				str = flag + ' ' + str;
			}
		}
		const consoleLevel = cmsjs.Logger.LogLevel.get(logLevel).ConsoleLevel;
		cmsjs.Logger.CONSOLE[consoleLevel](str,...args);
	}

}

/**
 * LogLevel subclass
 *
 * See the default definitions later in this file for usage.
 */
cmsjs.Logger.LogLevel = class {

	/**
	 * Collection of all log levels created.
	 */
	static Instances = new Map();

	/**
	 * Returns the specified log level.
	 */
	static get(id) {
		return cmsjs.Logger.LogLevel.Instances.get(id);
	}
	
	/**
	 * This class cannot do its own logging until after it the class
	 * is defined and default log levels have been created. So for
	 * some functions which need to be able to be called before any
	 * loggers have been created, defer to this function instead
	 * of calling the log directly.
	 */
	static _logIfPossible(logLevel, ...args) {
		if ( cmsjs.Logger.LogLevel.Log && cmsjs.Logger.LogLevel.Log[logLevel] ) {
			cmsjs.Logger.LogLevel.Log[logLevel](...args);
		}
	}
	
	/**
	 * Creates a new LogLevel and adds it to the Instances.
	 *
	 * This should always be used instead of the constructor, as it does
	 * sanity checking on the config variable.
	 */
	static create(id,config={}) {
		cmsjs.Logger.LogLevel._logIfPossible('trace','create(id,config)',id,config);
		if ( cmsjs.Logger.LogLevel.Instances.has(id) ) {
			cmsjs.Logger.LogLevel._logIfPossible('error','create(id,config) id exists:',id);
			return null;
		}

		Object.keys(config).forEach((configKey) => {
			const validConfig = ['ConsoleLevel','Enabled','Flag','Style','Processor'];
			if ( ! validConfig.includes(configKey) ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create(id,config) skipping invalid key in config:',configKey);
			}
		});

		// Whether logging is enabled by default
		if ( typeof config['Enabled'] !== 'undefined' ) {
			if ( typeof config['Enabled'] !== 'boolean' ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create() Enabled should be a boolean, not this:',config['Enabled']);
				config['Enabled'] = true;
			}
		}
		else {
			config['Enabled'] = true;
		}

		// Console logging level
		if ( config['ConsoleLevel'] ) {
			if ( ! cmsjs.Logger.CONSOLE[config['ConsoleLevel']] ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create() requested console log command not found:',config['ConsoleLevel']);
				config['ConsoleLevel'] = 'log';
			}
		}
		else {
			config['ConsoleLevel'] = 'log';
		}

		// Flag
		if ( config['Flag'] ) {
			if ( typeof config['Flag'] !== 'string' ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create() requested flag not a string:',config['Flag']);
				config['Flag'] = null;
			}
		}

		// Style
		if ( config['Style'] ) {
			if ( typeof config['Style'] !== 'string' ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create() requested style not a string:',config['Style']);
				config['Style'] = null;
			}
		}
		
		// Preprocessor
		if ( config['Preprocessor'] ) {
			if ( typeof config['Preprocessor'] !== 'function' ) {
				cmsjs.Logger.LogLevel._logIfPossible('warn','create() preprocessor is not a function:',config['Preprocessor']);
				config['Preprocessor'] = null;
			}
		}

		const newLogLevel = new cmsjs.Logger.LogLevel(id,config);
		cmsjs.Logger.LogLevel._logIfPossible('info','created new log level with id:',id);
		cmsjs.Logger.LogLevel.Instances.set(id,newLogLevel);
		cmsjs.Logger.Instances.forEach((logger,loggerID) => {
			logger.addLogLevel(id);
		});
		return newLogLevel;
	}

	/**
	 * Constructor. Do not call this directly.
	 *
	 * Always use the static create() method instead, which
	 * does error checking on config.
	 */
	constructor(id,config={}) {
		this.ID = id;
		this.ConsoleLevel = config['ConsoleLevel'] || 'log';
		this.Enabled = (typeof config['Enabled'] === 'boolean') ? config['Enabled'] : null;
		this.Flag = config['Flag'] || null;
		this.Style = config['Style'] || null;
		this.Preprocessor = config['Preprocessor'] || null;
	}
}


/*
 * Flag collection
 */
cmsjs.Logger.Flags = new Map();
cmsjs.Logger.Flags.set('Ant','\uD83D\uDC1C');
cmsjs.Logger.Flags.set('Fire','\uD83D\uDD25');
cmsjs.Logger.Flags.set('Water','\uD83D\uDCA7');
cmsjs.Logger.Flags.set('Bulb','\uD83D\uDCA1');
cmsjs.Logger.Flags.set('Splash','\uD83D\uDCA6');
cmsjs.Logger.Flags.set('Bell','\uD83D\uDD14');
cmsjs.Logger.Flags.set('Key','\uD83D\uDD11');
cmsjs.Logger.Flags.set('Lock','\uD83D\uDD12');
cmsjs.Logger.Flags.set('Wrench','\uD83D\uDD27');
cmsjs.Logger.Flags.set('Hammer','\uD83D\uDD28');
cmsjs.Logger.Flags.set('Magnet','\uD83E\uDDF2');
cmsjs.Logger.Flags.set('Clock','\uD83D\uDD50');
cmsjs.Logger.Flags.set('Timer','\u23F1');
cmsjs.Logger.Flags.set('Glass','\uD83D\uDD0D');
cmsjs.Logger.Flags.set('Skull','\uD83D\uDC80');
cmsjs.Logger.Flags.set('X','\u274C');
cmsjs.Logger.Flags.set('?','\u2753');
cmsjs.Logger.Flags.set('Pen','\uD83D\uDD8B');
cmsjs.Logger.Flags.set('Happy','\uD83D\uDE00');
cmsjs.Logger.Flags.set('Sad','\uD83D\uDE22');
cmsjs.Logger.Flags.set('Angry','\uD83D\uDE21');
cmsjs.Logger.Flags.set('Cry','\uD83D\uDE2D');
cmsjs.Logger.Flags.set('Crazy','\uD83E\uDD2A');
cmsjs.Logger.Flags.set('Sick','\uD83E\uDD22');
cmsjs.Logger.Flags.set('Cool','\uD83D\uDE0E');


/*
 * Default log levels
 */
cmsjs.Logger.LogLevel.create('trace', {
	ConsoleLevel: 'log',
	Enabled: false,
	Flag: cmsjs.Logger.Flags.get('Glass'),
	Style: 'color: #aaa;'
});
cmsjs.Logger.LogLevel.create('debug', {
	ConsoleLevel: 'debug',
	Enabled: false,
	Flag: cmsjs.Logger.Flags.get('Ant'),
	Style: 'font-weight: bold;'
});
cmsjs.Logger.LogLevel.create('info', {
	ConsoleLevel: 'info',
	Enabled: true,
	Flag: cmsjs.Logger.Flags.get('Pen'),
	Style: 'color: #33e;'
});
cmsjs.Logger.LogLevel.create('warn', {
	ConsoleLevel: 'warn',
	Enabled: true,
	Flag: cmsjs.Logger.Flags.get('Alert'),
	Style: 'color: #930;'
});
cmsjs.Logger.LogLevel.create('error', {
	ConsoleLevel: 'error',
	Enabled: true,
	Flag: cmsjs.Logger.Flags.get('X'),
	Style: 'color: #a00;'
});
cmsjs.Logger.LogLevel.create('fatal', {
	ConsoleLevel: 'error',
	Enabled: true,
	Flag: cmsjs.Logger.Flags.get('Skull'),
	Style: 'color: #a00; font-weight: bold;'
});
cmsjs.Logger.LogLevel.create('timer', {
	ConsoleLevel: 'log',
	Enabled: false,
	Flag: cmsjs.Logger.Flags.get('Timer'),
	Style: 'color: #090;',
	Preprocessor: (logger,str,...args) => {
		if ( typeof str !== 'string' ) {
			args = [str, ...args];
			str = '';
		}
		const now = (new Date()).getTime();
		const prev = logger.Time;
		const diff = now - prev;
		str += ' (' + diff + 'ms)';
		logger.Time = now;
		return [str,...args];
	}
});

/**
 * Create loggers for the cmsjs and cmsjs.Logger classes.
 *
 * The cmsjs.Logger class must be defined before loggers can be created.
 * Since the base class (cmsjs) must be defined before cmsjs.Logger it
 * cannot create its own logger; nor can cmsjs.Logger itself. So these
 * are created now.
 */
cmsjs.Logger.Log = cmsjs.Logger.create('cmsjs.Logger');
cmsjs.Log = cmsjs.Logger.create('cmsjs');
cmsjs.Logger.LogLevel.Log = cmsjs.Logger.create('cmsjs.Logger.LogLevel');

/**
 * State variables.
 */
cmsjs.State = class {

	static Log = cmsjs.Logger.create('cmsjs.State');

	static Map = new Map();
	
	static Listeners = new Map();

	/**
	 * Initialize a state variable.
	 *
	 * Call this to set a state variable without saving it and without
	 * triggering an onChange() event.
	 */
	static init(key,value) {
		cmsjs.State.Log.trace('init(key,value)',key,value);
		cmsjs.State.Map.set(key,value);
	}

	/**
	 * Get the value of a state variable.
	 */
	static get(key,arg) {
		return cmsjs.State.Map.get(key,arg)
	}

	/**
	 * Add a state change listener.
	 *
	 * By default, state change listeners fire whenever the value of
	 * state variable changes.
	 */
	static addListener(key, onChange) {
		cmsjs.State.Log.trace('addListener(key)',key);
		if ( ! cmsjs.State.Map.has(key) ) {
			cmsjs.State.Log.error('addListener(key,onChange) key not found:',key);
			return;
		}
		cmsjs.State.Listeners.set(key,onChange);
	}
	
	/**
	 * Add a state change listener.
	 *
	 * Call this to set a value without saving it.
	 *
	 * The third parameter is a boolean that controls whether or not to
	 * fire the listener. 'true' always fires, 'false' always does not,
	 * and if omitted it only fires if the new value is different.
	 */
	static set(key,value,fireOnChange=null) {
		cmsjs.State.Log.trace('set(key,value)',key,value);
		if ( ! cmsjs.State.Map.has(key) ) {
			cmsjs.State.Map.set(key,value);
		}
		else {
			const currentValue = cmsjs.State.Map.get(key);
			cmsjs.State.Map.set(key,value);
			if ( currentValue && cmsjs.State.Listeners.has(key) ) {
				if ( fireOnChange === true || ( fireOnChange !== false && currentValue !== value ) ) {
					cmsjs.State.Listeners.get(key)(value);
				}
			}
		}
	}

	/**
	 * Save a state variable.
	 *
	 * Saves a state variable to localStorage and/or browser-specific
	 * storage.
	 */
	static save(key,value) {
		cmsjs.State.Log.trace('save(key,value)',key,value);
		const jsonString = JSON.stringify(value);
		localStorage.setItem('cmsjs.State:' + key, jsonString);
		if ( typeof chrome !== 'undefined' && chrome && chrome.storage && chrome.storage.local ) {
			const chromeKey = key.replace('.','_');
			chrome.storage.local.set({ [chromeKey]: jsonString }, () => {
				//cmsjs.Log.debug('chrome settings saved chromeKey = ' + chromeKey + ' and jsonString = ',jsonString);
			});
		}
	}

	/**
	 * Set and save a state variable.
	 */
	static setAndSave(key,value) {
		cmsjs.State.Log.trace('setAndSave(key,value)',key,value);
		cmsjs.State.set(key,value);
		cmsjs.State.save(key,value);
	}

	/**
	 * Toggle a boolean state variable.
	 *
	 * This triggers the onChange() listener.
	 */
	static toggle(key) {
		cmsjs.State.Log.trace('toggle(key)',key);
		const value = cmsjs.State.get(key);
		if ( typeof value !== 'boolean' ) {
			cmsjs.Log.error('{cmsjs.State} not a boolean value: ',value);
			return undefined;
		}
		if ( value === true ) {
			cmsjs.State.change(key,false);
			return false;
		}
		else {
			cmsjs.State.change(key,true);
			return true;
		}
	}

	/**
	 * Toggle and save a boolean state variable.
	 *
	 * This triggers the onChange() listener.
	 */
	static toggleAndSave(key) {
		cmsjs.State.Log.trace('toggleAndSave(key)',key);
		const value = cmsjs.State.toggle(key);
		cmsjs.State.save(key,value);
	}

	/**
	 * Change a state variable.
	 *
	 * This behaves like set(), but triggers the onChange() listener
	 * regardless of whether or not the new value differs.
	 */
	static change(key,value) {
		cmsjs.State.Log.trace('change()',key,value);
		cmsjs.State.set(key,value,true);
	}

	/**
	 * Change and save state variable.
	 *
	 * This behaves like setAndSave(), but triggers the onChange() listener
	 * regardless of whether or not the new value differs.
	 */
	static changeAndSave(key,value) {
		cmsjs.State.Log.trace('changeAndSave(key,value)',key,value);
		cmsjs.State.change(key,value,true);
		cmsjs.State.save(key,value);
	}

	/**
	 * Load all user-defined state variables from localStorage.
	 */
	static loadLocalStorage(callback) {
		cmsjs.State.Log.trace('loadLocalStorage()');
		const mapArray = [null];
		cmsjs.State.NamespaceMaps.forEach((map,namespace) => { mapArray.push(namespace); });
		const allKeys = [];
		mapArray.forEach((namespace) => {
			const map = ( namespace ) ? cmsjs.State.NamespaceMaps.get(namespace) : cmsjs.State.DefaultMap;
			Array.from(map.keys()).forEach((key) => {
				const storageKey = ( namespace ) ? (namespace + '.' + key) : key;
				allKeys.push(storageKey);
			});
		});
		Array.from(cmsjs.State.Map.keys()).forEach((key) => {
			const val = localStorage.getItem('cmsjs.State:' + key);
			let parsedVal = null;
			if ( val !== undefined && val !== null ) {
				try {
					parsedVal = JSON.parse(val);
				}
				catch (error) {
					cmsjs.State.Log.error('loadLocalStorage() invalid JSON for key "' + key + ' ": ',val);
					cmsjs.State.Log.error(error);
				}
				cmsjs.State.Map.set(key,parsedVal);
				cmsjs.State.Log.debug('loadLocalStorage() set key/value:',key,parsedVal);
			}
		});
		callback();
	}

	/**
	 * Load all user-defined state variables from chrome.storage.
	 */
	static loadChromeStorage(callback) {
		cmsjs.State.Log.trace('loadChromeStorage()');
		const chromeKeys = [];
		const keymap = {};
		Array.from(cmsjs.State.Map.keys()).forEach((key) => {
			const chromeKey = key.replace(/\./g,'_');
			chromeKeys.push(chromeKey);
			keymap[chromeKey] = key;
		});
		chrome.storage.local.get(allKeys, (result) => {
			allKeys.forEach((key) => {
				// result is an array of all of the values of allKeys
				if ( key in result ) {
					const val = result[key];
					if ( val !== undefined ) {
						let parsedVal = null;
						try {
							parsedVal = JSON.parse(val);
						}
						catch (error) {
							cmsjs.State.Log.error('loadChromeStorage() invalid JSON for key "' + key + '": ',val);
							cmsjs.State.Log.error(error);
						}
						if ( key.includes('_') ) {
							const namespace = key.split('_')[0];
							const realKey = key.replace(namespace + '_','');
							const map = cmsjs.State.NamespaceMaps.get(namespace);
							map.set(realKey,parsedVal);
						}
						else {
							const map = cmsjs.State.DefaultMap;
							map.set(key,parsedVal);
						}
						cmsjs.State.Log.debug('loadChromeStorage() set ' + (key.replace('_','.')) + ': ',parsedVal);
					}
				}
			});
			callback();
		});
	}

	/**
	 * Set a cookie.
	 *
	 * 'name' and 'value' are mandatory; 'config' is optional
	 *
	 * Valid options for config are (not case-sensitive):
	 *
	 *   Domain		Defaults to current domain
	 *   Path		Defaults to current path
	 *   Expires	Expiration date (default none, expires at end of session)
	 *   Max-Age	Maximum age of cookie (defaut none, expires at end of session)
	 *		NOTE: If both Expires and Max-Age are specified, Expires will be ignored and only Max-Age will be used
	 *   Secure		Only send cookie over HTTPS (default false)
	 *   HttpOnly	Do not allow scripts to manipulate the cookie (default false)
	 *   SameSite	'Strict', 'Lax', or 'None' (default varies by browser)
	 *
	 * setCookie(name,value,{'Domain':'.example.com','Max-Age':30d,'Secure':true}
	 */
	setCookie(name, value, config={}) {
		cmsjs.State.Log.trace('setCookie(name,value)',name,value);
		let cookieString = name + '=' + encodeURIComponent(value);
		Object.entries(config).forEach(([key,value]) => {
			if ( key.toLowerCase() === 'expires' ) {
				const daysToExpire = value;
				const msToExpire = daysToExpire * 24 * 60 * 60 * 1000;
				const expirationDate = new Date(Date.now() + msToExpire).toUTCString();
				cookieString += '; Expires=' + expirationDate;
			}
			else if ( key.toLowerCase() === 'max-age' ) {
				const daysToExpire = value;
				const secsToExpire = daysToExpire * 24 * 60 * 60;
				cookieString += '; Max-Age=' + secsToExpire;
			}
			else if ( key.toLowerCase() === 'domain' ) {
				cookieString += '; Domain=' + value;
			}
			else if ( key.toLowerCase() === 'path' ) {
				cookieString += '; Path=' + value;
			}
			else if ( key.toLowerCase() === 'secure' && value === true ) {
				cookieString += '; Secure';
			}
			else if ( key.toLowerCase() === 'httponly' && value === true ) {
				cookieString += '; HttpOnly';
			}
			else if ( key.toLowerCase() === 'samesite' ) {
				cookieString += '; SameSite=' + value;
			}
		});
		cmsjs.State.Log.debug('setting cookie: ',cookieString);
		document.cookie = cookieString;
	}

	/**
	 * Get a cookie value by name.
	 */
	getCookie(name) {
		cmsjs.State.Log.trace('getCookie(name)',name);
		const cookies = document.cookie.split(';');
		for ( const cookie of cookies ) {
			const [cookieName, cookieValue] = cookie.trim().split('=');
			if ( cookieName === name ) {
				return decodeURIComponent(cookieValue);
			}
		}
		return null;
	}

}


/**
 * Console.
 *
 * This is the base of the UI.
 */
cmsjs.Console = class {

	static Log = cmsjs.Logger.create('cmsjs.Console');

	static Instances = new Map();

	static Initializers = new Map();
	
	static setActiveConsole(id) {
		cmsjs.Console.Log.trace('setActiveConsole(id)',id);
		if ( ! cmsjs.Console.Instances.has(id) ) {
			cmsjs.Console.Log.error('setActiveConsole(id) id not found:',id);
		}
		const console = cmsjs.Console.Instances.get(id);
		document.documentElement.replaceWith(console.elements.html);
		// option 2
		//const newDoc = document.implementation.createHTMLDocument();
		//newDoc.documentElement.replaceWith(console.elements.html);
		//document.replaceChild(newDoc.documentElement, document.documentElement);
	}

	static create(id,type='default') {
		cmsjs.Console.Log.trace('create(id,type)',id,type);
		if ( cmsjs.Console.Instances.has(id) ) {
			cmsjs.Console.Log.error('constructor() id exists:',id);
		}
		else if ( ! cmsjs.Console.Initializers.has(type) ) {
			cmsjs.Console.Log.error('constructor() no init function found for type:',id,type);
		}
		const instance = new cmsjs.Console(id,type);
		cmsjs.Console.Instances.set(id,instance);
		return instance;
	}

	constructor(id,initializer) {
		this.log = cmsjs.Logger.create('cmsjs.Console(' + id + ')');
		this.id = id;
		this.initializer = initializer;
		this.config = { };
		this.elements = { };
		this.initialized = false;
		this.rendered = false;
		this.root = null;
		this.panels = new Map();
		this.initialize();
	}

	initialize() {
		this.log.trace('initialize()');
		const html = document.createElement('html');
		const head = document.createElement('head');
		const body = document.createElement('body');
		const root = document.createElement('div');
		html.appendChild(head);
		html.appendChild(body);
		body.appendChild(root);
		this.elements.html = html;
		this.elements.head = head;
		this.elements.body = body;
		this.root = root;
		html.setAttribute('cmsjs-html','');
		head.setAttribute('cmsjs-head','');
		body.setAttribute('cmsjs-body','');
		root.setAttribute('cmsjs-console','');
		root.setAttribute('cmsjs-console-id',this.id);
		const initializer = cmsjs.Console.Initializers.get(this.initializer);
		initializer(this);
		this.initialized = true;
	}

	/**
	 * Configure grid rows.
	 */
	setGridRows(str) {
		this.log.trace('setGridRows(str)',str);
		this.root.style.gridTemplateRows = str;
	}
	
	/**
	 * Configure grid columns.
	 */
	setGridColumns(str) {
		this.log.trace('setGridColumns(str)',str);
		this.root.style.gridTemplateColumns = str;
	}

	
	setTitle(str) {
		this.log.trace('setTitle(str)',str);
		if ( ! this.elements.title ) {
			const title = document.createElement('title');
			title.setAttribute('cmsjs-title','');
			this.elements.title = title;
			this.elements.head.appendChild(title);
		}
		this.elements.title.innerHTML = str;
		this.config.title = str;
	}

	setFavicon(str) {
		this.log.trace('setFavicon()',str);
		if ( ! this.elements.favicon ) {
			const link = document.createElement('link');
			link.setAttribute('cmsjs-favicon','');
			link.setAttribute('rel','icon');
			this.elements.favicon = link;
			this.elements.head.appendChild(this.elements.favicon);
		}
		this.elements.favicon.setAttribute('href','data:image/x-icon;base64,' + str);
		this.config.favicon = str;
	}

	addStyles(arg) {
		this.log.trace('addStyles()',arg);
		const args = ( typeof arg === 'string' ) ? [arg] : arg;
		args.forEach((str) => { this.addStyle(str); });
	}
	
	addStyle(str) {
		this.log.trace('addStyle()',str);
		if ( ! this.elements.style ) {
			const style = document.createElement('style');
			style.setAttribute('cmsjs-style','');
			this.elements.style = style;
			this.elements.head.appendChild(this.elements.style);
		}
		this.elements.style.innerHTML += str + '\n';
		this.config.style += str + '\n';
	}
	
	addStylesheets(arg) {
		this.log.trace('addStylesheets()',arg);
		const args = ( typeof arg === 'string' ) ? [arg] : arg;
		args.forEach((str) => { this.addStyleheet(str); });
	}
	
	addStylesheet(str) {
		this.log.trace('addStylesheet()',str);
		const link = document.createElement('link');
		elt.setAttribute('cmsjs-stylesheet','');
		link.setAttribute('rel','stylesheet');
		link.setAttribute('href',str);
		this.elements.head.appendChild(link);
	}

	/**
	 * Adds a <script> tag to the <head> element.
	 */
	addScript(str) {
		this.log.trace('addScripts()',str);
		const script = document.createElement('script');
		script.innerHTML = str;
		this.elements.head.appendChild(script);
	}

	/**
	 * Adds a <script> tag to the <head> element.
	 */
	addScriptLink(url) {
		this.log.trace('addScriptLink()',url);
		const script = document.createElement('script');
		script.setAttribute('cmsjs-script','');
		script.src = url;
		this.elements.head.appendChild(script);
	}
	
	/**
	 * Adds a <script> tag just above the </body> tag.
	 */
	addLazyScript(str) {
		this.log.trace('addLazyScript()',str);
		const script = document.createElement('script');
		script.setAttribute('cmsjs-script','');
		script.innerHTML = str;
		this.elements.body.insertAdjacentElement('beforeend',script);
	}
	
	/**
	 * Adds a <script> tag just above the </body> tag.
	 */
	addLazyScriptLink(url) {
		this.log.trace('addLazyScriptLink()',url);
		script.setAttribute('cmsjs-script','');
		const script = document.createElement('script');
		script.src = url;
		this.elements.body.insertAdjacentElement('beforeend',script);
	}
	
	/**
	 * Adds a panel to the console.
	 *
	 * Note that this does not attach the panel to anything.
	 * For that you must call attachPanel().
	 */
	addPanel(id,config) {
		this.log.trace('addPanel(id)',id);
		const panel = cmsjs.Console.Panel.create(id,config);
		this.panels.set(id,panel);
		return panel;
	}

	/**
	 * Attach a panel the console.
	 *
	 * The panel must have already been created an initialized with
	 * grid position and span data.
	 */
	attachPanel(id) {
		cmsjs.Console.Log.trace('attachPanel(id)',id);
		const panel = this.panels.get(id);
		panel.element.style.gridRow = panel.row + ' / span ' + panel.rowSpan;
		panel.element.style.gridColumn = panel.col + ' / span ' + panel.colSpan;
		this.root.appendChild(panel.element);
	}

	/**
	 * Removes the specified panel from the DOM.
	 */
	removePanel(id) {
		cmsjs.Console.Log.trace('removePanel()',id);
		const panel = this.panels.get(id);
		panel.remove();
	}
	
	/**
	 * Removes all panels from the DOM.
	 */
	removeAllPanels() {
		cmsjs.Console.Log.trace('removeAllPanels()');
		Array.from(this.panels).forEach((panel) => { panel.remove(); });
	}

}


/**
 * Panel class.
 */
cmsjs.Console.Panel = class {

	static Log = cmsjs.Logger.create('cmsjs.Console.Panel');

	static Instances = new Map();

	static create(id,config) {
		cmsjs.Console.Panel.Log.trace('create(id)',id);
		const newPanel = new cmsjs.Console.Panel(id);
		if ( config ) {
			const parts = config.split(',');
			if ( parts.length === 2 ) {
				[newPanel.row, newPanel.col] = config.split(',');
				[newPanel.rowSpan, newPanel.colSpan] = [1,1];
			}
			else if ( parts.length === 4 ) {
				[newPanel.row,newPanel.col,newPanel.rowSpan,newPanel.colSpan] = config.split(',');
			}
		}
		cmsjs.Console.Panel.Instances.set(id,newPanel);
		return newPanel;
	}
	
	constructor(id) {
		cmsjs.Console.Panel.Log.trace('constructor(id)',id);
		this.log = cmsjs.Logger.create('cmsjs.Console.Panel(' + id + ')');
		this.element = document.createElement('div');
		this.element.setAttribute('cmsjs-panel','');
		this.element.setAttribute('cmsjs-panel-id',id);
		this.element.style.overflow = 'hidden';
		this.element.style.boxSizing = 'border-box';
	}
	
	setPosition(row,col) {
		this.log.trace('setPosition()',row,col);
		if ( arguments.length === 1 ) {
			if ( row.includes(',') ) { [row,col] = row.split(','); }
		}
		this.row = row;
		this.col = col;
	}
	setSpan(rows,cols) {
		this.log.trace('setSpan()',rows,cols);
		if ( arguments.length === 1 ) {
			if ( row.includes(',') ) { [row,col] = row.split(','); }
		}
		this.rowSpan = rows;
		this.colSpan = cols;
	}
	getElement() {
		this.log.trace('getElement()');
		return this.element;
	}
	hasComponent(id) {
		this.log.trace('hasComponent()',id);
		const component = cmsjs.Component.get(id);
		const componentPanel = component.parentElement.parentElement;
		if ( componentPanel === this.element ) { return true; }
		return false;
	}
	attachComponent(id) {
		this.log.trace('attachComponent()',id);
		const component = cmsjs.Component.get(id);
		component.remove();
		component.hide();
		component.build();
		component.show();
		this.element.appendChild(component.getWrapper());
	}
	showOnlyComponent(id) {
		this.log.trace('showOnlyComponent()',id);
		if ( ! this.hasComponent(id) ) { return; }
		const component = cmsjs.Component.get(id);
		if ( ! component.isBuilt() ) { component.build(); }
		this.hideAllComponents();
		component.show();
	}
	hideAllComponents() {
		this.log.trace('hideAllComponents()');
		if ( panel.children ) {
			Array.from(panel.children).forEach((item) => {
				item.style.display = 'none';
			});
		}
	}
	hideComponent(id) {
		this.log.trace('hideComponent()',id);
		if ( this.hasComponent(id) ) {
			this.component.parentElement.style.display = 'none';
		}
	}
	removeComponent(id) {
		this.log.trace('removeComponent()',id);
		const component = cmsjs.Component.get(id);
		component.remove();
	}
	clear() {
		this.log.trace('clear()');
		this.element.innerHTML = '';
	}
}

cmsjs.Console.Initializers.set('default', (instance) => {
	// do nothing by default
});

cmsjs.Console.Initializers.set('grid', (instance) => {
	const html = instance.elements.html;
	html.style.margin = '0px';
	html.style.padding = '0px';
	html.style.height = '100%';
	html.style.overflow = 'hidden';
	const body = instance.elements.body;
	body.style.margin = '0px';
	body.style.padding = '0px';
	body.style.height = '100%';
	body.style.overflow = 'hidden';
	const root = instance.root;
	root.style.boxSizing = 'border-box';
	root.style.display = 'grid';
	root.style.height = '100%';
	root.style.height = '100%';
	instance.addStyle('* { box-sizing: border-box; }');
	instance.addStyle('a { color: inherit; text-decoration: none; }');
	instance.addStyle('a:hover { text-decoration: underline; }');
	instance.addStyle('::-webkit-color-swatch { border: none; }');
	instance.addStyle('::-webkit-color-swatch-wrapper { padding: 1px 0px; }');
	instance.addStyle('::-moz-color-swatch { border: none; }');
});

/**
 * Component class.
 */
cmsjs.Component = class {
	
	static Log = cmsjs.Logger.create('cmsjs.Component');

	static Instances = new Map();

	static get(id) {
		const instance = cmsjs.Component.Instances.get(id);
		return instance;
	}

	static create(id, buildFunction = function() {}) {
		cmsjs.Component.Log.trace('create(id)',id);
		if ( cmsjs.Component.Instances.has(id) ) {
			cmsjs.Component.Log.error('create(id) id exists:',id);
			return null;
		}
		const instance = new cmsjs.Component(id,buildFunction);
		cmsjs.Component.Instances.set(id,instance);
		return instance;
	}
	
	constructor(id, buildFunction = function() {}) {
		cmsjs.Component.Log.trace('constructor(id)',id);
		this.id = id;
		this.log = cmsjs.Logger.create('cmsjs.Component(' + id + ')');
		this.buildFunction = buildFunction;
		this.wrapper = document.createElement('div');
		this.wrapper.setAttribute('cmsjs-component-wrapper','');
		this.element = document.createElement('div');
		this.element.setAttribute('cmsjs-component','');
		this.element.setAttribute('cmsjs-component-id',id);
		this.wrapper.appendChild(this.element);
	}
	
	setScrollable(xScroll,yScroll) {
		this.log.trace('setScrollable(x,y)',xScroll,yScroll);
		if ( arguments.length === 0 ) {
			xScroll = true;
			yScroll = true;
		}
		else if ( arguments.length == 1 ) {
			yScroll = xScroll;
		}
		if ( xScroll || yScroll ) {
			//wrapper.style.padding = '1rem';
			this.wrapper.setAttribute('cmsjs-component-scroller','');
			this.wrapper.style.height = '100%';
			this.wrapper.style.overflow = 'auto';
			//component.style.height = '100%';
		}
		else {
			this.wrapper.style.padding = '';
			this.wrapper.removeAttribute('cmsjs-component-scroller');
			this.wrapper.style.height = '';
			this.wrapper.style.overflow = '';
		}
	}
	
	getWrapper() {
		return this.wrapper;
	}
	
	getElement() {
		return this.element;
	}
	
	isBuilt() {
		if ( this.element.getAttribute('cmsjs-component-built') ) { return true; }
		return false;
	}
	
	build() {
		this.log.trace('build()');
		this.element.innerHTML = '';
		this.buildFunction(this);
		// Add a dummy spacer if this is a scrollable element. This is ugly.
		// If there is an easy way to get the desired padding at the bottom
		// of a scrollable component, I cannot find it...
		if ( this.wrapper.hasAttribute('cmsjs-component-scroller') ) {
			const dummy = $.ce('div');
			dummy.style.visibility = 'hidden';
			dummy.setAttribute('cmsjs-dummy-spacer','');
			dummy.style.paddingBottom = 'inherit';
			this.element.appendChild(dummy);
		}
		this.element.setAttribute('cmsjs-component-built','');
	}
	
	hide() {
		this.log.trace('hide()');
		this.wrapper.style.display = 'none';
	}
	
	show() {
		this.log.trace('show()');
		if ( ! this.isBuilt() ) { this.build(); }
		this.wrapper.style.display = 'block';
	}
	
	remove() {
		this.log.trace('remove()');
		this.wrapper.remove();
	}
}




/**
 * Interactive controls
 */
cmsjs.Controls = class {

	static Log = cmsjs.Logger.create('cmsjs.Controls');

	static Instances = new Map();
	
	static Creators = new Map();

	constructor(id,type,config) {
		this.id = id;
		this.type = type;
		this.config = config;
		const creatorFunction = cmsjs.Controls.Creators.get(type);
		creatorFunction(config);
	}
	
	static create(id,type,config={}) {
		if ( ! id ) {
			cmsjs.Controls.Log.debug('create(id) called with no id specified');
			return;
		}
		if ( ! type ) {
			cmsjs.Controls.Log.error('create(id) no type specified for id:',id);
			return;
		}
		if ( cmsjs.Controls.Instances.has(id) ) {
			cmsjs.Controls.Log.error('create(id) id exists:',id);
			return;
		}
		const creatorFunction = cmsjs.Controls.Creators.get(type);
		if ( ! creatorFunction ) {
			cmsjs.Controls.Log.error('no creator function found for type <' + type + '> (id: ' + id + ')');
		}
		cmsjs.Controls.Log.debug('creating <' + type + '> with id "' + id + '"');
		const control = new cmsjs.Controls(id,type,config);
		if ( ! control ) {
			cmsjs.Controls.Log.error('creator function did not return a control for type: ',type);
		}
		//const control = new Control(id,type,config);
		cmsjs.Controls.Instances.set(id,control);
		control.setAttribute('cmsjs-control-type',type);
		//cmsjs.Controls.Log.debug('{cmsjs.Controls} created <' + type + '> with id "' + id + '"');
		return control;
	}

	static get(id) {
		return cmsjs.Controls.Instances.get(id);
	}

	static getValue(id) {
		const control = cmsjs.Controls.Instances.get(id);
		const controlType = control.getAttribute('cmsjs-control-type');
		if ( controlType === 'toggle' ) {
			const input = control.querySelector('input');
			return input.checked;
		}
		else if ( controlType.startsWith('select') ) {
			const select = control.querySelector('select');
			return select.options[select.selectedIndex].textContent;
		}
		else if ( controlType.startsWith('input') ) {
			const input = control.querySelector('input');
			return input.value;
		}
	}

	static getLabelElement(id) {
		const control = cmsjs.Controls.get(id);
		const label = cmsjs.Controls.querySelector 
	}

	// Set a control value
	static setValue(id,value) {
		const control = cmsjs.Controls.get(id);
		const controlType = control.getAttribute('cmsjs-control-type');
		if ( controlType === 'toggle' ) {
			const slider = control.querySelector('.cmsjs-control-toggle-slider');
			const input = control.querySelector('input');
			if ( value === true && ! input.checked ) {
				input.click();
			}
			else if ( value === false && input.checked ) {
				input.click();
			}
		}
		else if ( controlType.startsWith('select') ) {
			const select = control.querySelector('select');
			for ( let i = 0; i < select.options.length; i++ ) {
				if ( select.options[i].text === value.toString() ) {
					if ( select.selectedIndex !== i ) {
						select.selectedIndex = i;
						select.dispatchEvent(new Event('change'));
					}
					break;
				}
			}
		}
		else if ( controlType.startsWith('input') ) {
			const input = control.querySelector('input');
			const currentValue = input.value;
			if ( currentValue !== value ) {
				input.value = value;
				input.dispatchEvent(new Event('change'));
			}
		}
	}
	
	// Toggles a boolean control.
	static toggle(id) {
		const control = cmsjs.Controls.get(id);
		if ( ! control.getAttribute('cmsjs-control-type') === 'toggle' ) {
			cmsjs.Controls.Log.error('{cmsjs.Controls} attempt to toggle non-tobblable control: ',control);
			return;
		}
		const input = control.querySelector('input');
		input.click();
	}

	// Utility function to set up listeners through the config mapping
	static addListeners(element,config) {
		for ( const key in config ) {
			if ( key.startsWith('listener:') ) {
				const action = key.split(':')[1];
				const functionToRun = config[key];
				element.addEventListener(action, (event) => {
					functionToRun(event);
				});
			}
		}
	}

	static appendLabel(control,config) {
		const wrapper = document.createElement('div');
		wrapper.setAttribute('cmsjs-control','');
		const controlID = 'cmsjs-control-' + Math.random().toString(36).substring(2,8);
		control.id = controlID;
		const labelText = config['label'] || config['text'];
		if ( ! labelText ) {
			wrapper.append(control);
			return wrapper;
		}
		wrapper.style.display = 'flex';
		wrapper.style.alignItems = 'center';
		const labelPosition = config['label-position'] || 'left';
		const label = document.createElement('div');
		label.setAttribute('cmsjs-control-label','');
		label.setAttribute('for',controlID);
		label.style.whiteSpace = 'nowrap';
		label.innerHTML = labelText;
		if ( labelPosition === 'left' ) {
			label.style.display = 'inline-block';
			label.style.marginRight = '1em';
			wrapper.style.justifyContent = 'flex-end';
			wrapper.append(label);
			wrapper.append(control);
		}
		else if ( labelPosition === 'right' ) {
			label.style.display = 'inline-block';
			label.style.marginLeft = '1em';
			wrapper.style.justifyContent = 'flex-start';
			wrapper.append(control);
			wrapper.append(label);
		}
		else if ( labelPosition === 'top-left' ) {
			label.style.marginBottom = '0.25em';
			wrapper.style.flexDirection = 'column';
			wrapper.style.alignItems = 'flex-start';
			wrapper.append(label);
			wrapper.append(control);
		}
		else if ( labelPosition === 'top-right' ) {
			label.style.marginBottom = '0.25em';
			wrapper.style.flexDirection = 'column';
			wrapper.style.alignItems = 'flex-end';
			wrapper.append(label);
			wrapper.append(control);
		}
		return wrapper;
	}

}

/*
 * Shortcut to get a drop-down box of integers.
 */
cmsjs.Controls.Creators.set('select:integer', (config) => {
	const min = config['min'] || 1;
	const max = config['max'] || 10;
	const array = [];
	for ( let i = min; i < max + 1; i++ ) {
		array.push(i);
	}
	config['items'] = array;
	config['inline'] = true;
	config['datatype'] = 'integer';
	const element = cmsjs.Controls.Creators.get('select')(config);
	return element;
});

/*
 * Drop-down select box
 */
cmsjs.Controls.Creators.set('select', (config) => {
	const text = config['text'] || '';
	const height = config['height'] || 16;
	const width = config['width'] || 3*height;
	const selected = config['value'] || null;
	const tabindex = config['tabindex'] || null;
	const datatype = config['datatype'] || 'string';
	let items = config['items'] || {};
	if ( Array.isArray(items) ) {
		items = items.reduce((map, item) => { map[item] = item; return map; }, {});
	}
	const borderRadius = height/2;
	const onChange = config['listener:change'];
	const select = $.ce('select');
	select.style.borderRadius = '0.2em';
	select.style.fontSize = 'inherit';
	select.style.color = 'inherit';
	select.style.backgroundColor = 'inherit';
	if ( tabindex ) { select.setAttribute('tabindex',tabindex); }
	Object.keys(items).forEach((key) => {
		const option = document.createElement('option');
		option.value = items[key];
		option.style.fontSize = 'inherit';
		option.style.color = 'black';
		option.style.backgroundColor = 'white';
		option.textContent = key;
		if ( items[key] == selected ) { option.selected = true; }
		select.append(option);
	});
	if ( onChange ) {
		select.addEventListener('change', (event) => {
			//changeState(id,event.target.value);
			let selectedValue = event.target.value;
			if ( datatype === 'integer' ) {
				selectedValue = parseInt(selectedValue);
			}
			else if ( datatype === 'boolean' ) {
				selectedValue = selectedValue ? true : false;
			}
			onChange(selectedValue);
		});
	}
	const wrapper = cmsjs.Controls.appendLabel(select,config);
	return wrapper;
});

/*
 * General-purpose toggle switch creator.
 * Used by drawControls().
 *
 * <div class='toggle'>
 *   <label class='switch'>  
 *   <input type='checkbox'>
 *   <span class='slider'>  NOTE: color/bgColor on this affects the GROOVE
 * </div>
 */
cmsjs.Controls.Creators.set('toggle', (config) => {
	const labelText = config['text'] || '';
	const tabindex = config['tabindex'] || null;
	const height = config['height'] || 12;
	const width = config['width'] || 3*height;
	const isOn = ( config['value'] === true || config['value'] === 'on' );
	const onChange = config['listener:change'];
	const sliderOnColor = config['slider-on-color'] || '#acf';
	const sliderOffColor = config['slider-off-color'] || '#bbb';
	const buttonOnColor = config['button-on-color'] || '#3af';
	const buttonOffColor = config['button-off-color'] || '#999';
	const sliderOnThemeValue = 	config['slider-on-theme-value'];
	const sliderOffThemeValue = config['slider-off-theme-value'];
	const buttonOnThemeValue = config['button-on-theme-value'];
	const buttonOffThemeValue = config['button-off-theme-value'];
	const borderRadius = height/2;
	const input = $.ce('input')
	input.setAttribute('type','checkbox');
	if ( tabindex ) { input.setAttribute('tabindex',tabindex); }
	const wrapper = cmsjs.Controls.appendLabel(input,config);
	const toggleType = 'switch';
	if ( toggleType === 'switch' ) {
		const dummy = $.ce('label');
		wrapper.append(dummy);
		const slider = $.ce('span');
		slider.classList.add('cmsjs-control-toggle-slider');
		dummy.append(input);
		dummy.append(slider);
		dummy.style.position = 'relative';
		dummy.style.display = 'inline-block';
		dummy.style.display = 'inline-block';
		dummy.style.width = width + 'px';
		dummy.style.height = height + 'px';
		input.style.opacity = '0';
		input.style.width = '0';
		input.style.height = '0';
		slider.style.position = 'absolute';
		slider.style.cursor = 'pointer';
		slider.style.top = '0';
		slider.style.left = '0';
		slider.style.right = '0';
		slider.style.bottom = '0';
		slider.style.transition = sliderOffColor;
		slider.style.borderRadius = borderRadius + 'px';
		if (isOn) {
			slider.style.backgroundColor = sliderOnColor;
			input.setAttribute('checked','');
		}
		else {
			slider.style.backgroundColor = sliderOffColor;
			input.removeAttribute('checked');
		}
		input.addEventListener('change', function() {
			//changeState(id,this.checked);
			onChange(this.checked);
			if (this.checked) { slider.style.backgroundColor = sliderOnColor }
			else { slider.style.backgroundColor = sliderOffColor }
		});
		// Pseudo-elements cannot be set using javascript... :/
		const styleElt = $.ce('style');
		const buttonDiameter = (4/3) * height;
		const translate = width - buttonDiameter;
		const bottomShift = (buttonDiameter - height)/2;
		styleElt.innerHTML = `
			.cmsjs-control-toggle-slider:before {
				content: "";
				position: absolute;
				height: ${buttonDiameter}px;
				width: ${buttonDiameter}px;
				left: 0px;
				bottom: -${bottomShift}px;
				transition: .25s;
				border-radius: 50%;
			}
			input:checked + .cmsjs-control-toggle-slider:before {
				transform: translateX(${translate}px);
			}
			`;
		wrapper.append(styleElt);
		const colorStyleElt = $.ce('style');
		colorStyleElt.classList.add('cmsjs-control-toggle-colors');
		const buttonColorOn = buttonOnColor;
		const buttonColorOff = buttonOffColor;
		colorStyleElt.innerHTML = `
			.cmsjs-control-toggle-slider:before { background-color: ${buttonColorOff}; }
			input:checked + .cmsjs-control-toggle-slider:before { background-color: ${buttonColorOn}; }
		`;
		wrapper.append(colorStyleElt);
	}
	return wrapper;
});


/*
 * Color Selector
 */
cmsjs.Controls.Creators.set('input:color', (config) => {
	const label = config['label'] || '';
	let value = config['value'] || '#ffffff';
	if ( value.length === 4 && value[0] === '#' ) { value = '#' + value[1] + value[1] + value[2] + value[2] + value[3] + value[3]; }
	const inputElt = document.createElement('input');
	inputElt.type = 'color';
	inputElt.value = value;
	cmsjs.Controls.addListeners(inputElt,config);
	const wrapper = cmsjs.Controls.appendLabel(inputElt,config);
	return wrapper;
});

/*
 * Text Input
 */
cmsjs.Controls.Creators.set('input:text', (config) => {
	const label = config['label'] || '';
	const value = config['value'] || '';
	const placeholder = config['placeholder'] || '';
	const inputElt = $.ce('input');
	inputElt.type = 'text';
	inputElt.value = value;
	inputElt.placeholder = placeholder;
	inputElt.style.width = '100%';
	inputElt.style.boxSizing = 'border-box';
	inputElt.style.fontSize = 'inherit';
	const wrapper = cmsjs.Controls.appendLabel(inputElt,config);
	cmsjs.Controls.addListeners(inputElt,config);
	return wrapper;
});

cmsjs.Database = class {

	static Log = cmsjs.Logger.create('cmsjs.Database');

	static Instances = new Map();

	static create(id) {
		if ( cmsjs.Database.Instances.has(id) ) {
			cmsjs.Database.Log.error('create(id) id exists:',id);
			return;
		}
		const instance = new cmsjs.Database(id);
		cmsjs.Database.Instances.set(id,instance);
		return instance;
	}

	constructor(id) {
	}

	addDataSource() {
	}

	addDataURL(id,url) {
	}

	addData(id,data) {
	}

	addRemoteData(id,url) {
	}

	getData(id) {
	}

	getRemoteData(id) {
	}
	
	static processStringKeys(data,lang,prefix) {
	    if (Array.isArray(data)) {
		    data.forEach(item => processStringKeys(item,lang,prefix));
	    }
		else if (typeof data === 'object' && data !== null) {
			Object.entries(data).forEach(([k,v]) => {
				if (typeof v === 'string' && v.startsWith(prefix) && lang[v.slice(2)] !== null ) {
	                data[k] = lang[v.slice(2)];
		        } else {
			        processStringKeys(v,lang,prefix);
				}
	        });
	    }
	}
	
}

/**

Taken from the old mod.DB file
	
export async function getData(module,id,lang='en') {
    self.log.track('<get-data>',arguments);
    if ( ! module ) {
        self.error('missing parameter: module');
    }
    const dataRoot = module.getRealURL() + 'DB'
    const dataDir = dataRoot + '/' + id;
    const dataURL = dataDir + '/' + 'data.json';
    const langURL = dataDir + '/' + 'data.lang.' + lang + '.json';
    let t0 = self.log.timer();
    let data = await fetch(dataURL).then((r) => r.json());
    //t0 = self.log.timer('[loaded data]',t0);
    let langStrings = await fetch(langURL).then((r) => r.json());
    //t0 = self.log.timer('[loaded strings]',t0);
    processStringKeys(data,langStrings,'$.');
    //self.log.timer('[applied strings]',t0);
    self.log.trackout('<get-data>');
    return data;
}

*/
/**
 * Dictionary class.
 *
 * This class provides a framework for internationalization.
 * Dictionarires are uniquely defined by their titles. Each
 * dictionary has its own set of languages that it can accomodate.
 */
cmsjs.Dictionary = class {

	static Log = cmsjs.Logger.create('cmsjs.Dictionary');
	
	static Instances = new Map();

	static Languages = new Map();

	static PreferredLanguageStateVariable = 'cmsjs.Dictionary.PreferredLanguage';

	static get(id,language) {
		const instance = cmsjs.Dictionary.Instances.get(id);
		if ( ! language ) {
			return instance;
		}
		else {
			return instance.getLanguageMap(language);
		}
	}

	static create(id) {
		cmsjs.Dictionary.Log.trace('create(id)',id);
		if ( cmsjs.Dictionary.Instances.has(id) ) {
			cmsjs.Dictionary.Log.error('create(id) dictionary already exists for id:',id);
		}
		const instance = new cmsjs.Dictionary(id);
		cmsjs.Dictionary.Instances.set(id,instance);
		return instance;
	}

	constructor(id) {
		cmsjs.Dictionary.Log.trace('constructor(id)',id);
		this.log = cmsjs.Logger.create('cmsjs.Dictionary(' + id + ')');
		this.id = id;
		this.languages = new Map();
		this.rootLanguage = null;
		this.fallback = null;
	}

	setRootLanguage(language) {
		this.log.trace('getLanguageList()');
		this.rootLanguage = language;
	}
	
	getLanguageList() {
		this.log.trace('getLanguageList()');
		return Array.from(this.languages.keys());
	}

	getLanguageStringList() {
		this.log.trace('getLanguageStringList()');
		const list = [];
		Array.from(this.languages.keys()).forEach((item) => {
			list.push(cmsjs.Dictionary.Languages[item]);
		});
		return list;
	}

	addLanguage(language) {
		this.log.trace('addLanguage(language)',language);
		if ( ! cmsjs.Dictionary.Languages.has(language) ) {
			this.log.error('addLanguage(language) language definition not found:',language);
			return;
		}
		if ( this.languages.has(language) ) {
			this.log.error('constructor(language) language already exists:',language);
			return;
		}
		this.languages.set(language,new Map());
	}

	addLanguages(array) {
		array.forEach((item) => {
			this.addLanguage(item);
		});
	}

	getLanguageMap(language) {
		this.log.trace('getLanguageMap(language)',language);
		if ( ! this.languages.has(language) ) {
			this.log.error('getLanguageMap(language) language does not exist in dictionary:',language);
			return undefined;
		}
		return this.languages.get(language);
	}

	hasLanguage(language) {
		return this.languages.has(language);
	}
	
	get(key,language) {
		this.log.trace('get(key,language)',key,language);
		if ( ! this.hasLanguage(language) ) {
			this.log.error('get(key,language) language does not exist in dictionary:',language);
		}
		return this.languages.get(language).get(key,key);
	}

}

cmsjs.Dictionary.Languages.set('ar','\u0627\u0644\u0639\u0631\u0628\u064a\u0629'); // Arabic
cmsjs.Dictionary.Languages.set('bn','\u09ac\u09be\u0982\u09b2\u09be'); // Bengali
cmsjs.Dictionary.Languages.set('de','Deutsch'); // German
cmsjs.Dictionary.Languages.set('en','English'); // English
cmsjs.Dictionary.Languages.set('es','Espa\u00f1ol'); // Spanish
cmsjs.Dictionary.Languages.set('fr','Fran\u00e7ais'); // French
cmsjs.Dictionary.Languages.set('hi','\u0939\u093f\u0928\u094d\u0926\u0940'); // Hindi
cmsjs.Dictionary.Languages.set('id','Indonesia'); // Indonesian
cmsjs.Dictionary.Languages.set('it','Italiano'); // Italian
cmsjs.Dictionary.Languages.set('ja','\u65e5\u672c\u8a9e'); // Japanese
cmsjs.Dictionary.Languages.set('ko','\ud55c\uad6d\uc5b4'); // Korean
cmsjs.Dictionary.Languages.set('ms','Bahasa Melayu'); // Malay
cmsjs.Dictionary.Languages.set('nl','Nederlands'); // Dutch
cmsjs.Dictionary.Languages.set('pt','Portugu\u00eas'); // Portuguese
cmsjs.Dictionary.Languages.set('ru','\u0420\u0443\u0441\u0441\u043a\u0438\u0439'); // Russian
cmsjs.Dictionary.Languages.set('ta','\u0ba4\u0bae\u0bbf\u0bb4\u0bcd'); // Tamil
cmsjs.Dictionary.Languages.set('te','\u0c24\u0c46\u0c32\u0c41\u0c67\u0c41'); // Telugu
cmsjs.Dictionary.Languages.set('th','\u0e44\u0e17\u0e22'); // Thai
cmsjs.Dictionary.Languages.set('tr','T\u00fcrk\u00e7e'); // Turkish
cmsjs.Dictionary.Languages.set('vi','Ti\u1ebfng Vi\u1ec7t'); // Vietnamese
cmsjs.Dictionary.Languages.set('zh','\u4e2d\u6587'); // Chinese

/**
 * Keybindings
 */

cmsjs.Keys = class {

	static Log = cmsjs.Logger.create('cmsjs.Keys');

	static Instances = new Map();
	
	// This has action names as keys and functions as values.
	static Actions = new Map();

	// This has strings like "C-n" for map keys, and action names as values.
	static KeyBindings = new Map();

	// Adds an action. Set "docs" to null to omit documentation.
	static addAction(name,action) {
		cmsjs.Keys.Actions.set(name,action);
	}

	// Assigns a keybinding to an action.
	static assign(keyBinding,actionName) {
		if ( ! actionName in cmsjs.Keys.Actions ) {
			cmsjs.Keys.Log.error('assign(...): no action with this name has been defined:',actionName);
		}
		if ( keyBinding.includes('-') ) {
			const parts = keyBinding.split('-');
			const modifiers = parts.slice(0, -1).sort();
			const key = parts[parts.length - 1];
			const validModifiers = ["Alt","Ctrl","Meta","Shift"];
			for (let modifier of modifiers) {
				if ( ! validModifiers.includes(modifier) ) {
					cmsjs.Keys.Log.error('assign(...): invalid modifier:',modifier);
					cmsjs.Keys.Log.error('valid modifiers are: Alt, Ctrl, Meta, and Shift');
					return;
				}
			}
			keyBinding = modifiers.join('-') + '-' + key;
		}
		if ( cmsjs.Keys.isBound(keyBinding) ) {
			console.warn('{cmsjs.Keys} assign(...): keybinding is already defined, skipping: ' + keyBinding);
			return;
		}
		cmsjs.Keys.Log.debug('attaching keybinding ' + keyBinding + ' to action ' + actionName);
		cmsjs.Keys.KeyBindings.set(keyBinding,actionName);
	}

	// Checks to see if a key is bound.
	static isBound(keyBinding) {
		if ( cmsjs.Keys.KeyBindings.has(keyBinding) ) { return true; }
		return false;
	}

	// Returns a text representation of the key sequence associated with the specified keybinding.
	// Example:   getBinding('search') would return "Ctrl-s" if it were bound.
	static getBinding(action) {
		for ( let [key,val] of cmsjs.Keys.KeyBindings.entries() ) {
			if ( val === action ) {
				return key;
			}
		}
		return undefined;
	}


	// Runs the action associated with the given keybinding.
	static runActionForKeyBinding(keyBinding) {
		cmsjs.Keys.Actions.get(cmsjs.Keys.KeyBindings.get(keyBinding))();
	}

	// Processes a keypress.
	static processEvent(event) {
		const parts = [];
		if ( event.altKey ) { parts.push('Alt'); }
		if ( event.ctrlKey ) { parts.push('Ctrl'); }
		if ( event.metaKey ) { parts.push('Meta'); }
		if ( event.shiftKey ) { parts.push('Shift'); }
		parts.push(event.key);
		const keyBinding = parts.join('-');
		if ( cmsjs.Keys.isBound(keyBinding) ) {
			event.preventDefault();
			cmsjs.Keys.runActionForKeyBinding(keyBinding);
		}
	}

}


/**
 * Modules
 */
cmsjs.Module = class {

	static Log = cmsjs.Logger.create('cmsjs.Module');

	static Instances = new Map();

	static Config = {
		ModuleRoot: 'mod.root',
		ModuleDirectoryPrefix: 'mod.'
	};
	
	static create(id) {
		if ( cmsjs.Module.Instances.has(id) ) {
			cmsjs.Module.Log.error('create(id) id exists:',id);
			return;
		}
		const instance = new cmsjs.Module(id);
		cmsjs.Module.Instances.set(id,instance);
		return instance;
	}

	constructor(id) {
		cmsjs.Module.Log.trace('constructor()',id);
		this.log = cmsjs.Logger.create('cmsjs.Module(' + id + ')');
		this.id = id;
		this.path = id;
		this.config = {};
		this.script = null;
	}

	// Load module (return from cache if already loaded)
	static async load(path) {
		cmsjs.Module.Log.track('load(path)',path);
		if ( ! path || ! path.startsWith('/') || ! path.endsWith('/') ) {
			cmsjs.Module.Log.error('invalid path:',path);
			return null;
		}
		if ( ! Module.Loaded.has(path) ) {
			cmsjs.Module.Log.info('load(' + path + ') [cache miss]');
			const instance = new Module(path);
			let loadScript = null;
			if ( path === '/' ) {
				loadScript = cmsjs.Module.Config.ModuleRoot.replace(new RegExp('^' + cmsjs.Module.Config.ModuleDirectoryPrefix),'') + '.js';
			}
			else {
				const parts = path.split('/').filter(part => part.trim() !== '');
				loadScript = parts[parts.length - 1] + '.js';
			}
			const url = instance.getRealURL(loadScript);
			console.warn(url);
			const script = await import(url);
			instance.script = script;
			if ( script.load.toString().includes('async') ) {
				await script.load(Module,instance);
			}
			else {
				script.load(Module,instance);
			}
			if ( ! instance.isStandalone() && path != '/' ) {
				const tmp = path.slice(0,-1);
				const parent = tmp.substring(0,tmp.lastIndexOf('/')+1);
				await Module.load(parent);
			}
			const keys = Object.keys(script);
			keys.forEach(key => { instance[key] = script[key]; });
			Module.Loaded.set(path,instance);
		}
		const mod = Module.Loaded.get(path);
		return mod;
	}

	// Returns a module from the cache. Useful when you know a
	// module will be loaded and don't want to use async.
	static get(path) {
		return Module.Loaded.get(path);
	}

	// All modules are considered standalone unless they explicitly
	// have config['StandAlone'] = false;
	isStandalone() {
		if ( this.path === '/' ) { return true; }
		if ( ! 'StandAlone' in this.config ) { return true; }
		if ( this.config['StandAlone'] !== false ) { return true; }
		return false;
	}
	
	// Returns the parent module if this module is not standalone
	getParent() {
		if ( this.isStandalone() ) { return null; }
		const chopped = this.path.slice(0,-1);
		const parentPath = chopped.substring(0,chopped.lastIndexOf('/')+1);
		return Module.get(parentPath);
	}
	
	// Gets an array of parent modules, including this one, from root down.
	getLineage() {
		let lineage = [];
		lineage.push(this);
		let module = this;
		while ((module = module.getParent()) != null) {
			lineage.push(module);
		}
		lineage.reverse();
		return lineage;
	}

	// Gets an array of all parents (only parents, not this module)
	getParents() {
		return this.getLineage().pop();
	}

	// Gets the root module of a package
	getPackageRoot() {
		return this.getLineage()[0];
	}

	// Gets a module relative to the package root
	getPackageModule(module) {
		return Module.get(this.getPackageRoot().path.slice(0,-1) + module);
	}
	
	// Gets rid of garbage in the path, such as /./ or //
	// Also resolves /../ up to the virtual document root.
	static resolvePath(path) {
		cmsjs.Module.Log.trace('resolvePath(path)',path);
		const parts = path.split('/');
		const stack = [];
		for ( const part of parts ) {
			if ( part === '..' ) { stack.pop(); }
			else if ( part !== '.' && part !== '' ) { stack.push(part); }
		}
		// The stack loses the leading and trailing slash. Also we must
		// account for when the stack is empty and not return two slashes.
		let resolvedPath = '/' + stack.join('/');
		if ( resolvedPath != '/' ) {
			resolvedPath += '/';
		}
		return resolvedPath;
	}

	// Gets the real URL to a resource owned by this module.
	// This is generally too verbose to log
	getRealURL(resource) {
		//this.log.trace('getRealURL(resource)',resource);
		if ( ! resource ) { resource = ''; }
		const realRoot = ENV.RealURL.pathname + cmsjs.Module.Config.ModuleRoot;
		const prefix = '/' + cmsjs.Module.Config.ModuleDirectoryPrefix;
		const realPath = this.path.slice(0,-1).replace(/\//g, prefix) + '/';
		const realURL = realRoot + realPath + resource;
		return realURL;
	}

	// Gets a link for use within a tag like <a href="...">
	// This is generally too verbose to log
	getVirtualURL(resource) {
		//this.log.trace('getVirtualURL(resource)',resource);
		if ( ! resource ) { resource = ''; }
		if ( ! resource.startsWith('/') ) {
			resource = this.path + resource;
		}
		const virtualURL = '#' + resource;
		return virtualURL;
	}

	// Fetches a file relative to the module's directory
	async fetchFile(url) {
		this.log.trace('fetchFile(url)',url);
		//const config = await fetch(url).then(r => (r.ok ? r.json() : {})).catch(() => ({}));
		// should get error checking in here
		const logstr = 'fetchFile(' + url + ')';
		const realURL = this.getRealURL(url);
		const filename = realURL.slice(realURL.lastIndexOf('/') + 1);
		const response = await fetch(realURL,{cache:'no-store'});
		const ct = response.headers.get('content-type');
		if ( ! ct || typeof ct !== 'string' ) {
			this.log.error('invalid Content-Type in response',ct);
			ct = 'application/octet-stream';
		}
		let output = null;
		const text = await response.text();
		// Dynamically loaded function file
		if ( filename.startsWith('f.') && filename.endsWith('.js') ) {
			this.log.info(filename);
			const script = new Function(text);
			output = script();
		}
		// JSON data
		else if ( filename.endsWith('.json') ) {
			output = JSON.parse(text);
		}
		// Any other text
		else if ( ct.startsWith('text/') ) {
			output = text;
		}
		// Binary data
		else {
			output = await response.blob();
		}
		return output;
	} // ends fetchFile(url)

	writePackageMetaTag() {
		const pkgRoot = this.getPackageRoot();
		const metaTag = document.head.querySelector('meta[cms-package]');
		if ( metaTag ) {
			metaTag.setAttribute('cms-package',pkgRoot.path)
		}
		else {
			const newMetaTag = document.createElement('meta');
			newMetaTag.setAttribute('cms-package',pkgRoot.path);
			document.head.appendChild(newMetaTag);
		}
	}


	
	/*
	 * Runs the specified action from the module script.
	 * Specified params are added to the params from the URL
	 * search. They may be sent as either maps or objects.
	 * Params sent in the query string override those called directly.
	 */
	async action(id, params={}) {
		this.log.trace('action(id,params={})',{id:id,params:params});
		// Convert passed params to a Map
		const paramMap = params instanceof Map ? params : new Map(Object.entries(params));
		// Append Map from URL params
		await this.script.init();
		ENV.Params.forEach((value, key) => {
			// NOTE: If you take away this conditional, ENV overwrites passed params.
			if ( ! paramMap.has(key) ) {
				paramMap.set(key, value);
			}
		});
		if ( ! id ) { id = '.'; }
		const logstr = '<' + id + '>';
		const actions = this.script.actions;
		this.log.trace(logstr,params);
		let output = null;
		if ( actions.has(id) ) {
			this.writePackageMetaTag();
			await actions.get(id)(paramMap);
		}
		else if ( actions.has('*') ) {
			this.log.error(logstr + ' action not found, running default');
			this.writePackageMetaTag();
			await actions.get('*')(paramMap);
		}
		else  {
			this.log.error(logstr + ' invalid action, valid actions:',actions);
		}
	}

	// Redirect to a new virtual url
	redirect(url) {
		this.log.trace('redirect(url)',url);
		url = this.getVirtualURL(url);
		history.replaceState(null,null,url);
		window.location.hash = url;
		window.dispatchEvent(new Event('hashchange'));
	}

	// Provides modules access to environment variables.
	env(variable) {
		return ENV[variable];
	}

	// Set a cookie
	setCookie(name, value) {
		this.log.trace('setCookie()',name);
		this.log.warn('setCookie(): realRoot is hard coded, need to pull from ENV');
		const config = {
			'max-age' : 30,
			'path' : '/'
		};
		cmsjs.State.setCookie(name,value,config);
	}

	// Get a cookie
	getCookie(name) {
		this.log.trace('getCookie(name)',name);
		return cmsjs.State.getCookie(this.path + name);
	}
	
}


/**
 * SPA: Single Page Application
 *
 * Manages hash tag URLs and behavior based on hash changes.
 */
cmsjs.SPA = class {

	static Log = cmsjs.Logger.create('cmsjs.SPA');
	
	static addWindowListeners() {
		// Initial page load
		window.addEventListener('load', async (event) => {
			console.log('cms: initializing from window "load" event');
			const log = new Log();
			await cmsjs.SPA.processURL();
			const module = await cmsjs.Module.load(cmsjs.SPA.ENV.Module);
			await module.action(cmsjs.SPA.ENV.Action);
		});
		// When the hash changes
		window.addEventListener('hashchange', async (event) => {
			const oldModule = await cmsjs.Module.load(cmsjs.SPA.ENV.Module);
			cmsjs.SPA.processURL();
			const module = await cmsjs.Module.load(cmsjs.SPA.ENV.Module);
			// only reload the page when the package changes
			if ( oldModule.getPackageRoot() != module.getPackageRoot() ) {
				location.reload();
			}
			else {
				await module.action(cmsjs.SPA.ENV.Action);
			}
		});
	}

	static CONFIG = {
		ModuleRoot: 'mod.root',
		ModuleDirectoryPrefix: 'mod.',
	};

	static ENV = {
	    RealURL: null,
		VirtualURL: null,
		Module: null,
		Action: null,
		Params: null,
		Hash: null
	};

	/**
	 * Parses the current URL and sets environment variables.
	 */
	static processURL() {
		const realURLString = window.location.href;
		const realURL = new URL(realURLString);
		const realHash = realURL.hash;
		const realOrigin = realURL.origin;
		const realPathname = realURL.pathname;
		const realParams = realURL.searchParams;
		// redirect to the root: /#/
		// Note that you need to redirect to the path directory or else
		// index.html will redirect to index.html#/
		const realPathDir = realPathname.substring(0,realPathname.lastIndexOf('/')+1);
	    if ( ! realHash ) {
			cmsjs.SPA.Log.info('no hash, redirecting to /#/');
		    window.location.href = realOrigin + realPathDir + '#/';
	    }
		else if ( realHash === '#' ) {
			cmsjs.SPA.Log.info('hash is simply "#", redirecting to /#/');
		    window.location.href = realOrigin + realPathDir + '#/';
		}
		const cmsURLString = realOrigin + realHash.replace(/^#/,'')
		const cmsURL = new URL(cmsURLString);
		const cmsPathname = cmsURL.pathname;
	    //  /path/to/dir/ -> ['', 'path', 'to', 'dir', '']
	    //  /path/to/dir/file -> ['', 'path', 'to', 'dir', 'file']
	    const cmsPathArray = cmsPathname.split('/');
		// grab the first four components of the above to recreate the path
		const module = cmsPathArray.slice(0,-1).join('/') + '/';
		// the path array will at a minimum be ['',''], so the action will exist
		const action = cmsPathArray[cmsPathArray.length - 1];
		// turn params into a map
		const params = new Map(cmsURL.searchParams.entries());
		// I forget what this wizardry does, and why I don't just use cmsURL.hash
		const hash = (cmsURL.hash || '#').replace(/^#/,'') || null;
		cmsjs.SPA.ENV.Module = module;
		cmsjs.SPA.ENV.Action = action;
		cmsjs.SPA.ENV.Params = params;
		cmsjs.SPA.ENV.Hash = hash;
		cmsjs.SPA.ENV.VirtualURL = cmsURL;
		cmsjs.SPA.ENV.RealURL = realURL;
		cmsjs.SPA.Log.info('processURL(): ENV set',cmsjs.SPA.ENV);
	}

	/*
	 * Runs the request as derived from the URL
	 */
	static async action() {
		cmsjs.SPA.Log.trace('action()');
		cmsjs.SPA.Log.debug('action(): running action: ' + cmsjs.SPA.ENV.Module + cmsjs.SPA.ENV.Action);
	}

}


cmsjs.Themes = class {

	/*
	static Keys = new Map();
	static DefaultMap = new Map();
	static NamespaceMaps = new Map();
	static ElementMap = new Map();
	static CurrentTheme = null;
	static EditorTheme = null;
	static OnThemeChange = null;
	*/

	/**
	 * Adds a key and default value to the specified object.
	 *
	 * This is only for internal use by the subclasses.
	 *
	 * @private
	 * @param {Map}    Map   The Map to add the key to.
	 * @param {string} key   The key to add to the Map.
	 * @param {string} value The default value for the key.
	 */
	static _addKeyVal(map, key, value) {
		function isValidJSONKey(str) {
			try { JSON.parse(`{"${str}":null}`); return true; }
			catch (error) { return false; }
		}
		function isValidIdentifier(str) {
			return /^[a-zA-Z_$][a-zA-Z0-9_$]*$/.test(str);
		}
		const parts = key.split('.');
		for ( let i = 0; i < parts.length; i++ ) {
			const str = parts[i];
			if ( ! isValidJSONKey(key) ) {
				LOG.error('{THEMES} invalid component "' + str + '" in key: ',key);
				return;
			}
		}
		let current = map;
		for ( let i = 0; i < parts.length; i++ ) {
			const part = parts[i];
			if ( ! current.has(part) ) {
				if ( i === parts.length - 1 ) {
					current.set(part,value);
				} else {
					current.set(part,new Map());
				}
			}
			current = current.get(part);
		}
	}


	static _jsonify(map) {
	}

	
	/**
	 * Loads a theme set from a JSON file.
	 *
	 * JSON Format:
	 *
	 * {
	 *   "Version" : "0.9.6",
	 *   "Themes" : {
	 *     "Colors" : {
	 *       "Light" : {
	 *         "Default.Background" : "#fff",
	 *         "Default.Text" : "#000",
	 *         ...
	 *       },
	 *       "Dark" : {
	 *         "Default.Background" : "#000",
	 *         "Default.Text" : "#fff",
	 *         ...
	 *       },
	 *       ...
	 *     },
	 *     "Fonts" : {
	 *       "Standard" : {
	 *         "Headings" : "Roboto",
	 *         "Text" : "Open Sans",
	 *         "Console" : "Rubik",
	 *         ...
	 *       },
	 *       "Gothic" : {
	 *         "Headings" : "Kanit",
	 *         "Text" : "Anta",
	 *         "Console" : "Jacquard 24",
	 *         ...
	 *       },
	 *       ...
	 *     },
	 *     "Layouts" : {
	 *       "Standard" : {
	 *         "GridRows" : "auto 1fr auto auto",
	 *         "GridCols" : "1fr auto",
	 *         "Panels" : {
	 *           "Header" : {
	 *             "GridRowStart" : "1",
	 *             "GridRowSpan" : "1",
	 *             "GridColStart" : "1",
	 *             "GridColSpan" : "2",
	 *           },
	 *           "content" : {
	 *             "GridRowStart" : "2",
	 *             "GridRowSpan" : "1",
	 *             "GridColStart" : "1",
	 *             "GridColSpan" : "1",
	 *           },
	 *           ...
	 *         }
	 *         ...
	 *       },
	 *       "Mobile" : {
	 *         "LayoutType" : "flex",
	 *         "GridRows" : "auto 1fr auto auto auto",
	 *         "GridCols" : "1fr",
	 *         ...
	 *       },
	 *       ...
	 *     },
	 *     "Controls" : {
	 *       "Standard" : {
	 *         "Toggle.Console" : "Slider",
	 *         "Select.Console" : "Scroller",
	 *         ...
	 *       },
	 *       "Plain" : {
	 *         "Toggle.Console" : "Checkbox",
	 *         "Select.Console" : "Standard",
	 *         ...
	 *       },
	 *       ...
	 *     }
	 *   }
	 * }
	 *
	 * @private
	 * @param {Object} object The object to add the key to.
	 * @param {string} key    The key to add to the object.
	 * @param {string} value  The default value for the key.
	 */
	static load(json) {
	}

	
}

/**
 * 
 *
 */
cmsjs.Themes.Colors = class {
	
	static Log = cmsjs.Logger.create('cmsjs.Themes.Colors');
	
	static Map = new Map();
	
	static ValidKeys = new Map();
	static CurrentTheme = null;
	static OnThemeChange = null;
	static getTheme(name) {
		cmsjs.Themes.Colors.Log.trace('getTheme()',name);
		return ( typeof name === 'string' ) ? cmsjs.Themes.Colors.Map(name) : name;
	}
	static addKey(key,val) {
		cmsjs.Themes.Colors.Log.trace('addKey()',key,val);
		cmsjs.Themes.Colors.ValidKeys.set(key,val);
	}
	static getCurrentTheme() {
		cmsjs.Themes.Colors.trace('getCurrentTheme()');
		return getTheme(cmsjs.Themes.Colors.CurrentTheme);
	}
	static setCurrentTheme(theme) {
		cmsjs.Themes.Colors.Log.trace('setCurrentTheme()',theme);
		theme = cmsjs.Themes.Colors.getTheme(theme);
		cmsjs.Themes.Colors.apply(theme)
		cmsjs.Themes.Colors.OnThemeChange(theme);
		cmsjs.Themes.Colors.CurrentTheme = theme.name;
	}
	static applyCurrentTheme(rootElement=document) {
		cmsjs.Themes.Colors.Log.trace('applyCurrentTheme()',rootElement);
		const theme = cmsjs.Themes.Colors.getTheme(cmsjs.Themes.Colors.CurrentTheme);
		cmsjs.Themes.Colors.apply(theme,rootElement);
	}
	static applyTheme(theme,rootElement=document) {
		cmsjs.Themes.Colors.Log.trace('applyTheme()',rootElement);
		theme = cmsjs.Themes.Colors.getTheme(theme);
		cmsjs.Themes.Colors.ValidKeys.forEach((set,csskey) => {
			Array.from(set).forEach((variable) => {
				const selector = '[cmsjs-theme-' + csskey + '="' + variable + '"]';
				//LOG.debug('{STYLE} applyTheme() ' + selector + ' set to value: ',theme.get(variable));
				rootElement.querySelectorAll(selector).forEach((elt) => {
					elt.style.setProperty(csskey,theme.get(variable));
				});
			});
		});
	}
	/*
	 * bindElement(someDiv,'color','string-color');
	 * bindElement(someDiv,'border-left-color','string-color');
	 */
	static bindElement(element,csskey,variable) {
		cmsjs.Themes.Colors.Log.trace('bindElement()',element,csskey,variable);
		element.setAttribute('cmsjs-theme-' + csskey, variable);
		if ( ! STYLE.ElementMap.has(csskey) ) {
			STYLE.ElementMap.set(csskey,new Set());
		}
		const variables = STYLE.ElementMap.get(csskey);
		variables.add(variable);
		//console.warn('setting ' + csskey + ' to ' + STYLE.getThemeValue(variable) + ' for element: ',element);
		element.style.setProperty(csskey,cmsjs.Themes.Colors.getThemeValue(variable));
		//[cmsjs-theme-background-color='background-color'] { background-color: var(--background-color); }
		//[cmsjs-theme-color='string-color'] { color: var(--string-color); }
		return element;
	}
	// Rethemes a specific element, in the event that it has been modified somehow.
	// This most often occurs during mouseenter events, where an element changes
	// color.
	static rethemeElement(element) {
		cmsjs.Themes.Colors.Log.trace('rethemeElement()',element);
		for ( let i = 0; i < element.attributes.length; i++ ) {
			const attr = element.attributes[i];
			if ( attr.name.startsWith('cmsjs-theme-') ) {
				const csskey = attr.name.replace('cmsjs-theme-','');
				const value = cmsjs.Themes.Colors.getThemeValue(attr.value);
				element.style.setProperty(csskey,value);
			}
		}
	}
	constructor(name) {
		cmsjs.Themes.Colors.Log.trace('constructor()',name);
		this.log = cmsjs.Logger.create('cmsjs.Themes.Colors(' + name + ')');
		this.name = name;
		this.keys = new Map();
		cmsjs.Themes.Colors.Map.set(this.name,this);
	}
	set(key,val) {
		this.log.trace('set()',key,val);
		if ( ! cmsjs.Themes.Colors.ValidKeys.has(key) ) {
			this.log.error('set(key,val) attempt to set invalid key:',key);
			return;
		}
		this.keys.set(key,val);
	}
	get(key) {
		this.log.trace('get()',key);
		return this.keys.get(key);
	}
	static setThemeChangeFunction(f) {
		this.log.trace('setThemeChangeFunction()',f);
		cmsjs.Themes.OnThemeChange = f;
	}
}


/**
 *
 * Fonts
 *
 */
cmsjs.Themes.Fonts = class {


	// Given an integer in the  range of 1-10, return
	// a string that can be used in elt.style.fontSize (e.g. '1.1rem')
	static getFontSize(size) {
		const realSize = 0.5 + (size*0.1)
		const sizeString = realSize + 'rem';
		return sizeString;
	}
	
	constructor(name,type,config) {
		this.name = name;
		this.type = type;
		this.config = config;
		cmsjs.Themes.Fonts.Map.set(this.name,this);
	}
	
	static registerLocalFont(fontName,defaults='sans-serif') {
	}

	static insertGooglePreconnects() {
		if ( ! document.querySelector('[cmsjs-fonts-google-preconnect]') ) {
			const link1 = document.createElement('link');
			link1.rel = 'preconnect';
			link1.href = 'https://fonts.googleapis.com';
			const link2 = document.createElement('link');
			link2.rel = 'preconnect';
			link2.href = 'https://fonts.gstatic.com';
			link2.setAttribute('crossorigin','');
			document.head.appendChild(link1);
			document.head.appendChild(link2);
		}
	}


	static registerGoogleFont(fontName,defaults='sans-serif') {
		var existingLink = document.head.querySelector('link[cmsjs-font-link="' + fontName + '"]');
		if ( ! existingLink ) {
			insertGooglePreconects();
			var link = document.createElement('link');
			const encodedName = fontName.replace(/ /g,'%20');
			link.setAttribute('cmsjs-font-link', fontName);
			link.href = 'https://fonts.googleapis.com/css2?family=' + encodedName + '&display=swap';
			link.rel = 'stylesheet';
			document.head.appendChild(link);
		}
	}

	/**
	 * Given an element, a Google font name, and a string (defaults),
	 * this first adds the <link rel='stylesheet'> element in <head>
	 * to load the font if it does not already exist, then sets the
	 * element's font family to "Font Name" followed by 'defaults'.
	 */
	static setGoogleFont(element,fontName,defaults='sans-serif') {
		if ( typeof element === 'string' ) {
			element = document.getElementById(element);
		}
		// I keep changing the 'default' name...
		if ( ! fontName || fontName === 'Default' || fontName === 'System Default' ) {
			element.style.fontFamily = defaults;
			return;
		}
		cmsjs.Themes.Fonts.registerGoogleFont(fontName,defaults);
		const fontString = '"' + fontName + '"' + ', ' + defaults;
		element.style.fontFamily = fontString;
	}
}

cmsjs.Themes.Controls = class {
}

cmsjs.Themes.Layouts = class {
	static Map = new Map();
	static ValidPanels = new Map();
	static getTheme(name) {
		return ( typeof name === 'string' ) ? cmsjs.Themes.Layouts.Map(name) : name;
	}
	static addPanel(str,defaultConfig) {
		cmsjs.Themes.Layouts.ValidPanels.set(str,defaultConfig);
	}
	static apply(theme) {
		theme = cmsjs.Themes.Layouts.getTheme(theme);
		cmsjs.Console.removeAllPanels();
		cmsjs.Console.setGridRows(theme.gridRows);
		cmsjs.Console.setGridColumns(theme.gridCols);
		theme.panels.forEach(([position,span],panelName) => {
			panel = cmsjs.Console.getPanel(panelName);
			cmsjs.Console.attachPanel(panel,position,span);
		});
		theme.components.forEach((panelName,componentName) => {
			panel = cmsjs.Console.getPanel(panelName);
			component = cmsjs.Component.get(componentName);
			panel.attachComponent(component);
		});
	}
	constructor(name) {
		this.name = name;
		this.panels = new Map();
		this.gridRows = '1fr';
		this.gridCols = '1fr';
	}
	apply() {
		cmsjs.Themes.Layouts.apply(this);
	}
	setGridRows(str) {
		this.gridRows = str; // Should look like "auto 1fr auto auto"
	}
	setGridCols(str) {
		this.gridCols = str; // Should look like "auto 1fr"
	}
	addPanel(panelID,position,span) {
		this.panels.set(panelID,[position,span]);
	}
	addComponent(panelID,componentID) {
		this.components.add(componentID,panelID);
	}
}



/**************************************
 *                                    *
 *           THEME EDITOR             *
 *                                    *
 **************************************/

cmsjs.ThemeEditor = class {

	static show(id) {
		cmsjs.State.setAndSave('cmsjs.ThemeEditor',id);
		const editor = CONSOLE.getPanel('cmsjs.ThemeEditor');
		themes.style.transition = 'max-height 0.3s ease-out';
		themes.style.maxHeight = '0';
		themes.style.overflow = 'hidden';
		//console.warn(id,editor);
		CONSOLE.showOnlyComponent(id);
		editor.style.maxHeight = '33vh';
		//editor.style.overflowY = 'scroll';
		editor.style.display = 'block';
	}
	
	static hide(id) {
		cmsjs.State.setAndSave('cmsjs.ThemeEditor',null);
		const editor = CONSOLE.getPanel('style');
		editor.style.display = 'none';
	}

	
	// Theme Editor
	static getThemeEditor(div) {
		const close = getCloseButton();
		close.addEventListener('click', (event) => { hideStyleEditor(); });
		div.append(close);
		div.style.fontSize = STYLE.getFontSize(cmsjs.State.get('Core.ConsoleFontSize'));
		STYLE.setGoogleFont(div,cmsjs.State.get('Core.ConsoleFont'),'sans-serif');
		div.style.textAlign = 'left';
		div.style.borderTop = 'solid 1px';
		div.style.padding = '1rem';
		const fragment = document.createDocumentFragment();
	
		const currentTheme = STYLE.getCurrentTheme();
		cmsjs.State.set('ENV.CurrentTheme',currentTheme);
		let newTheme = cmsjs.State.get('ENV.TempTheme');
		if ( ! newTheme ) {
			newTheme = new Map(currentTheme);
		//	cmsjs.State.set('ENV.TempTheme',newTheme);
		}
		//STYLE.EditorTheme = cmsjs.State.get('ENV.TempTheme');
	
		function getColorInput(label,id) {
			const config = {};
			config['label'] = label;
			config['value'] = newTheme.get(id);
			config['label-position'] = 'right';
			config['listener:change'] = ((event) => {
				newTheme.set(id,event.target.value);
				STYLE.applyTheme(newTheme);
				CONSOLE.buildComponent('controls');
			});
			const wrapper = CONTROLS.create('input:color',id,config);
			wrapper.style.margin = '0.5em';
			return wrapper;
		}
	
		const heading = $.ce('h1','Theme Editor');
		heading.style.fontSize = '1.5em';
		fragment.append(cmsjs.Themes.Colors.bindElement(heading,'color','heading-color'));
		const themeDiv = $.ce('div');
		themeDiv.style.display = 'flex';
		fragment.append(themeDiv);
		const div1 = $.ce('div');
		div1.style.flex = '1';
		themeDiv.append(div1);
		const div2 = $.ce('div');
		div2.style.flex = '1';
		themeDiv.append(div2);
		const div3 = $.ce('div');
		div3.style.flex = '1';
		themeDiv.append(div3);
		const div4 = $.ce('div');
		div4.style.flex = '1';
		themeDiv.append(div4);
	
		const h1 = $.ce('h2','Console');
		h1.style.fontSize = '1.25em';
		div1.append(cmsjs.Themes.Colors.bindElement(h1,'color','heading-color'));
		div1.append(getColorInput('Background','background-color'));
		div1.append(getColorInput('Text','text-color'));
		div1.append(getColorInput('Text 2','text-2-color'));
		div1.append(getColorInput('Heading','heading-color'));
		
		const h2 = $.ce('h2','Controls');
		h2.style.fontSize = '1.25em';
		div2.append(cmsjs.Themes.Colors.bindElement(h2,'color','heading-color'));
		div2.append(getColorInput('Slider On','console-toggle-slider-on-color'));
		div2.append(getColorInput('Button On','console-toggle-button-on-color'));
		div2.append(getColorInput('Slider Off','console-toggle-slider-off-color'));
		div2.append(getColorInput('Button Off','console-toggle-button-off-color'));
	
		const h3 = $.ce('h2','Keys/Values');
		h3.style.fontSize = '1.25em';
		div3.append(cmsjs.Themes.Colors.bindElement(h3,'color','heading-color'));
		div3.append(getColorInput('Keys','key-color'));
		div3.append(getColorInput('Nulls','null-color'));
		div3.append(getColorInput('Booleans','boolean-color'));
		div3.append(getColorInput('Numbers','number-color'));
		div3.append(getColorInput('Strings','string-color'));
		div3.append(getColorInput('Objects','object-color'));
	
		const h4 = $.ce('h2','JSON Controls');
		h4.style.fontSize = '1.25em';
		div4.append(cmsjs.Themes.Colors.bindElement(h4,'color','heading-color'));
		div4.append(getColorInput('Bytes','bytes-color'));
		div4.append(getColorInput('Controls','controls-color'));
		div4.append(getColorInput('Outlines','button-outline-color'));
		div4.append(getColorInput('Guideline 1','guideline-1-color'));
		div4.append(getColorInput('Guideline 2','guideline-2-color'));
		div4.append(getColorInput('Guideline 3','guideline-3-color'));
		div4.append(getColorInput('Guideline 4','guideline-4-color'));
	
		div.append(fragment);
		//STYLE.applyCurrentTheme(CONSOLE.getPanel('themes'));
		return div;
	}

	
	// Font Editor
	static getFontEditor(div) {
		const close = getCloseButton();
		close.addEventListener('click', (event) => { hideStyleEditor(); });
		div.append(close);
		//div.style.display = 'none';
		div.style.fontSize = STYLE.getFontSize(cmsjs.State.get('Core.ConsoleFontSize'));
		STYLE.setGoogleFont(div,cmsjs.State.get('Core.ConsoleFont'),'sans-serif');
		div.style.textAlign = 'left';
		div.style.borderTop = 'solid 1px';
		div.style.padding = '1rem';
		const fragment = document.createDocumentFragment();
		
		function getFontListInput(label) {
			const defaults = 'Roboto Mono, Kode Mono, Rubik, Anta, Kanit, Noto Sans, Roboto, Barlow, Lora, Open Sans';
			const config = {};
			config['label'] = 'Available Fonts:';
			console.warn(cmsjs.State.get('Core.FontList'));
			config['value'] = cmsjs.State.get('Core.FontList').join(', ');
			config['placeholder'] = 'Enter a comma-separated list of Google font names, e.g. "Roboto, Rubik, Noto Sans"';
			config['listener:change'] = ((event) => {
				cmsjs.State.changeAndSave('Core.FontList',event.target.value.split(',').map((item) => item.trim()));
			});
			return CONTROLS.create('input:text','Core.FontList',config);
		}
	
		const heading = $.ce('h1','Font Editor');
		heading.style.fontSize = '1.5em';
		fragment.append(cmsjs.Themes.Colors.bindElement(heading,'color','heading-color'));
		fragment.append(getFontListInput());
	
		const fontDiv = $.ce('div');
		fontDiv.style.display = 'flex';
		fontDiv.style.justifyContent = 'center';
		fontDiv.style.marginTop = '1rem';
		fragment.append(fontDiv);
		const fontTypes = ['Console','Code','Display'];
		fontTypes.forEach((fontType,index) => {
			const _div = $.ce('div');
			_div.style.flex = '1';
			_div.style.display = 'flex';
			_div.style.justifyContent = 'center';
			_div.style.flexDirection = 'column';
			fontDiv.append(_div);
			const fontID = 'Core.' + fontType + 'Font';
			const fontSizeID = 'Core.' + fontType + 'FontSize';
			const fontControl = createControl('select',fontID,{items:cmsjs.State.get('Core.FontList')});
			const fontSizeControl = createControl('select:integer',fontSizeID,{min:1,max:10});
			fontControl.style.margin = 'auto';
			fontControl.style.marginBottom = '0.75em';
			fontSizeControl.style.margin = 'auto';
			_div.append(fontControl);
			_div.append(fontSizeControl);
		});
	
		div.append(fragment);
		STYLE.applyCurrentTheme(CONSOLE.getPanel('fonts'));
		return div;
	}

}

cmsjs.HTML = class {
	
	static escapeString(text) {
		return text.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;').replace(/'/g,'&#39;');
	}
	
	static unescapeString(text) {
		return text.replace(/&amp;/g,'&').replace(/&lt;/g,'<').replace(/&gt;/g,'>').replace(/&quot;/g,'"').replace(/&#39;/g,"'");
	}

	static getGoogleIcon(id) {
		const fontName = 'Material Symbols Outlined';
		var existingLink = document.head.querySelector('link[cmsjs-font-link="' + fontName + '"]');
		if ( ! existingLink ) {
			var link = document.createElement('link');
			//link.href = 'https://fonts.googleapis.com/css2?family=Material%20Symbols%20Outlined;opsz,wght,FILL,GRAD@20,400,0,0';
			link.href = 'https://fonts.googleapis.com/css2?family=Material%20Symbols%20Outlined';
			link.rel = 'stylesheet';
			link.setAttribute('cmsjs-font-link', fontName);
			document.head.appendChild(link);
			var style = document.createElement('style');
			style.setAttribute('cmsjs-font-style', fontName);
			//style.innerHTML = '.material-symbols-outlined { font-size: inherit !important; font-weight: bold !important; }';
			style.innerHTML = '.material-symbols-outlined { font-size: inherit !important; }';
			document.head.appendChild(style);
		}
		const elt = $.ce('span');
		elt.classList.add('material-symbols-outlined');
		elt.style.verticalAlign = 'middle';
		elt.style.textAlign = 'center';
		elt.innerHTML = id;
		return elt;

	}

}




// jQuery-like functions

class $ {

	// document.createElement, extended to take contents as an arg
	static ce(tag,contents,attributes) {
		const elt = document.createElement(tag);
		if ( contents ) {
			if ( typeof contents === 'string' ) {
				elt.innerHTML = contents;
			}
			else {
				elt.append(contents);
			}
		}
		if ( attributes && typeof attributes === 'object' ) {
			Object.keys(attributes).forEach((item) => {
				item.setAttribute(item,attributes[item]);
			});
		}
		return elt;
	}

	// document.getElementById
	static id(str) { return document.getElementById(str); }

	// document.createTextNode
	static ctn(str) { return document.createTextNode(str); }

	// document.createDocumentFragment
	static cdf() { return document.createDocumentFragment(); }

	// This returns arrays.
	// Use $.id() if you want a single element.
	// Use $.ss() if you want output type to vary depending on query.
	static s(selector) { return $.select(selector); }
	static select(selector) {
		if ( ! selector || typeof selector !== 'string' ) {
			error('$.select: invalid selector',selector);
		}
		if ( selector.startsWith('#') ) {
			const match = document.getElementById(selector.slice(1));
			if ( match ) { return [match]; }
			else { return []; }
		}
		else {
			const matches = Array.from(document.querySelectorAll(selector));
			if ( matches ) { return matches; }
			else { return []; }
		}
		return [];
	}
	
	// Smart select.
	// Returns null if no matches.
	// Returns a single element if an id is requested.
	// Returns a single element if only a single element matches.
	// Returns an array if there are multiple matches.
	static ss(selector) { return $.smartSelect(selector); }
	static smartSelect(selector) {
		const matches = $.select(selector);
		if ( ! matches || matches.length == 0 ) {
			return null;
		}
		else if ( selector.startsWith('#') ) {
			return matches[0];
		}
		else {
			if ( matches.length == 1 ) {
				return matches[0];
			}
			return matches;
		}
	}
	
	// Sets the style on all elements matching 'selector'
	// 'name' and 'value' can be either strings or arrays.
	// Arrays are more efficient than calling this multiple times.
	static style(selector,name,value) {
		if ( typeof name === 'string' && typeof value === 'string' ) {
			name = [name];
			value = [value];
		}
		const elements = $.select(selector);
		elements.forEach(element => {
			name.forEach((key,index) => {
				element.style[key] = value[index]
			});
		});
	}
	
}

cmsjs.Logger.overrideAll(true);
const log = cmsjs.Logger.create('json-navigator');
log.setFlag(cmsjs.Logger.Flags.get('Hammer'));
log.setFlag('trace','----------------------------------------------->');
log.setStyle('color: #077; font-weight: bold;');
log.setStyle('trace','color: #909; font-weight: bold;');
log.trace('cms.logger.js');
log.trace('testing trace');
log.debug('testing debug');
log.info('testing info');
log.warn('testing warn');
log.error('testing error');
log.fatal('testing fatal');
log.timer('testing timer');

/*
cmsjs.Logger.LogLevel.create('success',{
//	Flag: '---------------------->',
//	Flag: '-- __ ==',
	Flag: cmsjs.Logger.Flags.get('Cool'),
	Style: 'border: dotted 1px #f0f; border-radius: 2px; padding: 1px 2px; background-color: #eee',
	ConsoleLevel: 'log',
	Enabled: true
});
log.success('testing success');


cmsjs.Log.timer('test2');
cmsjs.State.Log.timer('test2');

const statelog = cmsjs.State.Log;
statelog.setFlag(cmsjs.Logger.Flags.get('Splash'));
statelog.setStyle('color: #077; background-color: #eef;');
statelog.setStyle('trace','border: dashed 1px red;');
statelog.LogLevelEnabled.set('trace',false);
statelog.trace('testing statelog trace');
*/

log.trace('cms.state.js');

// Environment variables
cmsjs.State.init('ENV.RawContent',null);
cmsjs.State.init('ENV.RawContentProcessed',false);
cmsjs.State.init('ENV.JSON',null);
cmsjs.State.init('ENV.PageLoaded',null);
cmsjs.State.init('ENV.Bytes',null);
cmsjs.State.init('ENV.CurrentPage',null);
cmsjs.State.init('ENV.CurrentPath',null);
cmsjs.State.init('ENV.StandaloneMode',false);
cmsjs.State.init('ENV.EditorChanged',false);

// User-configurable variables
cmsjs.State.init('Config.BoldKeys',true);
cmsjs.State.init('Config.ContentTypes',['application/json']);
cmsjs.State.init('Config.ControlKeys',false);
cmsjs.State.init('Config.ConvertLinks',false);
cmsjs.State.init('Config.CodeFont',null);
cmsjs.State.init('Config.CodeFontSize',3);
cmsjs.State.init('Config.ConsoleFont',null);
cmsjs.State.init('Config.ConsoleFontSize',3);
cmsjs.State.init('Config.Debug',false);
cmsjs.State.init('Config.DisplayFont',null);
cmsjs.State.init('Config.DisplayFontSize',3);
cmsjs.State.init('Config.Extensions',[]);
//cmsjs.State.init('Config.EditorMode',false);
cmsjs.State.init('Config.FontList',['Roboto Mono','Kode Mono','Rubik','Anta','Kanit','Noto Sans','Roboto','Barlow','Lora','Open Sans']);
cmsjs.State.init('Config.Hyperlinks',true);
cmsjs.State.init('Config.Language','en');
cmsjs.State.init('Config.LineWrap',true);
cmsjs.State.init('Config.MarkupHTML',true);
cmsjs.State.init('Config.MarkupMode',true);
cmsjs.State.init('Config.NullOpacity',0.4);
cmsjs.State.init('Config.ShowBytes',true);
cmsjs.State.init('Config.ShowCount',true);
cmsjs.State.init('Config.ShowFooter',true);
cmsjs.State.init('Config.ShowNulls',true);
cmsjs.State.init('Config.ShowSearch',true);
cmsjs.State.init('Config.ShowTooltips',true);
cmsjs.State.init('Config.ShowURL',true);
cmsjs.State.init('Config.SortKeys',true);
cmsjs.State.init('Config.Spacing',3);
cmsjs.State.init('Config.StyleEditor',null);
cmsjs.State.init('Config.ThemeEditorOpened',false);
cmsjs.State.init('Config.IndentDepth',3);
cmsjs.State.init('Config.ShowGuidelines',true);
cmsjs.State.init('Config.AutoExpand',false);
cmsjs.State.init('Config.ExpandDepth',3);
cmsjs.State.init('Config.DarkMode',true);
cmsjs.State.init('Config.SelectedLight','Default');
cmsjs.State.init('Config.SelectedDark','Default');
//cmsjs.State.init('Fonts.JSON',['Default','Roboto Mono','Noto Sans Mono','Kode Mono','Rubik','Noto Sans']);
//cmsjs.State.init('Fonts.Console',['Default','Open Sans','Rubik','Montserrat','Anta','Kanit']);
//cmsjs.State.init('Fonts.Display',['Default','Open Sans','Rubik','Roboto','Barlow','Lora']);

cmsjs.State.addListener('Config.IndentDepth', (depth) => {
	if ( depth < 1 ) { cmsjs.State.set('Config.IndentDepth',1); return; }
	else if ( depth > 10 ) { cmsjs.State.set('Config.IndentDepth',10); return; }
	$.id('root').querySelectorAll('div.contents').forEach((div) => {
		div.style.marginLeft = depth/2 + 'rem';
	});
});

cmsjs.State.addListener('Config.ShowGuidelines', (state) => {
    Array.from(document.querySelectorAll('div.contents')).forEach((div) => {
		if (state) {
			div.style.borderLeftColor = getBorderColor(parseInt(div.parentElement.getAttribute('depth')) + 1);
		}
		else {
			div.style.borderLeftColor = 'rgba(0,0,0,0.0)';
		}
	});
});

/*
 * Config.ExpandDepth
 */
cmsjs.State.addListener('Config.ExpandDepth', (depth) => {
	if ( depth < 1 ) { cmsjs.State.set('Config.ExpandDepth',1); return; }
	else if ( depth > 10 ) { cmsjs.State.set('Config.ExpandDepth',10); return; }
});


/*
 * Config.Spacing
 */
cmsjs.State.addListener('Config.Spacing', (size) => {
	if ( size < 1 ) { cmsjs.State.set('Config.Spacing',1); return; }
	else if ( size > 10 ) { cmsjs.State.set('Config.Spacing',10); return; }
	const verticalPadding = cmsjs.State.get('Config.Spacing')/30;
	CONSOLE.getComponent('content-markup').querySelectorAll('div.item').forEach((div) => {
		div.style.paddingTop = verticalPadding + 'rem';
		div.style.paddingBottom = verticalPadding + 'rem';
	});
});

/*
 * Config.LineWrap
 */
cmsjs.State.addListener('Config.LineWrap', (value) => {
	CONSOLE.getComponent('content-markup').querySelectorAll('div.item[datatype="string"] > div.label > span.value').forEach((item) => {
		if ( value ) { item.style.whiteSpace = ''; }
		else { item.style.whiteSpace = 'nowrap'; }
	});
});

/*
 * Config.BoldKeys
 */
cmsjs.State.addListener('Config.BoldKeys', (value) => {
	CONSOLE.getComponent('content-markup').querySelectorAll('div.item > div.label > span.key').forEach((item) => {
		if ( value ) { item.style.fontWeight = 'bold'; }
		else { item.style.fontWeight = 'normal'; }
	});
});

/*
 * Config.AutoExpand
 */
cmsjs.State.addListener('Config.AutoExpand', (state) => {
	if (state) { expandAll(); }
});

/*
 * Config.MarkupHTML
 */
cmsjs.State.addListener('Config.MarkupHTML', (state) => {
	if (state) {
		CONSOLE.getComponent('content-markup').querySelectorAll('div.item[datatype="string"] > div.label > span.value').forEach((valElt) => {
			if ( ! valElt.classList.contains('converted-url') ) {
				valElt.innerHTML = HTML.unescapeString(valElt.innerHTML);
				if ( cmsjs.State.get('Config.LineWrap') ) {
					valElt.style.whiteSpace = '';
				}
				else {
					valElt.style.whiteSpace = 'nowrap';
				}
			}
		});
	}
	else {
		CONSOLE.getComponent('content-markup').querySelectorAll('div.item > div.label > span.value.string').forEach((item) => {
			if ( ! item.classList.contains('converted-url') ) {
				item.innerHTML = HTML.escapeString(item.innerHTML);
			}
			if ( cmsjs.State.get('Config.LineWrap') ) {
				item.style.whiteSpace = '';
			}
			else {
				item.style.whiteSpace = 'nowrap';
			}
		});
	}
});

/*
 * Config.ConvertLinks
 */
cmsjs.State.addListener('Config.ConvertLinks', (state) => {
	if (state) {
		CONSOLE.getComponent('content-markup').querySelectorAll('div.item > div.label > span.value.string').forEach((valElt) => {
			convertToLinkIfPossible(valElt);
		});
	}
	else {
		CONSOLE.getComponent('content-markup').querySelectorAll('div.item > div.label > span.value > a.converted-url').forEach((a) => {
			a.parentElement.innerHTML = '"' + a.href + '"';
		});
	}
});

/*
 * Config.MarkupMode
 */
cmsjs.State.addListener('Config.MarkupMode', (state) => {
	/*
	if ( cmsjs.State.get('ENV.EditorChanged') ) {
		console.log('changed mofo');
		buildPage('markup');
		buildPage('raw');
		cmsjs.State.set('ENV.EditorChanged',false);
	}
	*/
	if ( state && cmsjs.State.get('ENV.CurrentPage') !== 'content-markup' ) {
		showPage('content-markup');
	}
	else if ( cmsjs.State.get('ENV.CurrentPage') == 'content-markup' ) {
		/*
		if ( cmsjs.State.get('Config.EditorMode') ) {
			showPage('editor');
		}
		*/
		//else {
			showPage('content-raw');
		//}
	}
});

/*
 * Config.EditorMode
 *
 * EditorMode must set JSON on exit, but only if EditorMode has already
 * been loaded. 
 * 'init' => () { }; // when the flag is first initialized.
 */
/*
cmsjs.State.addListener('Config.EditorMode', (value) => {
	if ( cmsjs.State.get('Config.EditorMode') && ! cmsjs.State.get('Config.MarkupMode') ) {
		showPage('editor');
	}
	else {
		const input = $.id('editor').value;
		cmsjs.State.set('ENV.JSON',JSON.parse(input.trim()));
		if ( cmsjs.State.get('ENV.EditorChanged') ) {
			buildPage('markup');
			buildPage('raw');
			cmsjs.State.set('ENV.EditorChanged',false);
		}
		if ( cmsjs.State.get('Config.MarkupMode') ) {
			showPage('markup');
		}
		else {
			showPage('raw');
		}
	}
});
*/

/*
 * Config.ShowNulls
 */
cmsjs.State.addListener('Config.ShowNulls', (state) => {
    Array.from(document.querySelectorAll('div.null')).forEach((div) => {
		if (state) { div.style.display = 'block'; }
		else { div.style.display = 'none'; }
	});
});

/*
 * Config.SortKeys
 */
cmsjs.State.addListener('Config.SortKeys', (state) => {
	CONSOLE.getComponent('content-markup').querySelectorAll('.expanded-once.object').forEach((item) => {
		const jsonKeys = Object.keys(getJsonForItem(item));
		if ( jsonKeys.length > 0 ) {
			const contents = item.querySelector('div.contents');
			const keys = (state) ? jsonKeys.sort() : jsonKeys;
			keys.forEach((key) => {
				const subItem = contents.querySelector(`div.item[key="${key}"]`);
				contents.append(subItem);
			});
		}
	});
});

/*
 * Config.ShowCount
 */
cmsjs.State.addListener('Config.ShowCount', (state) => {
    Array.from(document.querySelectorAll('span.count')).forEach((div) => {
		if (state === 'on' || state === true ) {
			div.innerHTML = ' ' + div.getAttribute('count') + ' ';
		}
		else if (state === 'off' || state === false) {
			div.innerHTML = '...';
		}
	});
});


/*
 * Config.ShowSearch
 */
cmsjs.State.addListener('Config.ShowSearch', (state) => {
	if (state) {
		$.id('search').style.display = 'flex';
		CONSOLE.getComponent('header').parentElement.style.display = 'block';
	}
	else {
		$.id('search').style.display = 'none';
		if ( ! cmsjs.State.get('Config.ShowURL') ) {
			CONSOLE.getComponent('header').parentElement.style.display = 'none';
		}
	}
});

/*
 * Config.ShowURL
 */
cmsjs.State.addListener('Config.ShowURL', (state) => {
	if (state) {
		$.id('url').style.display = 'block';
		CONSOLE.getComponent('header').parentElement.style.display = 'block';
	}
	else {
		$.id('url').style.display = 'none';
		if ( ! cmsjs.State.get('Config.ShowSearch') ) {
			CONSOLE.getComponent('header').parentElement.style.display = 'none';
		}
	}
});

/*
 * Config.ShowFooter
 */
cmsjs.State.addListener('Config.ShowFooter', (state) => {
	if (state) { CONSOLE.getComponent('footer').parentElement.style.display = 'block'; }
	else { CONSOLE.getComponent('footer').parentElement.style.display = 'none'; }
});

/*
 * Config.Debug
 */
cmsjs.State.addListener('Config.Debug', (state) => {
});

/*
 * Config.ShowBytes
 */
cmsjs.State.addListener('Config.ShowBytes', (state) => {
    Array.from(document.querySelectorAll('span.bytes')).forEach((div) => {
		if (state) {
			if ( ! cmsjs.State.get('ENV.Bytes') ) {
				cmsjs.State.set('ENV.Bytes',JSON.stringify(cmsjs.State.get('ENV.JSON')).length);
				// FIXME: Need to implement looping over existing divs and updating bytes
			}
			div.style.display = 'inline';
		}
		else {
			div.style.display = 'none';
		}
	});
});

/*
 * Config.DarkMode
 */
cmsjs.State.addListener('Config.DarkMode', (state) => {
	if ( state ) {
		const theme = cmsjs.State.get('Config.SelectedDark');
		STYLE.setTheme('Dark.' + theme);
	}
	else {
		const theme = cmsjs.State.get('Config.SelectedLight');
		STYLE.setTheme('Light.' + theme);
	}
});

/*
 * Config.SelectedDark
 */
cmsjs.State.addListener('Config.SelectedDark', (value) => {
	CONTROLS.setValue('Config.DarkMode',true);
	STYLE.setTheme('Dark.' + value);
});

/*
 * Config.SelectedLight
 */
cmsjs.State.addListener('Config.SelectedLight', (value) => {
	CONTROLS.setValue('Config.DarkMode',false);
	STYLE.setTheme('Light.' + value);
});

/*
 * Config.CodeFont
 */
cmsjs.State.addListener('Config.CodeFont', (font) => {
	STYLE.setGoogleFont(CONSOLE.getComponent('content-markup'),font,'monospace, sans-serif');
	STYLE.setGoogleFont(CONSOLE.getComponent('content-raw'),font,'monospace, sans-serif');
});

/*
 * Config.ConsoleFont
 */
cmsjs.State.addListener('Config.ConsoleFont', (font) => {
	const components = ['header','controls','footer','themes','fonts'];
	components.forEach((item) => {
		STYLE.setGoogleFont(CONSOLE.getComponent(item),font,'sans-serif');
	});
});

/*
 * Config.DisplayFont
 */
cmsjs.State.addListener('Config.DisplayFont', (font) => {
	STYLE.setGoogleFont(CONSOLE.getComponent('content-docs'),font,'sans-serif');
});

/*
 * Config.CodeFontSize
 */
cmsjs.State.addListener('Config.CodeFontSize', (size) => {
	CONSOLE.getComponent('content-markup').style.fontSize = STYLE.getFontSize(size);
	CONSOLE.getComponent('content-raw').style.fontSize = STYLE.getFontSize(size);
});

/*
 * Config.ConsoleFontSize
 */
cmsjs.State.addListener('Config.ConsoleFontSize', (size) => {
	const components = ['header','controls','footer','themes','fonts'];
	components.forEach((item) => {
		CONSOLE.getComponent(item).style.fontSize = STYLE.getFontSize(size);
	});
});

/*
 * Config.DisplayFontSize
 */
cmsjs.State.addListener('Config.DisplayFontSize', (size) => {
	CONSOLE.getComponent('content-docs').style.fontSize = STYLE.getFontSize(size);
});

/*
 * Config.Language
 */
cmsjs.State.addListener('Config.Language', (lang) => {
	LANG.CurrentLanguage = lang;
	cmsjs.State.set('Config.Language',lang);
	CONSOLE.buildComponent('header');
	CONSOLE.buildComponent('footer');
	CONSOLE.buildComponent('controls');
	CONSOLE.buildComponent('content-docs');
});

/*
 * Config.ControlKeys
 */
cmsjs.State.addListener('Config.ControlKeys', (value) => {
	if ( cmsjs.State.get('Config.ControlKeys') ) { document.addEventListener('keydown',KEYS.processEvent); }
	else { document.removeEventListener('keydown',KEYS.processEvent); }
});



log.trace('loading cms.component.js');


// Header
cmsjs.Component.create('header', (component) => {
	log.debug('header');
	const wrapper = component.wrapper;
	wrapper.style.borderBottom = 'solid 1px';
	wrapper.style.padding = '0.25rem 0.5rem';
	const header = component.element;
	header.innerHTML = '';
	//STYLE.setGoogleFont(header.parentElement,cmsjs.State.get('Config.ConsoleFont'),'sans-serif');
	const urlDiv = $.ce('div');
	urlDiv.id = 'url';
	urlDiv.style.width = '100%';
	urlDiv.style.fontSize = '90%';
	urlDiv.style.padding = '0.25rem 0rem';
	urlDiv.style.marginBottom = '0.25rem';
	//urlDiv.classList.add('heading');
	if ( cmsjs.State.get('ENV.StandaloneMode') ) {
		urlDiv.innerHTML = 'JSON Navigator'
	}
	else {
		urlDiv.innerHTML = window.location.href.split('#')[0];
	}
	if ( ! cmsjs.State.get('Config.ShowURL') ) { urlDiv.style.display = 'none'; }
	header.append(urlDiv);
	const searchDiv = $.ce('div');
	searchDiv.id = 'search';
	searchDiv.style.width = '100%';
	searchDiv.style.padding = '0.25rem 0rem';
	searchDiv.style.display = 'flex';
	searchDiv.style.alignItems = 'left';
	if ( ! cmsjs.State.get('Config.ShowSearch') ) { searchDiv.style.display = 'none'; }
	header.append(searchDiv);
	if ( ! cmsjs.State.get('Config.ShowURL') && ! cmsjs.State.get('Config.ShowSearch') ) {
		header.parentElement.style.display = 'none';
	}
	const searchLabel = $.ce('label');
	searchLabel.setAttribute('for','search-input');
	searchLabel.style.marginRight = '0.5rem'
	searchLabel.innerHTML = cmsjs.Dictionary.get('Controls').languages.get('en').get('Search');
	searchDiv.append(searchLabel);
	const searchInput = $.ce('input');
	searchInput.id = 'search-input';
	searchInput.setAttribute('tabindex','-1');
	searchInput.style.flex = '1';
	searchInput.style.paddingRight = '1rem';
	searchDiv.append(searchInput);
	searchInput.addEventListener('keypress', function(event) {
		if ( event.key === 'Enter' ) {
			const value = event.target.value;
			LOG.debug('search document for value: ',value);
			const currentJSON = getValueForJsonPath(cmsjs.State.get('ENV.CurrentPath'));
			$.id('root').classList.remove('filtered');
			searchJsonWithPath(currentJSON,value);
			$.id('root').querySelectorAll('div.item > div.contents > div.item').forEach((item) => {
			});
			$.id('root').classList.add('filtered');
		}
	});
	//searchInput.addEventListener('input', function(event) {
	//});
});





// Footer
cmsjs.Component.create('footer', (component) => {
	const wrapper = component.wrapper;
	wrapper.style.borderTop = 'solid 1px';
	wrapper.style.padding = '0.1rem';
	const footer = component.element;
	footer.innerHTML = '';
	footer.style.width = '100%';
	footer.style.textAlign = 'center';
	footer.style.padding = '0.5rem';
	//footer.style.fontSize = STYLE.getFontSize(cmsjs.State.get('Config.ConsoleFontSize'));
	//STYLE.setGoogleFont(footer.parentElement,cmsjs.State.get('Config.ConsoleFont'),'sans-serif');
	let footerDiv = $.ce('div');
	const docLink = $.ce('a');
	docLink.href = '';
	docLink.textContent = cmsjs.Dictionary.get('Controls').languages.get('en').get('Documentation');
	docLink.addEventListener('click',function(event) { event.preventDefault(); showPage('content-docs'); });
	footerDiv.append(docLink);
	footerDiv.append($.ctn('\u00A0\u00A0\u00A0\u2022\u00A0\u00A0\u00A0'));
	const gitlabLink = $.ce('a');
	gitlabLink.href = "https://gitlab.com/r55man/json-navigator";
	gitlabLink.textContent = 'GitLab';
	footerDiv.append(gitlabLink);
	footerDiv.append($.ctn('\u00A0\u00A0\u00A0\u2022\u00A0\u00A0\u00A0'));
	const chromeLink = $.ce('a');
	chromeLink.href = "https://chromewebstore.google.com/detail/glgilclfmgofaeffphalkglkgbbpmigf";
	chromeLink.textContent = 'Chrome Store';
	footerDiv.append(chromeLink);
	footer.append(footerDiv);
});




/**
 * Control Panel
 */
cmsjs.Component.create('controls', (component) => {
	component.setScrollable(true);
	const wrapper = component.wrapper;
	wrapper.style.borderLeft = 'solid 1px';
	const controls = component.element;
	controls.style.textAlign = 'right';
	controls.style.padding = '1rem';
	
	/*
	controls.innerHTML = '';
	controls.parentElement.style.padding = '1rem 0.5rem 2rem 0.5rem';
	controls.parentElement.style.overflowY = 'scroll'; // keep 'scroll' to prevent redraw problems
	controls.style.height = '100%';
	controls.style.paddingBottom = '2rem';
	*/
	//controls.style.fontSize = STYLE.getFontSize(cmsjs.State.get('Config.ConsoleFontSize'));
	//STYLE.setGoogleFont(controls.parentElement,cmsjs.State.get('Config.ConsoleFont'),'sans-serif');
	// <div> control panel header
	const headerCP = document.createElement('div');
	//STYLE.bindElement(headerCP,'color','heading-color');
	headerCP.style.fontSize = '1rem';
	headerCP.style.fontWeight = 'bold';
	headerCP.style.textDecoration = 'underline';
	headerCP.style.textAlign = 'center';
	headerCP.style.marginBottom = '0.75rem';
	headerCP.classList.add('heading');
	headerCP.innerHTML = 'JSON Navigator';
	controls.append(headerCP);
	// JSON Interaction
	controls.append(createControl('toggle','Config.MarkupMode'));
	controls.append(createControl('toggle','Config.ShowNulls'));
	controls.append(createControl('toggle','Config.BoldKeys'));
	controls.append(createControl('toggle','Config.ShowBytes'));
	controls.append(createControl('toggle','Config.ShowCount'));
	controls.append(createControl('toggle','Config.SortKeys'));
	controls.append(createControl('toggle','Config.ShowGuidelines'));
	controls.append(createControl('toggle','Config.AutoExpand'));
	controls.append(createControl('toggle','Config.MarkupHTML'));
	controls.append(createControl('toggle','Config.ConvertLinks'));
	controls.append(createControl('select:integer','Config.IndentDepth',{min:1,max:10}));
	controls.append(createControl('select:integer','Config.ExpandDepth',{min:1,max:10}));
	controls.append(createControl('select:integer','Config.Spacing',{min:1,max:10}));
	controls.append(createControl('toggle','Config.LineWrap'));
	//controls.append(createControl('toggle','Config.EditorMode'));
	// UI
	const uiHeader = $.ce('div');
	uiHeader.style.fontWeight = 'bold';
	uiHeader.style.textAlign = 'center';
	uiHeader.style.margin = '0.5rem 0rem';
	uiHeader.style.padding = '0.25rem';
	uiHeader.style.border = 'solid 1px';
	uiHeader.classList.add('heading');
	uiHeader.innerHTML = cmsjs.Dictionary.get(cmsjs.State.get('Config.Language'),'Console');
	STYLE.bindElement(uiHeader,'color','heading-color');
	controls.append(uiHeader);
	controls.append(createControl('toggle','Config.DarkMode'));
	controls.append(createControl('toggle','Config.ShowURL'));
	controls.append(createControl('toggle','Config.ShowSearch'));
	controls.append(createControl('toggle','Config.ControlKeys'));
	controls.append(createControl('toggle','Config.ShowFooter'));
	controls.append(createControl('toggle','Config.Debug'));
	controls.append(createControl('select','Config.SelectedLight',{items:Array.from(STYLE.getThemeNamespace('Light').keys())}));
	controls.append(createControl('select','Config.SelectedDark',{items:Array.from(STYLE.getThemeNamespace('Dark').keys())}));
	//controls.append(createControl('select','Config.ControlPosition',{items:Object.keys{'Right':'Right','Left':'Left'}));
	//const languages = {}; LANG.keys().forEach((lang) => { languages[LANG.get(lang).get('label')] = lang; });
	controls.append(createControl('select','Config.Language',{items:cmsjs.Dictionary.getDictionaryIndex()}));
	
	const themeLink = $.ce('div');
	themeLink.style.margin = '2rem 0px 1rem 0px';
	themeLink.style.textAlign = 'center';
	themeLink.style.fontWeight = 'bold';
	themeLink.style.cursor = 'pointer';
	themeLink.innerHTML = 'Theme Editor'
	themeLink.addEventListener('click', () => { showStyleEditor('themes'); });
	controls.append(themeLink);

	const fontLink = $.ce('div');
	fontLink.style.margin = '2rem 0px 1rem 0px';
	fontLink.style.textAlign = 'center';
	fontLink.style.fontWeight = 'bold';
	fontLink.style.cursor = 'pointer';
	fontLink.innerHTML = 'Font Editor'
	fontLink.addEventListener('click', () => { showStyleEditor('fonts'); });
	controls.append(fontLink);

});



/****************************************
 *										*
 *			PAGE BUILDING				*
 *										*
 ****************************************/

/*
 * Never call a page building fuction directly. Use this wrapper function instead.
 * It ensures that the page is only built if it is not already built, and
 * also makes sure that it is not displayed unless it is the current page.
 */
function showPage(id) {
	if ( id === cmsjs.State.get('ENV.CurrentPage') ) {
		return;
	}
	cmsjs.Console.showOnlyComponent(id);
	cmsjs.State.set('ENV.CurrentPage',id);
	const component = cmsjs.Console.getComponent(id);
	STYLE.applyCurrentTheme(component);
}


/*
 * This is called in the various content pages below.
 * Put code common to all pages here.
 */
function initPageDiv(div) {
	cmsjs.Components.COMPONENTS.setScrollable(div);
	div.style.padding = '0.5rem 0.5rem 1.5rem 0.5rem';
}

/*
 * PAGE: Markup View
 */
cmsjs.Component.create('content-markup', (div) => {
	initPageDiv(div);
	STYLE.setGoogleFont(div,cmsjs.State.get('Config.CodeFont'),'monospace, sans-serif');
	div.style.fontSize = STYLE.getFontSize(cmsjs.State.get('Config.CodeFontSize'));
	//const rootElement = getItemElement(null,ENV.JSON,cmsjs.State.get('Config.ShowBytes'));
	if ( ! cmsjs.State.get('ENV.CurrentPath') ) {
		cmsjs.State.set('ENV.CurrentPath',window.location.hash.substring(1) || '$');
	}
	const pathDiv = getClickablePath(cmsjs.State.get('ENV.CurrentPath'));
	if ( ! $.id('json-path') ) {
		pathDiv.id = 'json-path';
		pathDiv.style.display = 'none';
		//pathDiv.style.marginTop = '-0.75rem';
		pathDiv.style.marginBottom = '0.5rem';
		pathDiv.style.position = 'sticky';
		pathDiv.style.zIndex = '100';
		STYLE.bindElement(pathDiv,'background-color','background-color');
		STYLE.bindElement(pathDiv,'color','heading-color');
		pathDiv.style.opacity = '1.0';
		pathDiv.style.top = '0';
		pathDiv.style.width = '100%';
		pathDiv.style.justifyContent = 'flex-start';
		div.parentElement.prepend(pathDiv);
		//div.append(pathDiv);
	}
	const pathElt = $.id('json-path');
	if ( cmsjs.State.get('ENV.CurrentPath') !== '$' ) {
		pathElt.style.display = 'flex';
	}
	const json = getValueForJsonPath(cmsjs.State.get('ENV.CurrentPath'));
	const rootElement = getItemElement(null,json,true);
	rootElement.id = 'root';
	rootElement.setAttribute('depth','0');
	rootElement.setAttribute('key','$');
	div.appendChild(rootElement);
	if ( (typeof json === 'object' && json !== null) || Array.isArray(json) ) {
		const valElt = rootElement.querySelector('div.label > span.value');
		//let isNull = false;
		if ( Array.isArray(json) ) {
			if ( json.length !== 0 ) {
				expandItem(rootElement,json);
			}
		}
		else if ( Object.keys(json).length !== 0 ) {
			expandItem(rootElement,json);
		}
		valElt.setAttribute('tabindex','1');
		if ( cmsjs.State.get('Config.AutoExpand') ) { expandAll(); }
		valElt.focus();
		//console.warn(json);
		//console.warn(valElt);
	}
	// reset the scrollbar to the top.
	div.parentElement.scrollTop = 0;
	STYLE.applyCurrentTheme(div);
});

/*
 * PAGE: Raw View
 */
cmsjs.Component.create('content-raw', (div) => {
	initPageDiv(div);
	const jsonText = JSON.stringify(cmsjs.State.get('ENV.JSON'),null,4);
	const escapedText = HTML.escapeString(jsonText);
	div.innerHTML = escapedText;
	div.style.whiteSpace = 'pre';
});

/*
 * PAGE: Editor View
 */
/*
pbf.set('editor', (div) => {
	div.style.height = '100%';
	const textarea = $.ce('textarea');
	textarea.id = 'editor';
	textarea.style.width = '100%';
	textarea.style.height = '100%';
	textarea.style.resize = 'none';
	textarea.style.whiteSpace = 'nowrap';
	textarea.addEventListener('change', (event) => {
		cmsjs.State.set('ENV.EditorChanged',true);
    });          
	div.append(textarea);
	textarea.value = JSON.stringify(cmsjs.State.get('ENV.JSON'),null,4);
});
*/

/*
function getContentHeading(text) {
	const heading = $.ce('div');
	heading.style.fontSize = '2rem';
	heading.style.fontWeight = 'bold';
	heading.style.marginTop = '1rem';
	heading.style.marginBottom = '1rem';
	heading.style.display = 'flex';
	heading.style.justifyContent = 'space-between';
	heading.classList.add('heading');
	STYLE.bindElement(heading,'color','heading-color');
	const title = $.ce('span');
	title.innerHTML = text;
	heading.append(title);
	return heading;
}
*/

function getCloseButton() {
	const close = $.ce('div');
	close.append(HTML.getGoogleIcon('close'));
	close.style.cursor = 'pointer';
	close.style.padding = '0.25rem 0.5rem';
	close.style.border = 'solid 1px';
	close.style.position = 'sticky';
	close.style.top = '0.5rem';
	close.style.float = 'right';
	STYLE.bindElement(close,'background-color','background-color');
	close.style.borderRadius = '5px';
	close.addEventListener('mouseenter', (event) => {
		close.style.backgroundColor = STYLE.getThemeValue('heading-color');
	});
	close.addEventListener('mouseleave', (event) => {
		STYLE.rethemeElement(close);
	});
	return close;
}

/*
 * PAGE: Documentation
 */
cmsjs.Component.create('content-docs', (div) => {
	initPageDiv(div);
	const close = getCloseButton();
	close.addEventListener('click', (event) => {
		if ( cmsjs.State.get('Config.MarkupMode') ) {
			showPage('content-markup');
		}
		else {
			showPage('content-raw');
		}
	});
	div.append(close);
	STYLE.setGoogleFont(div,cmsjs.State.get('Config.DisplayFont'),'serif');
	div.style.fontSize = STYLE.getFontSize(cmsjs.State.get('Config.DisplayFontSize'));
	const fragment = document.createDocumentFragment();
	
	function getDataTable(data) {
		const table = $.ce('table');
		table.style.border = 'solid 1px';
		const currentBorderColor = window.getComputedStyle(cmsjs.Console.getPanel('header')).color.replace('rgb','rbga').replace(')',', 0.1)');
		console.warn(currentBorderColor);
		table.style.borderColor = currentBorderColor;
		table.style.marginBottom = '1rem';
		table.style.borderCollapse = 'collapse';
		const tbody = $.ce('tbody');
		table.append(tbody);
		data.forEach((row) => {
			const tr = $.ce('tr');
			tbody.append(tr);
			row.forEach((item) => {
				const td = $.ce('td');
				tr.append(td);
				td.style.border = 'solid 1px';
				td.style.borderColor = currentBorderColor;
				td.style.borderCollapse = 'collapse';
				td.style.padding = '0.75rem';
				if ( item ) {
					const content = $.ce('div');
					td.append(content);
					content.innerHTML = item;
				}
			});
		});
		return table;
	}

	const cpHeader = $.ce('h1');
	cpHeader.innerHTML = 'Control Panel';
	STYLE.bindElement(cpHeader,'color','heading-color');
	fragment.append(cpHeader);
	const cpData = Array.from(LANG.getNamespace(cmsjs.State.get('Config.Language'),'Console.Docs').entries());
	const cpTable = getDataTable(cpData);
	cpTable.querySelectorAll('tr td:nth-child(1) div').forEach((div) => {
		STYLE.bindElement(div,'color','text-2-color');
		div.style.whiteSpace = 'pre';
		div.style.fontWeight = 'bold';
	});
	fragment.append(cpTable);

	const keyHeader = $.ce('h1');
	keyHeader.innerHTML = 'Keybindings';
	STYLE.bindElement(keyHeader,'color','heading-color');
	fragment.append(keyHeader);
	const keyData = LANG.getNamespace(cmsjs.State.get('Config.Language'),'Keybindings');
	const keyDataArray = [];
	keyData.forEach((docs,action) => {
		const keyBinding = KEYS.getBinding(action);
		keyDataArray.push([action,keyBinding,docs]);
	});
	const keyTable = getDataTable(keyDataArray);
	keyTable.querySelectorAll('tr td:nth-child(1) div').forEach((div) => {
		STYLE.bindElement(div,'color','text-2-color');
	});
	keyTable.querySelectorAll('tr td:nth-child(2) div').forEach((div) => {
		STYLE.bindElement(div,'color','text-2-color');
	});
	fragment.append(keyTable);
	
	// Append the fragment at the end.
	div.append(fragment);
});

/*
 * PAGE: Advanced Options
 */
/*
pbf.set('advanced', (div) => {
	populateThemeDiv(div);
});
*/

/*
 * PAGE: Standalone
 */
cmsjs.Component.create('content-demo', (div) => {
	initPageDiv(div);
	div.style.fontSize = STYLE.getFontSize(cmsjs.State.get('Config.DisplayFontSize'));
	const params = new URLSearchParams(window.location.search);
	const files = [
		{ 'tests/json1-psrd-20k.json' : '20kB Pathfinder System Reference Document (PFSRD)' },
		{ 'tests/json2-psrd-1MB.json' : '1MB Pathfinder System Reference Document (PFSRD)' },
		{ 'tests/json3-wotr-2MB.json' : '2MB Pathfinder WotR save file data' },
		{ 'tests/json4-pfkm-11MB-deep-nesting.json' : '11MB Pathfinder Kingmaker save file data (deep nesting)' },
		{ 'tests/json5-pfkm-8MB-35000-line-array.json' : '8MB Pathfinder Kingmaker language file (array with 35,000 items) [NOTE: may take several seconds to open with expand depth set higher than 1]' }
	]
	if ( params.get('f') ) {
		const index = params.get('f');
		const href = Object.keys(files[index])[0];
		fetch(href).then((r) => r.json()).then((json) => {
			cmsjs.State.set('ENV.JSON',json);
			if ( cmsjs.State.get('Config.MarkupMode') ) {
				cmsjs.State.set('ENV.CurrentPath',window.location.hash.substring(1) || '$');
				showPage('content-markup');
			}
			else {
				showPage('content-raw');
			}
		})
		.catch(error => {
			console.error('Fetch error:', error);
		});
	}
	else {
		const fragment = document.createDocumentFragment();
		fragment.append($.ce('h2','JSON Navigator Demo'));
		fragment.append($.ce('p','This is a demonstration of the <strong>JSON Navigator</strong> Chrome extension.'));
		fragment.append($.ce('p','When the extension is installed, all JSON files that you visit using Chrome will be loaded into this console.'));
		fragment.append($.ce('p','Some example JSON files are provided below. Select a file to load into the console. Reloading the page will return you to this screen:'));
		const ul = $.ce('ul');
		ul.style.marginTop = '1rem';
		fragment.append(ul);
		files.forEach((file,index) => {
			const a = $.ce('a');
			const href = Object.keys(file)[0];
			a.href = '?f=' + index;
			a.innerHTML = file[href];
			const li = $.ce('li');
			li.style.marginBottom = '1rem';
			li.append(a);
			ul.append(li);
		});
		const upload = $.ce('input');

		fragment.append($.ce('p','This is a demonstration of the <strong>JSON Navigator</strong> Chrome extension.'));
		div.append(fragment);
	}
});


function getFileUpload() {
	//ui.appendContent(self,$.ce('style',null,null,'#drop-area.hover { outline: dashed 2px #707 !important; background-color: #fef !important; }'));
	const dropArea = $.ce('div');
	dropArea.addEventListener('hover', (event) => {
		event.target.style.outline = 'dashed 2px #707 !important';
		event.target.style.backgroundColor = 'dashed 2px #fef !important';
	});
	dropArea.style.width = '80%';
	dropArea.style.textAlign = 'center';
	dropArea.style.margin = '1rem auto'
	dropArea.style.padding = '1rem 3rem'
	dropArea.style.borderRadius = '1rem'
	dropArea.style.outline = 'dashed 2px #060';
	dropArea.style.backgroundColor = '#dfd';
	dropArea.addEventListener('dragenter', da_hover, false);
	dropArea.addEventListener('dragleave', da_unhover, false);
	dropArea.addEventListener('dragover', da_hover, false);
	dropArea.addEventListener('drop', da_unhover, false);
	['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
		dropArea.addEventListener(eventName, da_preventDefaults, false);
	})
	dropArea.addEventListener('drop', da_handleDrop, false)
	function da_preventDefaults (e) { e.preventDefault(); e.stopPropagation(); }
	function da_hover (e) { dropArea.classList.add('hover') }
	function da_unhover(e) { dropArea.classList.remove('hover') }
	function da_handleDrop(e) { loadZKS(((e.dataTransfer).files)[0]); }
	dropArea.append($.ce('style',null,null,'p { margin: 2rem auto; max-width: 30rem; } p.note { margin-top: 2rem; font-style: italic; font-size: 0.9rem; } p.output { display: hidden; }'));
	dropArea.append($.ce('h2',null,null,'Kingmaker Save File Analyzer'));
	dropArea.append($.ce('p',null,null,'Welcome to the Kingmaker Save File Analyzer :)'));
	dropArea.append($.ce('p',null,null,'Uploading a .zks save file will allow you to get a full history of your game path and see what you have uncovered and what you might have missed.'));
	dropArea.append($.ce('p',null,null,'<strong>Drag and drop a .zks save file into this area.</strong>'));
	const fileSelector = $.ce('div');
	fileSelector.id = 'file-selector';
	const fileSelectorInput = $.ce('input');
	fileSelectorInput.type = 'file';
	fileSelectorInput.id = 'file-selector-input';
	fileSelectorInput.style.display = 'none';
	fileSelectorInput.addEventListener('change', function() {
		loadZKS(this.files[0]);
	});
	const fileSelectorButton = $.ce('button');
	fileSelectorButton.id = 'file-selector-button';
	fileSelectorButton.textContent = 'Or click here to select a .zks file';
	fileSelectorButton.style.fontSize = '1.1rem';
	fileSelectorButton.style.padding = '0.5rem';
	fileSelectorButton.style.borderRadius = '0.5rem';
	fileSelectorButton.addEventListener('click', function() {
		document.getElementById('file-selector-input').click();
	});
	fileSelector.appendChild(fileSelectorInput);
	fileSelector.appendChild(fileSelectorButton);
	dropArea.appendChild(fileSelector);
	const feedback = $.ce('p','droparea-feedback',null,'');
	dropArea.append(feedback);
	feedback.style.border = 'dotted 1px black';
	feedback.style.borderRadius = '0.3rem';
	feedback.style.backgroundColor = '#eff';
	feedback.style.padding = '1rem';
	if ( zks ) {
		feedback.innerHTML = 'You currently have a .zks file loaded.<br>Uploading a new file will replace the current one.';
	}
	else {
		feedback.innerHTML = 'You do not currently have a .zks file loaded.';
	}
	dropArea.append($.ce('p',null,'note','<strong>Note:</strong> Using "cleaner" mods may lead to incorrect results, as they sometimes erase the "history" file which is required for the analyzer to work properly.'));
	return dropArea;
}


/*
 * This performs a full-text search for keys or values matching str.
 */
function searchJsonWithPath(json, str) {
	const results = [];
	function search(obj, path = '') {
		if ( Array.isArray(obj) ) {
			obj.forEach((item, index) => {
				search(item, `${path}[${index}]`);
			});
		}
		else if ( typeof obj === 'object' && obj !== null ) {
			Object.keys(obj).forEach(key => {
				const newPath = path ? `${path}.${key}` : key;
				if ( typeof obj[key] === 'string' && obj[key].includes(str) ) {
					results.push(newPath);
				}
				else if ( key.includes(str) ) {
					results.push(newPath);
				}
				else {
					search(obj[key], newPath);
				}
			});
		}
	}
	search(json);
	if ( results.length === 0 ) { return; }
	const rootElt = $.id('root');
	if ( ! rootElt.classList.contains('expanded') ) {
		rootElt.querySelector('div.label > span.value').click();
	}
	for ( let i = 0; i < results.length; i++ ) {
		let currentDiv = rootElt;
		const jsonPath = results[i];
		//LOG.debug('processing search match: ', jsonPath);
		const components = jsonPath.split('.');
		components.forEach((item) => {
			//console.warn(item);
			if ( item.endsWith(']') ) {
				let [name,index] = item.split('[');
				index = index.slice(0,-1);
				const match = currentDiv.querySelector('div.contents > div.item[key="' + name + '"]');
				if ( match ) {
					if ( ! match.classList.contains('expanded') ) {
						match.querySelector('div.label > span.value').click();
					}
					const nthDiv = match.querySelector('div.contents > div.item:nth-child(' + (parseInt(index)+1) + ')')
					if ( ! nthDiv.classList.contains('expanded') ) {
						nthDiv.querySelector('div.label > span.value').click();
					}
					currentDiv = nthDiv;
				}
			}
			else {
				let name = item;
				const match = currentDiv.querySelector('div.contents > div.item[key="' + name + '"]');
				if ( match ) {
					if ( ! match.classList.contains('expandable') ) {
						match.classList.add('unfiltered');
					}
					else if ( ! match.classList.contains('expanded') ) {
						match.querySelector('div.label > span.value').click();
					}
					currentDiv = match;
				}
				else {
					//LOG.error('no match for component "' + item + '" in matching path: ',results[i]);
				}
			}
		});
	}
	return results;
}
	


/*
 * This takes a JSON Path syntax for a key and returns the corresponding value.
 */
function getValueForJsonPath(jsonPath) {
	if ( jsonPath === '$' ) { return cmsjs.State.get('ENV.JSON'); }
	const parts = jsonPath.split('.').slice(1);
	let value = cmsjs.State.get('ENV.JSON');
	for ( const part of parts ) {
		if ( part.endsWith(']') ) {
			let [keyname,index] = part.split('[');
			index = parseInt(index.slice(0,-1));
			value = value[keyname][index];
		} else {
			value = value[part];
		}
		
	}
	//LOG.debug('getValueForJsonPath(' + jsonPath + '): value is ',value);
	return value;
}

/*
 * Gets a clickable path for quick navigation.
 */
function getClickablePath(jsonPath) {
	const container = $.ce('div');
	//STYLE.bindElement(container,'color','heading-color');
	container.style.display = 'flex';
	container.style.fontFamily = 'sans-serif';
	container.style.alignItems = 'center';
	container.style.verticalAlign = 'middle';
	const div = $.ce('div');
	div.style.border = 'solid 1px';
	div.style.width = '100%';
	div.style.padding = '0.25rem 0.75rem';
	const parts = jsonPath.split('.');
	for (let i = 0; i < parts.length; i++) {
		if ( parts[i].endsWith(']') ) {
			let [key,index] = parts[i].split('[');
			index = index.slice(0,-1);
			const keyPath = parts.slice(0, i).join('.') + '.' + key;
			const link = $.ce('a');
			link.style.paddingLeft = '0.4em';
			link.href = '#' + keyPath;
			link.textContent = key;
			div.append(link);
			const link2 = $.ce('a');
			const indexPath = parts.slice(0, i+1).join('.');
			link2.href = '#' + indexPath;
			link2.textContent = ' [ ' + index + ' ] ';
			div.append(link2);
			if ( i < parts.length - 1 ) {
				div.append($.ctn('.'));
			}
		}
		else {
			const path = parts.slice(0, i+1).join('.');
			if ( i < parts.length ) {
				const link = $.ce('a');
				link.textContent = parts[i];
				link.style.paddingLeft = '0.4em';
				link.style.paddingRight = '0.4em';
				link.href = '#' + path;
				div.append(link);
			}
			else {
				div.append($.ctn(parts[i]));
			}
			if ( i < parts.length - 1 ) {
				div.append($.ctn('.'));
			}
		}
	}
	container.append(div);
	return container;
}

/*
 * Takes a JSON Path and returns the corresponding div.item element.
 */
function getItemForJsonPath(jsonPath) {
	const rootElement = $.id('root');
	if ( jsonPath === '$' ) { return rootElement; }
	const parts = jsonPath.split('.').slice(1);
	//parts.shift();
	let element = rootElement;
	for ( const part of parts ) {
		if ( part.endsWith(']') ) {
			let [keyname,index] = part.split('[');
			index = parseInt(index.slice(0,-1)) + 1;
			element = element.querySelector('div.contents > div.item[key="' + keyname + '"] > div.contents > :nth-child(' + index + ')');
		}
		else {
			element = element.querySelector('div.contents > div.item[key="' + part + '"]');
		}
		if ( ! element ) {
			LOG.error('could not find element for JSON Path: ', jsonPath);
		}
	}
	LOG.debug('getItemForJsonPath(' + jsonPath + '): returning element: ',element);
	return element;
}

/*
 * Takes a div.item element and returns the corresponding JSON Path.
 */
function getJsonPathForItem(element) {
	let jsonPath = '';
	while ( element.id !== 'root' ) {
		const parent = element.parentElement.parentElement;
		const keyAttribute = element.getAttribute('key');
		const parentAttribute = parent.getAttribute('key');
		if ( parent.classList.contains('array') ) {
			if ( jsonPath !== '' ) {
				jsonPath = `[${keyAttribute}].${jsonPath}`;
			}
			else jsonPath = `[${keyAttribute}]`;
		}
		else {
			if ( jsonPath.startsWith('[') ) {
				jsonPath = `${keyAttribute}${jsonPath}`;
			}
			else if ( jsonPath !== '' ) {
				jsonPath = `${keyAttribute}.${jsonPath}`;
			}
			else {
				jsonPath = `${keyAttribute}`;
			}
		}
		element = parent;
	}
	if ( jsonPath !== '' ) {
		jsonPath = '$.' + jsonPath;
	}
	else {
		jsonPath = '$';
	}
	//LOG.debug('getJsonPathForItem(): ' + jsonPath);
	return jsonPath;
}


/*
 * Takes a div.item element and returns the corresponding JSON data.
 */
function getJsonForItem(element) {
	const jsonPath = getJsonPathForItem(element);
	const json = getValueForJsonPath(jsonPath);
	return json;
}

/*
 * Expandable items are those that contain (or map to) a non-empty
 * array or object. This function takes an existing array/object
 * item (div.item.expandable) and the json array/object
 * that it maps to, and builds out the next level of items (div.item)
 *
 * This process is only done once. Once the element has been drawn
 * we simply turn it on and off with display:block/none.
 */
function expandItem(item,json) {
	if ( ! item.querySelector('div.contents') ) {
		const newItem = $.ce('div');
		const newDepth = parseInt(item.getAttribute('depth')) + 1;
		newItem.setAttribute('depth',newDepth);
		newItem.classList.add('contents');
		newItem.style.marginLeft = cmsjs.State.get('Config.IndentDepth')/2 + 'rem';
		newItem.style.paddingLeft = '0.25rem';
		newItem.style.borderLeft = 'dashed 1px';
		STYLE.bindElement(newItem,'border-left-color','guideline-' + newDepth + '-color');
		if ( cmsjs.State.get('Config.ShowGuidelines') ) {
			newItem.style.borderLeftColor = getBorderColor(newDepth);
		}
		else {
			newItem.style.borderLeftColor = 'rgba(0,0,0,0.0)';
		}
		const fragment = document.createDocumentFragment();
		if ( typeof json === 'object' )  { // this catches arrays too
			const _k = Object.keys(json);
			const keys = (Array.isArray(json)) ? _k : (cmsjs.State.get('Config.SortKeys')) ? _k.sort() : _k;
			keys.forEach((key,index) => {
				const value = json[key];
				const subItem = getItemElement(key,value,item.hasAttribute('bytes'));
				subItem.setAttribute('depth',newDepth);
				fragment.append(subItem);
			});
		}
		newItem.append(fragment);
		item.append(newItem)
	}
	checkItemNull(item);
	item.classList.add('expanded');
	item.classList.add('expanded-once');
	item.querySelector('div.contents').style.display = 'block';
	if ( $.id('root').classList.contains('filtered') ) {
		while ( item.parentElement && item.parentElement.id !== 'root' ) {
			item.parentElement.classList.add('unfiltered');
			item = item.parentElement;
		}
		//console.error('test--->');
		item.querySelectorAll('div.contents > div.item').forEach((x) => {
			x.classList.add('unfiltered');
		});
	}
	STYLE.applyCurrentTheme(item);
}

/*
 * As you reveal the DOM, items will be discovered to be null because 
 * all of their children are null. This function wraps the calls
 * necessary to set something null on the fly.
 */
function checkItemNull(item) {
	if ( item.classList.contains('null') ) {
		if ( item.parentElement && item.parentElement.classList.contains('item') ) {
			checkItemNull(item.parentElement);
		}
	}
	else {
		let hasNonNull = false;
		item.querySelectorAll('div.contents > div.item').forEach((item) => {
			if ( ! item.classList.contains('null') ) {
				hasNonNull = true;
				return;
			}
		});
		if ( ! hasNonNull ) {
			item.classList.add('null');
			item.style.opacity = (cmsjs.State.get('Config.NullOpacity') + 1)/2;
			if ( ! cmsjs.State.get('Config.ShowNulls') ) {
				item.style.display = 'none';
			}
			if ( item.parentElement && item.parentElement.classList.contains('item') ) {
				checkItemNull(item.parentElement);
			}
		}
	}
}

/*
 * Collapse the specified item.
 * This simply hides the element.
 */
function collapseItem(item) {
	const contents = item.querySelector('div.contents');
	if ( contents ) {
		contents.style.display = 'none';
	}
	item.classList.remove('expanded');
	item.classList.add('collapsed');
}

/*
 * Expand all elements from the specified up to Config.ExpandDepth.
 *
 * "endval" is an ugly hack to allow search/filter to use this without being
 * limited by depth.
 */
function expandAll(element,endval) {
	let currentDepth = 0;
	if ( ! element ) {
		//element = document.getElementById('console-content-page-markup');
		element = cmsjs.Console.getComponent('content-markup');
	}
	else if ( ! 'getAttribute' in element ) {
		LOG.error('expandAll(): called with something other than an element',element);
	}
	else {
		currentDepth = element.hasAttribute('depth') ? element.getAttribute('depth') : null;
		if ( ! currentDepth ) {
			LOG.error('expandAll(): called with element that has no depth attribute',element);
			LOG.error('expandAll(): setting currentDepth to zero and hoping for the best...');
			currentDepth = 0;
		}
	}
	const startVal = parseInt(currentDepth);
	LOG.debug("expandAll is still calling parseInt...");
	const endVal = parseInt(currentDepth) + parseInt(cmsjs.State.get('Config.ExpandDepth'));
	const startTime = LOG.timer();
	for ( let i = startVal; i < endVal; i++ ) {
		const t0 = LOG.timer();
		const matches = element.querySelectorAll('div.expandable[depth="' + i + '"]:not(.expanded) > div.label > span.value');
		if ( matches == 0 ) { break; }
		matches.forEach((val) => {
			val.dispatchEvent(new MouseEvent('click'));
		});
		const t1 = LOG.timer();
		const timeTaken = t1 - t0;
		if ( ! cmsjs.State.get('ENV.PageLoaded') ) {
			if ( timeTaken > 50 ) {
				LOG.debug('expandAll(): initial page load processing taking too long: ' + timeTaken + 'ms');
				LOG.debug('expandAll(): printing what we have and bailing');
				break;
			}
		}
		else if ( timeTaken > 250 ) {
			LOG.debug('expandAll(): processing taking too long: ' + timeTaken + 'ms');
			LOG.debug('printing what we have and bailing');
			break;
		}
	}
	const endTime = LOG.timer();
	LOG.debug('expandAll(): expansion took ' + (endTime - startTime) + 'ms');
}

/*
 * Collapse all children of specified item.
 */
function collapseAll(item) {
	item.querySelectorAll('div.expanded').forEach((c) => {
		collapseItem(c);
	});
}


/*
 * Process a key/value pair and return a DOM element.
 * div.item
 * - div.label
 *   - div.key
 *   - div.val
 *   - div.bytes
 *   - div.controls
 *     - div.expand
 *     - div.collapse
 *     - div.copy
 * - div.contents
 */
function getItemElement(key,val,computeBytes=false) {
	const item = $.ce('div');
	item.classList.add('item');
	//item.style.border = 'solid 1px white';
	const verticalPadding = cmsjs.State.get('Config.Spacing')/30;
	item.style.paddingTop = verticalPadding + 'rem';
	item.style.paddingBottom = verticalPadding + 'rem';
	const label = $.ce('div');
	label.classList.add('label');
	label.style.padding = '0.1rem';
	label.style.display = 'flex';
	label.style.flexWrap = 'nowrap';
	label.style.alignItems = 'top';
	if ( key !== null ) { // the root element does not have a key!
		const keyElt = $.ce('span');
		keyElt.classList.add('key');
		if ( cmsjs.State.get('Config.BoldKeys') ) {
			keyElt.style.fontWeight = 'bold';
		}
		keyElt.style.marginRight = '1rem';
		keyElt.style.alignSelf = 'flex-start';
		item.setAttribute('key',key);
		STYLE.bindElement(keyElt,'color','key-color');
		keyElt.innerHTML = key + ':';
		keyElt.setAttribute('tabindex','-1');
		label.append(keyElt);
	}

	/*********************
	 *      EDITOR       *
	 *********************/
	const editorElt = $.ce('span');
	editorElt.classList.add('editor');
	editorElt.style.display = 'none';
	label.append(editorElt);

	const valElt = $.ce('span');
	label.append(valElt);
	valElt.classList.add('value');
	if ( val == null ) {
		item.classList.add('null');
		item.setAttribute('datatype','null');
		valElt.classList.add('null');
		valElt.setAttribute('datatype','null');
		STYLE.bindElement(valElt,'color','null-color');
		item.style.opacity = parseFloat(cmsjs.State.get('Config.NullOpacity'));
		if ( ! cmsjs.State.get('Config.ShowNulls') ) { item.style.display = 'none'; }
		valElt.innerHTML = 'null';
	}
	else if ( typeof val === 'string' ) {
		valElt.classList.add('string');
		item.setAttribute('datatype','string');
		valElt.setAttribute('datatype','string');
		STYLE.bindElement(valElt,'color','string-color');
		if ( key !== null ) {
			valElt.style.maxWidth = '100%';
			if ( ! cmsjs.State.get('Config.LineWrap') ) {
				valElt.style.whiteSpace = 'nowrap';
			}
			valElt.style.overflow = 'hidden';
		}
		if ( val === '' ) {
			if ( ! cmsjs.State.get('Config.ShowNulls') ) { item.style.display = 'none'; }
			item.classList.add('null');
		}
		if ( ! cmsjs.State.get('Config.MarkupHTML') ) {
			val = HTML.escapeString(val);
		}
		valElt.innerHTML = '"' + val + '"';
		if ( cmsjs.State.get('Config.ConvertLinks') ) {
			convertToLinkIfPossible(valElt);
		}
	}
	else if ( typeof val === 'number' ) {
		valElt.classList.add('number');
		valElt.setAttribute('datatype','number');
		item.setAttribute('datatype','number');
		STYLE.bindElement(valElt,'color','number-color');
		valElt.innerHTML = val;
	}
	else if ( typeof val === 'boolean' ) {
		valElt.classList.add('boolean');
		valElt.setAttribute('datatype','boolean');
		item.setAttribute('datatype','boolean');
		STYLE.bindElement(valElt,'color','boolean-color');
		valElt.innerHTML = val;
	}
	else if ( typeof val === 'object' ) { // catches arrays
		valElt.classList.add('object');
		STYLE.bindElement(valElt,'color','object-color');
		valElt.style.cursor = 'pointer';
		valElt.style.borderRadius = '5px';
		valElt.style.outline = 'solid 1px transparent';
		valElt.addEventListener('mouseenter', () => {
			STYLE.bindElement(valElt,'outline-color','button-outline-color');
		});
		valElt.addEventListener('mouseleave', () => {
			valElt.style.outline = '';
		});
		const isArray = Array.isArray(val);
		const isObject = !isArray;
		if ( isArray ) {
			item.classList.add('array');
			valElt.setAttribute('datatype','array');
			item.setAttribute('datatype','array');
		}
		else {
			item.classList.add('object');
			valElt.setAttribute('datatype','object');
			item.setAttribute('datatype','object');
		}
		const length = (isArray) ? val.length : Object.keys(val).length;
		if ( length === 0 ) {
			item.classList.add('null');
			item.style.opacity = parseFloat(cmsjs.State.get('Config.NullOpacity'));
			if ( isArray ) { valElt.innerHTML = '[ ]'; }
			else { valElt.innerHTML = '{ }'; }
			if ( ! cmsjs.State.get('Config.ShowNulls') ) { item.style.display = 'none'; }
		}
		else {
			// THIS IS WHERE IT IS A NON-EMPTY ARRAY OR OBJECT. THE VALUE ELEMENT MUST BECOME CLICKABLE
			const tabindex = (key) ? '2' : '1';
			valElt.setAttribute('tabindex',tabindex);
			valElt.addEventListener('click', () => {
				item.classList.toggle('expanded');
				//valElt.focus();
				if ( item.classList.contains('expanded') ) {
					expandItem(item,val);
				}
				else {
					collapseItem(item);
				}
			});
			// Make "Enter" expand/collapse items
			valElt.addEventListener('keydown', (event) => {
				if ( event.keyCode === 13 ) {
					valElt.click();
				}
				else if ( event.ctrlKey && event.key == 'c' ) {
					navigator.clipboard.writeText(JSON.stringify(val,null,4));
				}
				// open (expand)
				else if ( event.ctrlKey && event.key == 'o' ) {
					event.preventDefault();
					expandAll(valElt.parentElement.parentElement);
					valElt.focus();
				}
				// invert (collapse)
				else if ( event.ctrlKey && event.key == 'i' ) {
					event.preventDefault();
					collapseAll(valElt.parentElement.parentElement);
					valElt.focus();
				}
			});
			item.classList.add('expandable');
			valElt.append(document.createTextNode((isArray) ? '[' : '{'));
			const count = $.ce('span');
			count.classList.add('count');
			count.setAttribute('count',length);
			if ( cmsjs.State.get('Config.ShowCount') ) { count.innerHTML = ' ' + length + ' '; }
			else { count.innerHTML = '...'; }
			valElt.append(count);
			valElt.append(document.createTextNode((isArray) ? ']' : '}'));

			// PRINT THE BYTES IF REQUESTED
			if ( computeBytes ) {
				const bytes = JSON.stringify(val).length;
				const ratio = Math.round((bytes/cmsjs.State.get('ENV.Bytes'))*100);
				if ( key === null || (bytes >= 5000 && ratio >= 1) ) {
					item.setAttribute('bytes',bytes);
					item.setAttribute('ratio',ratio);
					const bytesElt = $.ce('span');
					bytesElt.classList.add('bytes');
					const kb = Math.round(bytes/1000);
					bytesElt.innerHTML = '' + kb + 'k';
					bytesElt.style.margin = '0px 1rem';
					//bytesElt.style.fontSize = '0.9rem';
					bytesElt.style.fontWeight = 'bold';
					STYLE.bindElement(bytesElt,'color','bytes-color');
					if ( ! cmsjs.State.get('Config.ShowBytes') ) { bytesElt.style.display = 'none'; }
					label.append(bytesElt);
				}
			}
		}
	}
	else {
		LOG.error('unknown type (this should never happen)',typeof val);
	}
	/************************************
	 *									*
	 *		POPUP CONTROL PANEL			*
	 *									*
	 ************************************/
	const controls = $.ce('span');
	controls.classList.add('controls');
	controls.style.marginLeft = '1rem';
	controls.style.display = 'flex';
	controls.style.flexWrap = 'nowrap';
	controls.style.alignSelf = 'flex-start';
	STYLE.bindElement(controls,'color','controls-color');
	controls.style.fontWeight = 'bold';
	label.append(controls);
	function showControls() {
		if ( val !== null && typeof val === 'object' ) {
			if ( controls.innerHTML !== '' ) { return; }
			// COLLAPSE ALL CHILDREN
			const collapseElt = getJSONControl('COLLAPSE','arrow_upward', () => {
				collapseAll(item);
			});
			controls.append(collapseElt);
			// EXPAND ALL CHILDREN
			const expandElt = getJSONControl('EXPAND','arrow_downward', () => {
				expandAll(item);
			});
			controls.append(expandElt);
		}
		if ( val !== null && val !== true && val !== false ) {
			// COPY DATA
			const copyElt = getJSONControl('COPY','content_copy', () => {
				let copiedItem = val;
				if ( ! cmsjs.State.get('Config.ShowNulls') ) {
					copiedItem = removeNullValues(val);
				}
				navigator.clipboard.writeText(JSON.stringify(copiedItem,null,4));
				LOG.debug('{COPY} copied json: ',copiedItem);
			});
			controls.append(copyElt);
		}
		if ( key !== null ) {
			// BURROW
			let jsonPath = getJsonPathForItem(item);
			if ( cmsjs.State.get('ENV.CurrentPath') !== '$' ) {
				const wrongPath = jsonPath.slice(2);
				if ( wrongPath.startsWith('[') ) {
					jsonPath = cmsjs.State.get('ENV.CurrentPath') + jsonPath.slice(2);
				}
				else {
					jsonPath = cmsjs.State.get('ENV.CurrentPath') + '.' + jsonPath.slice(2);
				}
			}
			const burrow = getJSONControl('BURROW','arrow_forward', '#' + jsonPath);
			controls.append(burrow);
		}
		if ( key !== null ) {
			// EDIT
			// Javascript's "typeof" operator is almost useless on its own :/
			function getDataType(item) {
				if ( item === null ) { return 'null'; }
				if ( typeof item !== 'object' ) { return typeof item; }
				if ( Array.isArray(item) ) { return 'array'; }
				return 'object';
			}
			const edit = getJSONControl('EDIT','edit_note', () => {
				// get the json path for this item (will be a key)
				// get the value associated with this json path
				// get the value type (object, array, number, boolean, string, null)
				let jsonPath = getJsonPathForItem(item);
				let jsonValue = getValueForJsonPath(jsonPath);
				const valEltDataType = valElt.getAttribute('datatype');
				const editorSelect = $.ce('select');
				const editorOptions = ['null','true','false','number','string','array','object'];
				editorOptions.forEach((o) => {
					const option = $.ce('option');
					option.value = o;
					option.textContent = o;
					const type = getDataType(jsonValue);
					if ( jsonValue === true && o === 'true' ) {  option.selected = true; }
					else if ( jsonValue === false && o === 'false' ) {  option.selected = true; }
					else if ( getDataType(jsonValue) === o ) { option.selected = true; }
					editorSelect.appendChild(option);
				});
				const textEntry = $.ce('input');
				editorElt.appendChild(editorSelect);
				editorElt.appendChild(textEntry);
				editorElt.style.display = 'inline';
				valElt.style.display = 'none';
				const tmpValElt = valElt;
			});
			//controls.append(edit);
		}
	}
	function hideControls() {
		const delay = (controls.hasAttribute('control-clicked')) ? 1200 : 100;
		controls.removeAttribute('control-clicked');
		setTimeout(() => { controls.innerHTML = ''; },delay);
	}
	label.addEventListener('mouseenter', () => {
		showControls();
	});
	label.addEventListener('mouseleave', () => {
		hideControls();
	});
	item.append(label);
	return item;
}



/*
 * Removed null values from a json object.
 * Returns a copy. The original is unmolested.
 */
function removeNullValues(obj) {
    if ( typeof obj !== 'object' || obj === null ) {
        return obj;
    }
    else if ( Array.isArray(obj) ) {
		const newArray = obj.map(item => removeNullValues(item)).filter(item => item !== null);
		return newArray.length === 0 ? null : newArray;
    }
	else {
	    const newObject = Object.keys(obj).reduce((acc, key) => {
		    const value = removeNullValues(obj[key]);
			if ( value !== null ) { acc[key] = value; }
		    return acc;
	    }, {});
		if ( Object.keys(newObject).length === 0 ) {
			return null;
		}
		return newObject;
	}
}


/*
 * Takes a value element and converts it to a link if it starts with an acceptable prefix.
 *
 * These need to get tagged with a 'converted-url' class so MarkupHTML does not 
 * unlinkify them when it is turned off.
 */
function convertToLinkIfPossible(valElt) {
	if ( /^\"(http|https|pfsrd):\/\/.*$/.test(valElt.innerHTML) ) {
		const link = $.ce('a');
		link.href = valElt.innerHTML.replace(/^"|"$/g,'');
		link.textContent = valElt.innerHTML.slice(1,-1);;
		link.classList.add('converted-url');
		link.setAttribute('tabindex','-1');
		valElt.innerHTML = '';
		valElt.append(link);
		valElt.classList.add('converted-url');
	}
}


/*
 * This is the little popup control panel next to expandable items.
 *
 * 'action' is just used for logging
 */
function getJSONControl(action,icon,onClick) {
	const i = HTML.getGoogleIcon(icon);
	i.style.cursor = 'pointer';
	i.style.margin = '0px 0.2rem';
	i.style.padding = '0px 1rem';
	i.style.borderRadius = '0.25rem';
	const elt = $.ce('a');
	elt.appendChild(i);
	if ( typeof onClick === 'string' ) {
		elt.href = onClick;
	}
	else {
		elt.addEventListener('click', function(event) {
			event.preventDefault();
			LOG.debug('{' + action + '}');
			this.style.cursor = 'wait';
			STYLE.bindElement(this,'color','null-color');
			this.parentElement.setAttribute('control-clicked','true');
			setTimeout(() => {
				onClick();
				const spanElt = this.querySelector('span');
				const spanEltText = spanElt.innerHTML;
				STYLE.bindElement(this,'color','string-color');
				this.style.cursor = 'pointer';
				this.querySelector('span').innerHTML = 'done';
				setTimeout(() => {
					STYLE.bindElement(this,'color','controls-color');
					this.querySelector('span').innerHTML = spanEltText;
				},3000);
			},50);
		});
		elt.classList.add('button');
	}
	elt.style.outline = 'solid 1px transparent';
	elt.addEventListener('mouseenter', function() {
		STYLE.bindElement(elt,'outline-color','button-outline-color');
	});
	elt.addEventListener('mouseleave', function() {
		elt.style.outline = '';
	});
	return elt;
}


function getPopupTextElement(text,hoverText) {
	const textWrapper = $.ce('span');
	textWrapper.style.position = 'relative';
	const textElt = $.ce('span');
	textElt.classList.add('text');
	textElt.innerHTML = text;
	textElt.style.marginRight = '1rem';
	textElt.style.padding = '0.25rem';
	textWrapper.append(textElt);
	const popupNote = $.ce('span');
	popupNote.classList.add('popup-doc');
	popupNote.style.display = 'none';
	popupNote.style.position = 'absolute';
	popupNote.style.right = '100%'
	popupNote.style.width = '12rem'
	popupNote.style.top = '0'
	popupNote.style.color = '#eee';
	popupNote.style.backgroundColor = '#000';
	popupNote.style.opacity = '1.0';
	popupNote.style.border = 'solid 1px #ccc';
	popupNote.style.padding = '0.5rem 1rem';
	popupNote.style.zIndex = '20';
	popupNote.style.borderRadius = '0.5rem';
	popupNote.style.textAlign = 'left';
	popupNote.innerHTML = hoverText;
	textWrapper.append(popupNote);
	//textElt.addEventListener('mouseenter', function() {
	//	if ( cmsjs.State.get('Config.ShowTooltips') ) { popupNote.style.display = 'block'; }
	//});
	textElt.addEventListener('mouseleave', function() { popupNote.style.display = 'none'; } );
	return textWrapper;
}


function createControl(type,key,config={}) {
	config['id'] = key;
	config['value'] = cmsjs.State.get(key);
	config['tabindex'] = '3';
	config['label'] = cmsjs.Dictionary.get('Controls','en').get('Console.Labels',key);
	config['listener:change'] = ((value) => {cmsjs.State.changeAndSave(key,value);});
	config['label-position'] = 'left';
	if ( type === 'toggle' ) {
//		config['slider-on-color'] = STYLE.getThemeValue('console-toggle-slider-on-color');
//		config['slider-off-color'] = STYLE.getThemeValue('console-toggle-slider-off-color');
//		config['button-on-color'] = STYLE.getThemeValue('console-toggle-button-on-color');
//		config['button-off-color'] = STYLE.getThemeValue('console-toggle-button-off-color');
	}
	else if ( type === 'select' ) {
		config['label-position'] = 'top-right';
	}
	const control = cmsjs.Controls.create(type,key,config);
	control.style.margin = '0.75em 0em';
	return control;
}

/****************************************
 *										*
 *		CONTROL PANEL FUNCTIONS			*
 *										*
 ****************************************/

function changeState(id,value) {
	cmsjs.State.changeAndSave(id,value);
}


log.trace('cms.console.js');

let console1 = cmsjs.Console.create('Main','grid');

console1.setTitle('JSON Navigator');
console1.setFavicon('AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACQAAAC0AAAA4AAAAFgAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEQAAAJQAAADJAAAAiQAAALQAAABSAAAAAgAAAAAAAAAIAAAARwAAACUAAAAAAAAAHgAAACwAAAAAAAAAAAAAAEcAAAD3AAAArQAAAAcAAACeAAAA6QAAADsAAAAAAAAAAQAAAE8AAAAdAAAADgAAAKcAAABmAAAAAAAAAAAAAAAmAAAArAAAAGIAAAAAAAAAWgAAAP8AAAChAAAABAAAAAAAAABAAAAAGgAAAIUAAADdAAAAVAAAAAAAAAAAAAAAAQAAAAcAAAADAAAAAAAAACcAAADmAAAA4wAAACQAAAAAAAAAQQAAAHoAAADXAAAAWAAAADsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJAAAAtwAAAP8AAABYAAAAAAAAAHIAAADnAAAAXgAAAB4AAABIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHsAAAD/AAAAlAAAAAgAAABgAAAAXgAAAAUAAAArAAAARwAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABBAAAA9wAAAMwAAAASAAAAAAAAAAAAAAABAAAAAQAAAAEAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFwAAANQAAADzAAAAOAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAAACeAAAA/wAAAHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAAP8AAACsAAAABQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwAAADqAAAA3gAAAB4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALAAAAvwAAAPwAAABNAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgAAAJ8AAAD/AAAAlAAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEQAAAFkAAACvAAAAzwAAALsAAABfAAAADQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYAAAAWAAAAFAAAABIAAAAUAAAAFQAAAAQAAAAAAAAAAAAAAAAAAAAAg/8AAAETAAABAwAAEIMAABCDAADwgwAA+AEAAPhhAAD4fwAA+H8AAPw/AAD8PwAA/D8AAPwfAAD4DwAA+A8AAA==');
console1.addStyle('div.item.null>div.contents>div.item.null { opacicty: 1.0 !important; }');
console1.addStyle('#console-content-page-markup div#root.filtered div.item:not(.expanded) { display: none; }');
console1.addStyle('#console-content-page-markup div#root.filtered div.item.unfiltered { display: block; }');
console1.setGridRows('auto 1fr auto auto');
console1.setGridColumns('1fr auto');

console1.addStyle('div[cmsjs-panel] { min-height: 25px; min-width: 25px; border: solid 1px; }');

const Header = console1.addPanel('Header','1,1,1,1');
const Controls = console1.addPanel('Controls','1,2,2,1');
const Content = console1.addPanel('Content','2,1,1,1');
const Footer = console1.addPanel('Footer','3,1,1,2');
const ThemeEditor = console1.addPanel('ThemeEditor','3,1,1,2');
console1.attachPanel('Header');
console1.attachPanel('Controls');
console1.attachPanel('Content');
console1.attachPanel('Footer');
console1.attachPanel('ThemeEditor');


cmsjs.Console.setActiveConsole('Main');
log.debug('x',cmsjs.Component.Instances);
Header.attachComponent('header');
Footer.attachComponent('footer');
Footer.attachComponent('controls');

/*
//let str = '';
//for ( let i = 0; i < 10000; i++ ) { str += 'Lorem ipsum blah blah '; }
//console1.root.innerHTML = str;

let console2 = cmsjs.Console.create('Temp');
console2.setTitle('ok that is what is up');
console2.root.style.backgroundColor = '#ddd';
console2.root.innerHTML = str;

let currentConsole = 1;
function swapConsoles() {
	if ( currentConsole === 1 ) {
		cmsjs.Console.setActiveConsole('Temp');
		currentConsole = 2;
	}
	else {
		cmsjs.Console.setActiveConsole('Main');
		currentConsole = 1;
	}
}
//setInterval(swapConsoles,5000);


/*
let console = null;
let panel = null;
let component = null;

console = new cmsjs.Console('Main');

console.getPanel('Header').addComponent('header');
console.getPanel('Controls').addComponent('controls');
console.getPanel('Footer').addComponent('footer');
console.getPanel('Content').addComponent('content-markup');
console.getPanel('Content').addComponent('content-raw');
console.getPanel('Content').addComponent('content-docs');
console.getPanel('Content').addComponent('content-demo');
console.getPanel('Footer').addComponent('colors');
console.getPanel('Footer').addComponent('fonts');

console = new cmsjs.Console('Mobile');
console.setGridRows('auto 1fr auto auto');
console.setGridCols('1fr auto');
*/



let theme = null;

cmsjs.Themes.Colors.addKey('Default.Background','#f7f7f7');
cmsjs.Themes.Colors.addKey('Default.Text','#004');
cmsjs.Themes.Colors.addKey('Default.Heading','#004');
cmsjs.Themes.Colors.addKey('Default.Highlight','#404');

cmsjs.Themes.Colors.addKey('Docs.Text1','#000');
cmsjs.Themes.Colors.addKey('Docs.Text2','#040');
cmsjs.Themes.Colors.addKey('Docs.Text3','#004');
cmsjs.Themes.Colors.addKey('Docs.Borders','#777');

cmsjs.Themes.Colors.addKey('Controls.Labels');
cmsjs.Themes.Colors.addKey('Controls.ToggleSliderOn');
cmsjs.Themes.Colors.addKey('Controls.ToggleSliderOff');
cmsjs.Themes.Colors.addKey('Controls.ToggleButtonOn');
cmsjs.Themes.Colors.addKey('Controls.ToggleButtonOff');

cmsjs.Themes.Colors.addKey('JSON.Key');
cmsjs.Themes.Colors.addKey('JSON.Null');
cmsjs.Themes.Colors.addKey('JSON.Boolean');
cmsjs.Themes.Colors.addKey('JSON.Number');
cmsjs.Themes.Colors.addKey('JSON.String');
cmsjs.Themes.Colors.addKey('JSON.Object');

cmsjs.Themes.Colors.addKey('JSON2.Bytes');
cmsjs.Themes.Colors.addKey('JSON2.Controls');
cmsjs.Themes.Colors.addKey('JSON2.Guideline1');
cmsjs.Themes.Colors.addKey('JSON2.Guideline2');
cmsjs.Themes.Colors.addKey('JSON2.Guideline3');
cmsjs.Themes.Colors.addKey('JSON2.Guideline4');

theme = new cmsjs.Themes.Colors('Neon');
theme.set('Default.Background','#2f2f20');
theme.set('Default.Text','#fff');
theme.set('Default.Heading','#e87');
theme.set('Default.Highlight','#ff7');
theme.set('Docs.Text1','#cc0');
theme.set('Docs.Text2','#0cc');
theme.set('Docs.Text3','#ecf');
theme.set('Docs.Borders','#777');
theme.set('JSON.Key','#7df');
theme.set('JSON.Null','#77f');
theme.set('JSON.Boolean','#0ff');
theme.set('JSON.Number','#0ff');
theme.set('JSON.String','#0f0');
theme.set('JSON.Object','#999');
theme.set('JSON2.Bytes','#f0f');
theme.set('JSON2.Controls','#f7f');
theme.set('JSON2.Guideline1','#0a0');
theme.set('JSON2.Guideline2','#f00');
theme.set('JSON2.Guideline3','#aaa');
theme.set('JSON2.Guideline4','#77f');
theme.set('Controls.Labels','#9e7');
theme.set('Controls.ToggleSliderOn','#7ef');
theme.set('Controls.ToggleSliderOff','#666');
theme.set('Controls.ToggleButtonOn','#17f');
theme.set('Controls.ToggleButtonOff','#999');

if ( cmsjs.State.get('Core.GoogleFontList') ) {
	cmsjs.State.get('Core.GoogleFontList').forEach((font) => {
		cmsjs.Themes.Fonts.registerGoogleFont(font);
	});
}


theme = new cmsjs.Themes.Layouts('Default');
theme.setGridRows("auto 1fr auto auto");
theme.setGridCols("1fr auto");
theme.addPanel('Header','1,1','1,1').addComponent('header');
theme.addPanel('Controls','1,2','2,1').addComponent('controls');
theme.addPanel('Content','2,1','1,1').addComponent;
theme.addPanel('Footer','3,1','1,2').addComponent('footer');
theme.addPanel('ThemeEditor','4,1','1,2');

	cmsjs.Themes.Colors.bindElement(cmsjs.Console.Elements.Console,'background-color','background-color');
	cmsjs.Themes.Colors.bindElement(cmsjs.Console.Elements.Console,'color','text-color');
	
	cmsjs.Console.bindComponent('header','Header');
	cmsjs.Console.bindComponent('controls','RightSidebar');
	cmsjs.Console.bindComponent('footer','Footer');
	cmsjs.Console.bindComponent('colors','ThemeEditor');
	cmsjs.Console.bindComponent('fonts','ThemeEditor');
	
	cmsjs.Console.getComponent('header').build();
	cmsjs.Console.getComponent('controls').build();
	cmsjs.Console.getComponent('footer').build();
	cmsjs.Console.getComponent('themes').build();
	cmsjs.Console.getComponent('fonts').build();
	
	cmsjs.Console.getComponent('header').show();
	cmsjs.Console.getComponent('controls').show();
	cmsjs.Console.getComponent('footer').show();


	const pages = [];
	pages.push('markup');
	pages.push('raw');
	//pages.push('editor');
	pages.push('docs');
	pages.push('demo');
	//pages.push('config');
	pages.forEach((page) => {
		const pid = 'content-' + page;
		cmsjs.Console.addComponent(pid,cbf.get(pid));
		cmsjs.Console.bindComponent(pid,'Content');
	});

	if ( cmsjs.State.get('Core.StyleEditor') ) {
		showStyleEditor(cmsjs.State.get('Core.StyleEditor'));
	}


//cmsjs.Themes.Layouts.import(theme);

//theme = cmsjs.Themes.Layouts.new('Tablet');
//theme = cmsjs.Themes.Layouts.new('Mobile');


//cmsjs.Themes.load(STATE.get('Core.CustomThemes'));


/*
// Placeholder variable to build the themes
let theme = null;

// Light.Default
theme = cmsjs.Themes.addTheme('Light.Default');
theme.set('text-color','#000');
theme.set('text-2-color','#040');
theme.set('heading-color','#004');
theme.set('background-color','#f7f7ff');
theme.set('guideline-1-color','#00f');
theme.set('guideline-2-color','#f00');
theme.set('guideline-3-color','#222');
theme.set('guideline-4-color','#0c0');
theme.set('key-color','#700');
theme.set('null-color','#555');
theme.set('boolean-color','#007');
theme.set('number-color','#007');
theme.set('string-color','#050');
theme.set('object-color','#555');
theme.set('bytes-color','#707');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#707');
theme.set('console-toggle-slider-on-color','#acf');
theme.set('console-toggle-slider-off-color','#bbb');
theme.set('console-toggle-button-on-color','#79f');
theme.set('console-toggle-button-off-color','#888');

// Light.Firefox
theme = STYLE.addTheme('Light.Firefox');
theme.set('background-color','#fff');
theme.set('heading-color','#004');
theme.set('label-color','#004');
theme.set('highlight-color','#040');
theme.set('text-main-color','#000');
theme.set('text-alt-color','#040');
theme.set('guideline-1-color','#28e');
theme.set('guideline-2-color','#82e');
theme.set('guideline-3-color','#e28');
theme.set('guideline-4-color','#2e8');
theme.set('key-color','#2789ec');
theme.set('null-color','#777');
theme.set('boolean-color','#33a02f');
theme.set('number-color','#33a02f');
theme.set('string-color','#de0bac');
theme.set('object-color','#555');
theme.set('bytes-color','#707');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#707');
theme.set('console-toggle-slider-on-color','#acf');
theme.set('console-toggle-slider-off-color','#bbb');
theme.set('console-toggle-button-on-color','#79f');
theme.set('console-toggle-button-off-color','#888');

// Light.Subdued
theme = STYLE.addTheme('Light.Subdued');
theme.set('background-color','#ccd');
theme.set('heading-color','#004');
theme.set('label-color','#004');
theme.set('highlight-color','#004');
theme.set('text-main-color','#000');
theme.set('text-alt-color','#040');
theme.set('guideline-1-color','#28e');
theme.set('guideline-2-color','#82e');
theme.set('guideline-3-color','#e28');
theme.set('guideline-4-color','#2e8');
theme.set('key-color','#070');
theme.set('null-color','#777');
theme.set('boolean-color','#33a02f');
theme.set('number-color','#33a02f');
theme.set('string-color','#007');
theme.set('object-color','#555');
theme.set('bytes-color','#707');
theme.set('controls-color','#070');
theme.set('button-outline-color','#707');
theme.set('console-toggle-slider-on-color','#acc');
theme.set('console-toggle-slider-off-color','#bbb');
theme.set('console-toggle-button-on-color','#79f');
theme.set('console-toggle-button-off-color','#888');

// Light.Sandbox
theme = STYLE.addTheme('Light.Sandbox');
theme.set('text-color','#000');
theme.set('text-2-color','#040');
theme.set('heading-color','#004');
theme.set('background-color','#ffe');
theme.set('guideline-1-color','#28e');
theme.set('guideline-2-color','#82e');
theme.set('guideline-3-color','#e28');
theme.set('guideline-4-color','#2e8');
theme.set('key-color','#a60');
theme.set('null-color','#777');
theme.set('boolean-color','#33a02f');
theme.set('number-color','#33a02f');
theme.set('string-color','#f91');
theme.set('object-color','#555');
theme.set('bytes-color','#707');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#707');
theme.set('console-toggle-slider-on-color','#acf');
theme.set('console-toggle-slider-off-color','#bbb');
theme.set('console-toggle-button-on-color','#79f');
theme.set('console-toggle-button-off-color','#888');

// Light.Monochrome
theme = STYLE.addTheme('Light.Monochrome');
theme.set('text-color','#111');
theme.set('text-2-color','#333');
theme.set('heading-color','#004');
theme.set('background-color','#eee');
theme.set('guideline-1-color','#555');
theme.set('guideline-2-color','#777');
theme.set('guideline-3-color','#999');
theme.set('guideline-4-color','#aaa');
theme.set('key-color','#111');
theme.set('null-color','#999');
theme.set('boolean-color','#777');
theme.set('number-color','#999');
theme.set('string-color','#444');
theme.set('object-color','#aaa');
theme.set('bytes-color','#111');
theme.set('controls-color','#77f');
theme.set('button-outline-color','#70f');
theme.set('console-toggle-slider-on-color','#a7a7a7');
theme.set('console-toggle-slider-off-color','#ccc');
theme.set('console-toggle-button-on-color','#777777');
theme.set('console-toggle-button-off-color','#bbb');

// Dark.Default
theme = STYLE.addTheme('Dark.Default');
theme.set('text-color','#cce');
theme.set('text-2-color','#ecc');
theme.set('heading-color','#e87');
theme.set('background-color','#2f2f20');
theme.set('guideline-1-color','#0a0');
theme.set('guideline-2-color','#f00');
theme.set('guideline-3-color','#aaa');
theme.set('guideline-4-color','#77f');
theme.set('key-color','#e77');
theme.set('null-color','#999');
theme.set('boolean-color','#77f');
theme.set('number-color','#77f');
theme.set('string-color','#7d7');
theme.set('object-color','#999');
theme.set('bytes-color','#c5c');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#ff7');
theme.set('console-toggle-slider-on-color','#3ab');
theme.set('console-toggle-slider-off-color','#666');
theme.set('console-toggle-button-on-color','#05d');
theme.set('console-toggle-button-off-color','#999');

// Dark.Neon
theme = STYLE.addTheme('Dark.Neon');
theme.set('text-color','#cc0');
theme.set('text-2-color','#0cc');
theme.set('heading-color','#e87');
theme.set('background-color','#2f2f20');
theme.set('guideline-1-color','#0a0');
theme.set('guideline-2-color','#f00');
theme.set('guideline-3-color','#aaa');
theme.set('guideline-4-color','#77f');
theme.set('key-color','#7df');
theme.set('null-color','#77f');
theme.set('boolean-color','#0ff');
theme.set('number-color','#0ff');
theme.set('string-color','#0f0');
theme.set('object-color','#999');
theme.set('bytes-color','#f0f');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#ff7');
theme.set('console-toggle-slider-on-color','#7ef');
theme.set('console-toggle-slider-off-color','#666');
theme.set('console-toggle-button-on-color','#17f');
theme.set('console-toggle-button-off-color','#999');

// Dark.Nature
theme = STYLE.addTheme('Dark.Nature');
theme.set('text-color','#da0');
theme.set('text-2-color','#0ad');
theme.set('heading-color','#e87');
theme.set('background-color','#2f2f20');
theme.set('guideline-1-color','#d8a');
theme.set('guideline-2-color','#881');
theme.set('guideline-3-color','#18a');
theme.set('guideline-4-color','#a81');
theme.set('key-color','#3a4');
theme.set('null-color','#fa4');
theme.set('boolean-color','#fa4');
theme.set('number-color','#fa4');
theme.set('string-color','#3d4');
theme.set('object-color','#999');
theme.set('bytes-color','#2ae');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#ff7');
theme.set('console-toggle-slider-on-color','#7ef');
theme.set('console-toggle-slider-off-color','#666');
theme.set('console-toggle-button-on-color','#17f');
theme.set('console-toggle-button-off-color','#999');

// Dark.Lilac
theme = STYLE.addTheme('Dark.Lilac');
theme.set('text-color','#9ad');
theme.set('text-2-color','#da9');
theme.set('heading-color','#e87');
theme.set('background-color','#2f2f20');
theme.set('guideline-1-color','#d8a');
theme.set('guideline-2-color','#881');
theme.set('guideline-3-color','#18a');
theme.set('guideline-4-color','#a81');
theme.set('key-color','#b6d');
theme.set('null-color','#fa4');
theme.set('boolean-color','#fa4');
theme.set('number-color','#fa4');
theme.set('string-color','#3d4');
theme.set('object-color','#999');
theme.set('bytes-color','#2ae');
theme.set('controls-color','#f7f');
theme.set('button-outline-color','#ff7');
theme.set('console-toggle-slider-on-color','#9ad');
theme.set('console-toggle-slider-off-color','#666');
theme.set('console-toggle-button-on-color','#b8b');
theme.set('console-toggle-button-off-color','#999');

// Dark.Monochrome
theme = STYLE.addTheme('Dark.Monochrome');
theme.set('text-color','#ddd');
theme.set('text-2-color','#fff');
theme.set('heading-color','#e87');
theme.set('background-color','#333');
theme.set('guideline-1-color','#555');
theme.set('guideline-2-color','#777');
theme.set('guideline-3-color','#999');
theme.set('guideline-4-color','#aaa');
theme.set('key-color','#eee');
theme.set('null-color','#999');
theme.set('boolean-color','#777');
theme.set('number-color','#999');
theme.set('string-color','#eee');
theme.set('object-color','#999');
theme.set('bytes-color','#bbb');
theme.set('controls-color','#7f0');
theme.set('button-outline-color','#7ff');
theme.set('console-toggle-slider-on-color','#888');
theme.set('console-toggle-slider-off-color','#666');
theme.set('console-toggle-button-on-color','#ccc');
theme.set('console-toggle-button-off-color','#222');
*/
/*
 * Returns a border color based on the depth.
 * This is for the depth guidelines.
 */
function getBorderColor(depth,theme) {
	const colorNumber = ((depth-1) % 4) + 1;
	return STYLE.getThemeValue('guideline-' + colorNumber + '-color');
}


// Called whenever the theme changes
STYLE.setThemeChangeFunction((theme) => {
	const rootElt = CONSOLE.getComponent('content-markup');
	rootElt.querySelectorAll('div.contents').forEach((elt) => {
		if ( STATE.get('Indent.ShowGuidelines') ) {
			const depth = elt.getAttribute('depth');
			const colorNumber = ((depth-1) % 4) + 1;
			const color = theme.get('guideline-' + colorNumber + '-color');
			elt.style.borderLeftColor = color;
		}
		else {
			elt.style.borderLeftColor = 'rgba(0,0,0,0.0)';
		}
	});
	//CONSOLE.buildComponent('controls');
	CONSOLE.getComponent('controls').querySelectorAll('[cmsjs-control-type="toggle"]').forEach((toggle) => {
		const colorStyleElt = toggle.querySelector('style.cmsjs-control-toggle-colors');
		const sliderElt = toggle.querySelector('.cmsjs-control-toggle-slider');
		const inputElt = toggle.querySelector('input');
		const sliderColorOn = theme.get('console-toggle-slider-on-color');
		const sliderColorOff = theme.get('console-toggle-slider-off-color');
		const buttonColorOn = theme.get('console-toggle-button-on-color');
		const buttonColorOff = theme.get('console-toggle-button-off-color');
		colorStyleElt.innerHTML = `
			.cmsjs-control-toggle-slider:before { background-color: ${buttonColorOff}; }
			input:checked + .cmsjs-control-toggle-slider:before { background-color: ${buttonColorOn}; }
		`;
		//if ( inputElt.getAttribute('checked') !== null ) {
		if ( inputElt.checked ) {
			sliderElt.style.backgroundColor = theme.get('console-toggle-slider-on-color');
		}
		else {
			sliderElt.style.backgroundColor = theme.get('console-toggle-slider-off-color');
		}
	});
	STATE.set('ENV.TempTheme',null);
	CONSOLE.buildComponent('themes');
	//CONSOLE.buildComponent('fonts');
});


log.trace('cms.dictionary.js');

// Language definitions

cmsjs.Dictionary.DefaultLanguage = 'en';

let dict = null;
let d = null;



/**
 * Console Labels
 */
dict = cmsjs.Dictionary.create('Controls');
dict.addLanguages(['en','de','es','fr','ru','zh']);
dict.setRootLanguage('en');

/**
 * ENGLISH
 */
d = dict.getLanguageMap('en');
// Misc
d.set('Language','Language')
d.set('Documentation','Documentation');
d.set('Controls','Controls');
d.set('Interface','Interface');
d.set('Search','Search');
d.set('Console','Console');
// Labels
d.set('Label.MarkupMode','Navigator On');
d.set('Label.ShowNulls','Show Nulls');
d.set('Label.BoldKeys','Bold Keys');
d.set('Label.ShowBytes','Show Bytes');
d.set('Label.ShowCount','Show Count');
d.set('Label.SortKeys','Sort Keys');
d.set('Label.ShowGuidelines','Guidelines');
d.set('Label.AutoExpand','Auto Expand');
d.set('Label.MarkupHTML','Markup HTML');
d.set('Label.ConvertLinks','Convert Links');
d.set('Label.IndentDepth','Indent Depth');
d.set('Label.ExpandDepth','Expand Depth');
d.set('Label.DarkMode','Dark Mode');
d.set('Label.ShowURL','Show URL');
d.set('Label.ShowSearch','Show Search');
d.set('Label.ControlKeys','Control Keys');
d.set('Label.ShowFooter','Show Footer');
d.set('Label.Debug','Debugging');
d.set('Label.Spacing','Line Spacing');
d.set('Label.LineWrap','Wrap Lines');
d.set('Label.SelectedLight','Light Theme');
d.set('Label.SelectedDark','Dark Theme');
d.set('Label.CodeFont','JSON Font');
d.set('Label.CodeFontSize','Font Size');
d.set('Label.ConsoleFont','Console Font');
d.set('Label.ConsoleFontSize','Font Size');
d.set('Label.DisplayFont','Display Font');
d.set('Label.DisplayFontSize','Font Size');
d.set('Label.Language','Language');
// Docs
d.set('Docs.MarkupMode','Turning this off will display the raw underlying JSON content.');
d.set('Docs.ShowNulls','Turning this off will prevent the display of null values, empty objects, and empty arrays. Note that this will result in the object counts not representing the number of items actually shown on the screen.');
d.set('Docs.BoldKeys','Turning this on will display JSON keys in bold font.');
d.set('Docs.ShowBytes','Displays byte counts next to objects and arrays that contain more than a certain threhold of data. By default, byte counts are only given for items that contain more than 5kB of data and represent more than 1% of the total data in the file.');
d.set('Docs.ShowCount','Displays item counts for non-empty objects and arrays. Turning this on and off is purely cosmetic; it has no effect on performance.');
d.set('Docs.SortKeys','Sort object keys. <b>NOTE: Currently requires a manual page refresh to take effect!</b>');
d.set('Docs.ShowGuidelines','Toggle depth guidlines.');
d.set('Docs.AutoExpand','Automatically expand JSON file to "Expand" depth on initial page load. Note that this has a short-circult to ensure files open quickly. The JSON file is expanded breadth-first, and after each pass through at a new depth a timer is checked. If too much time has elapsed, expansion will stop and the browser will display what has been processed up to the point of short-ciruiting.');
d.set('Docs.MarkupHTML','Markup HTML, i.e. render things like &lt;p&gt; and &lt;table&gt; as HTML items.');
d.set('Docs.ConvertLinks','Converts any standalone strings starting with "http://" or "https://" to active hyperlinks.');
d.set('Docs.IndentDepth','Sets the block indentation level. You can also use Ctrl-LeftArrow and Ctrl-RightArrow to increase or decrease indent.');
d.set('Docs.ExpandDepth','Sets the depth of operation for the initial JSON expansion (if "Auto Expand" is turned on), as well as the depth of operation for the "Expand all children" controls.');
d.set('Docs.DarkMode','Switches between the selected Dark Theme and the selected Light Theme.');
d.set('Docs.ShowURL','Turning this off will prevent the URL from appearing at the top of the page.');
d.set('Docs.ShowSearch','Turning this off will prevent the "Search" bar from appearing at the top of the page.');
d.set('Docs.ControlKeys','Enables the use of custom keybindings, e.g. ctrl-g to enable/disable guidelines');
d.set('Docs.ShowFooter','Turning this off will prevent display off the footer at the bottom of the page.');
d.set('Docs.Debug','This will turn console debugging on and off.');
d.set('Docs.Spacing','Sets the vertical spacing between lines.');
d.set('Docs.LineWrap','Wrap text lines that would extend past the right side of the screen.');
d.set('Docs.SelectedLight','This is the theme that will be used when "Dark Mode" is disabled.');
d.set('Docs.SelectedDark','This is the theme that will be used when "Dark Mode" is enabled.');
d.set('Docs.CodeFont','Sets the JSON code font. The "Font Size" setting just below sets the code font size');
d.set('Docs.ConsoleFont','Sets the console font. The "Font Size" setting just below sets the console font size');
d.set('Docs.DisplayFont','Sets the display font. The "Font Size" setting just below sets the display font size');

/**
 * FRENCH
 */
d = dict.getLanguageMap('fr');
// Misc
d.set('Language','Language')
d.set('Language','Langue');
d.set('Documentation','Documentation');
d.set('Controls','Contr\u00F4les');
d.set('Interface','Interface');
d.set('Search','Recherche');
d.set('Console','Console');
// Label
d.set('Label.MarkupMode','Nav. Activ\u00E9e');
d.set('Label.ShowNulls','Aff. Nulls');
d.set('Label.BoldKeys','Cl\u00E9s En Gras');
d.set('Label.ShowBytes','Aff. Octets');
d.set('Label.ShowCount','Aff. Comptage');
d.set('Label.SortKeys','Trier Cl\u00E9s');
d.set('Label.ShowGuidelines','Lignes Direct.');
d.set('Label.AutoExpand','Auto \u00C9tendre');
d.set('Label.MarkupHTML','Baliser HTML');
d.set('Label.ConvertLinks','Convertir Liens');
d.set('Label.IndentDepth','Prof. Indent.');
d.set('Label.ExpandDepth','Prof. \u00c9tend.');
d.set('Label.DarkMode','Mode Sombre');
d.set('Label.ShowURL','Aff. URL');
d.set('Label.ShowSearch','Aff. Recherche');
d.set('Label.ControlKeys','Cl\u00e9s Contr\u00f4le');
d.set('Label.ShowFooter','Aff. Pied Page');
d.set('Label.Spacing','Espacement');
d.set('Label.SelectedLight','Th\u00e8me Clair');
d.set('Label.SelectedDark','Th\u00e8me Sombre');
d.set('Label.CodeFont','Police JSON');
d.set('Label.CodeFontSize','Taille Police');
d.set('Label.ConsoleFont','Police Console');
d.set('Label.ConsoleFontSize','Taille Police');
d.set('Label.DisplayFont','Police Affichage');
d.set('Label.DisplayFontSize','Taille Police');
d.set('Label.Language','Langue');
// Docs


/**
 * GERMAN
 */
d = dict.getLanguageMap('de');
// German
d.set('Language','Sprache');
d.set('Documentation','Dokumentation');
d.set('Controls','Steuerungen');
d.set('Interface','Schnittstelle');
d.set('Search','Suche');
d.set('Console','Konsole');
// Labels
d.set('Label.MarkupMode','Navigator Ein');
d.set('Label.ShowNulls','Nullen Anzeigen');
d.set('Label.BoldKeys','Fette Schl\u00fcssel');
d.set('Label.ShowBytes','Bytes Anzeigen');
d.set('Label.ShowCount','Z\u00e4hlung Anzeigen');
d.set('Label.SortKeys','Schl\u00fcssel Sortieren');
d.set('Label.ShowGuidelines','Leitlinien');
d.set('Label.AutoExpand','Auto Erweitern');
d.set('Label.MarkupHTML','HTML Markup');
d.set('Label.ConvertLinks','Links Konvertieren');
d.set('Label.IndentDepth','Einzugstiefe');
d.set('Label.ExpandDepth','Erweitern Tiefe');
d.set('Label.DarkMode','Dunkler Modus');
d.set('Label.ShowURL','URL Anzeigen');
d.set('Label.ShowSearch','Suche Anzeigen');
d.set('Label.ControlKeys','Steuerungstasten');
d.set('Label.ShowFooter','Fu\u00dfzeile Anzeigen');
d.set('Label.Spacing','Zeilenabstand');
d.set('Label.SelectedLight','Helles Thema');
d.set('Label.SelectedDark','Dunkles Thema');
d.set('Label.CodeFont','JSON-Schrift');
d.set('Label.CodeFontSize','Schriftgr\u00f6\u00dfe');
d.set('Label.ConsoleFont','Konsolenschrift');
d.set('Label.ConsoleFontSize','Schriftgr\u00f6\u00dfe');
d.set('Label.DisplayFont','Anzeigeschrift');
d.set('Label.DisplayFontSize','Schriftgr\u00f6\u00dfe');
d.set('Label.Language','Sprache');
// Docs



/*
// Spanish
dict = cmsjs.Lang.getDictionary('es');
dict.set('Language','Idioma');
dict.set('Documentation','Documentaci\u00F3n');
dict.set('Controls','Controles');
dict.set('Interface','Interfaz');
dict.set('Search','B\u00FAsqueda');
dict.set('Console','Consola');

// Console Labels
d = dict.addNamespace('Console.Labels');
d.set('Core.MarkupMode','Navegador Activado');
d.set('Core.ShowNulls','Mostrar Nulos');
d.set('Core.BoldKeys','Claves en Negrita');
d.set('Core.ShowBytes','Mostrar Bytes');
d.set('Core.ShowCount','Mostrar Conteo');
d.set('Core.SortKeys','Ordenar Claves');
d.set('Indent.ShowGuidelines','Pautas');
d.set('Expand.AutoExpand','Autoexpandir');
d.set('Core.MarkupHTML','Marcado HTML');
d.set('Core.ConvertLinks','Convertir Enlaces');
d.set('Indent.IndentDepth','Prof. de Sangr\u00eda');
d.set('Expand.ExpandDepth','Prof. de Expansi\u00f3n');
d.set('Theme.DarkMode','Modo Oscuro');
d.set('Core.ShowURL','Mostrar URL');
d.set('Core.ShowSearch','Mostrar B\u00fasqueda');
d.set('Core.ControlKeys','Teclas de Control');
d.set('Core.ShowFooter','Mostrar Pie');
d.set('Core.Spacing','Espaciado');
d.set('Theme.SelectedLight','Tema Claro');
d.set('Theme.SelectedDark','Tema Oscuro');
d.set('Core.CodeFont','Fuente JSON');
d.set('Core.CodeFontSize','Tama\u00f1o de Fuente');
d.set('Core.ConsoleFont','Fuente de Consola');
d.set('Core.ConsoleFontSize','Tama\u00f1o de Fuente');
d.set('Core.DisplayFont','Fuente de Visualizaci\u00f3n');
d.set('Core.DisplayFontSize','Tama\u00f1o de Fuente');
d.set('Core.Language','Idioma');

// Console Docs
d = dict.addNamespace('Console.Docs');


// Russian
dict = cmsjs.Lang.getDictionary('ru');
dict.set('Language','\u042F\u0437\u044B\u043A');
dict.set('Documentation','\u0414\u043E\u043A\u0443\u043C\u0435\u043D\u0442\u0430\u0446\u0438\u044F');
dict.set('Controls','\u0423\u043F\u0440\u0430\u0432\u043B\u0435\u043D\u0438\u0435');
dict.set('Interface','\u0418\u043D\u0442\u0435\u0440\u0444\u0435\u0439\u0441');
dict.set('Search','\u041F\u043E\u0438\u0441\u043A');
dict.set('Console','\u041A\u043E\u043D\u0441\u043E\u043B\u044C');

// Console Labels
d = dict.addNamespace('Console.Labels');
d.set('Core.MarkupMode','\u041d\u0430\u0432\u0438\u0433\u0430\u0442\u043e\u0440');
d.set('Core.ShowNulls','\u041d\u0443\u043b\u0438');
d.set('Core.BoldKeys','\u0416\u0438\u0440\u043d\u044b\u0435 \u041a\u043b\u044e\u0447\u0438');
d.set('Core.ShowBytes','\u0411\u0430\u0439\u0442\u044b');
d.set('Core.ShowCount','\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e');
d.set('Core.SortKeys','\u0421\u043e\u0440\u0442\u0438\u0440\u043e\u0432\u043a\u0430');
d.set('Indent.ShowGuidelines','\u041d\u0430\u0441\u0442\u0440\u043e\u0439\u043a\u0438');
d.set('Expand.AutoExpand','\u0410\u0432\u0442\u043e \u0420\u0430\u0437\u0432\u0435\u0440\u043d\u0443\u0442\u044c');
d.set('Core.MarkupHTML','HTML');
d.set('Core.ConvertLinks','\u0421\u0441\u044b\u043b\u043a\u0438');
d.set('Indent.IndentDepth','\u0412\u043b\u043e\u0436\u0435\u043d\u0438\u0435');
d.set('Expand.ExpandDepth','\u0413\u043b\u0443\u0431\u0438\u043d\u0430 \u0420\u0430\u0437\u0432\u0435\u0440\u043d\u0443\u0442\u044c');
d.set('Theme.DarkMode','\u0422\u0435\u043c\u043d\u044b\u0439 \u0420\u0435\u0436\u0438\u043c');
d.set('Core.ShowURL','\u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c URL');
d.set('Core.ShowSearch','\u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c \u041f\u043e\u0438\u0441\u043a');
d.set('Core.ControlKeys','\u041a\u043b\u0430\u0432\u0438\u0448\u0438 \u0423\u043f\u0440\u0430\u0432\u043b\u0435\u043d\u0438\u044f');
d.set('Core.ShowFooter','\u041f\u043e\u043a\u0430\u0437\u0430\u0442\u044c \u041f\u043e\u0434\u0432\u0430\u043b');
d.set('Core.Spacing','\u0418\u043d\u0442\u0435\u0440\u0432\u0430\u043b \u0421\u0442\u0440\u043e\u043a');
d.set('Theme.SelectedLight','\u0421\u0432\u0435\u0442\u043b\u0430\u044f \u0422\u0435\u043c\u0430');
d.set('Theme.SelectedDark','\u0422\u0435\u043c\u043d\u0430\u044f \u0422\u0435\u043c\u0430');
d.set('Core.CodeFont','\u0428\u0440\u0438\u0444\u0442 JSON');
d.set('Core.CodeFontSize','\u0420\u0430\u0437\u043c\u0435\u0440 \u0428\u0440\u0438\u0444\u0442\u0430');
d.set('Core.ConsoleFont','\u0428\u0440\u0438\u0444\u0442 \u041a\u043e\u043d\u0441\u043e\u043b\u0438');
d.set('Core.ConsoleFontSize','\u0420\u0430\u0437\u043c\u0435\u0440 \u0428\u0440\u0438\u0444\u0442\u0430');
d.set('Core.DisplayFont','\u0428\u0440\u0438\u0444\u0442 \u041e\u0442\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f');
d.set('Core.DisplayFontSize','\u0420\u0430\u0437\u043c\u0435\u0440 \u0428\u0440\u0438\u0444\u0442\u0430');
d.set('Core.Language','\u042F\u0437\u044B\u043A');

// Console Docs
d = dict.addNamespace('Console.Docs');


// Chinese
dict = cmsjs.Lang.getDictionary('zh');
dict.set('Language','\u8BED\u8A00');
dict.set('Documentation','\u6587\u6863');
dict.set('Controls','\u63A7\u5236');
dict.set('Interface','\u754C\u9762"');
dict.set('Search','\u641C\u7D22');
dict.set('Console','\u63A7\u5236\u53F0');

// Console Labels
d = dict.addNamespace('Console.Labels');
d.set('Core.MarkupMode','\u5bfc\u822a\u5668\u5f00\u542f');
d.set('Core.ShowNulls','\u663e\u793a\u7a7a\u503c');
d.set('Core.BoldKeys','\u52a0\u7c97\u952e');
d.set('Core.ShowBytes','\u663e\u793a\u5b57\u8282');
d.set('Core.ShowCount','\u663e\u793a\u6570\u91cf');
d.set('Core.SortKeys','\u6392\u5e8f\u952e');
d.set('Indent.ShowGuidelines','\u6309\u539f\u7ebf');
d.set('Expand.AutoExpand','\u81ea\u52a8\u5c55\u5f00');
d.set('Core.MarkupHTML','\u6807\u8bb0 HTML');
d.set('Core.ConvertLinks','\u8f6c\u6362\u94fe\u63a5');
d.set('Indent.IndentDepth','\u7f29\u8fdb\u6df1\u5ea6');
d.set('Expand.ExpandDepth','\u5c55\u5f00\u6df1\u5ea6');
d.set('Theme.DarkMode','\u6697\u8272\u6a21\u5f0f');
d.set('Core.ShowURL','\u663e\u793a URL');
d.set('Core.ShowSearch','\u663e\u793a\u641c\u7d22');
d.set('Core.ControlKeys','\u63a7\u5236\u952e');
d.set('Core.ShowFooter','\u663e\u793a\u5c3e\u90e8');
d.set('Core.Spacing','\u884c\u95f4\u8ddd');
d.set('Theme.SelectedLight','\u6d45\u8272\u4e3b\u9898');
d.set('Theme.SelectedDark','\u6697\u8272\u4e3b\u9898');
d.set('Core.CodeFont','\u4ee3\u7801\u5b57\u4f53');
d.set('Core.CodeFontSize','\u5b57\u4f53\u5927\u5c0f');
d.set('Core.ConsoleFont','\u63a7\u5236\u53f0\u5b57\u4f53');
d.set('Core.ConsoleFontSize','\u5b57\u4f53\u5927\u5c0f');
d.set('Core.DisplayFont','\u663e\u793a\u5b57\u4f53');
d.set('Core.DisplayFontSize','\u5b57\u4f53\u5927\u5c0f');
d.set('Core.Language','\u8BED\u8A00');

// Console Docs
d = dict.addNamespace('Console.Docs');
*/

dict = undefined;
d = undefined;



log.trace('loading cms.keys.js');

//const KEYS = cmsjs.Keys;
// Keybinding Definitions

dict = null;
d = null;

dict = cmsjs.Lang.getDictionary('en');
d = dict.addNamespace('Keybindings');


// open search and take focus
cmsjs.Keys.addAction('search', () => {
	CONTROLS.setValue('Core.ShowSearch',true);
	$.id('search-input').focus();
});
d.set('search','Open search bar and take focus');


// toggle guidlines
cmsjs.Keys.addAction('guidelines', () => { CONTROLS.toggle('Indent.ShowGuidelines'); });
d.set('guidelines','Toggle depth guidelines');

// toggle bytes
cmsjs.Keys.addAction('bytes', () => { CONTROLS.toggle('Core.ShowBytes'); });
d.set('bytes','Toggle display of bytes next to large objects and arrays');

// toggle bold keys
cmsjs.Keys.addAction('bold-keys', () => { CONTROLS.toggle('Core.BoldKeys'); });
d.set('bold-keys','Toggle bold font on JSON keys');

// toggle nulls
cmsjs.Keys.addAction('nulls', () => { CONTROLS.toggle('Core.ShowNulls'); });
d.set('nulls','Toggle display of null values');

// toggle item counts
cmsjs.Keys.addAction('count', () => { CONTROLS.toggle('Core.ShowCount'); });
d.set('count','Toggle item counts on objects and arrays');

// toggle item counts
cmsjs.Keys.addAction('sort-keys', () => { CONTROLS.toggle('Core.SortKeys'); });
d.set('sort-keys','Toggle alphabetical sorting of item keys');

// increase indent
cmsjs.Keys.addAction('indent-more', () => { CONTROLS.setValue('Indent.IndentDepth',STATE.get('Indent.IndentDepth')+1); });
d.set('indent-more','Increase indentation');

// decrease indent
cmsjs.Keys.addAction('indent-less', () => { CONTROLS.setValue('Indent.IndentDepth',STATE.get('Indent.IndentDepth')-1); });
d.set('indent-less','Decrease indentation');

// increase spacing
cmsjs.Keys.addAction('more-space', () => { CONTROLS.setValue('Core.Spacing',STATE.get('Core.Spacing')+1) });
d.set('more-space','Increase line spacing');

// decrease spacing
cmsjs.Keys.addAction('less-space', () => { CONTROLS.setValue('Core.Spacing',STATE.get('Core.Spacing')-1) });
d.set('less-space','Decrease line spacing');

// next key
cmsjs.Keys.addAction('next-key', () => {
	let focused = document.activeElement;
	if ( focused && focused.classList ) {
		if ( focused.classList.contains('object') && focused.classList.contains('value') ) {
			const parentItem = focused.parentElement.parentElement;
			if ( parentItem ) {
	            const firstItem = parentItem.querySelector('div.contents > div.item');
				if ( firstItem ) {
				    const nextKey = firstItem.querySelector('div.label > span.key');
					if ( nextKey ) { nextKey.focus(); }
				}
			}
		}
		else if ( focused.classList.contains('key') ) {
			let parent = focused.parentElement.parentElement;
			let foundParent = false;
			while ( parent && parent.id !== 'root' && ! foundParent ) {
				const nextItem = parent.nextSibling;
				if ( nextItem ) {
					const nextKey = nextItem.querySelector('div.label > span.key');
					if ( nextKey ) { nextKey.focus(); foundParent = true; break; }
				}
				else {
					parent = parent.parentElement;
				}
			}
		}
	}
});
d.set('next-key','Advance to next visible key (depth first)');

// prev key
docs = 'Advance to previous visible key (depth first)';
cmsjs.Keys.addAction('prev-key', () => {
	let focused = document.activeElement;
	if ( focused && focused.classList ) {
		if ( focused.classList.contains('object') && focused.classList.contains('value') ) {
			const prevKey = focused.previousSibling;
			if ( prevKey ) { prevKey.focus(); }
		}
		else if ( focused.classList.contains('key') ) {
			const parentItem = focused.parentElement.parentElement;
			if ( parentItem ) {
				const previousItem = parentItem.previousSibling;
				if ( previousItem ) {
					const prevKey = previousItem.querySelector('div.label > span.key');
					if ( prevKey ) { prevKey.focus(); }
				}
				else {
					const grandparentItem = parentItem.parentElement.parentElement;
					//console.log(grandparentItem);
					const prevKey = grandparentItem.querySelector('div.label > span.key');
					if ( prevKey ) { prevKey.focus(); }
				}
			}
		}
	}
});
d.set('prev-key','Advance to next visible key (depth first)');

// next sibling
cmsjs.Keys.addAction('next-sibling', () => {
	if ( document.activeElement ) {
		//console.log(document.activeElement);
	}
});
d.set('next-sibling','Move to next sibling');

// prev sibling
cmsjs.Keys.addAction('prev-sibling', () => {
	if ( document.activeElement ) {
		//console.log(document.activeElement);
	}
});
d.set('prev-sibling','Move to previous sibling');

// these all need to be in a function and called after logging loads
function attachKeyBindings() {
	cmsjs.Keys.assign('Alt-f','search');
	cmsjs.Keys.assign('Alt-g','guidelines');
	cmsjs.Keys.assign('Alt-y','bytes');
	cmsjs.Keys.assign('Alt-b','bold-keys');
	cmsjs.Keys.assign('Alt-n','nulls');
	cmsjs.Keys.assign('Alt-s','sort-keys');
	cmsjs.Keys.assign('Alt-k','next-key');
	cmsjs.Keys.assign('Alt-i','prev-key');
	cmsjs.Keys.assign('Alt-l','next-sibling');
	cmsjs.Keys.assign('Alt-j','prev-sibling');
	cmsjs.Keys.assign('Alt-h','count');
	cmsjs.Keys.assign('Alt-ArrowLeft','indent-less');
	cmsjs.Keys.assign('Alt-ArrowRight','indent-more');
	cmsjs.Keys.assign('Alt-ArrowUp','less-space');
	cmsjs.Keys.assign('Alt-ArrowDown','more-space');
}



log.trace('loading content.js');


/*
 * Starting point. Called at the end of the file. This branches off
 * to code that is specific to the environment (browser extension
 * vs standalone website) before remerging to initConsole().
 */
function init() {
	cmsjs.State.set('Core.Debug',true); // debugging needs to come online asap
	if ( typeof JSON_NAVIGATOR_STANDALONE_MODE !== 'undefined' && JSON_NAVIGATOR_STANDALONE_MODE ) {
		cmsjs.Log.debug('standalone mode enabled');
		cmsjs.State.set('ENV.StandaloneMode',true);
	}
	if ( ! cmsjs.State.get('ENV.StandaloneMode') ) {
		cmsjs.Log.debug('init(): running in extension mode');
		initExtension(() => {
			cmsjs.Log.debug('init(): extension initialized');
			cmsjs.State.set('ENV.CurrentPath','$');
			cmsjs.State.loadChromeStorage(initConsole);
		});
	}
	else {
		cmsjs.Log.debug('init(): running in standalone mode');
		cmsjs.State.loadLocalStorage(initConsole);
	}
}

/* 
 * If not in standalone (demo) mode, initialize the extension.
 *
 * The only thing this really accomplishes is to get the JSON content 
 * into ENV.JSON from wherever the browser dumps it.
 *
 * It takes one parameter, 'callback', which is a function that
 * should be run after ENV.JSON is set.
 *
 * This is quite hacky because there's no easy way of intercepting
 * a request and telling the browser not to generate a wrapper web
 * page for every resource, and no way to do it all that doesn't
 * require a bunch of browser-specific code.
 *
 * So, what we have to do is wait for the browser to generate the
 * wrapper page, pull the raw text JSON out using DOM queries,
 * parse it, then wipe the whole page out and start with a blank
 * slate. The wrappers that different browsers use are basically
 * all the same: <html><body><pre>JSON-CODE</pre></body></html>
 *
 * On large files, the time the browser spends printing the
 * JSON text is non-neglible and can take severaal seconds.
 * In order to prevent slow page loads, you have to grab the
 * <pre> tag as soon it becomes available and set it to
 * display:none. But it is not actually available right away,
 * necessitating a setTimeout loop.
 *
 */
function initExtension(callback) {
	// This auxilliary function does no error checking, it simply matches
	// content-type and/or extension to quickly decide whether this resource
	// should be processed at all.
	function isJSON() {
	    const contentType = document.contentType.toLowerCase();
		const isValidContentType = cmsjs.State.get('Core.ContentTypes').includes(contentType);
		if ( isValidContentType ) {
			cmsjs.Log.debug('isJSON(): matched content-type:',contentType);
			return true;
		}
		const url = new URL(document.location.href);
	    const extension = url.pathname.split('/').pop().split('.').pop().toLowerCase();
		const isValidExtension = cmsjs.State.get('Core.Extensions').includes(extension);
		if ( isValidExtension ) {
			cmsjs.Log.debug('isJSON(): matched extension:',extension);
			return true;
		}
		if ( cmsjs.State.get('Core.Debug') ) {
			cmsjs.Log.debug('isJSON(): extension did not match:',extension);
			cmsjs.Log.debug('isJSON(): content-type did not match:',contentType);
			cmsjs.Log.debug('isJSON(): deferring to browser');
		}
		return false;
	}

	let t0 = cmsjs.Log.timer('starting a timer at the beginning of init()');
	const startTime = t0;
	if ( ! isJSON() ) { return; } // if content is not JSON, bail immediately.
	t0 = cmsjs.Log.timer('initExtension(): isJSON() returned true, so treating as JSON content',t0);
	/*
	 * Right away, do the following: (NOTE: only documentElement (<html>) will be available at this time!!)
	 * - Make the document content hidden, to prevent display of raw JSON
	 * - Set a text color and background color
	 * - Set up a listener to watch when DOM content is finished loading
	 *    - When the DOM is finished, the listener:
     *       - Gets the content from the <pre> tag
	 *       - Parses it as JSON
     *       - runs the function passed as 'callback()'
	 */
	document.documentElement.style.visibility = 'hidden';
	document.documentElement.style.color = '#000';
	document.documentElement.style.backgroundColor = '#def';
	document.addEventListener('DOMContentLoaded', function() {
		try {
			let t = cmsjs.Log.timer('*DOMContentLoaded* listener fired, timer started');
			const pre = document.body.querySelector('pre');
			if ( ! pre ) {
				cmsjs.Log.error('*DOMContentLoaded* there was no <pre> tag, so the browser does not think this is text content.');
				document.documentElement.style.visibility = 'visible';
				return;
			}
			cmsjs.State.set('ENV.RawContent',pre.textContent);
			t = cmsjs.Log.timer('*DOMContentLoaded* got ENV.RawContent',t);
			cmsjs.State.set('ENV.JSON',JSON.parse(cmsjs.State.get('ENV.RawContent')));
			t = cmsjs.Log.timer('*DOMContentLoaded* got ENV.JSON from ENV.RawContent',t);
		}
		catch (error) {
			document.body.style.cursor = '';
			cmsjs.Log.error('*DOMContentLoaded* JSON parsing error: JSON.parse(document.body.textContent) failed.');
			console.error(error);
			const p = getP('error');
			p.style.color = 'red';
			//document.body.style.cursor = '';
			if ( $.id('loading') ) { $.id('loading').remove(); }
			document.body.appendChild(p);
			p.innerHTML = '<strong>Error parsing JSON file.<br>Parser reported:</strong> ';
			p.innerHTML += error.message;
			p.innerHTML += '<br><br><a style="color:black" href="?mode=edit">Open file in editor</a>';
			//cmsjs.Log.warn('*DOMContentLoaded* falling back to browser');
			//document.documentElement.style.visibility = 'visible';
			return;
		}
		cmsjs.State.set('ENV.RawContentProcessed',true);
		callback();
	});
	// This is an auxiliary function to get a <p> tag for use on the loading page:
	// The 'document.body' stuff is there to ensure that regardless of when a <p>
	// is requested, it always sets the appropriate body style.
	function getP(id) {
		const p = $.ce('p');
		if ( id ) { p.id = id; }
		p.style.position = 'absolute';
		p.style.top = '25%';
		p.style.left = '50%';
		p.style.transform = 'translate(-50%,-50%)';
		document.body.style.height = '100vh';
		document.body.style.display = 'flex';
		document.body.style.justifyContent = 'flex';
		document.body.style.alignItems = 'center';
		return p;
	}
	// Do not remove or modify this without benchmarking. For whatever reason,
	// the page loads significantly faster on large files with this code here. I
	// do not know if it is due to setting the <pre> tag to display:none or if
	// it's due to some other cause, but keep this code as-is until you figure out
	// why it speeds things up.
	function loading(ms=0) {
		if (document.body) {
			cmsjs.Log.debug('initExtension(): body arrived');
			if ( ! cmsjs.State.get('ENV.RawContentProcessed') ) {
				cmsjs.Log.debug('init(): this is the body that will have the autogenerated <pre> tag, so hide it');
				const pre = document.body.querySelector('pre');
				// BUT, if it's not a json file, it will not have the pre, so...
				if ( ! pre ) { return; }
				pre.style.display = 'none';
				const p = getP('loading');
				p.innerHTML = 'Please wait while document loads.'
				p.id = 'loading';
				if ( ! $.id('error') ) {
					document.body.style.cursor = 'wait';
					document.body.appendChild(p);
				}
				document.documentElement.style.visibility = 'visible';
			}
			else {
				cmsjs.Log.debug('init(): but as it turns out, DOMContentLoaded already fired, so this is the real body');
			}
			return;
		}
		else if ( ms < 5000 ) {
			cmsjs.Log.debug('initExtension(): waiting for body to become available to hide it...');
			const waitTime = 5;
			const total = ms + waitTime;
			setTimeout(() => loading(total), waitTime);
		}
		else {
			cmsjs.Log.debug('initExtension(): taking too long, aborting.');
		}
	}
	loading();
	return;
}


/*
 * This is the starting point for non-environment-specific code.
 */
function initConsole() {
	let t0 = cmsjs.Log.timer('running initConsole()');
	let startTime = t0;
	cmsjs.Lang.CurrentLanguage = cmsjs.State.get('Core.Language');
	if ( cmsjs.State.get('Theme.DarkMode') && cmsjs.State.get('Theme.SelectedDark') ) {
		cmsjs.Themes.Colors.CurrentTheme = 'Dark.' + cmsjs.State.get('Theme.SelectedDark');
	}
	else if ( ! cmsjs.State.get('Theme.DarkMode') && cmsjs.State.get('Theme.SelectedLight') ) {
		cmsjs.Themes.Colors.CurrentTheme = 'Light.' + cmsjs.State.get('Theme.SelectedLight');
	}
	if ( ! cmsjs.State.get('ENV.StandaloneMode') && cmsjs.State.get('ENV.JSON') ) {
		cmsjs.State.set('ENV.Bytes',JSON.stringify(cmsjs.State.get('ENV.JSON')).length);
		t0 = cmsjs.Log.timer('initConsole() got bytes (' + cmsjs.State.get('ENV.Bytes') + ')',t0);
	}
	const console = cmsjs.Console.init();
	console.style.cursor = 'wait';

	cmsjs.Theme.Layout.apply(cmsjs.State.get('cmsjs.Theme.Layout'));
	cmsjs.Theme.Colors.apply(cmsjs.State.get('cmsjs.Theme.Colors'));
	cmsjs.Theme.Fonts.apply(cmsjs.State.get('cmsjs.Theme.Fonts'));
	cmsjs.Theme.Controls.apply(cmsjs.State.get('cmsjs.Theme.Controls'));
	

	cmsjs.State.change('Core.ControlKeys',cmsjs.State.get('Core.ControlKeys'));

	t0 = cmsjs.Log.timer('initComponents() built components',t0);
	if ( cmsjs.State.get('ENV.StandaloneMode') ) {
		showPage('content-demo');
	}
	else if ( cmsjs.State.get('Core.MarkupMode') ) {
		cmsjs.State.set('ENV.CurrentPath',window.location.hash.substring(1) || '$');
		showPage('content-markup');
		//cmsjs.State.get('Core.EditorMode') = false;
	}
	//else if ( cmsjs.State.get('Core.EditorMode') ) {
	//	showPage('editor');
	//}
	else {
		showPage('content-raw');
	}
	t0 = cmsjs.Log.timer('buildConsole() displayed json',t0);
	//STYLE.applyTheme(STYLE.getCurrentTheme());
	//STYLE.OnThemeChange();
	STYLE.applyCurrentTheme();
	t0 = cmsjs.Log.timer('buildConsole() applied theme',t0);
	attachKeyBindings();
	t0 = cmsjs.Log.timer('buildConsole() attached keybindings',t0);
	cmsjs.State.set('ENV.PageLoaded',true);
	cmsjs.Log.timer('buildConsole() total execution time',startTime);
	window.addEventListener('hashchange', () => {
		$.id('json-path').remove();
		cmsjs.State.set('ENV.CurrentPath',window.location.hash.substring(1) || '$');
		cmsjs.Console.buildComponent('content-markup');
	});
	document.documentElement.style.cursor = '';
	cmsjs.Lang.CurrentLanguage = cmsjs.State.get('Core.Language');
}


/********************************************
 *				Starting Point				*
 ********************************************/
init();
